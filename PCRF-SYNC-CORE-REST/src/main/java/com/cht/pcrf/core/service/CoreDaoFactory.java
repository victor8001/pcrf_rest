package com.cht.pcrf.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cht.pcrf.core.common.CommonConstant;
import com.cht.pcrf.core.dao.PcrfConsistenLogRestDaoImpl;
import com.cht.pcrf.core.dao.PcrfConsistentRestDaoImpl;
import com.cht.pcrf.core.dao.PcrfInfoRestDaoImpl;
import com.cht.pcrf.core.dao.SubscriberRestDaoImpl;
import com.cht.pcrf.core.dao.TempDBNRestDaoImpl;
import com.cht.pcrf.core.dao.TransactionLogRestDaoImpl;

public class CoreDaoFactory {
    private static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CORE);

    private static ClassPathXmlApplicationContext ctx;
    public static CoreDaoFactory instance;
    
    private PcrfInfoRestDaoImpl pcrfInfoRestDao;
    private PcrfConsistenLogRestDaoImpl pcrfConsistenLogRestDao;
    private PcrfConsistentRestDaoImpl pcrfConsistentDao;
    private TransactionLogRestDaoImpl transactionLogDao;
    private SubscriberRestDaoImpl subscriberDao;
    private TempDBNRestDaoImpl tempDBNRestDao;
    
    public static CoreDaoFactory getInstance(){
        if(instance == null)
            instance = new CoreDaoFactory();
        
        return instance;
    }
    
    private CoreDaoFactory(){
        ctx = new ClassPathXmlApplicationContext("core-spring-bean.xml");
        logger.info("Initialize spring config finish!");
        pcrfInfoRestDao = ctx.getBean("pcrfInfoRestDao", PcrfInfoRestDaoImpl.class);
        pcrfConsistenLogRestDao = ctx.getBean("pcrfConsistenLogRestDao", PcrfConsistenLogRestDaoImpl.class);
        pcrfConsistentDao = ctx.getBean("pcrfConsistentRestDao", PcrfConsistentRestDaoImpl.class);
        transactionLogDao = ctx.getBean("transactionLogRestDao", TransactionLogRestDaoImpl.class);
        subscriberDao = ctx.getBean("subscriberRestDao", SubscriberRestDaoImpl.class);
        tempDBNRestDao = ctx.getBean("tempDBNRestDao", TempDBNRestDaoImpl.class);
    }

    public PcrfInfoRestDaoImpl getPcrfInfoRestDao() {
        return pcrfInfoRestDao;
    }

    public void setPcrfInfoRestDao(PcrfInfoRestDaoImpl pcrfInfoRestDao) {
        this.pcrfInfoRestDao = pcrfInfoRestDao;
    }

    public PcrfConsistenLogRestDaoImpl getPcrfConsistenLogRestDao() {
        return pcrfConsistenLogRestDao;
    }

    public void setPcrfConsistenLogRestDao(PcrfConsistenLogRestDaoImpl pcrfConsistenLogRestDao) {
        this.pcrfConsistenLogRestDao = pcrfConsistenLogRestDao;
    }

    public TransactionLogRestDaoImpl getTransactionLogRestDao() {
        return transactionLogDao;
    }

    public void setTransactionLogDao(TransactionLogRestDaoImpl transactionLogRestDao) {
        this.transactionLogDao = transactionLogRestDao;
    }

    public PcrfConsistentRestDaoImpl getPcrfConsistentRestDao() {
        return pcrfConsistentDao;
    }

    public void setPcrfConsistentDao(PcrfConsistentRestDaoImpl pcrfConsistentRestDao) {
        this.pcrfConsistentDao = pcrfConsistentRestDao;
    }

    public SubscriberRestDaoImpl getSubscriberRestDao() {
        return subscriberDao;
    }

    public void setSubscriberDao(SubscriberRestDaoImpl subscriberRestDao) {
        this.subscriberDao = subscriberRestDao;
    }

    public TempDBNRestDaoImpl getTempDBNRestDao() {
        return tempDBNRestDao;
    }

    public void setTempDBNDao(TempDBNRestDaoImpl tempDBNRestDao) {
        this.tempDBNRestDao = tempDBNRestDao;
    }

}
