package com.cht.pcrf.core.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.core.common.AlarmConstants;
import com.cht.pcrf.core.common.CommonConstant;
import com.cht.pcrf.core.common.LdapConstant;
import com.cht.pcrf.core.model.Subscriber;
import org.springframework.beans.factory.annotation.Value;

public class TempDBNRestDaoImpl extends BaseDao {
    protected final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CORE);
    private final String TB_NAME = "SUBSCRIBER_";
    private final String TB_LOG_NAME = "SUBSCRIBER_LOG_";

    @Value("${db.isRestSuffix}")
    private String restSuffix;
    public static final String SUB_QUAL_ATTRS_STRING = 
            "objectclass: epc-subscriberqualification" + CommonConstant.BR +
            "permissions: 15" + CommonConstant.BR +
            "EPC-Name: EPC-SubscriberQualification" + CommonConstant.BR +
            //"groupId: 4003",
            "EPC-SubscriberQualificationData: SubscriberChargingSystemName:ChtOcs" + CommonConstant.BR +
            //"ownerId: 0",
            "shareTree: nodeName=PGNode";
    
    public void selectExistInPri(final String exportPath, final String type, final String execTS, String priLocation,String nodename, String sTB) throws Exception {
        
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" selectExistInPri start ");
            
            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer
              .append(" SELECT ")
              .append(" '"+priLocation + " - " + nodename+"'").append(" , '").append(type).append("' ,")
              .append(" P.MSISDN, P.OBJECT_CLASS, P.INFO as INFO ")
              .append(" FROM ").append(TB_NAME).append(priLocation).append(" as P")
              .append(" WHERE lower(P.OBJECT_CLASS) = '" + LdapConstant.EPC_SUBSCRIBER_LOWCASE + "' ")
              .append(" AND P.CREATE_TIME <= '" + execTS + "'" )
              .append(" AND NOT EXISTS ( ")
              .append("     SELECT 1 ")
              .append("     FROM ").append(sTB).append(" as S ")
              .append("     WHERE P.MSISDN = S.MSISDN) ")
              //.append(" 	AND S.has_qual != '2') ")	//20200701 for 5G //20220110 取消
              //.append(" limit 1 ")
              ;

            
            logger.info("sql: " + sqlBuffer.toString());
            
            String sql = sqlBuffer.toString();
            sql = " ( SELECT * FROM ( " + sql + " ) T ";
            sql += " ) ";
            
            copyToFile(exportPath, sql, ",");
            
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "selectExistInPri exception! e:"+ e);
            throw e;
        }finally{
            logger.info("selectExistInPri execute time:"+(System.currentTimeMillis()-startTime));
        }

    }
    
    public void selectExistInSec(final String exportPath, final String type, final String execTS, String priLocation,String nodename , String sTB) throws Exception {
        
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" selectExistInSec start ");
            
            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer
                .append(" SELECT ")
                .append(" '"+priLocation + " - " + nodename+"'").append(" , '").append(type).append("' ,")
                .append(" S.MSISDN ")
                .append(" , '" + LdapConstant.EPC_SUBSCRIBER_LOWCASE + "' AS OBJECT_CLASS ")
                .append(" ,CASE WHEN S.col11_1 is not null THEN S.col11_1 || chr(10) ELSE '' END ")
                .append(" || CASE WHEN S.col11_2 is not null THEN S.col11_2 || chr(10) ELSE '' END ")
                .append(" || CASE WHEN S.col11_3 is not null THEN S.col11_3 || chr(10) ELSE '' END ")
                .append(" || CASE WHEN S.col11_4 is not null THEN S.col11_4 || chr(10) ELSE '' END ")
                .append(" || CASE WHEN S.col11_5 is not null THEN S.col11_5 || chr(10) ELSE '' END ")
                .append(" || CASE WHEN S.col11_6 is not null THEN S.col11_6 || chr(10) ELSE '' END ")
                .append(" || CASE WHEN S.col11_7 is not null THEN S.col11_7 || chr(10) ELSE '' END ")
                .append(" || CASE WHEN S.col11_8 is not null THEN S.col11_8 || chr(10) ELSE '' END ")
                .append(" || CASE WHEN S.col11_9 is not null THEN S.col11_9 || chr(10) ELSE '' END ")
                .append(" || CASE WHEN S.col11_10 is not null THEN S.col11_10 || chr(10) ELSE '' END  as INFO ")
                .append(" FROM ").append(sTB).append(" as S")
                .append(" WHERE NOT EXISTS ( ")
                .append("     SELECT 1 ")
                .append("     FROM ").append(TB_NAME).append(priLocation).append(" as P")
                .append("     WHERE P.MSISDN = S.MSISDN AND lower(P.OBJECT_CLASS) = '" + LdapConstant.EPC_SUBSCRIBER_LOWCASE + "' ")
                .append("     AND P.CREATE_TIME <= '" + execTS + "' " )
                .append("     UNION ALL " )
                .append("     SELECT ")
                .append("     1 ")
                .append("     FROM ").append(TB_LOG_NAME).append(priLocation).append(" as P")
                .append("     WHERE P.MSISDN = S.MSISDN AND lower(P.OBJECT_CLASS) = '" + LdapConstant.EPC_SUBSCRIBER_LOWCASE + "' ")
                .append("     AND P.MODIFY_TIME BETWEEN '" + execTS + "' AND now() ")
                .append("     AND P.CREATE_TIME <= '" + execTS + "'" )
                .append("     )" )
                ;

            logger.info("sql: " + sqlBuffer.toString());

            String sql = sqlBuffer.toString();
            sql = " ( SELECT * FROM ( " + sql + " ) T ";
            sql += " ) ";
            
            copyToFile(exportPath, sql, ",");
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "selectExistInSec exception! e:"+ e);
            throw e;
        }finally{
            logger.info("selectExistInSec execute time:"+(System.currentTimeMillis()-startTime));
        }

    }

    public void selectDiff(final String exportPath, final String type, final String execTS, String priLocation,String nodename ,String sTB) throws Exception {
        
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" selectExistInPri start ");
            
            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer
              .append(" SELECT ")
              .append(" '"+priLocation + " - " + nodename+"'").append(" , '").append(type).append("' ,")
              .append(" P.MSISDN, P.OBJECT_CLASS, P.INFO as P_INFO")
              .append(" ,CASE WHEN S.col11_1 is not null THEN S.col11_1 || chr(10) ELSE '' END ")
              .append(" || CASE WHEN S.col11_2 is not null THEN S.col11_2 || chr(10) ELSE '' END ")
              .append(" || CASE WHEN S.col11_3 is not null THEN S.col11_3 || chr(10) ELSE '' END ")
              .append(" || CASE WHEN S.col11_4 is not null THEN S.col11_4 || chr(10) ELSE '' END ")
              .append(" || CASE WHEN S.col11_5 is not null THEN S.col11_5 || chr(10) ELSE '' END ")
              .append(" || CASE WHEN S.col11_6 is not null THEN S.col11_6 || chr(10) ELSE '' END ")
              .append(" || CASE WHEN S.col11_7 is not null THEN S.col11_7 || chr(10) ELSE '' END ")
              .append(" || CASE WHEN S.col11_8 is not null THEN S.col11_8 || chr(10) ELSE '' END ")
              .append(" || CASE WHEN S.col11_9 is not null THEN S.col11_9 || chr(10) ELSE '' END ")
              .append(" || CASE WHEN S.col11_10 is not null THEN S.col11_10 || chr(10) ELSE '' END  as S_INFO ")
              .append(" FROM ")
              .append(TB_NAME).append(priLocation).append(" as P,")
              .append(sTB).append(" as S ")
              .append(" WHERE P.MSISDN = S.MSISDN ")
              .append(" AND lower(P.OBJECT_CLASS) = '" + LdapConstant.EPC_SUBSCRIBER_LOWCASE + "' ")
              .append(" AND P.CREATE_TIME <= '" + execTS + "'" )
              .append(" AND (coalesce(P.col11_1,'') != coalesce(S.col11_1,'') ")
              .append(" OR coalesce(P.col11_2,'') != coalesce(S.col11_2,'') ")
              .append(" OR coalesce(P.col11_3,'') != coalesce(S.col11_3,'') ")
              .append(" OR coalesce(P.col11_4,'') != coalesce(S.col11_4,'') ")
              .append(" OR coalesce(P.col11_5,'') != coalesce(S.col11_5,'') ")
              .append(" OR coalesce(P.col11_6,'') != coalesce(S.col11_6,'') ")
              .append(" OR coalesce(P.col11_7,'') != coalesce(S.col11_7,'') ")
              .append(" OR coalesce(P.col11_8,'') != coalesce(S.col11_8,'') ")
              .append(" OR coalesce(P.col11_9,'') != coalesce(S.col11_9,'') ")
              .append(" OR coalesce(P.col11_10,'') != coalesce(S.col11_10,'')) "); 
            
            logger.info("sql: " + sqlBuffer.toString());

            String sql = sqlBuffer.toString();
            sql = " ( SELECT * FROM ( " + sql + " ) T ";
            sql += " ) ";
            
            copyToFile(exportPath, sql, ",");
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "selectExistInPri exception! e:"+ e);
            throw e;
        }finally{
            logger.info("selectExistInPri execute time:"+(System.currentTimeMillis()-startTime));
        }

    }
    
    /*
    public List<SapcInfo> getSapcInfoList(String location) throws Exception {

        List<SapcInfo> result = new ArrayList();
        long startTime = System.currentTimeMillis();
        
        try{
            //TODO
            logger.info(" getSapcInfoList start ");
            
//            OracleManagement.getConnection();
            
//            SapcInfo sapcInfo = OracleManagement.selectSapcInfo(nodeName);

        }catch(Exception e){
            logger.error("getSapcInfoList exception!", e);
            throw e;
        }finally{
            logger.info("getSapcInfoList execute time:"+(System.currentTimeMillis()-startTime));
//            OracleManagement.closeConnection();
        }

        return result;
    }
    
    public Map<String, List<SapcInfo>> getSapcInfoMap() throws Exception {

        long startTime = System.currentTimeMillis();
        Map<String, List<SapcInfo>> sapcMap = new HashMap();
        try{
            logger.info(" getSapcInfoMap start ");

            List<SapcInfo> result = getSapcInfoList();
            
            for(SapcInfo sapcInfo : result){
                String key = sapcInfo.getLocation();
                if(sapcMap.get(key) == null){
                    sapcMap.put(key, new ArrayList());
                }
                sapcMap.get(key).add(sapcInfo);
            }

        }catch(Exception e){
            logger.error("getSapcInfoMap exception!", e);
            throw e;
        }finally{
            logger.info("getSapcInfoMap execute time:"+(System.currentTimeMillis()-startTime));
//            OracleManagement.closeConnection();
        }

        return sapcMap;
    }

    public List<SapcInfo> getSapcInfoList() throws Exception{
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" getSapcInfoList start ");
            
            final String sql = "SELECT LOCATION, NODE_NAME, NODE_HOST, NODE_PORT"
                + ", LOGIN_ID, LOGIN_PW FROM PCRF_INFO WHERE STATUS = 1 "; 

            return convertToObj(getJdbcTemplate().queryForList(sql));

        }catch(Exception e){
            logger.error("getSapcInfoList exception!", e);
            throw e;
        }finally{
            logger.info("getSapcInfoList execute time:"+(System.currentTimeMillis()-startTime));
        }

    }
    */

    private List<Subscriber> convertToObj(List<Map<String, Object>> datas) {
        List<Subscriber> result = new ArrayList();
        Subscriber model = null;
        if(datas == null) return result;
        
        for(Map<String, Object> rs : datas){
            model = new Subscriber();
            model.setMsisdn((String) rs.get("MSISDN"));
            model.setObjectClass((String) rs.get("OBJECT_CLASS"));
            model.setInfo((String) rs.get("INFO"));
            model.setPriInfo((String) rs.get("P_INFO"));
            model.setSecInfo((String) rs.get("S_INFO"));
            result.add(model);
        }
        return result;
    }
    
    public void truncateTB(String dbnTB) throws Exception{
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" truncateTB start ");
            
            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer.append(" truncate table " + dbnTB); 
            
            logger.info("sql: " + sqlBuffer.toString());
            
            getJdbcTemplate().execute(sqlBuffer.toString());
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "truncateTB exception! e:"+ e);
            throw e;
        }finally{
            logger.info("truncateTB execute time:"+(System.currentTimeMillis()-startTime));
        }
    } 
    
    public void findNoQualInTempDBN(final String exportPath, final String type, String priLocation,String nodename, String sTB) throws Exception {
        
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" selectExistInPri start ");
            
            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer
              .append(" SELECT ")
              .append(" '"+priLocation + " - " + nodename+"'").append(" , '").append(type).append("' ,")
              .append(" P.MSISDN, '"+LdapConstant.EPC_SUBSCRIBERQUALIFICATION_LOWCASE + "' , ")
              .append(" '" + SUB_QUAL_ATTRS_STRING +"' as INFO ")
              .append(" FROM ").append(sTB).append(" as P")
              .append(" WHERE P.HAS_QUAL = '0' ")
              //.append(" limit 1 ")
              ;

            
            logger.info("sql: " + sqlBuffer.toString());
            
            String sql = sqlBuffer.toString();
            sql = " ( SELECT * FROM ( " + sql + " ) T ";
            sql += " ) ";
            
            //copyToFile(exportPath, sql, ","); //20220110 取消 不再傳HAS_QUAL
            
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "selectExistInPri exception! e:"+ e);
            throw e;
        }finally{
            logger.info("selectExistInPri execute time:"+(System.currentTimeMillis()-startTime));
        }

    }
    
  //20200701 for 5G
    public void selectQciExistInSec(final String exportPath, final String type, final String execTS, String priLocation,String nodename , String sTB) throws Exception {
        
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" selectQosExistInSec start ");
            
            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer
                .append(" SELECT ")
                .append(" '"+priLocation + " - " + nodename+"'").append(" , '").append(type).append("' ,")
                .append(" S.MSISDN ")
                .append(" , 'epc-subject' AS OBJECT_CLASS ")
                .append(" ,'EPC-PolicyIds: ' || S.POLICY_ID ||'" + CommonConstant.BR + "EPC-ContextName: QoS' ")
                .append(" as INFO ")
                .append(" FROM ").append(sTB).append(" as S")
                .append(" WHERE NOT EXISTS ( ")
                .append("     SELECT 1 ")
                .append("     FROM ").append(TB_NAME).append(priLocation).append(" as P")
                .append("     WHERE P.MSISDN = S.MSISDN AND lower(P.OBJECT_CLASS) = 'epc-subject' ")
                .append("     AND P.CREATE_TIME <= '" + execTS + "' " )
//                .append("     UNION ALL " )
//                .append("     SELECT ")
//                .append("     1 ")
//                .append("     FROM ").append(TB_LOG_NAME).append(priLocation).append(" as P")
//                .append("     WHERE P.MSISDN = S.MSISDN AND lower(P.OBJECT_CLASS) = '" + LdapConstant.EPC_SUBJECT_LOWCASE + "' ")
//                .append("     AND P.MODIFY_TIME BETWEEN '" + execTS + "' AND now() ")
//                .append("     AND P.CREATE_TIME <= '" + execTS + "'" )
                .append("     )" )
                //.append(" AND COALESCE(S.POLICY_ID,'') != '' ") //20220110 取消比對POLICY_ID 不再傳POLICY_ID
                ;

            logger.info("sql: " + sqlBuffer.toString());

            String sql = sqlBuffer.toString();
            sql = " ( SELECT * FROM ( " + sql + " ) T ";
            sql += " ) ";
            
            copyToFile(exportPath, sql, ",");
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "selectQosExistInSec exception! e:"+ e);
            throw e;
        }finally{
            logger.info("selectQosExistInSec execute time:"+(System.currentTimeMillis()-startTime));
        }

    }
    
    //20200701 for 5G
    public void selectQciExistInPri(final String exportPath, final String type, final String execTS, String priLocation,String nodename, String sTB) throws Exception {
        
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" selectQosExistInPri start ");
            
            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer
              .append(" SELECT ")
              .append(" '"+priLocation + " - " + nodename+"'").append(" , '").append(type).append("' ,")
              .append(" P.MSISDN, P.OBJECT_CLASS, P.INFO as INFO ")
              .append(" FROM ").append(TB_NAME).append(priLocation).append(" as P")
              .append(" WHERE lower(P.OBJECT_CLASS) = 'epc-subject' ")
              .append(" AND P.CREATE_TIME <= '" + execTS + "'" )
              .append(" AND NOT EXISTS ( ")
              .append("     SELECT 1 ")
              .append("     FROM ").append(sTB).append(" as S ")
              .append("     WHERE P.MSISDN = S.MSISDN )")
              //.append(" 	AND COALESCE(S.POLICY_ID,'') != '') ") //20220110取消比對POLICY ID 不再傳POLICY_ID
              //.append(" limit 1 ")
              ;

            
            logger.info("sql: " + sqlBuffer.toString());
            
            String sql = sqlBuffer.toString();
            sql = " ( SELECT * FROM ( " + sql + " ) T ";
            sql += " ) ";
            
            copyToFile(exportPath, sql, ",");
            
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "selectQosExistInPri exception! e:"+ e);
            throw e;
        }finally{
            logger.info("selectQosExistInPri execute time:"+(System.currentTimeMillis()-startTime));
        }

    }
    
    //20200701 for 5G
    public void selectQciDiff(final String exportPath, final String type, final String execTS, String priLocation,String nodename ,String sTB) throws Exception {
        
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" selectQosDiff start ");
            
            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer
              .append(" SELECT ")
              .append(" '"+priLocation + " - " + nodename+"'").append(" , '").append(type).append("' ,")
              .append(" P.MSISDN, P.OBJECT_CLASS, P.INFO as P_INFO")
//              .append(" ,'EPC-PolicyIds: ' || S.POLICY_ID as S_INFO ")
              .append(" ,'EPC-PolicyIds: ' || S.POLICY_ID ||'" + CommonConstant.BR + "EPC-ContextName: QoS' ")
              .append(" as S_INFO ")
              .append(" FROM ")
              .append(TB_NAME).append(priLocation).append(" as P,")
              .append(sTB).append(" as S ")
              .append(" WHERE P.MSISDN = S.MSISDN ")
              .append(" AND lower(P.OBJECT_CLASS) = 'epc-subject' ")
              .append(" AND P.CREATE_TIME <= '" + execTS + "'" )
              //.append(" AND coalesce(P.col4_1,'') != coalesce(S.POLICY_ID,'') ") //20220110取消比對POLICY ID, 不再傳POLICY
              //.append(" AND COALESCE(S.POLICY_ID,'') != '' ") //20220110取消比對POLICY ID, 不再傳POLICY
              ; 
            
            logger.info("sql: " + sqlBuffer.toString());

            String sql = sqlBuffer.toString();
            sql = " ( SELECT * FROM ( " + sql + " ) T ";
            sql += " ) ";
            
            copyToFile(exportPath, sql, ",");
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "selectQosDiff exception! e:"+ e);
            throw e;
        }finally{
            logger.info("selectQosDiff execute time:"+(System.currentTimeMillis()-startTime));
        }

    }

    public String getRestSuffix() {
        return restSuffix;
    }

    public void setRestSuffix(String restSuffix) {
        this.restSuffix = restSuffix;
    }

    private String getTableName(){
        boolean isRest = (StringUtils.isBlank(restSuffix))?false:Boolean.parseBoolean(restSuffix);
        return "TempDBN"+(isRest?"_REST":"");
    }
}

