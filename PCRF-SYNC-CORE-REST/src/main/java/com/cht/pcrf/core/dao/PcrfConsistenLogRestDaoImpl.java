package com.cht.pcrf.core.dao;

import java.sql.Timestamp;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.cht.pcrf.core.common.AlarmConstants;
import com.cht.pcrf.core.common.CommonConstant;
import com.cht.pcrf.core.common.LdapConstant;
import com.cht.pcrf.core.model.PcrfConsistenLog;

public class PcrfConsistenLogRestDaoImpl extends JdbcDaoSupport {
    protected final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CORE);

    @Value("${db.isRestSuffix}")
    private String restSuffix;
    public long getNextPK() throws Exception {
         
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("getNextPK start ");
            
            final String sql = 
                    " SELECT  nextval('pcrf_consistent_log_id_seq') " ;
            
            return getJdbcTemplate().queryForLong(sql);
            
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "getNextPK exception! e:"+ e);
            throw e;
        }finally{
            logger.info("getNextPK execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    public void save(PcrfConsistenLog log) throws Exception {

        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("save start ");
            String tableName = getTableName();
            final String sql = 
                    " INSERT INTO "+tableName+"( " +
                    "         ID, PCRF_ID, EXEC_START_TIME, EXEC_END_TIME, EXEC_RESULT, EXEC_USER, " + 
                    "         LOG_PATH, RPT_PATH, SYNC_RESULT, IS_QCI) " +
                    " VALUES (?, ?, ?, ?, ?, ?, " +
                    "         ?, ?, ?, ?) " ;
            
            Object[] params = {
                    log.getId()
                    , log.getPcrfId()
                    , log.getExecStartTime()
                    , log.getExecEndTime()
                    , log.getExecResult()
                    , log.getExecUser()
                    , log.getLogPath()
                    , log.getRptPath()
                    , log.getSyncResult() == null ? PcrfConsistenLog.SYNC_RESULT_0 : log.getSyncResult()
                    , log.getIsQci()
            };
            
            getJdbcTemplate().update(sql, params);
            
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "save exception! e:"+ e);
            throw e;
        }finally{
            logger.info("save execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    public void updateExecResult(PcrfConsistenLog log) throws Exception {

        long startTime = System.currentTimeMillis();
        String tableName = getTableName();
        try{
            logger.info("update start ");
            
            final String sql = 
                    " UPDATE "+tableName
                    + " SET EXEC_END_TIME = ? " 
                    + "     ,EXEC_RESULT = ? "
                    + "     ,RPT_PATH  = ? " 
                    + " WHERE ID = ? ";
            
            Object[] params = {
                    log.getExecEndTime()
                    , log.getExecResult()
                    , log.getRptPath()
                    , log.getId()
            };
            
            getJdbcTemplate().update(sql, params);
            
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "update exception! e:"+ e);
            throw e;
        }finally{
            logger.info("update execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    public void updateSyncResult(long logId, int syncResult) throws Exception {

        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("updateSyncResult start ");
            String tableName = getTableName();
            final String sql = 
                    " UPDATE "+ tableName +
                    "   SET SYNC_RESULT = " + syncResult +
                    " WHERE ID = ? " ;
            
            Object[] params = {
                    logId
            };
            
            getJdbcTemplate().update(sql, params);
            
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "updateSyncResult exception! e:"+ e);
            throw e;
        }finally{
            logger.info("updateSyncResult execute time:"+(System.currentTimeMillis()-startTime));
        }
    }

    public void updateSyncResult(long logId, Timestamp endTime, Integer execResult,
            String rptPath, Integer syncResult) throws Exception {

        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("updateSyncResult start ");
            String tableName = getTableName();
            final String sql = 
                    " UPDATE "+tableName
                    + " SET EXEC_END_TIME = ? " 
                    + "     ,EXEC_RESULT = ? "
                    + "     ,RPT_PATH  = ? " 
                    + "     ,SYNC_RESULT  = ? " 
                    + " WHERE ID = ? ";
            
            Object[] params = {
                   endTime, execResult, rptPath, syncResult, logId
            };
            
            getJdbcTemplate().update(sql, params);
            
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "updateSyncResult exception! e:"+ e);
            throw e;
        }finally{
            logger.info("updateSyncResult execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    public long findDBNCount() throws Exception{
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("findDBNCount start ");
            
            final String sql = " SELECT COUNT(*) FROM "+getTableName();
            
            return getJdbcTemplate().queryForLong(sql);
            
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "findDBNCount exception! e:"+ e);
            throw e;
        }finally{
            logger.info("findDBNCount execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    public long findSubsriberCount(final String location, final String execTS) throws Exception {
        
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("findSubsriberCount start ");
            
            final String sql = " SELECT COUNT(*) FROM SUBSCRIBER_" + location
                    + " WHERE lower(OBJECT_CLASS) = '" + LdapConstant.EPC_SUBSCRIBER_LOWCASE + "' "
                    + " AND CREATE_TIME <= '" + execTS + "' ";
            
            return getJdbcTemplate().queryForLong(sql);
            
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "findSubsriberCount exception! e:"+ e);
            throw e;
        }finally{
            logger.info("findSubsriberCount execute time:"+(System.currentTimeMillis()-startTime));
        }

    }
    private String getTableName(){
        boolean isRest = (StringUtils.isBlank(restSuffix)) ? false : Boolean.parseBoolean(restSuffix);
        return "PCRF_CONSISTENT_LOG"+(isRest?"_REST":"");
    }
    /*
    private List<SapcInfo> convertToObj(List<Map<String, Object>> datas) {
        List<SapcInfo> result = new ArrayList();
        SapcInfo model = null;
        if(datas == null) return result;
        
        for(Map<String, Object> rs : datas){
            model = new SapcInfo();
            model.setLocation((String) rs.get("LOCATION"));
            model.setNodeName((String) rs.get("NODE_NAME"));
            model.setNodeHost((String) rs.get("NODE_HOST"));
            model.setNodePort((String) rs.get("NODE_PORT"));
            model.setLoginId((String) rs.get("LOGIN_ID"));
            model.setLoginPw((String) rs.get("LOGIN_PW"));
            
            model.setLdapEnv();
            result.add(model);
        }
        return result;
    }
    */
    public String getRestSuffix() {
        return restSuffix;
    }

    public void setRestSuffix(String restSuffix) {
        this.restSuffix = restSuffix;
    }
}
