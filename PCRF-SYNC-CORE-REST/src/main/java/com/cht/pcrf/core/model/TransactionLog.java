/**
 * Project Name : CHT-PCRF-LDAP
 * File Name 	: TransactionLog.java
 * Package Name : com.cht.pcrf.adaptor.model
 * Date 		: 2017年4月13日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.core.model;

import java.sql.Timestamp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class TransactionLog {
    public static final String OPERATION_CREATE_2 = "2";
    public static final String OPERATION_DELETE_3 = "3";
    // Attribute
    private Long txId;
    private String location;
    private String msisdn;
    private String interFace;
    private String operation;
    private String requestDn;
    private String request;
    private String response;
    private Integer status;
    private Timestamp createTime;
    public Long getTxId() {
        return txId;
    }
    public void setTxId(Long txId) {
        this.txId = txId;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public String getMsisdn() {
        return msisdn;
    }
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
    public String getInterFace() {
        return interFace;
    }
    public void setInterFace(String interFace) {
        this.interFace = interFace;
    }
    public String getOperation() {
        return operation;
    }
    public void setOperation(String operation) {
        this.operation = operation;
    }
    public String getRequestDn() {
        return requestDn;
    }
    public void setRequestDn(String requestDn) {
        this.requestDn = requestDn;
    }
    public String getRequest() {
        return request;
    }
    public void setRequest(String request) {
        this.request = request;
    }
    public String getResponse() {
        return response;
    }
    public void setResponse(String response) {
        this.response = response;
    }
    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public Timestamp getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
