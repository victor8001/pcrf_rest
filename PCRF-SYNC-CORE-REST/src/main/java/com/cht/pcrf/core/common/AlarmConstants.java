/**
 * Project Name : PCRF-SYNC-WEB
 * File Name 	: AlarmConstants.java
 * Package Name : com.cht.pcrf.common
 * Date 		: 2017年5月20日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.core.common;

public class AlarmConstants {
    public static final String DB_EX = "[DB_EX] ";
    public static final String SAPC_EX = "[SAPC_EX] ";
}
