package com.cht.pcrf.core.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.core.common.AlarmConstants;
import com.cht.pcrf.core.common.CommonConstant;
import com.cht.pcrf.core.model.Subscriber;

public class SubscriberRestDaoImpl extends BaseDao {
    protected final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CORE);
    private final String TB_NAME = "SUBSCRIBER_";
    
    
    public void selectExistInPri(final String exportPath, final String type, final String execTS, String priLocation, String secLocation) throws Exception {
        
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" selectExistInPri start ");
            
            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer
              .append(" SELECT ")
              .append(" '"+priLocation + " - " + secLocation +"', '").append(type).append("' ,")
              .append(" P.MSISDN, P.OBJECT_CLASS, P.INFO as INFO ")
              .append(" FROM ").append(TB_NAME).append(priLocation).append(" as P")
              .append(" WHERE P.CREATE_TIME <= '" + execTS + "' " )
              .append(" AND NOT EXISTS ( ")
              .append("     SELECT 1 ")
              .append("     FROM ").append(TB_NAME).append(secLocation).append(" as S")
              .append("     WHERE P.MSISDN = S.MSISDN AND P.OBJECT_CLASS = S.OBJECT_CLASS  ")
              .append("     AND P.CREATE_TIME <= '" + execTS + "' " )
              .append("     AND S.CREATE_TIME <= '" + execTS + "' )" )
              //.append(" limit 1 ")
              ; 
            
            logger.info("sql: " + sqlBuffer.toString());
            
            String sql = sqlBuffer.toString();
            sql = " ( SELECT * FROM ( " + sql + " ) T ";
            sql += " ) ";
            
            copyToFile(exportPath, sql, ",");
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "selectExistInPri exception! e:"+ e);
            throw e;
        }finally{
            logger.info("selectExistInPri execute time:"+(System.currentTimeMillis()-startTime));
        }

    }
    
    public void selectExistInSec(final String exportPath, final String type, final String execTS, String priLocation, String secLocation) throws Exception {
        
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" selectExistInSec start ");
            
            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer
              .append(" SELECT ")
              .append(" '"+priLocation + " - " + secLocation +"', '").append(type).append("' ,")
              .append(" S.MSISDN, S.OBJECT_CLASS, S.INFO as INFO ")
              .append(" FROM ").append(TB_NAME).append(secLocation).append(" as S")
              .append(" WHERE S.CREATE_TIME <= '" + execTS + "' " )
              .append(" AND NOT EXISTS ( ")
              .append("     SELECT 1 ")
              .append("     FROM ").append(TB_NAME).append(priLocation).append(" as P")
              .append("     WHERE P.MSISDN = S.MSISDN AND P.OBJECT_CLASS = S.OBJECT_CLASS ")
              .append("     AND P.CREATE_TIME <= '" + execTS + "' " )
              .append("     AND S.CREATE_TIME <= '" + execTS + "' )" ); 
            
            logger.info("sql: " + sqlBuffer.toString());
            
            String sql = sqlBuffer.toString();
            sql = " ( SELECT * FROM ( " + sql + " ) T ";
            sql += " ) ";
            
            copyToFile(exportPath, sql, ",");
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "selectExistInSec exception! e:"+ e);
            throw e;
        }finally{
            logger.info("selectExistInSec execute time:"+(System.currentTimeMillis()-startTime));
        }

    }

    public void selectDiff(final String exportPath, final String type, final String execTS, String priLocation, String secLocation) throws Exception {
        
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" selectExistInPri start ");
            
            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer
              .append(" SELECT ")
              .append(" '"+priLocation + " - " + secLocation +"', '").append(type).append("' ,")
              .append(" S.MSISDN, S.OBJECT_CLASS, S.INFO as S_INFO, P.INFO as P_INFO ")
              .append(" FROM ")
              .append(TB_NAME).append(priLocation).append(" as P,")
              .append(TB_NAME).append(secLocation).append(" as S ")
              .append(" WHERE P.MSISDN = S.MSISDN ")
              .append(" AND P.OBJECT_CLASS = S.OBJECT_CLASS ")
              .append(" AND P.CREATE_TIME <= '" + execTS + "' " )
              .append(" AND S.CREATE_TIME <= '" + execTS + "' " )
              //.append(" AND P.INFO != S.INFO ");
              .append(" AND (coalesce(P.col11_1,'') != coalesce(S.col11_1,'') ")
              .append(" OR coalesce(P.col11_2,'') != coalesce(S.col11_2,'') ")
              .append(" OR coalesce(P.col11_3,'') != coalesce(S.col11_3,'') ")
              .append(" OR coalesce(P.col11_4,'') != coalesce(S.col11_4,'') ")
              .append(" OR coalesce(P.col11_5,'') != coalesce(S.col11_5,'') ")
              .append(" OR coalesce(P.col11_6,'') != coalesce(S.col11_6,'') ")
              .append(" OR coalesce(P.col11_7,'') != coalesce(S.col11_7,'') ")
              .append(" OR coalesce(P.col11_8,'') != coalesce(S.col11_8,'') ")
              .append(" OR coalesce(P.col11_9,'') != coalesce(S.col11_9,'') ")
              .append(" OR coalesce(P.col11_10,'') != coalesce(S.col11_10,'')) "); 
            
            logger.info("sql: " + sqlBuffer.toString());
            
            String sql = sqlBuffer.toString();
            sql = " ( SELECT * FROM ( " + sql + " ) T ";
            sql += " ) ";
            
            copyToFile(exportPath, sql, ",");
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "selectExistInPri exception! e:"+ e);
            throw e;
        }finally{
            logger.info("selectExistInPri execute time:"+(System.currentTimeMillis()-startTime));
        }

    }

    public List<Subscriber> findByMsisdnAndObjectClass(String msisdn,String object_class,String location){
        long startTime = System.currentTimeMillis();

        try {
            logger.info(" select Exist Subscribers start ");

            StringBuffer sqlBuffer = new StringBuffer();
            sqlBuffer
                    .append(" SELECT ")
                    .append(" P.MSISDN, P.OBJECT_CLASS, P.COL11_1 as COL11")
                    .append(" FROM ")
                    .append(TB_NAME).append(location).append(" as P")
                    .append(" WHERE P.MSISDN = '" + msisdn + "'")
                    .append(" AND Lower(P.OBJECT_CLASS) = '" + object_class + "'")
                    .append(" order by P.create_time desc, P.modify_time desc ;");

            logger.info("sql: " + sqlBuffer.toString());

            String sql = sqlBuffer.toString();

            return convertToObj(getJdbcTemplate().queryForList(sql));
        }catch (Exception e){
            e.printStackTrace();
            logger.error("SQL Exception: "+e.getMessage());
            return null;
        }finally {
            logger.info("selectExistInPri execute time:"+(System.currentTimeMillis()-startTime));
        }

    }
    /*
    public List<SapcInfo> getSapcInfoList(String location) throws Exception {

        List<SapcInfo> result = new ArrayList();
        long startTime = System.currentTimeMillis();
        
        try{
            //TODO
            logger.info(" getSapcInfoList start ");
            
//            OracleManagement.getConnection();
            
//            SapcInfo sapcInfo = OracleManagement.selectSapcInfo(nodeName);

        }catch(Exception e){
            logger.error("getSapcInfoList exception!", e);
            throw e;
        }finally{
            logger.info("getSapcInfoList execute time:"+(System.currentTimeMillis()-startTime));
//            OracleManagement.closeConnection();
        }

        return result;
    }
    
    public Map<String, List<SapcInfo>> getSapcInfoMap() throws Exception {

        long startTime = System.currentTimeMillis();
        Map<String, List<SapcInfo>> sapcMap = new HashMap();
        try{
            logger.info(" getSapcInfoMap start ");

            List<SapcInfo> result = getSapcInfoList();
            
            for(SapcInfo sapcInfo : result){
                String key = sapcInfo.getLocation();
                if(sapcMap.get(key) == null){
                    sapcMap.put(key, new ArrayList());
                }
                sapcMap.get(key).add(sapcInfo);
            }

        }catch(Exception e){
            logger.error("getSapcInfoMap exception!", e);
            throw e;
        }finally{
            logger.info("getSapcInfoMap execute time:"+(System.currentTimeMillis()-startTime));
//            OracleManagement.closeConnection();
        }

        return sapcMap;
    }

    public List<SapcInfo> getSapcInfoList() throws Exception{
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" getSapcInfoList start ");
            
            final String sql = "SELECT LOCATION, NODE_NAME, NODE_HOST, NODE_PORT"
                + ", LOGIN_ID, LOGIN_PW FROM PCRF_INFO WHERE STATUS = 1 "; 

            return convertToObj(getJdbcTemplate().queryForList(sql));

        }catch(Exception e){
            logger.error("getSapcInfoList exception!", e);
            throw e;
        }finally{
            logger.info("getSapcInfoList execute time:"+(System.currentTimeMillis()-startTime));
        }

    }
    */

    private List<Subscriber> convertToObj(List<Map<String, Object>> datas) {
        List<Subscriber> result = new ArrayList();
        Subscriber model = null;
        if(datas == null) return result;
        
        for(Map<String, Object> rs : datas){
            model = new Subscriber();
            model.setMsisdn((String) rs.get("MSISDN"));
            model.setObjectClass((String) rs.get("OBJECT_CLASS"));
            model.setInfo((String) rs.get("INFO"));
            model.setPriInfo((String) rs.get("P_INFO"));
            model.setSecInfo((String) rs.get("S_INFO"));
            model.setCol11((String) rs.get("COL11"));
            result.add(model);
        }
        return result;
    }
    
}

