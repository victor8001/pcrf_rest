/**
 * Project Name : PCRF-SYNC-CORE
 * File Name 	: CommonConstant.java
 * Package Name : com.cht.pcrf.core.common
 * Date 		: 2017年5月7日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.core.common;

public class CommonConstant {
    public final static String PCRF_SYNC_CORE = "PCRF_SYNC_CORE";
    public static final String BR = "\n";
}
