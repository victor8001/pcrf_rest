/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: SyncDataType.java
 * Package Name : com.cht.pcrf.api.common
 * Date 		: 2017年4月26日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.core.common;

public enum SyncDataType {
    TYPE_1("1","Data in pcrf only"),//type:1 只存在在pcrf
    TYPE_2("2","Data in pcrf-db only"),//type:2 只存在在db sync
    TYPE_3("3","PCRF DB not sync"),//type:3 pcrf與db sync資料不一致
    TYPE_4("4","TP/HK not sync"),//type:4 tp/kh db sync資料不一致(只存在在tp db sync)
    TYPE_5("5","TP/HK not sync"),//type:5 tp/kh db sync資料不一致(只存在在kh db sync)
    TYPE_6("6","TP/HK not sync"),//type:6 tp/kh db sync資料不一致(tp/kh不一致)
    ;

    private String id;
    private String name;

    private SyncDataType(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
}
