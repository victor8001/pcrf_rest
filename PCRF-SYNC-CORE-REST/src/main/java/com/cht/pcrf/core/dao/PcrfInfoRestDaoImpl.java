/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: PcrfInfoRestDaoImpl.java
 * Package Name : com.cht.pcrf.core.dao
 * Date 		: 2022年1月5日 
 * Author 		: AlanChen
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.core.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.cht.pcrf.core.common.AlarmConstants;
import com.cht.pcrf.core.common.CommonConstant;
import com.cht.pcrf.core.model.PcrfInfo;

public class PcrfInfoRestDaoImpl extends JdbcDaoSupport {
    protected final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CORE);
    @Value("${db.isRestSuffix}")
    private String restSuffix;

    public Long findIdByNodename(String nodename) throws Exception {

        long startTime = System.currentTimeMillis();
        boolean restEnable = Boolean.parseBoolean(restSuffix);
        String tablenm = "PCRF_INFO"+ (restEnable ? "_rest":"");
        try{
            logger.info("findIdByNodename start ");
            
            //final String sql = " SELECT ID FROM PCRF_INFO WHERE NODE_NAME = :NODE_NAME" ;
            final String sql = " SELECT ID FROM "+tablenm+" WHERE NODE_NAME = ?" ;
            
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("NODE_NAME", nodename);
            
            //List<Map<String, Object>> result = getJdbcTemplate().queryForList(sql, params);
            
            List<Map<String, Object>> result = getJdbcTemplate().queryForList(sql, new Object[]{nodename});
            
            if(result != null && result.size() > 0){
                return (Long) result.get(0).get("ID");
            }

            return 0L;
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "findIdByNodename exception! e:"+ e);
            throw e;
        }finally{
            logger.info("findIdByNodename execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    public Map<String, List<PcrfInfo>> getPcrfInfoMap() throws Exception {

        long startTime = System.currentTimeMillis();
        Map<String, List<PcrfInfo>> sapcMap = new HashMap();
        try{
            logger.info(" getPcrfInfoMap start ");

            List<PcrfInfo> result = getPcrfInfoList();
            
            for(PcrfInfo sapcInfo : result){
                String key = sapcInfo.getLocation();
                if(sapcMap.get(key) == null){
                    sapcMap.put(key, new ArrayList());
                }
                sapcMap.get(key).add(sapcInfo);
            }

        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "getPcrfInfoMap exception! e:"+ e);
            throw e;
        }finally{
            logger.info("getPcrfInfoMap execute time:"+(System.currentTimeMillis()-startTime));
//            OracleManagement.closeConnection();
        }

        return sapcMap;
    }

    public List<PcrfInfo> getPcrfInfoList() throws Exception{
        long startTime = System.currentTimeMillis();
        boolean restEnable = Boolean.parseBoolean(restSuffix);
        String tablenm = "PCRF_INFO"+ (restEnable ? "_rest":"");
        try{
            logger.info(" getPcrfInfoList start ");
            
            final String sql = "SELECT LOCATION, NODE_NAME, NODE_HOST, NODE_PORT"
                + ", LOGIN_ID, LOGIN_PW FROM "+tablenm+" WHERE STATUS = 1 ";

            return convertToObj(getJdbcTemplate().queryForList(sql));

        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "getPcrfInfoList exception! e:"+ e);
            throw e;
        }finally{
            logger.info("getPcrfInfoList execute time:"+(System.currentTimeMillis()-startTime));
        }

    }

    private List<PcrfInfo> convertToObj(List<Map<String, Object>> datas) {
        List<PcrfInfo> result = new ArrayList();
        PcrfInfo model = null;
        if(datas == null) return result;
        
        for(Map<String, Object> rs : datas){
            model = new PcrfInfo();
            model.setLocation((String) rs.get("LOCATION"));
            model.setNodeName((String) rs.get("NODE_NAME"));
            model.setNodeHost((String) rs.get("NODE_HOST"));
            model.setNodePort((String) rs.get("NODE_PORT"));
            model.setLoginId((String) rs.get("LOGIN_ID"));
            model.setLoginPw((String) rs.get("LOGIN_PW"));
            
            model.setLdapEnv();
            result.add(model);
        }
        return result;
    }

    public String getRestSuffix() {
        return restSuffix;
    }

    public void setRestSuffix(String restSuffix) {
        this.restSuffix = restSuffix;
    }
}