/**
 * Project Name : PCRF-SYNC-CORE
 * File Name 	: TransactionLogDaoImpl.java
 * Package Name : com.cht.pcrf.core.dao
 * Date 		: 2022年1月5日 
 * Author 		: AlanChen
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.core.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.cht.pcrf.core.common.AlarmConstants;
import com.cht.pcrf.core.common.SyncDataType;
import com.cht.pcrf.core.model.TransactionLog;

public class TransactionLogRestDaoImpl extends BaseDao{
    private static final String TABLE_NAME = "TRANSACTION_LOG_";
    
    public void findTransactionLog(String exportPath, String location,String nodename , String sTime, String eTime) throws Exception {

        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("findTransactionLog start ");
            
            final String sql = 
                    " ( SELECT * FROM ( " +
                    "   SELECT " +
                    "       '" + location + " - " + nodename + "', " +
                    "       CASE WHEN OPERATION = '3' THEN '" + SyncDataType.TYPE_1.getId() + "' ELSE '" + SyncDataType.TYPE_2.getId() + "' END, " +
                    "       MSISDN, "+
                    "       CASE WHEN OPERATION = '3' THEN 'EPC-Subscriber' ELSE COL1_1 END as COL1_1, " +
                    "       REQUEST "+
                    "   FROM TRANSACTION_LOG_" + location + 
                    "   WHERE CREATE_TIME BETWEEN '"+sTime+"' AND '" + eTime + "' " + 
                    "   ORDER BY TX_ID " +
                    " ) T ) ";
            
            logger.info("sql:" + sql);
            copyToFile(exportPath, sql, ",");
            
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "findTransactionLogRest exception! e:"+ e);
            throw e;
        }finally{
            logger.info("findTransactionLogRest execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    
    public void save(String location,TransactionLog vo, StringBuffer appendKeySQL,
            StringBuffer appendValSQL, List<String> updateVal) throws Exception {

        StringBuffer sql = new StringBuffer();
        Object[] saveVal = null;
        
        try{
            
            sql.append("INSERT INTO " + TABLE_NAME + location
                    + "( location, msisdn, interface, operation, request_dn, request, "
                    + " response, status, create_time ");
            
            if(appendKeySQL != null){
                sql.append(appendKeySQL);
            }
            sql.append(" ) ");
            
            sql.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? ");
            if(appendValSQL != null){
                sql.append(appendValSQL);
            }
            sql.append(" ) ");
            
            int colNum = 9 + (updateVal == null ? 0 : updateVal.size());
            int i=0;
            saveVal = new Object[colNum];
            saveVal[i++] = vo.getLocation();
            saveVal[i++] = vo.getMsisdn();
            saveVal[i++] = vo.getInterFace();
            saveVal[i++] = vo.getOperation();
            saveVal[i++] = vo.getRequestDn();
            saveVal[i++] = vo.getRequest();
            saveVal[i++] = vo.getResponse();
            saveVal[i++] = vo.getStatus();
            saveVal[i++] = vo.getCreateTime();
            
            if(updateVal != null){
                for(String u : updateVal){
                    saveVal[i++] = u;
                }
            }
            
            getJdbcTemplate().update(sql.toString(), saveVal);
        }catch(Exception e){
            try{
                String errSQL = sql.toString();
                if(saveVal != null){
                    for(Object val : saveVal){
                        errSQL = errSQL.replaceFirst("\\?", "'" + val +"' ");
                    }
                }
                logger.error("insert TransactionLogRest fail ,ex:{} ,sql:{}", e.getMessage(), errSQL);
            }catch(Exception e2){
                logger.error("insert TransactionLogRest fail ,ex:{} ,sql:{}", e.getMessage(), sql.toString());
            }
            logger.error(AlarmConstants.DB_EX + " save tx exception, e:"+ e);
            logger.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }
    
    public void createTxDataByFn(String location, String msisdn, String groupId) throws Exception {

        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("create_tx_data start ");
            
            final String sql = 
                    " SELECT  create_tx_data ('" + location + "', '" + msisdn + "','" + groupId +"') ";
            
            logger.info("sql:" + sql);
            
            
            final SimpleJdbcCall updateEmployeeCall = new SimpleJdbcCall(getJdbcTemplate()).withFunctionName("create_tx_data");
            final Map<String, Object> params = new HashMap<>();
            params.put("lc", location);
            params.put("num", msisdn);
            params.put("groupId", groupId);

//            final Map<String, Object> result = updateEmployeeCall.execute(params);
            
            String name = updateEmployeeCall.executeFunction(String.class, params);
            
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "create_tx_data exception! e:"+ e);
            throw e;
        }finally{
            logger.info("create_tx_data execute time:"+(System.currentTimeMillis()-startTime));
        }
    }    
    
}
