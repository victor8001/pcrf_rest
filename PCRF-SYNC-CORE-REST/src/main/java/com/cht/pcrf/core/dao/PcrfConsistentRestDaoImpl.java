/**
 * Project Name : PCRF-SYNC-CORE
 * File Name 	: PcrfConsistentDaoImpl.java
 * Package Name : com.cht.pcrf.core.dao
 * Date 		: 2022年1月5日 
 * Author 		: AlanChen
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.core.dao;

import java.util.Map;

import com.cht.pcrf.core.common.AlarmConstants;

public class PcrfConsistentRestDaoImpl extends BaseDao{

    public String findSyncType() throws Exception {

        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("findSyncType start ");
            
            final String sql = 
                    " SELECT SYNC_TYPE FROM PCRF_CONSISTENT ";
            
            logger.info("sql:" + sql);
            
            Map<String, Object> data = getJdbcTemplate().queryForMap(sql);
            
            if(data == null) return null;
            
            return (String) data.get("SYNC_TYPE");
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "findSyncType exception! e:"+ e);
            throw e;
        }finally{
            logger.info("findSyncType execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    public String findAutoRecMinStartTime(String sTime) throws Exception {

        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("findAutoRecMinEndTime start ");
            
            final String sql = 
            " SELECT to_char(to_timestamp('"+sTime+"','YYYY-MM-DD HH24:MI:SS') " + 
            "       - (AUTO_RECOVERY_MIN * interval '1 minute'),'YYYY-MM-DD HH24:MI:SS') as START_TIME" + 
            " FROM PCRF_CONSISTENT ";

            logger.info("sql:" + sql);
            
            Map<String, Object> data = getJdbcTemplate().queryForMap(sql);
            
            return (String) data.get("START_TIME");
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "findAutoRecMinEndTime exception! e:"+ e);
            throw e;
        }finally{
            logger.info("findAutoRecMinEndTime execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
}
