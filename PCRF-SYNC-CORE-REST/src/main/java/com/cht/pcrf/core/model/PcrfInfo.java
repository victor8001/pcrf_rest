package com.cht.pcrf.core.model;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.pool.factory.PoolingContextSource;
import org.springframework.ldap.pool.validation.DirContextValidator;

import com.cht.pcrf.core.common.LdapConstant;

public class PcrfInfo extends BaseModel {

	private Long id;
	private String location;
	private String nodeName;
	private String nodeHost;
	private String nodePort;
	private String loginId;
	private String loginPw;
	private Integer status; // 1：online 2：offline (default) 3：處理中 
	private Timestamp lastSyncTime;
	private Integer sequence;

	//private Properties ldapEnv;
	//private Hashtable ldapEnv;
	private LdapTemplate ldapEnv;
	
	//private LdapTemplate ldapTemplate;
	
    /**
     * <code>StatusCode</code>: SAPC 目前狀態 1：online 2：offline (default) 3：處理中
     * 
     * @author $Author: weilinchu $
     * @version $Id: SapcInfo.java 305 2016-01-27 05:49:53Z weilinchu $
     */
    public enum SapcStatusCode {
        ONLINE(1), OFFLINE(2), PROCESSING(3);

        private Integer code;

        private SapcStatusCode(Integer code) {
            this.code = code;
        }

        public Integer getCode() {
            return code;
        }
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getNodeHost() {
		return nodeHost;
	}

	public void setNodeHost(String nodeHost) {
		this.nodeHost = nodeHost;
	}

	public String getNodePort() {
		return nodePort;
	}

	public void setNodePort(String nodePort) {
		this.nodePort = nodePort;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getLoginPw() {
		return loginPw;
	}

	public void setLoginPw(String loginPw) {
		this.loginPw = loginPw;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Timestamp getLastSyncTime() {
		return lastSyncTime;
	}

	public void setLastSyncTime(Timestamp lastSyncTime) {
		this.lastSyncTime = lastSyncTime;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	
	public void setLdapEnv(){
	    /*
        Hashtable env = new Hashtable(11);
        String url = LdapConstant.LDAP_URL + LdapConstant.IP_PORT.replace("${ip}", getNodeHost())
                .replace("${port}", getNodePort());
        String username = LdapConstant.USERNAME.replace("${username}", getLoginId())
                        .replace("${nodeName}", getNodeName());
        String password = getLoginPw();

        //http://docs.oracle.com/javase/jndi/tutorial/ldap/connect/pool.html
        //http://docs.oracle.com/javase/jndi/tutorial/ldap/connect/config.html
        //Properties env = new Properties();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, url);
        env.put(Context.SECURITY_PRINCIPAL, username);
        env.put(Context.SECURITY_CREDENTIALS, password);
        
        
        // Enable connection pooling
        env.put("com.sun.jndi.ldap.connect.pool", "true");
     
        */
        this.ldapEnv = newLdapTemplate();
        
    }
    
    public LdapTemplate getLdapEnv(){
        return ldapEnv;
    }

    private LdapTemplate newLdapTemplate() {
        
        //LOGGER.info("Initial LdapContextSource from SapcInfo: {}, connection timeout:{}, read timeout:{}, sapcLdapPoolMaxActive:{}, sapcLdapPoolMaxIdle:{}, sapcLdapPoolMinIdle:{} ", new Object[] { sapcInfo, connTimeout, readTimeout, sapcLdapPoolMaxActive, sapcLdapPoolMaxIdle, sapcLdapPoolMinIdle });
        
        LdapContextSource ldapContextSource = new LdapContextSource();
        
        String url = LdapConstant.LDAP_URL + LdapConstant.IP_PORT.replace("${ip}", getNodeHost())
                .replace("${port}", getNodePort());
        String username = LdapConstant.USERNAME.replace("${username}", getLoginId())
                        .replace("${nodeName}", getNodeName());
        String password = getLoginPw();
        
        ldapContextSource.setUrl(url);
        ldapContextSource.setBase("");
        ldapContextSource.setUserDn(username);
        ldapContextSource.setPassword(password);
        ldapContextSource.setPooled(true);
        
        Map<String, Object> baseEnvironmentProp = new HashMap<>();
        baseEnvironmentProp.put("com.sun.jndi.ldap.connect.timeout", "30000");
        baseEnvironmentProp.put("com.sun.jndi.ldap.read.timeout", "30000");
        ldapContextSource.setBaseEnvironmentProperties(baseEnvironmentProp);
        
        ldapContextSource.afterPropertiesSet();

        PoolingContextSource poolingContextSource = getPoolingContextSource(10, 10, 0, 30000);
        poolingContextSource.setContextSource(ldapContextSource);
        
        //LOGGER.info("Initial LdapContextSource Url:{}, UserDN:{}", url, userDn);
        
        return new LdapTemplate(poolingContextSource);
    }

    /**
     * get Default PoolingContextSource
     * @return
     */
    private PoolingContextSource getPoolingContextSource(
            int sapcLdapPoolMaxActive, int sapcLdapPoolMaxIdle, int sapcLdapPoolMinIdle
            ,int sapcReadTimeout
            ){
        PoolingContextSource pcs = new PoolingContextSource();
        pcs.setMaxActive(sapcLdapPoolMaxActive);
        pcs.setMaxIdle(sapcLdapPoolMaxIdle);
        pcs.setMinIdle(sapcLdapPoolMinIdle);
//        pcs.setMaxWait(Integer.parseInt(sapcReadTimeout));
        pcs.setDirContextValidator(getDirContextValidator());
        pcs.setTestOnBorrow(true);
        pcs.setTestWhileIdle(true);
        pcs.setTestOnReturn(true);
        return pcs;
    }
    
    private DirContextValidator getDirContextValidator(){
        return new org.springframework.ldap.pool.validation.DefaultDirContextValidator();
    }
    
    @Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
