<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
	<title>login</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=10" />
	<script type="text/javascript" src="<c:url value="/js/jquery-1.10.1.min.js"/>"></script>
	<style type="text/css">
	<!--
	.text {
		font-size: 10pt
	}
	
	.table_border {
		background-color: #E6E6E6;
		border: 1px solid #999999;
	}
	
	.title {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12pt;
		font-weight: bold;
		color: #666666;
	}
	
	hr {
		border: 0;
		height: 1px;
		background-color: #d4d4d4;
		color: #d4d4d4;
		width: 95%;
		background-position: center;
	}
	-->
	</style>


	<script type="text/javascript">
	
		// 畫面載入
		function body_onload() {
			
			// 顯示訊息
			//showMessage();
		}
		// 顯示訊息
		function showMessage() {
			
			if ("${message}" != "") {
				alert("${message}");
			}
		} 
	
		// 點選登入
		function imgLogin_onclick() {
	
			
			top.location.href = "login.jsp";
			
		}
	
		/*
		// 主檔非空檢核
		function emptyCheck() {

			if (!chkRangeOfWords($("userID"), 1, 20, '帳號')) {
	
				return false;
			}
	
			if (!chkRangeOfWords($("password"), 1, 30, '密碼')) {
	
				return false;
			}
	
			return true;
		}
		*/
	</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0"
	marginheight="0" onload="body_onload();" >
<!-- Save for Web Slices (login.jpg) -->
<form id="frmLogin" name="frmLogin" method="post" action="<c:url value="/logio/login.do"/>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="80">&nbsp;</td>
	</tr>
	<tr>
		<td>
		<div align="center">
		<table id="___01" width="640" height="220" border="0" cellpadding="0"
			cellspacing="0">

			<tr>
				<td colspan="3"><img src="images/login_02.jpg" width="640"
					height="26" alt=""></td>
			</tr>
			<tr>
				<td width="40" background="images/login_03.jpg">&nbsp;</td>
				<td width="560">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="50"><span class="title">&nbsp;&nbsp;系統逾時</span>
						<hr />
						</td>
					</tr>
					<tr>
						<td>
						<div align="center">
						<table width="400" cellpadding="0" cellspacing="0">
							<tr>
								<td>
								<p>&nbsp;</p>
								<p><span class="text"><strong>請按下[確定]重新登入。</strong><br /></span></p>
								
								<p>&nbsp;</p>
								</td>
							</tr>
							
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
						</div>
						</td>
					</tr>
					<tr>
						<td align="center"><input name="button" type="button"
							onclick="imgLogin_onclick();" value="確定"></td>
					</tr>
				</table>
				</td>
				<td width="40" background="images/login_05.jpg">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3"><img src="images/login_06.jpg" width="640"
					height="39" alt=""></td>
			</tr>

		</table>
		</div>
		</td>
	</tr>
</table>
</form>
<!-- End Save for Web Slices -->

</body>
</html>
