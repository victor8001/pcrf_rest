// setting button style
$(function() {
    $("input[type=submit], input[type=button], a, button, reset").button();
});

// 去空白
function trim (str) {
    return str.replace(/^\s+|\s+$/g,"");
}

// 簡易IP驗證(數字/小數點)
function validateIP(str) {
	ipRegex = /[0-9]*.[0-9]*.[0-9]*.[0-9]*/;
	return ipRegex.test(str);
};

// 隱藏 Spring validator tags
function hideSpringValidator(errorFields) {
    if($("#" + errorFields + "\\.errors").length > 0) {
        $("#" + errorFields + "\\.errors").hide();
    }
}