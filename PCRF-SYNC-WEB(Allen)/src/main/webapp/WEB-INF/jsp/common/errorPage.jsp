<%@ page language="java" isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<script language="javascript">		
		
		</script>
	</head>
	
	<body onload="body_onload();">

		<form id="frmException" name="frmException">
			
			<table width="761" border="0" cellpadding="1" cellspacing="1" bgcolor="#333333" align="center">
				<tr>
					<td bgcolor="#FFFFFF">
						<center>
							程式發生錯誤<br>
							請將畫面複製後寄給系統管理人員<br>
							<br>
							Program Exception<br>
							Please send the screen copy to system administrators.<br>
						</center>
					</td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF">
						<%=dateFormat.format(new Date()) %>
						<br> 
						${sysException}
					</td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF">
						<c:forEach items="${sysException.stackTrace}" var="st">
							${st}<br>
						</c:forEach>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>