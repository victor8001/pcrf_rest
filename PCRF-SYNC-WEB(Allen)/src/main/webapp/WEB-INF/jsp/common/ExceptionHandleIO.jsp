<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<fmt:setBundle basename="ResourcesMessages" var="button"/>
<c:set value="${sessionScope['com.mpinfo.gcrs.sysinfo'].authorization.roleId}" var="roleids"/>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<script type="text/javascript" src="<c:url value="/js/commonFunc.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/dwr/interface/closeWindowAction.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
		
		<script language="javascript">	
			
			// 回功能首頁		
			function btnReLoad_onclick() {
				
				if (window.dialogArguments == "isDialog") {
					
					// 清除Session資訊
					clearSessionData();
					
					window.close();
				}
				else {
					<c:set value="${sessionScope['com.mpinfo.gcrs.sysinfo'].functionInformations}" var="functionInformations"/>
					document.location.href = "<c:url value="/${functionInformations.function.no}.do?method=load&funcid=${functionInformations.function.no}&_isnewfunc=1&roleids=${roleids}"/>";
				}
			}
			
			function body_onload() {
			
				if (window.dialogArguments == "isDialog") {
					document.getElementById("cmdReload").value = "<fmt:message bundle="${button}" key="BTN.CLOSEWINDOW"/>";
				}
			}
		</script>
	</head>

	<body onload="body_onload();">

		<form id="frmException" name="frmException">
			
			<table width="761" height="300" border="0" cellpadding="1" cellspacing="1" bgcolor="#333333" align="center">
				<tr>
					<td bgcolor="#FFFFFF" align="center">
						<h1>
							<fmt:bundle basename="ExceptionMessages">
								<fmt:message key="exceptionhandleio.description"/>
							</fmt:bundle>
						</h1>
					</td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF">${sysException}</td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF">
						<c:forEach items="${sysException.stackTrace}" var="st">
							${st}<br>
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td align="center" bgcolor="#FFFFFF">
					
						<input type="button" class="button" id="cmdReload"
							value="<fmt:message bundle="${button}" key="BTN.RELOAD"/>" 
							onclick="btnReLoad_onclick();">
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>