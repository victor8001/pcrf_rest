<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="../common/PageUtil.jsp" %>

<fmt:setBundle basename="ResourcesMessages" var="button"/>

<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<base target="_self"> 
	
	<script type="text/javascript" src="<c:url value="/js/commonFunc.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/dwr/interface/closeWindowAction.js"/>"></script>
	
	<script language="javascript">		
	
		// 關閉視窗動作	
		function cmdClose_onclick() {
			
			if (window.dialogArguments == "isDialog"){
				
				closeWindow();
			}
			else {
				
				document.location.href = "about:blank";
			}
		}
	</script>
	</head>
	
	<body>

		<form id="frmException" name="frmException">
			
			<table width="761" border="0" cellspacing="0" cellpadding="1" align="center">
				<tr>
					<td><img src="<c:url value="/images/GCRS_title.jpg"/>" width="761" height="43"></td>
				</tr>
			</table>
			
			<table width="761" height="300" border="0" cellpadding="1" cellspacing="1" bgcolor="#333333" align="center">
				<tr>
					<td bgcolor="#FFFFFF" align="center">
						<h1>${accessDeniedMessage}</h1>
					</td>
				</tr>
				<tr>
					<td align="center" bgcolor="#FFFFFF">
						<input type="button" class="button" 
							value="<fmt:message bundle="${button}" key="BTN.CLOSE"/>" 
							onclick="cmdClose_onclick();">
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>