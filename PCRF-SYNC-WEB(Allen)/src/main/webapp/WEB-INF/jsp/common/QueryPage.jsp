
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!--
<script type="text/javascript" src="<c:url value="/js/jquery-1.10.1.min.js"/>"></script>
-->
<script>
	window.dhx_globalImgPath="<c:url value="/images/"/>";
</script>

<!--
<c:set value="${requestScope['functionId']}_PageControler" var="pageControllerKey"/>
<c:set value="${sessionScope[pageControllerKey]}" var="pageController"/>
-->

<c:set var="pageController" value="${PAGES}"/>
<c:choose>
	<c:when test="${empty pageController || pageController.totalRowCount == 0}">
		<div id="_page_inner" style="width:100%">
			<table border="0" width="100%">
				<tr>
					<!-- 第 0頁,共 0頁 / 總共 0筆資料,顯示 0 - 0筆 -->
					<td align="left" class="pageBanner" nowrap style="border: 0px;">
						<span id="showPage"></span>
					</td>
				</tr>
			</table>
		</div>
		
		<script type="text/javascript">
			
			// 顯示分頁資訊
			
			$("#showPage").text("第 0頁,共 0頁 / 總共 0筆資料,顯示 0 - 0筆");
		</script>
	</c:when>
	
	<c:otherwise>
		<div id="_page_inner" style="width:100%">
			<table border="0" width="100%">
				<tr>
					<td align="left" class="pageBanner" width="30%" nowrap style="border: 0px;">
						<!-- 第 1頁,共 2頁 / 總共 15筆資料,顯示 1 - 10筆 -->
						<span id="showPage"></span>
					</td>
					<td width="30%" style="border: 0px;">&nbsp;</td>
					<td width="15%" align="right" nowrap style="border: 0px;">
						<c:if test="${pageController.currentPage != 1}">
							&nbsp;[<a href="#" onclick="setNextPage(1);">第一頁 </a>/
							<a href="#" onclick="setNextPage(${pageController.currentPage - 1});">前一頁 </a>]&nbsp;
						</c:if>
						<c:if test="${pageController.currentPage == 1}">
							&nbsp;[<a href="#"> 第一頁 </a>/
							<a href="#"> 前一頁 </a>]&nbsp;
						</c:if>
					</td>
					<td width="10%" align="center" nowrap style="border: 0px;">
						<select id="_go_page_no" onchange="setPageNo(this.value);" style="width: 50px;">
							<c:forEach items="${pageController.showPageNo}" var="pno">
								<option ${(pno == pageController.currentPage) ? 'selected' : ''} value="${pno}">${pno}</option>
							</c:forEach>
						</select>
					</td>
					<td width="15%" align="left" nowrap style="border: 0px;">
						<c:if test="${pageController.currentPage != pageController.totalPageCount}">
							&nbsp;[<a href="#" onclick="setNextPage(${pageController.currentPage + 1});"> 下一頁 </a>/
							<a href="#" onclick="setNextPage(${pageController.totalPageCount});"> 最後一頁 </a>]&nbsp;
						</c:if>
						<c:if test="${pageController.currentPage == pageController.totalPageCount}">
							&nbsp;[<a href="#"> 下一頁  </a>/
							<a href="#"> 最後一頁 </a>]&nbsp;
						</c:if>
					</td>
				</tr>
			</table>
		</div>
		
		<c:choose>
			<c:when test="${pageController.currentPage == 1}">
				<c:set var="showDataFrom" value="1"/>
			</c:when>
			<c:when test="${pageController.currentPage > 1}">
				<c:set var="showDataFrom" value="${(pageController.currentPage - 1) * 50 + 1}"/>
			</c:when>
		</c:choose>
		<c:choose>
			<c:when test="${(pageController.currentPage*50) >= pageController.totalRowCount}">
				<c:set var="showDataTo" value="${pageController.totalRowCount}"/>
			</c:when>
			<c:otherwise>
				<c:set var="showDataTo" value="${pageController.currentPage*50}"/>
			</c:otherwise>
		</c:choose>
		
		<script type="text/javascript">
			
			// 可輸入的下拉選單設定
			//var z=dhtmlXComboFromSelect("_go_page_no");
		
			// 翻頁動作
			function setNextPage(pageno) {
				
				document.getElementById("pageNo").value = pageno;
				goNextPage();
			}
			
			// 選擇頁面動作
			function setPageNo(nextPage) {
			
				//var nextPage = $('#_go_page_no').val();
			
				if (nextPage == null) {
					nextPage = z.getComboText()
					var patten = /^\d+$/;
					
					if (!patten.test(nextPage)) {
						z.setComboValue(${pageController.currentPage});
						return ;
					}
					
					if (nextPage < 1) {
						nextPage = 1;
					}
					
					if (nextPage > ${pageController.totalPageCount}) {
						nextPage = ${pageController.totalPageCount};
					}
				}

				setNextPage(nextPage);
				/*
				document.getElementById("pageNo").value = nextPage;
				
				goNextPage();
				*/
			}

			
			// create form and submit
			function goNextPage() {
				
				cmdQuery_onclick();
			}

			// 顯示分頁資訊
			$("#showPage").text("第 ${pageController.currentPage}頁,共 ${pageController.totalPageCount}頁 / 總共 ${pageController.totalRowCount}筆資料,顯示 ${showDataFrom} - ${showDataTo}筆");
		</script>
		
	</c:otherwise>
</c:choose>
<input type="hidden" id="pageNo" name="pageNo" value="${pageController.currentPage}">

<script type="text/javascript">
	function newQuery(){
		document.getElementById("pageNo").value = 1;
	}

	// 取代字串內容
	function replaceAll(txt, replace, with_this) {
		return txt.replace(new RegExp(replace, 'g'),with_this);
	}	
</script>
