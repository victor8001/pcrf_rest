<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<fmt:setBundle basename="ResourcesMessages" var="button"/>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<base target="_self"> 
		
		<script type="text/javascript" src="<c:url value="/js/commonFunc.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/dwr/interface/closeWindowAction.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
	
		<script language="javascript">		
		
			// 重新登入
			function cmdLogin_onclick() {
				
				if (window.dialogArguments == "isDialog") {
					
					window.close();
				}
				else {
					
					document.location.href = "<c:url value="/login.jsp"/>";
				}
			}
			
			// 初始載入
			function body_onload() {
				
				if (top != window) {
					
					top.location.href = "SessionTimeOut.do";
				}
				
				if (window.dialogArguments == "isDialog") {
					
					document.frmException.cmdReturn.value = "關閉";
				}
			}
		</script>
	</head>
	
	<body onload="body_onload();">

		<form id="frmException" name="frmException">
			
			<table width="761" border="0" cellspacing="0" cellpadding="1" align="center">
				<tr>
					<td></td>
				</tr>
			</table>
			
			<table width="761" height="300" border="0" cellpadding="1" cellspacing="1" bgcolor="#333333" align="center">
				<tr>
					<td bgcolor="#FFFFFF" align="center">
						<h1>
							SESSION TIMEOUT
						</h1>
					</td>
				</tr>
				<tr>
					<td align="center" bgcolor="#FFFFFF">
						
						<input type="button" id="cmdReturn" name="cmdReturn" value="重新登入" 
          					onclick="cmdLogin_onclick();">
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>