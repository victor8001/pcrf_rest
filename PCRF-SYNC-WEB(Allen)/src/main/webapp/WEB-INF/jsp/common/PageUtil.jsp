<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.test.com.tw/taglibs" prefix="common"%>
<%@ include file="../common/PageData.jsp" %>
<%@ include file="../common/PageComJs.jsp" %>

<c:if test="${fn:contains(pageContext.request.servletPath, '/menu.jsp') != true}">
    <!-- for setting button style -->
    <script type="text/javascript" src="<c:url value="/js/commons.js"/>"></script>
</c:if>