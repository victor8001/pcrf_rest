<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
%>
<fmt:setBundle basename="ResourcesMessages" var="button"/>
<c:set value="${sessionScope['com.mpinfo.gcrs.sysinfo'].authorization.roleId}" var="roleids"/>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<script type="text/javascript" src="<c:url value="/js/commonFunc.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/dwr/interface/closeWindowAction.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/dwr/util.js"/>"></script>
		
		<script language="javascript">		
		
			// 回功能首頁
			function btnReLoad_onclick() {
			
				if (window.dialogArguments == "isDialog") {
				
					// 清除Session資訊
					clearSessionData();
					
					window.close();
				}
				else {
					<c:set value="${sessionScope['com.mpinfo.gcrs.sysinfo'].functionInformations}" var="functionInformations"/>
					document.location.href = "<c:url value="/${functionInformations.function.no}.do?method=load&funcid=${functionInformations.function.no}&_isnewfunc=1&roleids=${roleids}"/>";
				}
			}
			
			function body_onload() {
			
				if (window.dialogArguments == "isDialog") {
					document.getElementById("cmdReload").value = "<fmt:message bundle="${button}" key="BTN.CLOSEWINDOW"/>";
				}
			}
		</script>
	</head>
	
	<body onload="body_onload();">

		<form id="frmException" name="frmException">
			
			<table width="761" height="300" border="0" cellpadding="1" cellspacing="1" bgcolor="#333333" align="center">
				<tr>
					<td bgcolor="#FFFFFF">
						<center>
							程式發生錯誤<br>
							請將畫面複製後寄給系統管理人員<br>
							<br>
							Program Exception<br>
							Please send the screen copy to system administrators.<br>
						</center>
					</td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF">
						<%=dateFormat.format(new Date()) %>
						<br> 
						${sysException}
					</td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF">
						<c:forEach items="${sysException.stackTrace}" var="st">
							${st}<br>
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td align="center" bgcolor="#FFFFFF">
						<input type="button" class="button" id="cmdReload"
							value="<fmt:message bundle="${button}" key="BTN.RELOAD"/>" 
							onclick="javascript:history.go(-1);">
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>