<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="zh_TW"/>
<fmt:setBundle basename="message_zh_TW" var="lang"/>

<c:set var="showMessage"/>

<c:if test="${messageCode != null && messageCode != ''}">
	<c:set var="showMessage">
	    <fmt:message key="${messageCode}" bundle="${lang}"/>
	</c:set>

	<c:if test="${messageParams != null && messageParams != ''}">
	<c:set var="showMessage">
	    <fmt:message key="${messageCode}" bundle="${lang}">
	        <fmt:param value="${messageParams}"/>
	    </fmt:message>
	</c:set>
	</c:if>
</c:if>

<c:set var="PUB_SHOWMESSAGE">${showMessage}</c:set>

<!-- ShowMessage 訊息 -->
<script type="text/javascript">
	// 畫面載入時處理的邏輯
	//$(document).ready(function() {
	$(window).load(function() {
		// 載入後若有訊息則顯示
		showPubMessage();
	});
	
	// 顯示訊息
	function showPubMessage() {
		
		var _pubShowMessage = "${fn:replace(PUB_SHOWMESSAGE,'\"','\\\"')}";
		//_pubShowMessage = _pubShowMessage.replace('<br>','\n');
        _pubShowMessage = _pubShowMessage.replace(/<br\s*[\/]?>/gi, "\n");

		if ( '' != _pubShowMessage ) alert( _pubShowMessage );
		
	}
	
</script>