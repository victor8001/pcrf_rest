<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/PageUtil.jsp"%>
<%@ include file="../common/ShowMessage.jsp"%><!--  顯示訊息 -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:set var="pageTitle">
    <spring:message code="label.pcrfinfo.manage"></spring:message>
</c:set>

<c:set var="functionUrl" value="/pcrfMGMT"/>
<c:set var="results" value="${entity }"/>

<spring:url var="addUrl" value="${functionUrl }/new.do"/>
<spring:url var="editUrl" value="${functionUrl }/{0}/edit.do"/>
<spring:url var="deleteUrl" value="${functionUrl }/{0}/delete.do"/>
<spring:url var="currenUrl" value="${functionUrl }/list.do" />
<spring:url var="consistentUrl" value="${functionUrl}/{0}/dataConsistent.do"/>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SAPC Management</title>

<link type="text/css" rel="stylesheet" href="<c:url value="/css/apstyle.css"/>">
<script type="text/javascript" src="<c:url value='/js/jquery-ui-timepicker-addon.js'/>"></script>
<link href="<c:url value='/css/jquery-ui-timepicker-addon.css'/>" rel="stylesheet" type="text/css">

<script type="text/javascript">
  
</script>

</head>
<body>
    <FIELDSET class="fieldsetBody">
        <table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
            <tr>
                <td align="left"><h2>${pageTitle}</h2></td>
            </tr>
        </table>
        <FIELDSET ID=fldCode2 class="fieldsetNormal">
            
           
            <div id=qry_set STYLE="width: 100%; OVERFLOW-X: auto; HEIGHT: 290PX; OVERFLOW-Y: auto;">
            
            	<common:button funcId="PCRF_MGMT" btnId="add">
	       			<a href="${addUrl }" target="mainFrame" style="font-stretch: ultra-condensed;"><spring:message code="btn.new.sapc"/></a>
        		</common:button>
        		
        		
        		
                <table ID=tblInformation style="WIDTH: 100%" border="0"
                    cellspacing="0" cellpadding="2" bordercolor="#FFFFFF">
                    <tbody id="tdy">
                        <tr id="hh" height="50px">
                            <td class="FixedTitleColumnCenter">Node Location</td>
                            <td class="FixedTitleColumnCenter">Node Name</td>
                            <td class="FixedTitleColumnCenter">Node Host</td>
                            <td class="FixedTitleColumnCenter">Function</td>
                        </tr>
                        <c:choose>
                            <c:when test="${not empty results }">
                                <c:forEach var="data" items="${results}" varStatus="status">
                                    <tr>
                                        <td class="FixedDataTableRowCenter">${data.nodeLocation}</td>
                                        <td class="FixedDataTableRow" align="center">${data.node_name}</td>
                                        <td class="FixedDataTableRow" align="center">${data.nodeHost}</td>
                                        <td class="FixedDataTableRow" align="center">
	                                        <common:button funcId="PCRF_MGMT" btnId="edit">
	                                            <a href="${fn:replace(editUrl, '{0}', data.id)}" target="mainFrame"><spring:message code='btn.edit'/></a>
	                                        </common:button>
	                                        <common:button funcId="PCRF_MGMT" btnId="delete">
	                                            <a href="${fn:replace(deleteUrl, '{0}', data.id)}"><spring:message code='btn.delete'/></a>
	                                        </common:button>
                                            <common:button funcId="PCRF_MGMT" btnId="dataConsistent">
                                                <a href="${fn:replace(consistentUrl, '{0}', data.id)}"><spring:message code='btn.dataConsistent'/></a>
                                            </common:button>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <tr><td>查無資料</td></tr>
                            </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
        </FIELDSET>
    </FIELDSET>
</body>
</html>