<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../common/PageUtil.jsp"%>
<%@ include file="../common/ShowMessage.jsp"%><!--  顯示訊息 -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:set var="pageTitle">
    <spring:message code="label.pcrfinfo.manage"/> - <spring:message code="title.edit"/>
</c:set>

<c:set var="functionUrl" value="/pcrfMGMT"/>
<spring:url var="editUrl" value="${functionUrl }/{0}/edit.do"/>
<spring:url var="listUrl" value="${functionUrl }/list.do"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="<c:url value="/css/apstyle.css"/>">
<title>New SAPC</title>

<script type="text/javascript">
$(function () {
    $("#submitBtn").click(function(event) {
//         if(!validate()) { return false; }
        
        event.preventDefault();

        // trim text
        $(':password').val(function ()  {  return trim($(this).val());  });
        $(':input').val(function ()  {  return trim($(this).val());  });
        
        $(':button').prop('disabled', true);
        $('#editSapcForm').submit();
    });
    
    $("#nodeHost").blur(function(){
    	hideSpringValidator("nodeHost");
        var validateNodeHostResult = validateNodeHost();
        if(validateNodeHostResult.trim().length != 0) {
            $("#nodeHostErrorMsg").text(validateNodeHostResult);
        } else {
            $("#nodeHostErrorMsg").text('');
        }
    });
    
    $("#nodePort").blur(function(){
    	hideSpringValidator("nodePort");
        var validateNodePortResult = validateNodePort();
        if(validateNodePortResult.trim().length != 0) {
            $("#nodePortErrorMsg").text(validateNodePortResult);
        } else {
            $("#nodePortErrorMsg").text('');
        }
    });
    
    $("#loginId").blur(function(){
    	hideSpringValidator("loginId");
        var validateNodeLoginIdResult = validateLoginId();
        if(validateNodeLoginIdResult.trim().length != 0) {
            $("#loginIdErrorMsg").text(validateNodeLoginIdResult);
        } else {
            $("#loginIdErrorMsg").text('');
        }
    });
    
    $("#loginPw").blur(function(){
    	hideSpringValidator("loginPw");
        var validateNodeLoginPwResult = validateLoginPw();
        if(validateNodeLoginPwResult.trim().length != 0) {
            $("#loginPwErrorMsg").text(validateNodeLoginPwResult);
        } else {
            $("#loginPwErrorMsg").text('');
        }
    });

    $("#sequence").blur(function(){
        hideSpringValidator("sequence");
        var validateNodeSequenceResult = validateSequence();
        if(validateNodeSequenceResult.trim().length != 0) {
            $("#sequenceErrorMsg").text(validateNodeSequenceResult);
        } else {
            $("#sequenceErrorMsg").text('');
        }
    });
    
});

function validate() {
    var validateNodeHostResult = validateNodeHost();
    var validateNodePortResult = validateNodePort();
    var validateNodeLoginIdResult = validateLoginId();
    var validateNodeLoginPwResult = validateLoginPw();
    var validateNodeSequenceResult = validateSequence();
    
    if(validateNodeHostResult.trim().length != 0) {
        alert(validateNodeHostResult);
        return false;
    }
    if(validateNodePortResult.trim().length != 0) {
        alert(validateNodePortResult);
        return false;
    }
    if(validateNodeLoginIdResult.trim().length != 0) {
        alert(validateNodeLoginIdResult);
        return false;
    }
    if(validateNodeLoginPwResult.trim().length != 0) {
        alert(validateNodeLoginPwResult);
        return false;
    }
    if(validateNodeSequenceResult.trim().length != 0) {
        alert(validateNodeSequenceResult);
        return false;
    }
    return true;
}


//validate NodeHost
function validateNodeHost() {
    var errorMsg = "";
    if($("#nodeHost").val() == '') {
        errorMsg = '<spring:message code="NotEmpty.entity.nodeHost"/>';
    } else {
        /* if(validateIP($("#nodeHost").val()) != true) { */
       	if(/^([\d]{1,})(\.)([\d]{1,})(\.)([\d]{1,})(\.)([\d]{1,})$/.test($("#nodeHost").val()) != true) {
            errorMsg = '<spring:message code="Pattern.entity.nodeHost"/>';
        }
    }
    return errorMsg;
}

//validate NodePort
function validateNodePort() {
    var errorMsg = "";
    if($("#nodePort").val() == '') {
        errorMsg = '<spring:message code="NotEmpty.entity.nodePort"/>';
    } else {
        if(/[1-9]d*/.test($("#nodePort").val()) != true) {
            errorMsg = '<spring:message code="Pattern.entity.nodePort"/>';
        }
    }
    return errorMsg;
}

//validate loginId
function validateLoginId () {
    var errorMsg = "";
    if($("#loginId").val() == '') {
        errorMsg = '<spring:message code="NotEmpty.entity.loginId"/>';
    }
    return errorMsg;
}

//validate loginPw
function validateLoginPw() {
    var errorMsg = "";
    if($("#loginPw").val() == '') {
        errorMsg = '<spring:message code="NotEmpty.entity.loginPw"/>';
    }
    return errorMsg;
}

//validate sequence
function validateSequence() {
    var errorMsg = "";
    var sequenceVal = $("#sequence").val();
    if(sequenceVal == '' || sequenceVal < 1 || sequenceVal > 100) {
        errorMsg = '<spring:message code="Size.entity.sequence"/>';
    }
    return errorMsg;
}
function hideSpringValidator(errorFields) {
    if($("#" + errorFields + "\\.errors").length > 0) {
        $("#" + errorFields + "\\.errors").hide();
    }
}
</script>

</head>
<body>
    <FIELDSET class="fieldsetBody">
        <table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
            <tr><TD align="left"><h2>${pageTitle}</h2></TD></tr>
        </table>
        <FIELDSET ID=fldCode2 class="fieldsetNormal">
            <div align="left">
                <form:form method="POST" modelAttribute="entity" id="editSapcForm" action="${fn:replace(editUrl, '{0}', entity.id)}">
					<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#d0d0d0">
                        <tr>
							<td class="topTableRowLeft" width="15%">
                                <div align="right" class="text">Node Name：</div>
                            </td>
                            <td class="topTableRowRight">${entity.node_name }</td>
                        </tr>
                        <tr>
							<td class="topTableRowLeft" width="15%">
                                <div align="right" class="text">
                                    <label style="color: red; font-weight: bolder;">*</label>Node Host：
                                </div>
                            </td>
                            <td class="topTableRowRight">
                                <form:input path="nodeHost"/>
                                <label id="nodeHostErrorMsg" class="errorMsgStyle" style="color: red;"></label>
                                <form:errors path="nodeHost" cssClass="errorMsgStyle" element="label"></form:errors>
                            </td>
                        </tr>
                        <tr>
							<td class="topTableRowLeft" width="15%">
                                <div align="right" class="text">
                                    <label style="color: red; font-weight: bolder;">*</label>Node Port：
                                </div>
                            </td>
                            <td class="topTableRowRight">
                                <form:input path="nodePort"/>
                                <label id="nodePortErrorMsg" class="errorMsgStyle" style="color: red;"></label>
                                <form:errors path="nodePort" cssClass="errorMsgStyle" element="label"></form:errors>
                            </td>
                        </tr>
                        <tr>
							<td class="topTableRowLeft" width="15%">
                                <div align="right" class="text">
                                    <label style="color: red; font-weight: bolder;">*</label>Login ID：
                                </div>
                            </td>
                            <td class="topTableRowRight">
                                <form:input path="loginId"/>
                                <label id="loginIdErrorMsg" class="errorMsgStyle" style="color: red;"></label>
                                <form:errors path="loginId" cssClass="errorMsgStyle" element="label"></form:errors>
                            </td>
                        </tr>
                        <tr>
							<td class="topTableRowLeft" width="15%">
                                <div align="right" class="text">
                                    <label style="color: red; font-weight: bolder;">*</label>Login Password：
                                </div>
                            </td>
                            <td class="topTableRowRight">
                                <form:password path="loginPw" size="21"/>
                                <label id="loginPwErrorMsg" class="errorMsgStyle" style="color: red;"></label>
                                <form:errors path="loginPw" cssClass="errorMsgStyle" element="label"></form:errors>
                            </td>
                        </tr>
                    </table>
					<table width="100%">
                        <tr align="center">
                            <td colspan="3">
                                <common:button funcId="PCRF_MGMT" btnId="edit">
                                    <form:button id="submitBtn"><spring:message code='btn.save'/></form:button>
                                </common:button>
                                <button type="reset" onclick="window.location.replace('${listUrl}')"><spring:message code="btn.cancel" /></button>
                            </td>
                        </tr>
                    </table>
                </form:form>
            </div>
        </FIELDSET>
    </FIELDSET>
</body>
</html>