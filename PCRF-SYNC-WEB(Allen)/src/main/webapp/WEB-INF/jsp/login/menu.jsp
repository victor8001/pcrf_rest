﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ include file="../common/PageUtil.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<head>


<style type="text/css">
.arrowlistmenu {
	width: 100%; /*width of accordion menu*/
}

.arrowlistmenu .menuheader {
	/*CSS class for menu headers in general (expanding or not!)*/
	font: bold 20px 新細明體;
	color: white;
	background: #3594ce url('<%=request.getContextPath()%>'/images/menuButton.png) repeat-x center left;
	margin-bottom: 10px;
	/*bottom spacing between header and rest of content*/
	text-transform: uppercase;
	padding: 4px 0 4px 10px; /*header text is indented 10px*/
	cursor: hand;
	cursor: pointer;
	-moz-border-radius: 5px;
	border-radius: 5px;
	-moz-box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.5);
	-webkit-box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.5);
	box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.5);
}

.arrowlistmenu .openheader {
	/*CSS class to apply to expandable header when it's expanded*/
	background-image: url('<%=request.getContextPath()%>'/images/titlebar-active.png);
}

.arrowlistmenu ul { /*CSS for UL of each sub menu*/
	list-style-type: none;
	margin: 0;
	padding: 0;
	margin-bottom: 8px;
	/*bottom spacing between each UL and rest of content*/
}

.arrowlistmenu ul li {
	padding-bottom: 2px; /*bottom spacing between menu items*/
}

.arrowlistmenu ul li .opensubheader {
	/*Open state CSS for sub menu header*/
	background: lightblue !important;
}

.arrowlistmenu ul li .closedsubheader {
	/*Closed state CSS for sub menu header*/
	background: lightgreen !important;
}

.arrowlistmenu ul li a {
	color: #000079;
	background: url('<%=request.getContextPath()%>'/images/icon_diamond.png) no-repeat center left;
	/*custom bullet list image*/
	font: bold 22px 新細明體;
	display: block;
	padding: 6px 0;
	padding-left: 19px; /*link text is indented 19px*/
	text-decoration: none;
	font-weight: bold;
	border-bottom: 1px solid #dadada;
	font-size: 100%;
}

.arrowlistmenu ul li a:visited {
	color: #000079;
}

.arrowlistmenu ul li a:hover { /*hover state CSS*/
	background-color: #3594ce;
	color: #ffffff;
}

.arrowlistmenu ul li a.subexpandable:hover {
	/*hover state CSS for sub menu header*/
	background: lightblue;
}
.liMunuSelected {
	background: lightblue;
}
</style>

<script>
	$(document).ready(function() 
	{
		$("#ulMenu").on("click", ".liMenuClass", function(event){
			$("li").removeClass("liMunuSelected");
			$(this).addClass("liMunuSelected");
		});
	});
</script>
</head>

<body bgcolor="lightblue">

<!--
<div class="arrowlistmenu">
<spring:url var="editUrl" value="/userControl/{0}/edit.do"/>

<h3 class="menuheader expandable">MENU&nbsp;</h3>
	<ul class="categoryitems">
		<li><a href="<c:url value="/userControl/list.do?isNewQuery=Y"/>" target="mainFrame">使用者管理</a></li>		
        
		<li><a href="${fn:replace(editUrl, '{0}', userData.USER_ACCOUNT)}?isNewQuery=Y" target="mainFrame">個人資料管理</a></li>
		<li><a href="<c:url value="/apiLog/list.do?isNewQuery=Y"/>" target="mainFrame">TransactionLogQuery</a></li>
		<li><a href="<c:url value="/sapcManagement/list.do"/>" target="mainFrame">SAPC管理</a></li>
		<li><a href="<c:url value="/subscriber/list.do?isNewQuery=Y"/>" target="mainFrame">門號管理</a></li>
		<li><a href="<c:url value="/groups/list.do?isNewQuery=Y"/>" target="mainFrame">群組管理</a></li>
	</ul>

</div>
-->


<div class="arrowlistmenu">
<!--  <h3 class="menuheader expandable">${data.MENU_NAME}&nbsp;</h3> -->
<ul id="ulMenu" class="categoryitems">
	<c:forEach items="${menuList}" var="data">
	  <c:forEach items="${data.SUB_MENU_LIST}" var="subData">
	      <li id="liMenu" class="liMenuClass"><a href="<c:url value="${fn:replace(subData.FUNC_URL, '{0}', userData.USER_ACCOUNT)}"/>?isNewQuery=Y" target="mainFrame">${subData.FUNC_CNAME}</a></li>
	  </c:forEach>
	</c:forEach>
	<li id="liMenu" class="liMenuClass"><a href="<c:url value="${fn:replace('/personalInfo/{0}/edit.do', '{0}', userData.USER_ACCOUNT)}"/>?isNewQuery=Y" target="mainFrame">編輯密碼</a></li>
</ul>



</div>
</body>
</html>
