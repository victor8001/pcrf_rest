﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ include file="../common/PageUtil.jsp" %>

<%
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
%>

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style>
		div {
		  	color: #000079;
		  	font: bold 13px Arial Black;
		  	cursor: hand;
			cursor: pointer;
		}
		span {
		  	color: #000079;
		  	font: bold 16px 新細明體;
		  	cursor: hand;
			cursor: pointer;
		}
	</style>

	<SCRIPT type="text/javascript">
		function resizeFrame1(){
	        var frameSet = window.parent.document.getElementsByTagName("frameset")[1];
			//var a = document.getElementById("showtab1").getElementsByTagName("div");
			//var b = a[0].getElementsByTagName("td");
			//a[2].innerHTML = "顯示MEMU";
	        frameSet.cols="0px,*";
	        var obj = document.getElementById("rightBlock");
	        obj.style.display="block";
			var obj = document.getElementById("leftBlock");
	        obj.style.display="none";
	        return true;
		}

		function resizeFrame2(){
	        var frameSet = window.parent.document.getElementsByTagName("frameset")[1];
			//var a = document.getElementById("showtab1").getElementsByTagName("tr");
			//var b = a[0].getElementsByTagName("td");
			//b[2].innerHTML = "隱藏MEMU";
	        var obj = document.getElementById("rightBlock");
	        obj.style.display="none";
			var obj = document.getElementById("leftBlock");
	        obj.style.display="block";
	        frameSet.cols="228px,*";
	        return true;
		}
		
		function logout(){
			parent.location.href = '<c:url value="/login/logout.do?method=logout"/>';
		}
	</SCRIPT>

	<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<form id="frmtitle01" name="frmtitle01" method="post"  enctype="multipart/form-data">
		<!-- Save for Web Slices (top.psd) -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<!--tr>
				<td width="44px"><img src="<c:url value="/images/top_01.jpg"/>" width="104" height="87"></td>
				<td width="124px"><img src="<c:url value="/images/top_02.jpg"/>" width="124" height="87"></td>
				<td width="799px" colspan="2"><img src="<c:url value="/images/top_03.jpg"/>" width="799" height="87"></td>
				<td background="<c:url value="/images/top_04.jpg"/>" width="449px" >&nbsp;</td>
			</tr-->
			<tr>
				<td style="background-color:lightblue">
				<img src="<c:url value="/images/chunghwalogo.png"/>"/>
				</td>
				<td style="background-color:lightblue" align="left" width="67%">
					<span>
						登入帳號：<U>${userData.USER_ACCOUNT}&nbsp;${userData.USER_NAME}</U>&nbsp;&nbsp;&nbsp;&nbsp;					
						 登入時間：<U><%=dateFormat.format(new Date()) %></U>					</span>				</td>
				<td style="background-color:lightblue" align="right"><span> <img
					src="<c:url value="/images/logout.jpg"/>" alt="登出" onClick="logout();" width="73"
					height="23" style="-moz-box-shadow: 3px 3px 3px #444; -webkit-box-shadow: 3px 3px 3px #444; box-shadow: 3px 3px 3px rgba(255, 0, 0, .5);">
					 &nbsp;&nbsp;&nbsp;&nbsp;</span></td>
				<td style="background-color:lightblue">&nbsp;				</td>
			</tr>
			<tr>
				
				<td style="background-color:lightblue">
					<div id="rightBlock" style="display:none;" align="right" onClick="resizeFrame2();">
						MENU&nbsp;
						<img src="<c:url value="/images/arrow_right.png"/>" alt="顯示MENU" width="15" height="11">
						<img src="<c:url value="/images/arrow_right.png"/>" alt="顯示MENU" width="15" height="11">					</div>
						<div id="leftBlock" style="display:block" align="right" onClick="resizeFrame1();">
						<img src="<c:url value="/images/arrow_left.png"/>" alt="隱藏MENU" width="15" height="11">
						<img src="<c:url value="/images/arrow_left.png"/>" alt="隱藏MENU" width="15" height="11">
						&nbsp;MENU					</div>				</td>
				<td colspan="2" ><img src="<c:url value="/images/top_11.jpg"/>" width="67%" height="19"></td>
				<td>&nbsp;</td>
			</tr>
	</table>
	</form>
		<!-- End Save for Web Slices -->
	</body>
</html>

