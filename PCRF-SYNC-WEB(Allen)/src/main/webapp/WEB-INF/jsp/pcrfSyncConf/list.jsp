<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/PageUtil.jsp"%>
<%@ include file="../common/ShowMessage.jsp"%><!--  顯示訊息 -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<c:set var="pageTitle">
    <spring:message code="label.pcrfconsistent.manage"></spring:message>
</c:set>

<c:set var="functionUrl" value="/pcrfSyncConf"/>
<c:set var="execTypeSelect" value="${execTypeSelect}"/>

<spring:url var="addUrl" value="${functionUrl }/new.do"/>
<spring:url var="editUrl" value="${functionUrl }/{0}/edit.do"/>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SAPC Management</title>

<link type="text/css" rel="stylesheet" href="<c:url value="/css/apstyle.css"/>">
<script type="text/javascript" src="<c:url value='/js/jquery-ui-timepicker-addon.js'/>"></script>
<link href="<c:url value='/css/jquery-ui-timepicker-addon.css'/>" rel="stylesheet" type="text/css">


<script type="text/javascript">
	
	
	$(function(){
		var errorMsg = "";
		$('#execWeek').blur(function(){
			$('#execMonth').val("");
			$('#execDay').val("");
			
			var week = $('#execWeek').val();
			
			if(week != ''){
				var week1 = week.split(',');
				
				for(var i=0 ; i<week1.length ; i++){
					if(week1[i] <= 0 ||week1[i] > 7){
						errorMsg = '<spring:message code="out.of.range"/>';
						alert(errorMsg);
						$(this).val("");
					}
				}
			}
		});

		$('#execMonth').blur(function(){
			$('#execWeek').val("");
			
			var month = $('#execMonth').val();
			
			if(month != ''){
				var month1 = month.split(',');

				for(var i=0 ; i<month1.length ; i++){
					if(month1[i] <= 0 || month1[i] > 12){
						errorMsg = '<spring:message code="out.of.range"/>';
						alert(errorMsg);
						$(this).val("");
					}
				}
			}
		});
		
		$('#execDay').blur(function(){
			$('#execWeek').val("");
			
			var day = $('#execDay').val();
			
			if(day != ''){
				
				var day1 = day.split(',');
				
				for(var i=0 ; i<day1.length ; i++){
					if(day1[i] <= 0 || day1[i] > 31){
						errorMsg = '<spring:message code="out.of.range"/>';
						alert(errorMsg);
						$(this).val("");
					}
				}
			}
		});
		
		$('#execHour').blur(function(){
			var hour = $('#execHour').val();
			var hour1 = hour.split(',');
			
			for(var i=0 ; i<hour1.length ; i++){
				if(hour1[i] > 23){
					errorMsg = '<spring:message code="out.of.range"/>';
					alert(errorMsg);
					$(this).val("");
				}
			}
		});
		
		$('#submitBtn').click(function(){
			var week = $('#execWeek').val();
			var month = $('#execMonth').val();
			var day = $('#execDay').val();
			var hour = $('#execHour').val();
			
			if(week != '' && hour == ''){
				errorMsg = '<spring:message code="execHour.not.empty"/>';
				alert(errorMsg);
				return false;
			}
		
			if(month != '' && day == ''){
				errorMsg = '<spring:message code="execDay.not.empty"/>';
				alert(errorMsg);
				return false;
			}
			if(month != '' && hour == ''){
				errorMsg = '<spring:message code="execHour.not.empty"/>';
				alert(errorMsg);
				return false;
			}
			
			if(day != '' && month == ''){
				errorMsg = '<spring:message code="execMonth.not.empty"/>';
				alert(errorMsg);
				return false;
			}
			if(day != '' && hour == ''){
				errorMsg = '<spring:message code="execHour.not.empty"/>';
				alert(errorMsg);
				return false;
			}
			
			if(hour != '' && week =='' && month ==''){
				errorMsg = '<spring:message code="WeekAndMonth.not.empty"/>';
				alert(errorMsg);
				return false;
			}
		});
		
	})
	
	
	
</script>

</head>
<body>
    <FIELDSET class="fieldsetBody">
        <table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
            <tr>
                <td align="left"><h2>${pageTitle}</h2></td>
            </tr>
        </table>
        <FIELDSET ID=fldCode2 class="fieldsetNormal">
            <div id=qry_set STYLE="width: 100%; OVERFLOW-X: auto; HEIGHT: 500PX; OVERFLOW-Y: auto;">
	            <form:form method="POST" modelAttribute="entity" id="editSapcForm" action="${fn:replace(editUrl, '{0}', entity.id)}">
		   			<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#d0d0d0">
		   				<tr>
		   					<td class="topTableRowLeft" width="15%">資料自動回補 : </td>
		   					<td class="topTableRowRight">
		   						<div align="left" class="text">
		   						啟用 <form:radiobutton path="syncType" value="A" /> &nbsp;
		   						停用 <form:radiobutton path="syncType" value="M" />
		   						</div>
		   					</td>
		   				</tr>
		   			</table>
		   			<br>
		   			<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#d0d0d0">
		   				<tr>
		   					<td class="topTableRowLeft" width="15%">PCRF RECOVERY  <br>資料回補區間設定(分): </td>
		   					<td class="topTableRowRight">
		   						<form:input path="autoRecoveryMin" id="autoRecoveryMin"  class="noZero"/> 
		   					</td>
		   				</tr>
		   			</table>
		   			<br>
		   			<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#d0d0d0">
		   				<tr>
		   					<td class="topTableRowLeft" width="15%">執行週 : </td>
		   					<td class="topTableRowRight">
		   						<form:input path="execWeek" id="execWeek"  class="noZero" onkeyup="this.value=this.value.replace(/[^0-9,*]/g,'')" /> <spring:message code="text.eg.week" />	
		   					</td>
		   				</tr>
		   				<tr>
		   					<td class="topTableRowLeft" width="15%">執行月 : </td>
		   					<td class="topTableRowRight">
		   						<form:input path="execMonth" id="execMonth" class="noZero" onkeyup="this.value=this.value.replace(/[^0-9,*]/g,'')"/> <spring:message code="text.eg.month" />
		   					</td>
		   				</tr>
		   				<tr>
		   					<td class="topTableRowLeft" width="15%">執行日 : </td>
		   					<td class="topTableRowRight">
		   						<form:input path="execDay" id="execDay"  class="noZero" onkeyup="this.value=this.value.replace(/[^0-9,*]/g,'')"/> <spring:message code="text.eg.day" />
		   					</td>
		   				</tr>
		   				<tr>
		   					<td class="topTableRowLeft" width="15%">執行時 : </td>
		   					<td class="topTableRowRight">
		   						<form:input path="execHour" id="execHour" onkeyup="this.value=this.value.replace(/[^0-9,*]/g,'')"/> <spring:message code="text.eg.hour" />
		   					</td>
		   				</tr>
		   			</table>
		   			<br>
		   			
		   			<label class="txt">範例：</label>
                    <br>
                    <TABLE border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" width="100%">
                        <TR>
                            <TD> 
                            1.每天0、6、12、1 8點執行一次<br>
                                                                                            週：* ； 月：* ；日：*； 時：0,6,12,18<br>
                             <br>
                            2.每2天、1點執行一次<br>
                                                                                        週：* ； 月：* ；日：*/2 ； 時：1<br>
                            <br>
                            3.每週日、1點執行<br>
                                                                                        週：1 ； 月：* ；日：* ； 時：1<br>
                            </TD>
                        </TR>
                    </TABLE>
		   			<table width="100%">		
                        <tr align="center">
                            <td colspan="3">
                                <common:button funcId="PCRF_SYNC_CONF" btnId="add">
                                   <form:button id="submitBtn" onclick="aaa()"><spring:message code='btn.save'/></form:button>
                                </common:button>
                                <!-- button type="reset" onclick="window.location.replace('${listUrl}')"><spring:message code="btn.cancel" /></button-->
                            </td>
                        </tr>
                	 </table>
		   			
		   			
   				</form:form>
   			
            </div>
        </FIELDSET>
    </FIELDSET>
</body>
</html>