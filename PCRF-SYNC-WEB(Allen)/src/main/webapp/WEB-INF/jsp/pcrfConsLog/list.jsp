<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/PageUtil.jsp"%>
<%@ include file="../common/ShowMessage.jsp"%><!--  顯示訊息 -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:set var="pageTitle">
    <spring:message code="label.pcrfconslog.manage"></spring:message>
</c:set>

<c:set var="functionUrl" value="/pcrfConsLog"/>
<spring:url var="listUrl" value="${functionUrl }/list.do"/>
<spring:url var="downloadUrl" value="${functionUrl }/downLoadFile.do"/>
<spring:url var="dataSyncUrl" value="${functionUrl }/dataSync.do"/>
<spring:url var="viewUrl" value="${functionUrl }/view.do"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PCRF Consistent Log Management</title>

<link type="text/css" rel="stylesheet" href="<c:url value="/css/apstyle.css"/>">
<script type="text/javascript" src="<c:url value='/js/jquery-ui-timepicker-addon.js'/>"></script>
<link href="<c:url value='/css/jquery-ui-timepicker-addon.css'/>" rel="stylesheet" type="text/css">

<style type="text/css">

</style>

<script type="text/javascript">
$(function () {
	
	var dates = $( ":text" ).datepicker({
        changeMonth: true,
        changeYear: true,
        duration: "slow",
        dateFormat: 'yy-mm-dd',
      }); 
	
	$('#submitBtn').click(function() {
		var startTime = $('#startTime').val();
		var endTime = $('#endTime').val();
		if(startTime ==null || startTime =="" || endTime ==null || endTime =="" ){
			alert("查詢日期未輸入完整，請再次輸入");
			return false;
		}
        $('#queryForm').submit();
	});
    
	
})

function cmdQuery_onclick(){
	$('#submitBtn').click();
}
</script>

</head>
<body>
    <FIELDSET class="fieldsetBody">
    
        <table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
            <tr>
                <td align="left"><h2>${pageTitle}</h2></td>
            </tr>
        </table>
        <FIELDSET ID=fldCode2 class="fieldsetNormal">
            <form method="POST" id="queryForm" action="${listUrl}" >
            <div class="divPage">
                <!-- 加入分頁模組 -->
                <jsp:include page="../common/QueryPage.jsp" />
            </div>
            <div id=qry_set STYLE="width: 100%; OVERFLOW-X: auto; HEIGHT: 590PX; OVERFLOW-Y: auto;">
	                <div id="search_block">
			   			<table>
		                	<tr>
			                	<td>
			                    	<label>Node Location:</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			                    		<select name="node_location" >
			                				<c:forEach var="commons" items="${commons }">
			                    				<option value="${commons.valueType}" <c:if test="${node_location == commons.valueType}">selected</c:if>>${commons.valueName}</option>
			                    			</c:forEach>
			                    		</select>
			                    </td>
		                	</tr>
		                	<tr>
		                		<td>
		                			<label>*開始/結束 日期 :</label>&nbsp;&nbsp;&nbsp;&nbsp;
		                			<input type="text" id="startTime" name="startTime" value="${startTime}"> ~
		                			<input type="text" id="endTime" name="endTime" value="${endTime}">
		                		</td>
		                	</tr>
		                	<tr>
		                		<td>
		                			<label>執行結果 :</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		                				<select name="execResult">
                                            <option value="-1">全部</option>
		                					<option value="0" <c:if test="${execResult == '0'}">selected</c:if>>執行中</option>
		                					<option value="1" <c:if test="${execResult == '1'}">selected</c:if>>失敗</option>
		                					<option value="2" <c:if test="${execResult == '2'}">selected</c:if>>成功(都一致)</option>
		                					<option value="3" <c:if test="${execResult == '3'}">selected</c:if>>成功(不一致)</option>
		                				</select>
		                		</td>
		                	</tr>
		                	 <tr align="center">
			                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			                        <common:button funcId="PCRF_CONS_LOG" btnId="query">
			                            <input type="button" id="submitBtn" name="submitBtn" value="<spring:message code='btn.submit'/>"/>
			                        </common:button>
			                    </td>
		                    </tr>
		                </table>
		   			</div>
   				</form>
   				
   				<LEGEND class="txt"><spring:message code="title.query.result"/></LEGEND>
   				<table style="WIDTH: 100%" border="0" cellspacing="0" cellpadding="2" bordercolor="#FFFFFF">
					<tr height="50px">
						<td class="FixedTitleColumnCenter" width="10%">Node Location</td>
						<td class="FixedTitleColumnCenter" width="10%">Node Name</td>
						<td class="FixedTitleColumnCenter" width="20%">執行時間</td>
						<td class="FixedTitleColumnCenter" width="10%">執行結果</td>
                        <td class="FixedTitleColumnCenter" width="10%">同步結果</td>
						<td class="FixedTitleColumnCenter" width="20%">功能</td>
					</tr>
						<c:forEach items="${listResult}" var="subData">
							<tr>
								<!-- c:forEach items="${data.SUB_DATA}" var="subData"-->
									<td class="FixedDataTableRow" align="center">${(subData.LOCATION =='TP')?'台北': ((subData.LOCATION =='KH') ? '高雄' : '') }</td>
									<td class="FixedDataTableRow" align="center">${subData.NODE_NAME }</td>
									<td class="FixedDataTableRow" align="center">${subData.EXEC_START_TIME }~${subData.EXEC_END_TIME }</td>
									<td class="FixedDataTableRow" align="center">
										<c:choose>
										    <c:when test="${subData.EXEC_RESULT == '0'}">
										    	執行中
										    </c:when>
											<c:when test="${subData.EXEC_RESULT == '1'}">
										    	失敗
										    </c:when>
										    <c:when test="${subData.EXEC_RESULT == '2'}">
										    	成功(都一致)
										    </c:when>
	                                        <c:when test="${subData.EXEC_RESULT == '3'}">
												成功(不一致)
	                                        </c:when>
	                                        <c:when test="${subData.EXEC_RESULT == '4'}">
												執行成功
	                                        </c:when>
										    <c:otherwise>
												${subData.EXEC_RESULT}
										    </c:otherwise>
										</c:choose>
										<c:if test="${subData.IS_QCI == 'Y'}"><br>(QCI)</c:if>
									</td>
                                    <td class="FixedDataTableRow" align="center">
	                                    <c:choose>
	                                        <c:when test="${subData.SYNC_RESULT == '1'}">失敗</c:when>
	                                        <c:when test="${subData.SYNC_RESULT == '2'}">成功</c:when>
                                            <c:when test="${subData.SYNC_RESULT == '3'}">執行中</c:when>
	                                        <c:otherwise>&nbsp;</c:otherwise>
	                                    </c:choose>
                                    </td>
							    	<td class="FixedDataTableRow" align="center">
							    	    <c:if test="${subData.EXEC_RESULT == '1' || subData.EXEC_RESULT == '3'}">
							    		<common:button funcId="PCRF_CONS_LOG" btnId="export">
	                                    	<form action="${downloadUrl}" method="post">
	                                        	<input type="hidden" name="path" value="${subData.RPT_PATH}">
	                                       		<input type="submit" value="<spring:message code='btn.download'/>"> 
	                                        </form>
	                                    </common:button>
	                                    </c:if>
	                                    <c:if test="${subData.LOCATION != null && subData.LOCATION != '' && subData.EXEC_RESULT == '3'}">
                                        <common:button funcId="PCRF_CONS_LOG" btnId="dataSync">
                                            <form action="${dataSyncUrl}" method="post">
                                                <input type="hidden" name="logId" value="${subData.ID}">
                                                <input type="hidden" name="node_location" value="${node_location}">
                                                <input type="hidden" name="startTime" value="${startTime}">
                                                <input type="hidden" name="endTime" value="${endTime}">
                                                <input type="hidden" name="execResult" value="${execResult}">
                                                <input type="hidden" name="syncType" value="all">
                                                <input type="submit" value="<spring:message code='btn.dataSync'/>"> 
                                            </form>
                                        </common:button>
                                        
                                        <common:button funcId="PCRF_CONS_LOG" btnId="logView">
                                            <form action="${viewUrl}" method="post">
                                                <input type="hidden" name="logId" value="${subData.ID}">
                                                <input type="submit" value="<spring:message code='label.view'/>"> 
                                            </form>
                                        </common:button>
                                        </c:if>
    								</td>
							    	 	
							    <!--/c:forEach-->
							</tr>
						</c:forEach>
						<c:if test="${empty listResult}">
							<tr><td>查無資料</td></tr>
						</c:if>
   				</table>
            </div>
        </FIELDSET>
        </FIELDSET>
</body>
</html>