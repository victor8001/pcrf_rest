<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/PageUtil.jsp"%>
<%@ include file="../common/ShowMessage.jsp"%><!--  顯示訊息 -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:set var="pageTitle">
    <spring:message code="label.pcrfconslog.manage"></spring:message>
</c:set>

<c:set var="functionUrl" value="/pcrfConsLog"/>
<spring:url var="dataSyncUrl" value="${functionUrl }/dataSync.do"/>
<spring:url var="listUrl" value="${functionUrl }/list.do"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PCRF Consistent Log Management</title>

<link type="text/css" rel="stylesheet" href="<c:url value="/css/apstyle.css"/>">
<script type="text/javascript" src="<c:url value='/js/jquery-ui-timepicker-addon.js'/>"></script>
<link href="<c:url value='/css/jquery-ui-timepicker-addon.css'/>" rel="stylesheet" type="text/css">

<style type="text/css">

</style>

<script type="text/javascript">
</script>

</head>
<body>
    <FIELDSET class="fieldsetBody">
        <label id="showtxt"></label>
        <table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
            <tr>
                <td align="left"><h2>${pageTitle}</h2></td>
            </tr>
        </table>
        <FIELDSET ID=fldCode2 class="fieldsetNormal">
            <div id=qry_set STYLE="width: 100%; OVERFLOW-X: auto; HEIGHT: 590PX; OVERFLOW-Y: auto;">
   				<LEGEND class="txt"><spring:message code="title.query.result"/></LEGEND>
   				<table style="WIDTH: 100%" border="0" cellspacing="0" cellpadding="2" bordercolor="#FFFFFF">
					<tr height="50px">
                        <td class="FixedTitleColumnCenter" width="10%"></td>
						<td class="FixedTitleColumnCenter" width="10%">Node Location</td>
						<td class="FixedTitleColumnCenter" width="10%">TYPE</td>
						<td class="FixedTitleColumnCenter" width="20%">MSISDN</td>
						<!-- 
						<td class="FixedTitleColumnCenter" width="10%">OBJECTCLASS</td> -->
                        <td class="FixedTitleColumnCenter" width="10%">INFO(in DB SYNC)</td>
						<td class="FixedTitleColumnCenter" width="20%">INFO(in PCRF)</td>
                        <td class="FixedTitleColumnCenter" width="10%">result</td>
					</tr>
						<c:forEach items="${rptResult}" var="subData">
							<tr>
							     <td class="FixedDataTableRow" align="center">
                                       <common:button funcId="PCRF_CONS_LOG" btnId="dataSync">
                                            <form action="${dataSyncUrl}" method="post">
                                                <input type="hidden" name="logId" value="${logId}">
                                                <input type="hidden" name="type" value="${subData.TYPE}">
                                                <input type="hidden" name="rptRawData" value='${subData.R_DATA}'>
                                                <input type="hidden" name="msisdn" value="${subData.MSISDN}">
                                                <input type="hidden" name="syncType" value="one">
                                                <input type="submit" value="<spring:message code='btn.dataSync'/>"> 
                                            </form>
                                        </common:button>
                                </td>
								<td class="FixedDataTableRow" align="center">${subData.NODE}</td>
								<td class="FixedDataTableRow" align="center">
                                    <c:choose>
                                        <c:when test="${subData.TYPE == '1'}">${subData.TYPE}&nbsp;存在PCRF
                                        </c:when>
                                        <c:when test="${subData.TYPE == '2'}">${subData.TYPE}&nbsp;存在DB SYNC
                                        </c:when>
                                        <c:when test="${subData.TYPE == '3'}">${subData.TYPE}&nbsp;存在PCRF及DB SYNC，資料不一致
                                        </c:when>
                                        <c:otherwise>${subData.TYPE}
                                        </c:otherwise>
                                    </c:choose>
								</td>
								<td class="FixedDataTableRow" align="center">${subData.MSISDN}</td>
                                <td class="FixedDataTableRow" align="center">
                                    <c:if test="${subData.TYPE == '2' || subData.TYPE == '3'}">${subData.INFO1}</c:if>&nbsp;</td>
                                <td class="FixedDataTableRow" align="center">
                                    <c:if test="${subData.TYPE == '1' || subData.TYPE == '3'}">${subData.INFO2}</c:if>&nbsp;</td>
                                    
                                <td class="FixedDataTableRow" align="center">${subData.RESULT}&nbsp;</td>
							</tr>
						</c:forEach>
   				</table>
            </div>
        </FIELDSET>
        </FIELDSET>
</body>
</html>