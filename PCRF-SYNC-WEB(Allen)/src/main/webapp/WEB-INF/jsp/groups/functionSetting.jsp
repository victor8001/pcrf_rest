<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../common/PageUtil.jsp"%>
<%@ include file="../common/ShowMessage.jsp"%><!--  顯示訊息 -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<c:set var="pageTitle">
    <spring:message code="label.groups.manage" /> - <spring:message code="label.function.setting"/>
</c:set>

<c:set var="functionUrl" value="/groups"/>
<spring:url var="userSettingUrl" value="${functionUrl }/functionSetting.do"/>
<spring:url var="listUrl" value="${functionUrl }/list.do"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="<c:url value="/css/apstyle.css"/>">
<title>Groups Function Setting</title>

<script type="text/javascript">
    $(function () {
        $("#submitBtn").click(function() {
//             if(!validate()) { return false; }
            
            $(':button').prop('disabled', true);
            $('#submitForm').submit();
        });
        
        $("#checkAll").click(function() {
        	if($("#checkAll").prop('checked')) {
	        	$(":checkbox[name='btnIds']").prop('checked', true);
        	} else {
        		$(":checkbox[name='btnIds']").prop('checked', false);
        	}
        });		
    });
    
    function validate() {
        if($(":checkbox:checked[name='btnIds']").length == 0) {
//             alert('<spring:message code="text.user.checked"/>');
//             return false;
        	return window.location.replace('${listUrl}');
        }
        return true;
    }
    
	function clickThisRow(obj,rowNum){
		//var s = $tr.find('td').eq(rowNum+2).text().trim();
		
//		var tr = $(obj).closest('tr'); 
		var id = rowNum+"_btnIds";

		if(obj.checked) {
			$(":checkbox[id='"+id+"']").prop('checked', true);
		} else {
			$(":checkbox[id='"+id+"']").prop('checked', false);
		}

	}
</script>

</head>
<body>
<form:form method="POST" modelAttribute="entity" id="submitForm" action="${userSettingUrl }">
    <input type="hidden" name="roleId" value="${entity.roleId }"/>

    <FIELDSET class="fieldsetBody">
        <table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
            <tr><TD align="left"><h2>${pageTitle}</h2></TD></tr>
        </table>
        <FIELDSET ID=fldCode2 class="fieldsetNormal">
            <div align="left">
                    <table width="500" cellpadding="0" cellspacing="0" class="table_border">
                        <tr>
                            <td width="30%">
                                <div align="left" class="text">
                                    <label style="color: red; font-weight: bolder;">
                                        <spring:message code="label.Asterisk"/>
                                    </label>
                                    <spring:message code="label.groups.name"/>
                                    <spring:message code="label.colon"/>
                                    <label>${entity.roleName }</label>
                                </div>
                            </td>
                        </tr>
                        <tr align="left">
                            <td>
                                <common:button funcId="GROUP_MGMT" btnId="groupfunc">
                                    <form:button id="submitBtn"><spring:message code='btn.save'/></form:button>
                                </common:button>
                                <button type="reset" onclick="window.location.replace('${listUrl}')"><spring:message code="btn.cancel" /></button>
                            </td>
                        </tr>
                    </table>
            </div>
        </FIELDSET>
        <FIELDSET ID=fldCode2 class="fieldsetNormal">
                <div id=qry_set STYLE="width: 100%;">
                    <LEGEND class="txt">
                        <spring:message code="label.groups.function.all.function"/>
                    </LEGEND>
                    <table ID=tblInformation style="WIDTH: 100%; border-collapse: collapse;">
                        <tbody id="tdy">
                            <tr id="hh" height="50px">
                                <td class="FixedTitleColumnCenter" width="10%">
                                    <spring:message code="label.column.index"/>
                                </td>
                                <td class="FixedTitleColumnCenter" width="25%">
                                    <spring:message code="label.groups.function.name"/>
                                </td>
                                <td class="FixedTitleColumnCenter" width="65%">
                                    <label>
                                        <spring:message code="btn.checkbox"/>&nbsp&nbsp 
                                        <input type="checkbox" id="checkAll"/>
                                        <spring:message code="label.select.all"/>
                                    </label>
                                </td>
                            </tr>
                            <c:choose>
                                <c:when test="${not empty funcs }">
                                    <c:forEach var="data" items="${funcs}" varStatus="status">
                                        <tr>
                                            <td class="FixedDataTableRow" align="center">${status.index + 1}</td>
                                            <td class="FixedDataTableRow" align="left">
											
												<input type="checkbox" id="checkThisRow" onclick="clickThisRow(this,'${status.index}');" />
												${data.funcCname }

											</td>
                                            <td class="FixedDataTableRow" align="left">
                                                
												<c:forEach items="${funcBtns}" var="funcBtn">
												    <c:if test="${funcBtn.funcId eq data.funcId }">
												    
		                                                <c:set value="false" var="checkedFlag"/>    <!-- initial flag value -->
												        <c:forEach items="${sysRoleBtns }" var="sysRoleBtn">
													        <c:if test="${sysRoleBtn.btnId eq funcBtn.btnId }">
														        <c:set value="true" var="checkedFlag"/>
													        </c:if>
												        </c:forEach>
												        
		                                                <input type="checkbox" id="${status.index}_btnIds" name="btnIds" value="${funcBtn.btnId }"  ${checkedFlag eq true ? 'checked="checked"' : '' }/>
		                                                &nbsp<label>${funcBtn.btnCname}</label>
		                                                
												    </c:if>
												</c:forEach>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    <tr><td>
                                        <spring:message code="data.not.found" />
                                    </td></tr>
                                </c:otherwise>
                            </c:choose>
                        </tbody>
                    </table>
                </div>
            </FIELDSET>
    </FIELDSET>
</form:form>
</body>
</html>