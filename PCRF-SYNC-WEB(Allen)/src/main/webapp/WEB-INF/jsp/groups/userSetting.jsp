<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../common/PageUtil.jsp"%>
<%@ include file="../common/ShowMessage.jsp"%><!--  顯示訊息 -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<c:set var="pageTitle">
    <spring:message code="label.groups.manage" /> - <spring:message code="label.user.setting"/>
</c:set>

<c:set var="functionUrl" value="/groups"/>
<spring:url var="userSettingUrl" value="${functionUrl }/userSetting.do"/>
<spring:url var="listUrl" value="${functionUrl }/list.do"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="<c:url value="/css/apstyle.css"/>">
<title>Groups User Setting</title>

<script type="text/javascript">
    $(function () {
        $("#submitBtn").click(function() {
//             if(!validate()) { return false; }
            
            $(':button').prop('disabled', true);
            $('#submitForm').submit();
        });
        
        $("#checkAll").click(function() {
        	if($("#checkAll").prop('checked')) {
	        	$(":checkbox[name='userId']").prop('checked', true);
        	} else {
        		$(":checkbox[name='userId']").prop('checked', false);
        	}
        });
    });
    
    function validate() {
        if($(":checkbox:checked[name='userId']").length == 0) {
//             alert('<spring:message code="text.user.checked"/>');
//             return false;
        	return window.location.replace('${listUrl}');
        }
        return true;
    }
    
</script>

</head>
<body>
<form:form method="POST" modelAttribute="entity" id="submitForm" action="${userSettingUrl }">
    <input type="hidden" name="roleId" value="${SysRoleEntity.roleId }"/>

    <FIELDSET class="fieldsetBody">
        <table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
            <tr><TD align="left"><h2>${pageTitle}</h2></TD></tr>
        </table>
        <FIELDSET ID=fldCode2 class="fieldsetNormal">
            <div align="left">
                    <table width="500" cellpadding="0" cellspacing="0" class="table_border">
                        <tr>
                            <td width="30%">
                                <div align="left" class="text">
                                    <label style="color: red; font-weight: bolder;">
                                        <spring:message code="label.Asterisk"/>
                                    </label>
                                    <spring:message code="label.groups.name"/>
                                    <spring:message code="label.colon"/>
                                    <label>${SysRoleEntity.roleName }</label>
                                </div>
                            </td>
                        </tr>
                        <tr align="left">
                            <td>
                                <common:button funcId="GROUP_MGMT" btnId="groupuser">
                                    <form:button id="submitBtn"><spring:message code='btn.save'/></form:button>
                                </common:button>
                                <button type="reset" onclick="window.location.replace('${listUrl}')"><spring:message code="btn.cancel" /></button>
                            </td>
                        </tr>
                    </table>
            </div>
        </FIELDSET>
        <FIELDSET ID=fldCode2 class="fieldsetNormal">
                <div id=qry_set STYLE="width: 100%;">
                    <LEGEND class="txt">
                        <spring:message code="label.groups.user.all.user" />
                    </LEGEND>
                    <table ID=tblInformation style="WIDTH: 100%; border-collapse: collapse;">
                        <tbody id="tdy">
                            <tr id="hh" height="50px">
                                <td class="FixedTitleColumnCenter" width="10%"><spring:message code="label.column.index"/></td>
                                <td class="FixedTitleColumnCenter" width="10%">
                                    <label>
                                        <spring:message code="btn.checkbox"/><br/>
                                        <input type="checkbox" id="checkAll"/>&nbsp
                                        <spring:message code="label.select.all"/>
                                    </label>
                                </td>
                                <td class="FixedTitleColumnCenter" width="80%"><spring:message code="label.groups.name"/></td>
                            </tr>
                            <c:choose>
                                <c:when test="${not empty entity }">
                                    <c:forEach var="data" items="${entity}" varStatus="status">
                                        <tr>
                                            <td class="FixedDataTableRow" align="center">${status.index + 1}</td>
                                            <td class="FixedDataTableRow" align="left">
                                                
                                                <c:set value="false" var="checkedFlag"/>    <!-- initial flag value -->
												<c:forEach items="${userIdChecked}" var="checkedUser">
												    <c:if test="${checkedUser eq data.user_id }">
												        <c:set value="true" var="checkedFlag"/>
												    </c:if>
												</c:forEach>
                                                <input type="checkbox" name="userId" value="${data.user_id }" ${checkedFlag eq true ? 'checked="checked"' : '' } />
                                                &nbsp<label>${data.account}</label>
                                            </td>
                                            <td class="FixedDataTableRow" align="center">${SysRoleEntity.roleName }</td>
                                        </tr>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    <tr><td><spring:message code="data.not.found" /></td></tr>
                                </c:otherwise>
                            </c:choose>
                        </tbody>
                    </table>
                </div>
            </FIELDSET>
    </FIELDSET>
</form:form>
</body>
</html>