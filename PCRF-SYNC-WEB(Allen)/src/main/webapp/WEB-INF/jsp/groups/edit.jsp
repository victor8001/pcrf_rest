<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../common/PageUtil.jsp"%>
<%@ include file="../common/ShowMessage.jsp"%><!--  顯示訊息 -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:set var="pageTitle">
    <spring:message code="label.groups.manage" /> - <spring:message code="title.edit"/>
</c:set>

<c:set var="functionUrl" value="/groups"/>
<spring:url var="editUrl" value="${functionUrl }/{0}/edit.do"/>
<spring:url var="listUrl" value="${functionUrl }/list.do"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="<c:url value="/css/apstyle.css"/>">
<title>New Groups</title>

<script type="text/javascript">
    $(function () {
        $("#submitBtn").click(function() {
            if(!validate()) { return false; }
            
            // trim text
            $(':password').val(function ()  {  return trim($(this).val());  });
            $(':input').val(function ()  {  return trim($(this).val());  });
            
            $(':button').prop('disabled', true);
            $('#editForm').submit();
        });
        
        $("#roleName").blur(function(){
            hideSpringValidator("roleName");
            var validateRoleNameResult = validateRoleName();
            if(validateRoleNameResult.trim().length != 0) {
                $("#roleNameErrorMsg").text(validateRoleNameResult);
            } else {
                $("#roleNameErrorMsg, #roleName.errors").text('');
            }
        });
        
    });
    
    function validate() {
        var validateRoleNameResult = validateRoleName();
        
        if(validateRoleNameResult.trim().length != 0) {
            alert(validateRoleNameResult);
            return false;
        }
        return true;
    }
    
    //validate RoleName
    function validateRoleName() {
        var errorMsg = "";
        if($("#roleName").val() == '') {
            errorMsg = '<spring:message code="NotEmpty.entity.roleName"/>';
        }
        return errorMsg;
    }
    
    function hideSpringValidator(errorFields) {
        if($("#" + errorFields + "\\.errors").length > 0) {
            $("#" + errorFields + "\\.errors").hide();
        }
    }
</script>

</head>
<body>
    <FIELDSET class="fieldsetBody">
        <table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
            <tr><TD align="left"><h2>${pageTitle}</h2></TD></tr>
        </table>
        <FIELDSET ID=fldCode2 class="fieldsetNormal">
            <div align="left">
                <form:form method="POST" modelAttribute="entity" id="editForm" action="${fn:replace(editUrl, '{0}', entity.roleId)}">
					<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#d0d0d0">
                        <tr>
                            <td class="topTableRowLeft" width="15%">
                                <div align="right" class="text">
                                    <label style="color: red; font-weight: bolder;">
                                        <spring:message code="label.Asterisk"/>
                                    </label>
                                    <spring:message code="label.groups.name" /><spring:message code="label.colon" />
                                </div>
                            </td>
                            <td class="topTableRowRight">
                                <form:input path="roleName" id="roleName"/>
								<label id="roleNameErrorMsg" class="errorMsgStyle" style="color: red;"></label>
                                <form:errors path="roleName" cssClass="errorMsgStyle" element="label"></form:errors>
                            </td>
                        </tr>
                    </table>
					
					<table width="100%">
                        <tr align="center">
                            <td colspan="3">
                                <common:button funcId="GROUP_MGMT" btnId="edit">
                                    <form:button id="submitBtn"><spring:message code='btn.save'/></form:button>
                                </common:button>
                                <button type="reset" onclick="window.location.replace('${listUrl}')"><spring:message code="btn.cancel" /></button>
                            </td>
                        </tr>
                    </table>

                </form:form>
            </div>
        </FIELDSET>
    </FIELDSET>
</body>
</html>