<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/PageUtil.jsp"%>
<%@ include file="../common/ShowMessage.jsp"%><!--  顯示訊息 -->
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:set var="pageTitle">
    <spring:message code="label.groups.manage" />
</c:set>
<c:set var="functionUrl" value="/groups"/>
<c:set var="results" value="${entity }"/>

<spring:url var="listUrl" value="${functionUrl }/list.do"/>
<spring:url var="addUrl" value="${functionUrl }/new.do"/>
<spring:url var="editUrl" value="${functionUrl }/{0}/edit.do"/>
<spring:url var="deleteUrl" value="${functionUrl }/{0}/delete.do"/>
<spring:url var="userSettingUrl" value="${functionUrl }/{0}/userSetting.do"/>
<spring:url var="functionSettingUrl" value="${functionUrl }/{0}/functionSetting.do"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Groups management</title>

<link type="text/css" rel="stylesheet" href="<c:url value="/css/apstyle.css"/>">

<script type="text/javascript">
$(function() {
	
	$("#roleName").on("keyup", function(){
		  this.value = this.value.toUpperCase();
		});
	
	$('#submitBtn').click(function() {
		var editorInputs = $(':text, textarea, input:hidden');

        editorInputs.val(function () {  return trim($(this).val());  });
        
        $(':button').prop('disabled', true);
        $('#queryForm').submit();
	});
	
	$('#newGroups').click(function() {
		$('#queryForm').attr('action', '${addUrl }').attr('method', 'GET').submit();
	});
});

function deleteConfirm() {
    if(!confirm('<spring:message code="text.user.isDelete"/>')) { 
        return false;
    }
}
</script>

</head>
<body>
<form:form id="queryForm" method="POST" action="${listUrl }">
    <FIELDSET class="fieldsetBody">
        <table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
            <tr>
                <td align="left">
                    <h2>${pageTitle}</h2>
                </td>
            </tr>
        </table>
        <FIELDSET ID=fldCode2 class="fieldsetNormal">
            <div class="divPage">
                <!-- 加入分頁模組 -->
                <jsp:include page="../common/QueryPage.jsp" />
            </div>
            <FIELDSET ID=fldCode class="fieldsetNormal">
                <div id="search_block">
                    <LEGEND class="txt"><spring:message code="title.query.condition"/></LEGEND>
	                <table>
	                    <tr>
	                        <td>
	                            <label>
	                                <spring:message code="label.groups.name"/>
	                                <spring:message code="label.colon"/>
	                            </label>
	                            <input type="text" id="roleName" name="roleName" value="${roleName }"/>
	                        </td>
	                    </tr>
	                    <tr align="center">
		                    <td>
			                    <common:button funcId="GROUP_MGMT" btnId="query">
			                        <input type="button" id="submitBtn" name="submitBtn" value="<spring:message code='btn.submit'/>"/>
			                    </common:button>
			                    <common:button funcId="GROUP_MGMT" btnId="add">
			                        <input type="button" id="newGroups" name="newGroups" value="<spring:message code='btn.new.groups'/>"/>
			                    </common:button>
		                    </td>
	                    </tr>
	                </table>
                </div>
            </FIELDSET>
            <FIELDSET ID=fldCode2 class="fieldsetNormal">
	            <div id=qry_set STYLE="width: 100%;">
	                <LEGEND class="txt"><spring:message code="title.query.result" /></LEGEND>
	                <table ID=tblInformation style="WIDTH: 100%" border="0"
	                    cellspacing="0" cellpadding="2" bordercolor="#FFFFFF">
	                    <tbody id="tdy">
	                        <tr id="hh" height="50px">
	                            <td class="FixedTitleColumnCenter"><spring:message code="label.column.index"/></td>
	                            <td class="FixedTitleColumnCenter"><spring:message code="label.groups.name"/></td>
	                            <td class="FixedTitleColumnCenter"><spring:message code="label.modify.user"/></td>
	                            <td class="FixedTitleColumnCenter"><spring:message code="label.modify.time"/></td>
	                            <td class="FixedTitleColumnCenter"><spring:message code="label.operate"/></td>
	                        </tr>
	                        <c:choose>
	                            <c:when test="${not empty results }">
	                                <c:forEach var="data" items="${results}" varStatus="status">
	                                    <tr>
	                                        <td class="FixedDataTableRowCenter">${status.index + 1}</td>
	                                        <td class="FixedDataTableRow" align="center">${data.roleName}</td>
	                                        <td class="FixedDataTableRow" align="center">${data.modifyUserName}</td>
	                                        <td class="FixedDataTableRow" align="center">${data.modifyTime}</td>
	                                        <td class="FixedDataTableRow" align="center">
		                                        <common:button funcId="GROUP_MGMT" btnId="edit"> 
		                                            <a href="${fn:replace(editUrl, '{0}', data.roleId)}" target="mainFrame"><spring:message code='btn.edit'/></a>
		                                        </common:button>
		                                        <common:button funcId="GROUP_MGMT" btnId="delete">
	                                                <a href="${fn:replace(deleteUrl, '{0}', data.roleId)}" onclick="return deleteConfirm();"><spring:message code='btn.delete'/></a>
	                                            </common:button>
	                                            <common:button funcId="GROUP_MGMT" btnId="groupfunc">
		                                            <a href="${fn:replace(functionSettingUrl, '{0}', data.roleId)}"><spring:message code="label.function.setting"/></a>
	                                            </common:button>
	                                            <common:button funcId="GROUP_MGMT" btnId="groupuser">
		                                            <a href="${fn:replace(userSettingUrl, '{0}', data.roleId)}"><spring:message code="label.user.setting"/></a>
	                                            </common:button>
	                                        </td>
	                                    </tr>
	                                </c:forEach>
	                            </c:when>
	                            <c:otherwise>
	                                <tr>
	                                   <td><spring:message code="data.not.found"/></td>
	                                </tr>
	                            </c:otherwise>
	                        </c:choose>
	                    </tbody>
	                </table>
	            </div>
            </FIELDSET>
        </FIELDSET>
    </FIELDSET>
</form:form>
</body>
</html>