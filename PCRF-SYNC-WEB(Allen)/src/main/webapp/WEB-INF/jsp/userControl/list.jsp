<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../common/PageUtil.jsp"%>
<!--  顯示訊息 -->
<%@ include file="../common/ShowMessage.jsp"%>

<c:set var="pageTitle">
    <spring:message code="text.UserControl"></spring:message>
</c:set>
<c:set var="results" value="${USER_RS}"/>
<c:set var="functionUrl" value="/user"/>

<spring:url var="deleteAccountUrl" value="${functionUrl }/{0}/delete.do"/>
<spring:url var="editPageUrl" value="${functionUrl }/{0}/edit.do"/>
<spring:url var="blockUrl" value="${functionUrl }/{0}/block_reset.do"/>

<HTML>
    <HEAD>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link type="text/css" rel="stylesheet" href="<c:url value="/css/apstyle.css"/>">

        <script type="text/javascript">
            function deleteConfirm() {
                if(!confirm('<spring:message code="text.user.isDelete"/>')) {
                    return false;
                }
            }
        </script>

    </HEAD>

    <BODY>

        <form:form id="frm001" name="frm001" method="POST" >
            <FIELDSET class="fieldsetBody">
                <table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
                    <tr>
                        <TD align="left"><h2>${pageTitle}</h2></TD>
                    </tr>
                </table>
                <c:if test="${results != null}">
                 <FIELDSET ID=fldCode2 class="fieldsetNormal">
                <!--
                    <LEGEND class="txt">查詢結果</LEGEND>
                    -->
                    <div class="divPage">
                        
                        <!-- 加入分頁模組 -->
                        <jsp:include page="../common/QueryPage.jsp"/>
                    </div>
                    <c:if test="${fn:length(results) > 0}">
                    <TABLE border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" width="100%">
                        <TR>
                            <TD>
                                <DIV id=qry_set STYLE=" width:100%;OVERFLOW-X:auto;HEIGHT:290PX;OVERFLOW-Y:auto;" >
                                <TABLE ID=tblInformation style="WIDTH:100%" border="0" cellspacing="0" cellpadding="2" bordercolor="#FFFFFF">
                                
                                <tbody id="tdy">
                                    <TR id ="hh" height="50px">
                                        <td class="FixedTitleColumnCenter">
                                            <spring:message code="label.user.account"></spring:message>
                                        </td>
                                        <td class="FixedTitleColumnCenter"><spring:message code="label.modify.time"/></td>
                                        <td class="FixedTitleColumnCenter"><spring:message code="label.operate"/></td>
                                    </tr>
                                    <c:forEach var="data" items="${results}" varStatus="status">
                                        <tr>
                                            <td class="FixedDataTableRowCenter">${data.account}</td>
                                            <td class="FixedDataTableRow" align="center">${data.modifyTime}</td>
                                            <td class="FixedDataTableRow" align="center">
                                                <common:button funcId="USER_MGMT" btnId="edit">
                                                    <a href="${fn:replace(editPageUrl, '{0}', data.account)}" target="mainFrame"><spring:message code='btn.edit'/></a>
                                                </common:button>
                                                <common:button funcId="USER_MGMT" btnId="delete">
                                                    <a href="${fn:replace(deleteAccountUrl, '{0}', data.account)}" onclick="return deleteConfirm();"><spring:message code='btn.delete'/></a>
                                                </common:button>
                                                <common:button funcId="USER_MGMT" btnId="block">
	                                                <c:if test="${data.blocked == 1}">
	                                                    <a href="${fn:replace(blockUrl, '{0}', data.account)}"><spring:message code='btn.unblock'/></a>
	                                                </c:if>
                                                </common:button>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                                </TABLE>
                                </DIV>
                            </TD>
                        </TR>
                    </TABLE>
                    </c:if>
                    <c:if test="${fn:length(results) == 0}">
                        <TABLE border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" width="100%">
                            <TR>
                                <TD><spring:message code="data.not.found"/></TD>
                            </TR>
                        </TABLE>
                    </c:if>
                    <input type="hidden" id="h_clickCount" name="h_clickCount" value="">
                 </FIELDSET>
            </c:if>
            </FIELDSET>
        </form:form>
        
        
        <!-- 存儲 -->
        <common:button funcId="USER_MGMT" btnId="add">
            <a href="<c:url value="/user/add_before.do?isNewQuery=Y"/>" target="mainFrame"><spring:message code='btn.new.user'/></a>
        </common:button>
    </body>
</html>
