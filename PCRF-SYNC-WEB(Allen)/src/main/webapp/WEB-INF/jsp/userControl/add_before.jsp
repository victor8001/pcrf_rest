<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../common/PageUtil.jsp"%>

<!--  顯示訊息 -->
<%@ include file="../common/ShowMessage.jsp"%>

<c:set var="pageTitle">
    <spring:message code="text.UserControl"></spring:message> - <spring:message code="title.add"/>
</c:set>
<c:set var="results" value="${USER_RS}" />
<c:set var="roleResults" value="${ROLE_RS}"/>
<c:set var="selectUserRoles" value="${USER_ROLES_MAP}"/>
<c:set var="functionUrl" value="/user" />

<spring:url var="editUrl" value="${functionUrl }/{0}/edit.do"/>
<spring:url var="listUrl" value="${functionUrl }/list.do"/>

<script type="text/javascript">
    function imgLogin_onclick() {
        // 非空檢核
        if ($("#userID").val() == '' || $("#password").val() == '') {
            alert("<spring:message code='error.account.passwd.empty'/>");
            return false;
        }

        if (null == document.addUser.userID.value.match(/^[A-Za-z0-9]+$/) //|| null==document.addUser.password.value.match(/^[A-Za-z0-9]+$/)
        ) {
            alert("<spring:message code='error.account.input.rule'/>");
        } else {
            var isExistUser = false;

            <c:forEach var="data" items="${results}" varStatus="status">
            if ("${data.account}" == document.addUser.userID.value)
                isExistUser = true;
            </c:forEach>

            if (isExistUser)
                alert("<spring:message code='error.account.duplicate'/>");
            else
                document.addUser.submit();

        }

        /*
        document.addUser.submit();
         */
    }
</script>


<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet"
    href="<c:url value="/css/apstyle.css"/>">

<script>
    
</script>
</HEAD>

<BODY>

    <form id="addUser" name="addUser" method="post"
        action="<c:url value="/user/add.do"/>">
        <FIELDSET class="fieldsetBody">
            <table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
                <tr>
                    <TD align="left"><h2>${pageTitle}</h2></TD>
                </tr>
            </table>
            <FIELDSET ID=fldCode2 class="fieldsetNormal">
                <div align="left">
					<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#d0d0d0">
                        <tr>
                            <td class="topTableRowLeft" width="15%">
                                <div align="right" class="text">
	                                <spring:message code="label.user.name"/>
	                                <spring:message code="label.colon"/>
                                </div>
                            </td>
                            <td class="topTableRowRight">
                                <label> 
                                    <input type="text" id="userID" name="userID" size="26" style="ime-mode: disabled;" value="${userID}" onblur="this.value = this.value.toLowerCase();"/>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td class="topTableRowLeft" width="15%">
                                <div align="right" class="text">
	                                <spring:message code="label.password"/>
	                                <spring:message code="label.colon"/>
                                </div>
                            </td>
                            <td class="topTableRowRight"><label> <input type="password" id="password"
                                    name="password" size="27" style="ime-mode: disabled;"
                                    onKeyPress="if(event.keyCode == 13){return imgLogin_onclick();}"
                                    value="">
                            </label></td>
                        </tr>
                        <tr>
                            <td class="topTableRowLeft" width="15%">
                                <div align="right" class="text">
	                                <spring:message code="label.user.groups"/>
	                                <spring:message code="label.colon"/>
                                </div>
                            </td>
                            <td class="topTableRowRight">
				                <c:if test="${roleResults != null}">
                                    <c:forEach var="data" items="${roleResults}" varStatus="status">
										<c:set value="false" var="checkedFlag"/>
										
										<c:if test="${selectUserRoles.get(data.roleId) != null}">
											<c:set value="true" var="checkedFlag"/>
										</c:if>

										<input type="checkbox" name="roleIds" value="${data.roleId}" ${checkedFlag eq true ? 'checked="checked"' : '' } />
										&nbsp<label>${data.roleName}</label>

									</c:forEach>
								</c:if>
							
							</td>
                        </tr>
                    </table>
					<table width="100%">
                        <tr align="center">
                            <td colspan="2">
                                <common:button funcId="USER_MGMT" btnId="add">
                                    <input name="button" type="button" onclick="imgLogin_onclick();" value="<spring:message code='btn.save'/>">
                                </common:button>
                                <button type="reset" onclick="window.location.replace('${listUrl}')"><spring:message code="btn.cancel" /></button>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--                 <input type="hidden" id="h_clickCount" name="h_clickCount" value=""> -->
            </FIELDSET>
        </FIELDSET>
    </form>

</body>
</html>
