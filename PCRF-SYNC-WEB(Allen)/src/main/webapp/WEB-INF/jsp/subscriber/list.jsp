<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/PageUtil.jsp"%>
<%@ include file="../common/ShowMessage.jsp"%><!--  顯示訊息 -->
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:set var="pageTitle">
    <spring:message code="text.Subscriber"></spring:message>
</c:set>

<c:set var="functionUrl" value="/subscriber"/>
<c:set var="results" value="${entity }"/>

<spring:url var="listUrl" value="${functionUrl }/list.do"/>
<spring:url var="expUrl" value="${functionUrl }/exp.do"/>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Subscriber management</title>

<link type="text/css" rel="stylesheet" href="<c:url value="/css/apstyle.css"/>">

<script type="text/javascript">
$(function() {
	var message = "${message}";
	
	if(message != ""){
		alert(message);
	}
	
	$('#submitBtn').click(function() {
		var editorInputs = $(':text, textarea, input:hidden');

        editorInputs.val(function () {  return trim($(this).val());  });
        
        $(':button').prop('disabled', true);
        $('#queryForm').submit();
	});
});
</script>

</head>
<body>

    <FIELDSET class="fieldsetBody">
        <table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
            <tr>
                <td align="left"><h2>${pageTitle}</h2></td>
            </tr>
        </table>
        <FIELDSET ID=fldCode2 class="fieldsetNormal">
          
            <FIELDSET ID=fldCode class="fieldsetNormal">
            <form:form id="queryForm" method="POST" action="${listUrl }">
                <div id="search_block">
                    <LEGEND class="txt"><spring:message code="title.query.condition"/></LEGEND>
	                <table>
	                	<tr>
	                	<td>
	                    	<label>Node Location:</label>&nbsp;&nbsp;&nbsp;
	                    		<select name="node_location" >
	                				<c:forEach var="commons" items="${commons }">
	                    				<option value="${commons.valueType}" <c:if test="${OriginalLocation == commons.valueType}">selected</c:if>>${commons.valueName}</option>
	                    			</c:forEach>
	                    		</select>
	                    </td>
	                	</tr>
	                    <tr>
	                        <td>
	                            <label>*門號:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                            <input type="text" id="msisdn" name="msisdn" value="${msisdn }"/>
	                        </td>
	                    </tr>
	                    <tr align="center">
		                    <td>
		                        <common:button funcId="SUBSCRIBER_MGMT" btnId="query">
		                            <input type="button" id="submitBtn" name="submitBtn" value="<spring:message code='btn.submit'/>"/>
		                        </common:button>
		                    </td>
	                    </tr>
	                </table>
                </div>
                </form:form>
            </FIELDSET>
            <FIELDSET ID=fldCode2 class="fieldsetNormal">
	            <div id=qry_set STYLE="width: 100%;">
	                <LEGEND class="txt"><spring:message code="title.query.result"/></LEGEND>
	                <table ID=tblInformation style="WIDTH: 100%" border="0"
	                    cellspacing="0" cellpadding="2" bordercolor="#FFFFFF">
	                    <tbody id="tdy">
	                        <tr id="hh" height="50px">
	                            <td class="FixedTitleColumnCenter" width="15%">Node Location</td>
	                            <td class="FixedTitleColumnCenter" width="15%">門號</td>
	                            <td class="FixedTitleColumnCenter" width="55%">內容</td>
	                            <td class="FixedTitleColumnCenter" width="15%">功能</td>
	                        </tr>
	                        <c:choose>
	                            <c:when test="${not empty results }">
	                                <c:forEach var="data" items="${results}" varStatus="status">
	                                    <tr>
	                                    	<td class="FixedDataTableRow" align="center">${data.nodeLocation}</td>
	                                        <td class="FixedDataTableRow" align="center">${data.msisdn}</td>
	                                        <td class="FixedDataTableRow" align="center">${data.info}</td>
	                                        <td class="FixedDataTableRow" align="center">
	                                            <common:button funcId="SUBSCRIBER_MGMT" btnId="edit">
	                                                <form action="${expUrl}" method="POST"  >
	                                                	<input type="hidden" name="OriginalLocation" value="${OriginalLocation}">
				                                    	<input type="hidden" name="location" value="${data.nodeLocation}">
				                                    	<input type="hidden" name="type" value="3">
				                                    	<input type="hidden" name="msisdn" value="${data.msisdn}">
				                                    	<input type="hidden" name="info" value="${data.info}"> 
				                                    	<input type="hidden" name="col1_1" value="${data.col1_1}">
				                                    	<input type="hidden" name="col11_1" value="${data.col11_1}">
				                                    	<input type="submit" value="<spring:message code='btn.export.pcrf'/>" class="exp"> 
				                                    </form>
	                                            </common:button>
	                                        </td>
	                                    </tr>
	                                </c:forEach>
	                            </c:when>
	                        </c:choose>
	                        <c:choose>
	                            <c:when test="${not empty resultKH}">
	                                <c:forEach var="data" items="${resultKH}" varStatus="status">
	                                    <tr>
	                                    	<td class="FixedDataTableRow" align="center">${data.nodeLocation}</td>
	                                        <td class="FixedDataTableRow" align="center">${data.msisdn}</td>
	                                        <td class="FixedDataTableRow" align="center">${data.info}</td>
	                                        <td class="FixedDataTableRow" align="center">
	                                            <common:button funcId="SUBSCRIBER_MGMT" btnId="edit">
	                                               <form action="${expUrl}" method="POST"  >
	                                               		<input type="hidden" name="OriginalLocation" value="${OriginalLocation}">
				                                    	<input type="hidden" name="location" value="${data.nodeLocation}">
				                                    	<input type="hidden" name="type" value="3">
				                                    	<input type="hidden" name="msisdn" value="${data.msisdn}">
				                                    	<input type="hidden" name="info" value="${data.info}"> 
				                                    	<input type="hidden" name="col1_1" value="${data.col1_1}">
				                                    	<input type="hidden" name="col11_1" value="${data.col11_1}">
				                                    	<input type="submit" value="<spring:message code='btn.export.pcrf'/>" class="exp">
				                                    </form>
	                                            </common:button>
	                                        </td>
	                                    </tr>
	                                </c:forEach>
	                            </c:when>
	                        </c:choose>
	                          <c:choose>
	                            <c:when test="${not empty resultTP}">
	                                <c:forEach var="data" items="${resultTP}" varStatus="status">
	                                    <tr>
	                                    	<td class="FixedDataTableRow" align="center">${data.nodeLocation}</td>
	                                        <td class="FixedDataTableRow" align="center">${data.msisdn}</td>
	                                        <td class="FixedDataTableRow" align="center">${data.info}</td>
	                                        <td class="FixedDataTableRow" align="center">
	                                            <common:button funcId="SUBSCRIBER_MGMT" btnId="edit">
	                                                <form action="${expUrl}" method="POST"  >
	                                                	<input type="hidden" name="OriginalLocation" value="${OriginalLocation}">
				                                    	<input type="hidden" name="location" value="${data.nodeLocation}">
				                                    	<input type="hidden" name="type" value="3">
				                                    	<input type="hidden" name="msisdn" value="${data.msisdn}">
				                                    	<input type="hidden" name="info" value="${data.info}">
				                                    	<input type="hidden" name="col1_1" value="${data.col1_1}">
				                                    	<input type="hidden" name="col11_1" value="${data.col11_1}">
				                                    	<input type="submit" value="<spring:message code='btn.export.pcrf'/>" class="exp"> 
				                                    </form>
	                                            </common:button>
	                                        </td>
	                                    </tr>
	                                </c:forEach>
	                            </c:when>
	                        </c:choose>
	                        <c:if test="${empty resultKH && empty resultTP && empty results}">
	                                <tr><td><spring:message code="data.not.found"/></td></tr>
	                        </c:if>
	                    </tbody>
	                </table>
	            </div>
            </FIELDSET>
        </FIELDSET>
    </FIELDSET>

</body>
</html>