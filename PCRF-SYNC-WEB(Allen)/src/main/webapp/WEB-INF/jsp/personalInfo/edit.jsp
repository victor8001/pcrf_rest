<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="../common/PageUtil.jsp"%>

<!--  顯示訊息 -->
<%@ include file="../common/ShowMessage.jsp"%>

<c:set var="functionUrl" value="/personalInfo" />
<spring:url var="editUrl" value="${functionUrl }/{0}/edit.do"/>

<script type="text/javascript">
    $(function() {
        $("#submitBtn").click(function(event) {
        	if(!validate()) { return false; }
        	
            event.preventDefault();
    
            $(':password').val(function ()  {  return trim($(this).val());  });
            $(':button').prop('disabled', true);
            $('#editUser').submit();
        });
        
    });
    
    function validate() {
    	var result = true;
        //validate password
        if($("#password").val() == '') {
            alert("<spring:message code='error.account.passwd.empty'/>");
            result = false;
        }
        return result;
    }
</script>

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet"
	href="<c:url value="/css/apstyle.css"/>">
</HEAD>

<BODY>
<FIELDSET class="fieldsetBody">
	<table class="titleStyle" border="0" cellpadding="3" cellspacing="1">
		<tr>
			<TD align="left"><h2><spring:message code="title.edit"/></h2></TD>
		</tr>
	</table>
	<FIELDSET ID=fldCode2 class="fieldsetNormal">
		<div align="left">
		    <form:form id="editUser" name="editUser" method="POST" modelAttribute="entity" action="${fn:replace(editUrl, '{0}', entity.account)}">
				<table width="100%" border="0" cellpadding="3" cellspacing="1" bordercolor="#FFFFFF" bgcolor="#d0d0d0">
                    <tr>
                        <td class="topTableRowLeft" width="15%">
							<div align="right" class="text">
								<spring:message code="label.user.name"/>
								<spring:message code="label.colon"/>
							</div>
						</td>
                        <td class="topTableRowRight"><label>${entity.account }</label></td>
					</tr>
					<tr>
                        <td class="topTableRowLeft" width="15%">
							<div align="right" class="text">
								<spring:message code="label.password"/>
								<spring:message code="label.colon"/>
							</div>
						</td>
                        <td class="topTableRowRight">
						    <label>
								<form:password path="password" id="password" cssStyle="ime-mode: disabled;"
								    size="26" maxlength="13"/>
						    </label>
						</td>
					</tr>
                </table>
                <table width="100%">
                    <tr align="center">
						<td colspan="2">
								<br>
<!-- 						    common:button funcId="PERSON_MANAGE" btnId="edit" -->
						        <input type="button" id="submitBtn" name="submitBtn" value="<spring:message code='btn.edit'/>"/>
<!-- 						    /common:button -->
						</td>
					</tr>
                </table>
			</form:form>
		</div>
	</FIELDSET>
</FIELDSET>
</body>
</html>
