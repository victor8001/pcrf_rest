package com.cht.pcrf.json;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubDataPlan {
    private String dataplanName;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer priority;
    public String getDataplanName() {
        return dataplanName;
    }
    public void setDataplanName(String dataplanName) {
        this.dataplanName = dataplanName;
    }
    public Integer getPriority() {
        return priority;
    }
    public void setPriority(int priority) {
        this.priority = priority;
    }
    @Override
    public String toString() {
        return "SubDataPlan [dataplanName=" + dataplanName + ", priority="
                + priority + "]";
    }
    
}

