
package com.cht.pcrf.common;

/**
 * <code>Constants</code>: 常數類別
 * @version $Id$
 */
public class Constants {
    public static String SERVER_NAME = "";
	public static final String OS_BR = "\n";
//	public static final String OS_DB_BR1 = "\r\n";
//	public static final String OS_DB_BR2 = "\r";
//	public static final String OS_DB_BR3 = "\n";
	public static final String OS_DB_BR_RPA = "_SPLITE_CRLF_";
    public static final String SPLIT_KEY = ":";
	public static final String MENU_DATA = "menuData";
    public static final String FUNC_BTN_DATA = "funcBtnData";
	public static final String USER_DATA = "userData";
    public static final String USER_AUTHED_FUNC = "funcData";
    public static final String USER_AUTHED_BTN = "userAuthBtn";
    public static final String MESSAGE_CODE = "messageCode";
    public static final String MESSAGE_PARAMS = "messageParams";
    public static final String USER_ID = "USER_ID";
    public static final String USER_ACCOUNT = "USER_ACCOUNT";

	public static final String MESSAGE = "message";
    public static final String SYS_EXCEPTION_MESSAGE = "sysException";
	public static final String ERROR_PAGE = "/common/errorPage";


    public static final String LIST_PAGE = "LIST_PAGE";
    public static final String VIEW_PAGE = "VIEW_PAGE";
    public static final String ADD_PAGE = "ADD_PAGE";
    public static final String ADD_BEFORE_PAGE = "ADD_BEFORE_PAGE";
    public static final String EDIT_PAGE = "EDIT_PAGE";
    public static final String EDIT_BEFORE_PAGE = "EDIT_BEFORE_PAGE";
    public static final String DELETE_PAGE = "DELETE_PAGE";

    public static final String PAGES = "PAGES";
    public static final String SUBCRIBER_PAGE = "SUB_PAGE";

    public static final String LOGGER_PCRF_WEB = "pcrf-web";

    public static final String SUBSCRIBER_FILL_PATH = "pcrf.subscriber.fill.path";
    
    public static final String OS_NAME = "os.name";
    
    public static final String DATA_SYNC_SHELL = "data.sync.shell";
    
    public static final String DATA_CONSISTENT_SHELL = "data.consistent.shell";
    
    public static final String DATA_CONSISTENT_AUTO_SHELL = "data.consistent.auto.shell";
    
    public static final String DATA_CONSISTENT_SS_MM = "data.consistent.ssmm";
    

    /** 屬性名稱: 物件的內部識別碼 - {@value}。 */
    public static final String ATTRIBUTE_ID = "id";

    /** 頁面: SpringFramework 之頁面重導向前置詞 - {@value}。 */
    public static final String PAGE_REDIRECT_PREFIX = "redirect:";

    /** 網址: 查詢功能 - {@value}。 */
    public static final String URL_LIST = "/list";

    /** 網址: 新增功能 - {@value}。 */
    public static final String URL_NEW = "/new";

    /** 網址: 刪除功能 - {@value}。 */
    public static final String URL_DELETE = "/delete";

    /** 網址: 編輯功能 - {@value}。 */
    public static final String URL_EDIT = "/edit";
    
    /** 網址: 檢視功能 - {@value}。 */
    public static final String URL_VIEW = "/view";

    /** 網址: 刪除物件功能 - {@value}。 */
    public static final String URL_ENTITY_DELETE = "/{" + ATTRIBUTE_ID + "}" + URL_DELETE;

    /** 網址: 編輯物件功能 - {@value}。 */
    public static final String URL_ENTITY_EDIT = "/{" + ATTRIBUTE_ID + "}" + URL_EDIT;

    /** 屬性名稱: 查詢條件 - {@value}。 */
    public static final String ATTRIBUTE_QUERY_CRITERIA = "queryCriteria";

    /** 屬性名稱: 實體/物件 - {@value}。 */
    public static final String ATTRIBUTE_ENTITY = "entity";

    public static String getSERVER_NAME() {
        return SERVER_NAME;
    }
    public static void setSERVER_NAME(String sERVER_NAME) {
        SERVER_NAME = sERVER_NAME;
    }
}
