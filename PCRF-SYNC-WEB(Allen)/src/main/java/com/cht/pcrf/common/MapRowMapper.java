/**
 * Project Name : PG-WEB
 * File Name 	: MapRowMapper.java
 * Package Name : com.fet.pg.common
 * Date 		: 2015年11月29日 
 * Author 		: LauraChu
 * Copyright (c) 2015 All Rights Reserved.
 */
package com.cht.pcrf.common;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;

public class MapRowMapper extends BasicRowMapper<Map> {

    /**
     * <pre>
     * Method Name : mapColumnValue
     * Description : 依欄位名稱填入Form中
     * </pre>
     * 
     * @param rs
     *            ResultSet
     * @return BasicForm
     * @throws SQLException
     *             SQLException
     */
    @Override
    public Map mapColumnValue(ResultSet rs) throws SQLException {

        String columnName = null;
        String tempString = "";
        Map returnForm = new HashMap();
        Object record;
        //CLOB會用到
        final LobHandler lobHandler=new DefaultLobHandler();

        ResultSetMetaData rsmd = rs.getMetaData();

        for (int i = 1; i <= rsmd.getColumnCount(); i++) {

            // 取得欄位
            columnName = rsmd.getColumnName(i).toUpperCase();

            if (columnName == null) {
                continue;
            }

            // 取得資料
            record = rs.getObject(rsmd.getColumnName(i));

            if (record != null) {

                // 日期型態特別處理( for Sql Server 2005 DB)
                if (record.getClass().getName().equals("java.sql.Date")
                    || record.getClass().getName().equals("java.sql.Timestamp")) {
                    
                    record = rs.getTimestamp(columnName);
                    SimpleDateFormat workDateFormat = null;
                    workDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    tempString = workDateFormat.format(record);
//                  
//                  SimpleDateFormat workDateFormat = null;
//                  
//                  // 如果欄位是[建檔日期CR_DATE]，[修改日期UPD_DATE]時，顯示格式為yyyy/MM/dd HH:mm:ss
//                  if ("CR_DATE".equals(columnName) || "UPD_DATE".equals(columnName)) {
//                      
//                      workDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//                  }
//                  else {
//                      
//                      workDateFormat = new SimpleDateFormat("yyyy/MM/dd");
//                  }
//
//                  tempString = workDateFormat.format(record);
//                  
//                  // 如果是異動時間的話，自動加入PK_UPD_DATE值
//                  if ("UPD_DATE".equals(columnName)) {
//                      
//                      returnForm.put("PK_UPD_DATE", tempString);
//                  }
                }
                //jtds的Clob型別，目前是用這個
                else if(record.getClass().getName().equals("net.sourceforge.jtds.jdbc.ClobImpl")){
                    tempString = lobHandler.getClobAsString(rs, columnName);
                }
                //java.math.BigDecimal
                else if(record.getClass().getName().equals("java.math.BigDecimal")){

                    // BigDecimal處理
                    if(rs.getBigDecimal(rsmd.getColumnName(i)).compareTo(BigDecimal.ZERO) == 0){
                        tempString = "0";
                    }else{
                        //把小數點後的零去掉,去除科學(e)記號
                        tempString = rs.getBigDecimal(rsmd.getColumnName(i)).stripTrailingZeros().toPlainString().toString();
                    }
                }
                else {

                    // 其他型態一般處理
                    tempString = String.valueOf(record);
                }
            }
            else {

                // 其他型態一般處理
                tempString = String.valueOf(record);
            }

            tempString = (tempString == null || tempString.equals("null")) ? "" : tempString;

            returnForm.put(columnName, tempString);
        }

        return returnForm;
    }
}
