/*
 * File Name       : BasicRowMapper.java 											<p>
 * Function Number : BasicRowMapper.java											<p>
 * Module          : com.base										<p>
 * Description     : 														<p>
 * Company         : 											<p>
 * Copyright       : Copyright c 2012									<p>
 * Author          : LauraChu												<p>
 * History         : V1.00 2012/7/20 LauraChu 									<p>
 * Version         : 1.00													<p>
 */
package com.cht.pcrf.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

public abstract class BasicRowMapper<T> implements ParameterizedRowMapper {

	// 變數宣告
	private boolean isAllRead = false;
	private int pageStartRow = 0;
	private int pageEndRow = 0;

	/**
	 * <pre>
	 * Method Name : setPageStartRow
	 * Description : 設定分頁起始筆數
	 * </pre>
	 * 
	 * @param setPageStartRow
	 *            int
	 */
	public void setPageStartRow(int setPageStartRow) {

		pageStartRow = setPageStartRow;
	}

	/**
	 * <pre>
	 * Method Name : getPageStartRow
	 * Description : 取得分頁起始筆數
	 * </pre>
	 * 
	 * @return int
	 */
	public int getPageStartRow() {

		return pageStartRow;
	}

	/**
	 * <pre>
	 * Method Name : setPageEndRow
	 * Description : 設定分頁結束筆數
	 * </pre>
	 * 
	 * @param setPageEndRow
	 *            int
	 */
	public void setPageEndRow(int setPageEndRow) {

		pageEndRow = setPageEndRow;
	}
	
	/**
	 * <pre>
	 * Method Name : getPageEndRow
	 * Description : 取得分頁結束筆數
	 * </pre>
	 * 
	 * @return int
	 */
	public int getPageEndRow() {

		return pageEndRow;
	}

	/**
	 * <pre>
	 * Method Name : setAllRead
	 * Description : 設定全部資料讀取(讀取時是否受限於分頁)
	 * </pre>
	 * 
	 * @param setAllRead
	 *            boolean
	 */
	public void setAllRead(boolean setAllRead) {

		isAllRead = setAllRead;
	}
	
	/**
	 * <pre>
	 * Method Name : isAllRead
	 * Description : 判斷是否是全部資料讀取(讀取時是否受限於分頁)
	 * </pre>
	 * 
	 * @return boolean
	 */
	public boolean isAllRead() {

		return isAllRead;
	}

	/**
	 * <pre>
	 * Method Name : mapRow
	 * Description : 將RS單筆資料對應至Objct
	 * </pre>
	 * 
	 * @param rs
	 *            ResultSet
	 * @param index
	 *            int
	 * @return Object
	 * @throws SQLException
	 *             SQL異常
	 */
	public Object mapRow(ResultSet rs, int index) throws SQLException {

		T workForm = null;

//		if (!isAllRead) {
//			if (index >= pageStartRow && index <= pageEndRow) {
//			    workForm = mapColumnValue(rs);
//			}
//		}
//		else {
		    workForm = mapColumnValue(rs);
//		}

		return workForm;
	}

	public abstract T mapColumnValue(ResultSet rs) throws SQLException;
    
//	/**
//	 * <pre>
//	 * Method Name : mapColumnValue
//	 * Description : 依欄位名稱填入Form中
//	 * </pre>
//	 * 
//	 * @param rs
//	 *            ResultSet
//	 * @return BasicForm
//	 * @throws SQLException
//	 *             SQLException
//	 */
    /*
	public Map mapColumnValue(ResultSet rs) throws SQLException {

		String columnName = null;
		String tempString = "";
		Map returnForm = new HashMap();
		Object record;
		//CLOB會用到
		final LobHandler lobHandler=new DefaultLobHandler();

		ResultSetMetaData rsmd = rs.getMetaData();

		for (int i = 1; i <= rsmd.getColumnCount(); i++) {

			// 取得欄位
			columnName = rsmd.getColumnName(i).toUpperCase();

			if (columnName == null) {
				continue;
			}

			// 取得資料
			record = rs.getObject(rsmd.getColumnName(i));

			if (record != null) {

				// 日期型態特別處理( for Sql Server 2005 DB)
				if (record.getClass().getName().equals("java.sql.Date")
					|| record.getClass().getName().equals("java.sql.Timestamp")) {
					
					record = rs.getTimestamp(columnName);
					SimpleDateFormat workDateFormat = null;
					workDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					tempString = workDateFormat.format(record);
//					
//					SimpleDateFormat workDateFormat = null;
//					
//					// 如果欄位是[建檔日期CR_DATE]，[修改日期UPD_DATE]時，顯示格式為yyyy/MM/dd HH:mm:ss
//					if ("CR_DATE".equals(columnName) || "UPD_DATE".equals(columnName)) {
//						
//						workDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//					}
//					else {
//						
//						workDateFormat = new SimpleDateFormat("yyyy/MM/dd");
//					}
//
//					tempString = workDateFormat.format(record);
//					
//					// 如果是異動時間的話，自動加入PK_UPD_DATE值
//					if ("UPD_DATE".equals(columnName)) {
//						
//						returnForm.put("PK_UPD_DATE", tempString);
//					}
				}
				//jtds的Clob型別，目前是用這個
				else if(record.getClass().getName().equals("net.sourceforge.jtds.jdbc.ClobImpl")){
					tempString = lobHandler.getClobAsString(rs, columnName);
				}
				//java.math.BigDecimal
				else if(record.getClass().getName().equals("java.math.BigDecimal")){

					// BigDecimal處理
					if(rs.getBigDecimal(rsmd.getColumnName(i)).compareTo(BigDecimal.ZERO) == 0){
						tempString = "0";
					}else{
						//把小數點後的零去掉,去除科學(e)記號
						tempString = rs.getBigDecimal(rsmd.getColumnName(i)).stripTrailingZeros().toPlainString().toString();
					}
				}
				else {

					// 其他型態一般處理
					tempString = String.valueOf(record);
				}
			}
			else {

				// 其他型態一般處理
				tempString = String.valueOf(record);
			}

			tempString = (tempString == null || tempString.equals("null")) ? "" : tempString;

			returnForm.put(columnName, tempString);
		}

		return returnForm;
	}
	*/

}
