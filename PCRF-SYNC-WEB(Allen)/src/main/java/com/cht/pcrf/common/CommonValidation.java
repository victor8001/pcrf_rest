package com.cht.pcrf.common;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.model.User;

/**
 * <code>CommonValidation</code>: 共用驗證類別
 * @author $Author: rayliao $
 * @version $Id: CommonValidation.java 25 2015-11-19 07:51:02Z rayliao $
 */
public class CommonValidation {
    private static Logger logger = LoggerFactory.getLogger(CommonValidation.class);

    /**
     * 密碼規則
     * @param password
     * @return 正確回傳TRUE
     */
    public static boolean complyWithPasswordRules(String password) {
        boolean result = false;
        if(StringUtils.isNotBlank(password)
                && password.length()>=6
                && password.matches(".*[A-Za-z]{1,}.*[A-Za-z]{1,}.*")
                && password.matches(".*[^A-Za-z]{1,}.*")) {
            result = true;
        }
        return result;
    }

    /**
     * validate password:新修改的密碼不可與前五次相同
     * @param destPasswdEncrypted(已加密)
     * @param user
     * @return 曾經使用過，回傳TRUE
     */
    public static boolean passwdHasBeenUsed(String destPasswdEncrypted, User user) {
        boolean result = false;
        if(destPasswdEncrypted.equals(user.getPassword())
                || destPasswdEncrypted.equals(user.getPassword_first())
                || destPasswdEncrypted.equals(user.getPassword_second())
                || destPasswdEncrypted.equals(user.getPassword_third())
                || destPasswdEncrypted.equals(user.getPassword_fourth())) {

            logger.info("destPasswdEncrypted:{}, user Password{}, first_passwd:{}, second_passwd:{}, third_passwd:{}, fourth_passwd:{}",
                    new Object[]{destPasswdEncrypted, user.getPassword(), user.getPassword_first(), user.getPassword_second(),
                            user.getPassword_third(), user.getPassword_fourth()});

            result = true;
        }
        return result;
    }
}
