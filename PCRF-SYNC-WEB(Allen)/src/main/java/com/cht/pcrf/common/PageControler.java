/*
 * File Name       : PageControler.java 											<p>
 * Function Number : PageControler.java											<p>
 * Module          : com.base										<p>
 * Description     : 														<p>
 * Company         : 											<p>
 * Copyright       : Copyright c 2012									<p>
 * Author          : LauraChu												<p>
 * History         : V1.00 2012/7/20 LauraChu 									<p>
 * Version         : 1.00													<p>
 */
package com.cht.pcrf.common;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class PageControler implements Serializable {

	// ~ Static Fields
	// ==================================================
	private static final long serialVersionUID = 4378773552352207760L;
	private static final int PAGE_RANGE = 3;
	private static final int ROWS_IN_PAGE = 50;
	
	// ~ Fields
	// ==================================================
	
	private int totalRowCount;
	private int currentPage = 1;
	private int rowsPerPage; // 每一頁顯示的筆數
	private List<Integer> showPageNo; // 要顯示的頁碼
	private String seqNo; // 暫存檔序號
	private boolean firstQuery = true;
	
	// ~ Constructors
	// ==================================================

	/**
	 * 
	 * 建構元
	 * 
	 */
	public PageControler() {

		initPageControler();
	}
	
	// ~ Methods
	// ==================================================

	/**
	 * <pre>
	 * Method Name : getSeqNo
	 * Description : 獲得SeqNo 暫存檔序號
	 * </pre>
	 * 
	 * @return String
	 */
	public String getSeqNo() {
	
		return seqNo;
	}

	/**
	 * <pre>
	 * Method Name : setSeqNo
	 * Description : 設定SeqNo
	 * </pre>
	 * @param seqNo String
	 */
	public void setSeqNo(String seqNo) {
	
		this.seqNo = seqNo;
	}
	
	/**
	 * <pre>
	 * Method Name : initPageControler
	 * Description : 初始化分頁控制器
	 * </pre>
	 */
	private void initPageControler(){

		setRowsPerPage(ROWS_IN_PAGE);
	}

	/**
	 * <pre>
	 * Method Name : getTotalRowCount
	 * Description : 獲得總資料筆數
	 * </pre>
	 * @return int
	 */
	public int getTotalRowCount() {
		return totalRowCount;
	}

	/**
	 * <pre>
	 * Method Name : setTotalRowCount
	 * Description : 設定總資料筆數
	 * </pre>
	 * @param totalRowCount int
	 */
	public void setTotalRowCount(int totalRowCount) {
		this.totalRowCount = totalRowCount;
		if(totalRowCount == 0){
			setCurrentPage(1);
		}else if(getCurrentPage() > getTotalPageCount()){
			setCurrentPage(getTotalPageCount());
		}
	}

	/**
	 * <pre>
	 * Method Name : getTotalPageCount
	 * Description : 獲得總頁數
	 * </pre>
	 * @return int
	 */
	public int getTotalPageCount() {
		
		BigDecimal totalRowCount = new BigDecimal(getTotalRowCount());
		BigDecimal rowsPerPage = new BigDecimal(getRowsPerPage());
		
		return totalRowCount.divide(rowsPerPage, 0, RoundingMode.UP).intValue();		
	}

	/**
	 * <pre>
	 * Method Name : getCurrentPageStartRow
	 * Description : 獲得目前頁開始行號
	 * </pre>
	 * @return int
	 */
	public int getCurrentPageStartRow() {
		return (getCurrentPage() - 1) * getRowsPerPage() + 1;
	}

	/**
	 * <pre>
	 * Method Name : getCurrentPageEndRow
	 * Description : 獲得目前頁結束行號
	 * </pre>
	 * @return int
	 */
	public int getCurrentPageEndRow() {
		int endRow = getCurrentPage() * getRowsPerPage();
		
		return endRow;
	}

	/**
	 * <pre>
	 * Method Name : getCurrentPage
	 * Description : 獲得目前頁碼
	 * </pre>
	 * @return int
	 */
	public int getCurrentPage() {
		return currentPage;
	}

	/**
	 * <pre>
	 * Method Name : setCurrentPage
	 * Description : 設定目前頁碼
	 * </pre>
	 * @param currentPage int
	 */
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	/**
	 * <pre>
	 * Method Name : getRowsPerPage
	 * Description : 獲得每頁行數
	 * </pre>
	 * @return int
	 */
	public int getRowsPerPage() {
		return rowsPerPage;
	}

	/**
	 * <pre>
	 * Method Name : setRowsPerPage
	 * Description : 設定每頁行數
	 * </pre>
	 * @param rowsPerPage int
	 */
	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	/**
	 * <pre>
	 * Method Name : getShowPageNo
	 * Description : 獲得下拉列表中可顯示的頁碼
	 * </pre>
	 * @return List<Integer>
	 */
	public List<Integer> getShowPageNo() {
		
		showPageNo = new ArrayList<Integer>();
		/*
		int i = getCurrentPage() - PAGE_RANGE;
		int j = getCurrentPage() + PAGE_RANGE;
		
		if(i < 1){
			i = 1;
		}
		
		if(j > getTotalPageCount()){
			j = getTotalPageCount();
		}
		
		for ( ; i <= j; i++) {
			showPageNo.add(i);
		}
		*/

        for (int i = 1; i <= getTotalPageCount(); i++) {
            showPageNo.add(i);
        }
        
		return showPageNo;
	}

	/**
	 * <pre>
	 * Method Name : isFirstQuery
	 * Description : 判斷是否是首次查詢
	 * </pre>
	 * 
	 * @return boolean
	 */
	public boolean isFirstQuery() {
	
		return firstQuery;
	}

	/**
	 * <pre>
	 * Method Name : setFirstQuery
	 * Description : 設定是否是首次查詢
	 * </pre>
	 * 
	 * @param firstQuery boolean
	 */
	public void setFirstQuery(boolean firstQuery) {
	
		this.firstQuery = firstQuery;
	}

}
