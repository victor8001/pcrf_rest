package com.cht.pcrf.common;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import com.cht.pcrf.model.BaseModel;
import com.cht.pcrf.model.User;

public class BaseAction extends Constants{

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * <pre>
	 * Method Name : getModelAndView
	 * Description : 取得Model和View
	 * </pre>
	 *
	 * @return ModelAndView
	 */
	protected ModelAndView getModelAndView(Map<String, Object> viewMap,String forwardPage) {

		ModelAndView mav = new ModelAndView();

		mav.setViewName(getViewPage(viewMap,forwardPage));

		return mav;
	}

	protected String getViewPage(Map<String, Object> viewMap,String forwardPage) {

		if (viewMap.containsKey(forwardPage)) {

			forwardPage = viewMap.get(forwardPage).toString();
		}

		return forwardPage;
	}

	protected void putDataToSession(HttpServletRequest request,
			String name ,Object object) {
		request.getSession().setAttribute(
				getFunctionId(request) + "_"+ name, object);//
	}

	protected void putResultToSession(HttpServletRequest request,
			List<Map> results) {
		putDataToSession(request,"results",results);
//		request.getSession().setAttribute(
//				getFunctionId(request) + "_results", results);//
	}

	protected Object getDataToSession(HttpServletRequest request
			, String name) {
		return request.getSession().getAttribute(
				getFunctionId(request) + "_" + name);//
	}

	protected List<Map> getResultToSession(HttpServletRequest request) {
		return (List<Map>) getDataToSession(request,"results");
	}

	protected void setPageControler(HttpServletRequest request,PageControler pageControler) {
		String fid = getFunctionId(request);
		request.getSession().setAttribute(
				fid + "_PageControler", pageControler);//
		request.setAttribute("functionId",fid);//

	}

	protected PageControler getPageControler(HttpServletRequest request){
		//currentPage
		PageControler pc = (PageControler) request.getSession().getAttribute(
				getFunctionId(request) + "_PageControler");
		if(pc == null){
			pc = new PageControler();
		}

		// 設定是否是[首次查詢]
		pc.setFirstQuery(!"N".equals(request.getParameter("isNewQuery")));

        int currentPage = 1;

        if(!"Y".equalsIgnoreCase(request.getParameter("isNewQuery"))){
            if (NumberUtils.isNumber(request.getParameter("pageNo"))) {
                currentPage = Integer.parseInt(request.getParameter("pageNo"));
            }
        }

		pc.setCurrentPage(currentPage);
		return pc;
	}

	protected void getResultDatas(HttpServletRequest request,
			List<Map<String, String>> results, int dataSize, String[] columns) {

		Map<String, String> dataMap = null;
		for(int i=0;i<dataSize;i++){
			dataMap = results.get(i);
			for(String col : columns){
				dataMap.put(col, request.getParameter(col+"_"+i));
			}
		}
	}

	protected String getFunctionId(HttpServletRequest request){
		String strRequestURI = request.getRequestURI();

		if (strRequestURI.indexOf(request.getContextPath()) == 0) {

			strRequestURI = strRequestURI.substring(request.getContextPath().length());
		}

		return strRequestURI;
	}

    protected void setMessage(HttpServletRequest request, String messageCode) {
        setMessage(request, messageCode, new String[]{});
    }

	protected void setMessage(HttpServletRequest request,
	        String messageCode, String[] messageParams) {

		request.setAttribute(Constants.MESSAGE_CODE, messageCode);
        request.setAttribute(Constants.MESSAGE_PARAMS, messageParams);
	}


	protected void cleanLoginUserData(HttpServletRequest request) {
        request.getSession().setAttribute(Constants.USER_DATA, null);
        request.getSession().setAttribute(Constants.MENU_DATA, null);
        request.getSession().setAttribute(Constants.FUNC_BTN_DATA, null);
        request.getSession().setAttribute(Constants.USER_AUTHED_FUNC, null);
        request.getSession().setAttribute(Constants.USER_AUTHED_BTN, null);
    }

	protected Map getUserData(HttpServletRequest request) {
		return (Map) request.getSession().getAttribute(Constants.USER_DATA);
	}

	protected ModelAndView exceptionProcess(Exception ex) {
        logger.error(ex.getMessage(),ex);
        return new ModelAndView(Constants.ERROR_PAGE).addObject(
                Constants.SYS_EXCEPTION_MESSAGE, ex);
	}

    /**
     * 取得使用者account
     * @return
     */
	protected String getUserAccount(HttpServletRequest request) {
	    Map userData = getUserData(request);
	    return (String)userData.get(Constants.USER_ACCOUNT);
	}

    /**
     * 取得使用者id
     * @return
     */
    protected Long getUserId(HttpServletRequest request) {
        Map userData = getUserData(request);
        return (Long) userData.get(Constants.USER_ID);
    }

    /**
     * 取得修改日期
     * @return
     */
    protected Date getModifyDate() {
        return new Date();
    }

    /**
     * 取得修改日期
     * @return
     */
    protected Timestamp getModifyTime() {
        return new Timestamp(new Date().getTime());
    }

    /**
     * 根據ID取得管理者名稱
     * @param allUsers
     * @param results
     * @return
     */
    protected List<? extends BaseModel> getUserNameByUserId(List<User> allUsers, List<? extends BaseModel> results) {

        Map<Long,String> allUserMap = new HashMap<Long,String>();
        if(allUsers != null && !allUsers.isEmpty()) {
            for(User user : allUsers) {
                allUserMap.put(user.getUser_id(), user.getAccount());
            }
        }
        if(allUserMap != null) {
            for(BaseModel resultModel : results) {
                /*
                for(User user : allUsers) {
                    if(resultModel.getModifyUser().equals(user.getId())) {
                        resultModel.setModifyUserName(user.getAccount());
                    }
                    if(resultModel.getCreateUser().equals(user.getId())) {
                        resultModel.setCreateUserName(user.getAccount());
                    }
                }
                */
                if(allUserMap.get(resultModel.getModifyUser()) != null) {
                    resultModel.setModifyUserName(allUserMap.get(resultModel.getModifyUser()));
                }
                if(allUserMap.get(resultModel.getCreateUser()) != null) {
                    resultModel.setCreateUserName(allUserMap.get(resultModel.getCreateUser()));
                }
            }
        }

        return results;
    }
}
