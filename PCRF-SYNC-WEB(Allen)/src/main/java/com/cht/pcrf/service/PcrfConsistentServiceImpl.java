package com.cht.pcrf.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.dao.PcrfConsistentDAO;
import com.cht.pcrf.dao.SysFuncBtnDAO;
import com.cht.pcrf.dao.SysRoleBtnDAO;
import com.cht.pcrf.dao.SysRoleUserDAO;
import com.cht.pcrf.dao.SysSeqDAO;
import com.cht.pcrf.dao.UserDAO;
import com.cht.pcrf.model.PcrfConsistent;
import com.cht.pcrf.model.SysRoleUser;
import com.cht.pcrf.model.User;
import com.cht.pcrf.utils.EncryptUtility;


@Service()
public class PcrfConsistentServiceImpl {

    private static final Logger logger = LoggerFactory.getLogger(PcrfConsistentServiceImpl.class);

    @Autowired
    private PcrfConsistentDAO pcrfConsistentDAO;

    public PcrfConsistent find() {
        return pcrfConsistentDAO.find();
    }
    
    public void update(PcrfConsistent pcrfConsistent) {
    	pcrfConsistentDAO.update(pcrfConsistent);
    }
    
}
