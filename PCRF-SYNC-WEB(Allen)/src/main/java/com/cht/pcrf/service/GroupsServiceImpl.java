package com.cht.pcrf.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.dao.SysFuncBtnDAO;
import com.cht.pcrf.dao.SysFuncDAO;
import com.cht.pcrf.dao.SysRoleBtnDAO;
import com.cht.pcrf.dao.SysRoleDAO;
import com.cht.pcrf.dao.SysRoleFuncDAO;
import com.cht.pcrf.dao.SysRoleUserDAO;
import com.cht.pcrf.model.SysFunc;
import com.cht.pcrf.model.SysFuncBtn;
import com.cht.pcrf.model.SysRole;
import com.cht.pcrf.model.SysRoleBtn;
import com.cht.pcrf.model.SysRoleFunc;
import com.cht.pcrf.model.SysRoleUser;
import com.cht.pcrf.model.User;

/**
 * <code>GroupsServiceImpl</code>:
 * @author $Author: weilinchu $
 * @version $Id: GroupsServiceImpl.java 365 2016-03-02 03:01:31Z weilinchu $
 */
@Service
public class GroupsServiceImpl {
    private static final Logger logger = LoggerFactory
            .getLogger(GroupsServiceImpl.class);

    @Autowired
    private SysRoleDAO sysRoleDAO;

    @Autowired
    private SysRoleUserDAO sysRoleUserDAO;

    @Autowired
    private SysFuncDAO sysFuncDAO;

    @Autowired
    private SysFuncBtnDAO sysFuncBtnDAO;

    @Autowired
    private SysRoleBtnDAO sysRoleBtnDAO;

    @Autowired
    private SysRoleFuncDAO sysRoleFuncDAO;

    public List<SysRole> findAll(PageControler pageControler) {
        return sysRoleDAO.findAll(pageControler);
    }

    @Transactional
    public void saveSysRole(SysRole sysRole) {
        sysRoleDAO.saveSysRole(sysRole);
    }

    public SysRole findById(Long id) {
        return sysRoleDAO.findById(id);
    }

    public List<SysRole> findByRoleName(String roleName, PageControler pageControler) {
        return sysRoleDAO.findByRoleName(roleName, pageControler);
    }

    @Transactional
    public void updateSysRole(SysRole sysRole) {
        sysRoleDAO.updateSysRole(sysRole);;
    }

    @Transactional
    public void deleteSysRoleByRoleId(Long roleId) {
        sysRoleDAO.deleteSysRoleByRoleId(roleId);
    }

    @Transactional
    public void deleteSysRoleUserByRoleId(Long roleId) {
        sysRoleUserDAO.deleteSysRoleUserByRoleId(roleId);
    }

    @Transactional
    public void deleteSysRoleUserByUserId(Long userId) {
        sysRoleUserDAO.deleteSysRoleUserByUserId(userId);
    }

    @Transactional
    public void deleteSysRoleUserByUserId(Map<String, Object> deleteUserData) {
        sysRoleUserDAO.deleteSysRoleUserByUserId(deleteUserData);
    }

    @Transactional
    public void saveSysRoleUser(Long roleId, Long[] userIds, Long modifyUserId) {
        List<SysRoleUser> sysRoleUsers = new ArrayList<SysRoleUser>();

        for(Long userId : userIds) {
            SysRoleUser sysRoleUser = new SysRoleUser();
            sysRoleUser.setRoleId(roleId);
            sysRoleUser.setUserId(userId);
            sysRoleUser.setModifyUser(modifyUserId);
            sysRoleUser.setModifyTime(new Timestamp(new Date().getTime()));

            sysRoleUsers.add(sysRoleUser);
        }

        sysRoleUserDAO.saveSysRoleUser(sysRoleUsers);
    }
    
    @Transactional
    public void saveSysRoleUserByRoleIds(Long[] roleIds, Long userId, Long modifyUserId) {
        List<SysRoleUser> sysRoleUsers = new ArrayList<SysRoleUser>();

        for(Long roleId : roleIds) {
            SysRoleUser sysRoleUser = new SysRoleUser();
            sysRoleUser.setRoleId(roleId);
            sysRoleUser.setUserId(userId);
            sysRoleUser.setModifyUser(modifyUserId);
            sysRoleUser.setModifyTime(new Timestamp(new Date().getTime()));

            sysRoleUsers.add(sysRoleUser);
        }
        sysRoleUserDAO.deleteSysRoleUserByUserId(userId);
        sysRoleUserDAO.saveSysRoleUser(sysRoleUsers);
    }

    public List<SysRoleUser> findSysRoleUserByRoleId(Long roleId) {
        return sysRoleUserDAO.findByRoleId(roleId);
    }

    public List<SysRoleUser> findSysRoleUserByUserId(Long userId) {
        return sysRoleUserDAO.findByUserId(userId);
    }

    public List<SysFunc> findAllFuncs(PageControler pageControler) {
        return sysFuncDAO.findAll(pageControler);
    }

    public List<SysFuncBtn> findAllSysFuncBtns(PageControler pageControler) {
        return sysFuncBtnDAO.findAll(pageControler);
    }

    public List<SysFuncBtn> findSysFuncBtnByFuncId(Long funcId) {
        return sysFuncBtnDAO.findByFuncId(funcId);
    }

    public SysFuncBtn findSysFuncBtnByBtnId(Long btnId) {
        return sysFuncBtnDAO.findByBtnId(btnId);
    }

    @Transactional
    public void saveSysRoleBtn(List<SysRoleBtn> sysRoleBtns) {
        sysRoleBtnDAO.saveSysRoleBtn(sysRoleBtns);
    }

    @Transactional
    public void saveSysRoleFunc(List<SysRoleFunc> sysRoleFuncs) {
        sysRoleFuncDAO.saveSysRoleFunc(sysRoleFuncs);
    }

    @Transactional
    public void deleteSysRoleBtnByRoleId(Long roleId) {
        sysRoleBtnDAO.deleteSysRoleBtnByRoleId(roleId);
    }

    @Transactional
    public void deleteSysRoleFuncByRoleId(Long roleId) {
        sysRoleFuncDAO.deleteSysRoleFuncByRoleId(roleId);
    }

    public List<SysRoleBtn> findAllSysRoleBtn() {
        return sysRoleBtnDAO.findAll();
    }

    public List<SysRoleBtn> findSysRoleBtnByRoleId(Long roleId) {
        return sysRoleBtnDAO.findByRoleId(roleId);
    }

    public List<SysRoleFunc> findAllSysRoleFunc() {
        return sysRoleFuncDAO.findAll();
    }

    public List<SysRoleFunc> findSysRoleFuncByRoleId(Long roleId) {
        return sysRoleFuncDAO.findByRoleId(roleId);
    }

    /**
     * 取得被勾選過的使用者
     * @param users
     * @param sysRoleUsers
     * @return
     */
    public List<String> getCheckedUsers(List<User> users, List<SysRoleUser> sysRoleUsers) {
        List<String> userIds = new ArrayList<String>();

        Map<Long,String> allUserMap = new HashMap<Long,String>();
        if(users != null && !users.isEmpty()) {
            for(User user : users) {
                allUserMap.put(user.getUser_id(), user.getAccount());
            }
        }
        
        for(SysRoleUser sysRoleUser : sysRoleUsers) {
            /*
            for(User user : users) {
                if(sysRoleUser.getUserId().equals(user.getId())) {
                    userIds.add(user.getId().toString());
                }
            }
            */
            if(allUserMap.get(sysRoleUser.getUserId()) != null) {
                userIds.add(sysRoleUser.getUserId().toString());
            }
        }
        return userIds;
    }

    /**
     * 依據 BtnId 取得對應的 {@link SysFuncBtn}
     * @param btnIds
     * @return
     */
    public List<SysFuncBtn> getSysFuncBtnByBtnIds(Long[] btnIds) {
        List<SysFuncBtn> sysFuncBtns = new ArrayList<SysFuncBtn>();

        for(Long btnId : btnIds) {
            // Get function id
            SysFuncBtn sysFuncBtn = this.findSysFuncBtnByBtnId(btnId);
            sysFuncBtns.add(sysFuncBtn);
        }
        return sysFuncBtns;
    }

    /**
     * 初始化要新增的 {@link SysRoleBtn}
     * @param roleId
     * @param userId
     * @param sysFuncBtns
     * @return
     */
    public List<SysRoleBtn> initialSysRoleBtnToUpdate(Long roleId, Long userId, List<SysFuncBtn> sysFuncBtns) {
        List<SysRoleBtn> sysRoleBtns = new ArrayList<SysRoleBtn>();

        for(SysFuncBtn sysFuncBtn : sysFuncBtns) {
            // Get function id
            Long functionId = sysFuncBtn.getFuncId();
            Long btnId = sysFuncBtn.getBtnId();

            logger.info("Initial SysRoleBtn. Role Id:{}, Function Id:{}, Btn Id:{}",
                    new Object[]{roleId, functionId, btnId});

            // Initial SysRoleBtn
            SysRoleBtn sysRoleBtn = new SysRoleBtn(roleId, functionId, btnId
                    , userId, new Timestamp(new Date().getTime()));
            sysRoleBtns.add(sysRoleBtn);
        }
        return sysRoleBtns;
    }

    /**
     * 初始化要新增的 {@link SysRoleFunc}
     * @param roleId
     * @param userId
     * @param sysFuncBtns
     * @return
     */
    public List<SysRoleFunc> initialSysRoleFuncToUpdate(Long roleId, Long userId, List<SysFuncBtn> sysFuncBtns) {
        List<SysRoleFunc> sysRoleFuncs = new ArrayList<SysRoleFunc>();
        Set<Long> functionIds = new HashSet<Long>();    // Get function id for initial SysRoleFunc

        for(SysFuncBtn sysFuncBtn : sysFuncBtns) {
            // Get function id
            Long functionId = sysFuncBtn.getFuncId();
            functionIds.add(functionId);
        }

        for(Long functionId : functionIds) {
            logger.info("Initial SysRoleFunc. Role Id:{}, Function Id:{}",
                    new Object[]{roleId, functionId});

            // Initial SysRoleFunc
            SysRoleFunc sysRoleFunc = new SysRoleFunc(roleId, functionId
                    , userId, new Timestamp(new Date().getTime()));
            sysRoleFuncs.add(sysRoleFunc);
        }

        return sysRoleFuncs;
    }

    /**
     * 更新 {@link SysRoleBtn}
     * step1. 先根據 roleId 刪除該權限所擁有的按鈕清單
     * step2. 再新增重新選取的按鈕清單
     * @param roleId
     * @param sysRoleBtns
     */
    @Transactional
    public void updateSysRoleBtn(Long roleId, List<SysRoleBtn> sysRoleBtns) {
        this.deleteSysRoleBtnByRoleId(roleId);
        this.saveSysRoleBtn(sysRoleBtns);
    }

    /**
     * 更新 {@link SysRoleFunc}
     * step1. 先根據 roleId 刪除該權限所擁有的功能清單
     * step2. 再新增重新選取的功能清單
     * @param roleId
     * @param sysRoleFuncs
     */
    @Transactional
    public void updateSysRoleFunc(Long roleId, List<SysRoleFunc> sysRoleFuncs) {
        this.deleteSysRoleFuncByRoleId(roleId);
        this.saveSysRoleFunc(sysRoleFuncs);
    }

    /**
     * 更新群組功能
     * @param roleId
     * @param userId
     * @param btnIds
     */
    public void updateFunctionSetting(Long roleId, Long userId, Long[] btnIds) {

        List<SysFuncBtn> sysFuncBtns = getSysFuncBtnByBtnIds(btnIds);
        List<SysRoleBtn> sysRoleBtns = initialSysRoleBtnToUpdate(roleId, userId, sysFuncBtns);
        List<SysRoleFunc> sysRoleFuncs = initialSysRoleFuncToUpdate(roleId, userId, sysFuncBtns);

        updateSysRoleBtn(roleId, sysRoleBtns);
        updateSysRoleFunc(roleId, sysRoleFuncs);
    }

    @Transactional
    public void updateSysRoleUser(Long roleId, Long userId, Long[] userIds) {
        deleteSysRoleUserByRoleId(roleId);
        saveSysRoleUser(roleId, userIds, userId);
    }
    
    @Transactional
    public void updateSysRoleUserNullUserIds(Long roleId) {
        deleteSysRoleUserByRoleId(roleId);
    }

    /**
     * 刪除群組
     * @param roleId
     */
    @Transactional
    public void deleteGroups(Long roleId) {
        /*
         * delete functionSetting
         * 1. delete SysRoleBtn
         * 2. delete SysRoleFunc
         */
        deleteSysRoleBtnByRoleId(roleId);
        deleteSysRoleFuncByRoleId(roleId);
        /*
         * delete sysRoleUser
         * 1. delete SysRoleUser
         */
        deleteSysRoleUserByRoleId(roleId);
        // delete SysRole
        deleteSysRoleByRoleId(roleId);
    }
    
    public Map<Long,String> getUserRole(Long userId){
        List<SysRoleUser> datas = sysRoleUserDAO.findByUserId(userId);
        Map<Long,String> userRolesMap = new HashMap<Long,String>();
        if(datas != null){
            for(SysRoleUser roleUser : datas){
                userRolesMap.put(roleUser.getRoleId(), "Y");
            }
        }
        
        return userRolesMap;
    }
}
