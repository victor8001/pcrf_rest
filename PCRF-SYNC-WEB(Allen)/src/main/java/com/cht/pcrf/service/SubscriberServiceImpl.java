package com.cht.pcrf.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.dao.SubscriberDAO;
import com.cht.pcrf.model.Subscriber;


/**
 * <code>SubscriberServiceImpl</code>:
 * @author $Author: weilinchu $
 * @version $Id: SubscriberServiceImpl.java 313 2016-01-27 17:33:39Z weilinchu $
 */
@Service
public class SubscriberServiceImpl {
    private static final Logger logger = LoggerFactory
            .getLogger(SubscriberServiceImpl.class);

    @Autowired
    private SubscriberDAO subscriberDAO;

    /**
     * find {@link Subscriber} by msisdn
     * @param msisdn
     * @param pageControler
     * @return
     */
    public List<Subscriber> findByMsisdn(String msisdn, PageControler pageControler,String location) {
        List<Subscriber> datas = subscriberDAO.findByMsisdn(msisdn, pageControler,location);
        return datas;
    }
}
