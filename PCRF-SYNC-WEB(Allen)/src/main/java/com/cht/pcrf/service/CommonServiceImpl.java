package com.cht.pcrf.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.dao.CommonDAO;
import com.cht.pcrf.dao.SysFuncBtnDAO;
import com.cht.pcrf.dao.SysRoleBtnDAO;
import com.cht.pcrf.dao.SysRoleUserDAO;
import com.cht.pcrf.dao.SysSeqDAO;
import com.cht.pcrf.dao.UserDAO;
import com.cht.pcrf.model.ConfValueInfo;
import com.cht.pcrf.model.SysRoleUser;
import com.cht.pcrf.model.User;
import com.cht.pcrf.utils.EncryptUtility;


@Service()
public class CommonServiceImpl {

    private static final Logger logger = LoggerFactory.getLogger(CommonServiceImpl.class);



    @Autowired
    private CommonDAO commonDAO;


    public List<ConfValueInfo> queryConfValue(String nodeLocation) {

        return commonDAO.getConfValue(nodeLocation);
    }
    
    public List<ConfValueInfo> queryConfValueAndTypeNotNegativeOne(String nodeLocation) {

        return commonDAO.getConfValueAndTypeNotNegativeOne(nodeLocation);
    }

    
}
