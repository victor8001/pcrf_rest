/*
 * File Name       : RedirPageServiceImpl.java 											<p>
 * Function Number : RedirPageServiceImpl.java											<p>
 * Module          : com.tiis.login.service										<p>
 * Description     : 														<p>
 * Company         : 											<p>
 * Copyright       : Copyright c 2012									<p>
 * Author          : LauraChu												<p>
 * History         : V1.00 2012/7/30 LauraChu 									<p>
 * Version         : 1.00													<p>
 */
package com.cht.pcrf.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cht.pcrf.common.Constants;
import com.cht.pcrf.dao.MenuDAO;

@Service
public class RedirPageServiceImpl {
    private static final Logger logger = LoggerFactory.getLogger(RedirPageServiceImpl.class);

    @Autowired
	private MenuDAO menuDAO;

	public List<Map> getMenuData(Long userId){
		List<Map> datas = menuDAO.getMenuData(userId);
		
		List<Map> rtDatas = new ArrayList();
		Map<String,Map> pIdMap = new LinkedHashMap();
		Map menuModel = null;
		if(datas != null){
			for(Map data:datas){
				String pId = StringUtils.trimToEmpty((String)data.get("MENU_ID"));
				String pName = StringUtils.trimToEmpty((String)data.get("MENU_NAME"));
				//String fId = StringUtils.trim((String)data.get("FUNC_ID"));
				
				menuModel = pIdMap.get(pId);
				if(menuModel == null){
					menuModel = new HashMap();
					menuModel.put("MENU_ID", pId);
					menuModel.put("MENU_NAME", pName);
				}
				if(menuModel.get("SUB_MENU_LIST") == null){
					menuModel.put("SUB_MENU_LIST",new ArrayList());
				}
				((List) menuModel.get("SUB_MENU_LIST")).add(data);
				
				/*
				if(menuModel == null){
					menuModel = new MenuModel();
					menuModel.setPId(pId);
					menuModel.setPName(pName);
				}
				menuModel.getSubMenuList().add(data);
				*/
				pIdMap.put(pId, menuModel);
			}
			
			for (Entry<String, Map> entry : pIdMap.entrySet()) {
				rtDatas.add(entry.getValue());
			}
		}

		return rtDatas;
	}

    public List<Map> getFuncBtnData(Long userId) {

        return menuDAO.getRoleFuncBtnDataByUserId(userId);
    }
    
    /**
     * 將使用者的權限放到session
     * @param request
     */
    public void getAuthFunc(HttpServletRequest request) {

        Map userData = (Map) request.getSession().getAttribute(Constants.USER_DATA);
        List<Map> menuList = (List<Map>) request.getSession().getAttribute(
                Constants.MENU_DATA);
        List<Map> funcBtnList = (List<Map>) request.getSession().getAttribute(
                Constants.FUNC_BTN_DATA);
        Map<String, String> funcAuthedMap = (Map<String, String>) request
                .getSession().getAttribute(Constants.USER_AUTHED_FUNC);
        Map<String,String> btnAuthedMap = new HashMap();
        
        if(funcAuthedMap == null){
            funcAuthedMap = new HashMap();
            for(Map map : menuList){
                List<Map> subList = (List<Map>) map.get("SUB_MENU_LIST");
                for(Map func : subList){
                    String funcUrl = StringUtils.trimToEmpty((String) func.get("FUNC_URL"));
                    funcUrl = funcUrl.replaceFirst("\\{0\\}", (String)userData.get(Constants.USER_ACCOUNT));
                    
                    funcAuthedMap.put(funcUrl, "Y");
                }
            }
            for(Map map : funcBtnList){
                String funcUrl = StringUtils.trimToEmpty((String) map.get("BTN_URL"));
                funcAuthedMap.put(funcUrl, "Y");

                String btnKey = StringUtils.trimToEmpty((String) map.get("FUNC_NAME")) 
                        + Constants.SPLIT_KEY + StringUtils.trimToEmpty((String) map.get("BTN_NAME"));
                btnAuthedMap.put(btnKey, "Y");
            }
            request.getSession().setAttribute(Constants.USER_AUTHED_FUNC, funcAuthedMap);
            request.getSession().setAttribute(Constants.USER_AUTHED_BTN, btnAuthedMap);
        }

        logger.info("funcMap:"+funcAuthedMap);
    }

}
