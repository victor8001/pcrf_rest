package com.cht.pcrf.service;

import java.text.ParseException;

import org.apache.commons.lang3.StringUtils;
import org.quartz.CronScheduleBuilder;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.cht.pcrf.common.Constants;
import com.cht.pcrf.utils.SysParameterPropsReader;

@Service()
public class QuartzServiceImpl {
	
    @Autowired
    private ApplicationContext context;

	private static final Logger logger = LoggerFactory.getLogger(QuartzServiceImpl.class);

	public void setJobTime(String execWeek ,String execMonth ,String execDay ,String execHour ) throws SchedulerException, ParseException, InterruptedException{
		
		StdScheduler sd = (StdScheduler) context.getBean("scheduler");
	
	    String name = "cronTrigger";
	    TriggerKey triggerKey = TriggerKey.triggerKey( name );
	      
	 
	    Trigger trigger = sd.getTrigger(triggerKey);
	
	    JobDetailImpl jobDetail = (JobDetailImpl) context.getBean("complexJobDetail");
	    sd.deleteJob(jobDetail.getKey());
	
	      
	    String newCronExpression="";
	    
	    String ssMM = StringUtils.trimToEmpty(SysParameterPropsReader.getConfig(Constants.DATA_CONSISTENT_SS_MM));
	    if("".equals(ssMM)){
	        ssMM = "0 15";
	    }
	    
	    if(execWeek.trim().equals("") ){
	    	newCronExpression = ssMM + " "+execHour+" "+execDay+" "+execMonth+" ?";
	      }else{
	    	  newCronExpression = ssMM + " "+execHour+" ? * "+execWeek;
	      }
	
	      //String newCronExpression = "0/7 * * * * ?"; // the desired cron expression
	      //String newCronExpression = "10 0,6,12,18,24 11 8 5 ?"; // the desired cron expression
	 
	      TriggerBuilder tb2 = trigger.getTriggerBuilder();   
	      Trigger newTrigger2 = 
	              tb2.withIdentity(triggerKey)
	              .startNow()
	              .withSchedule(CronScheduleBuilder.cronSchedule(newCronExpression))
	              .build();
	    
	      sd.scheduleJob(jobDetail, newTrigger2);
      
		  
	}

	
}
