package com.cht.pcrf.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.dao.SysFuncBtnDAO;
import com.cht.pcrf.dao.SysRoleBtnDAO;
import com.cht.pcrf.dao.SysRoleUserDAO;
import com.cht.pcrf.dao.SysSeqDAO;
import com.cht.pcrf.dao.UserDAO;
import com.cht.pcrf.model.SysRoleUser;
import com.cht.pcrf.model.User;
import com.cht.pcrf.utils.EncryptUtility;


@Service()
public class UserServiceImpl {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired(required = true)
    private UserDAO userDAO;

    @Autowired
    private SysRoleUserDAO sysRoleUserDAO;

    @Autowired
    private SysFuncBtnDAO sysFuncBtnDAO;

    @Autowired
    private SysRoleBtnDAO sysRoleBtnDAO;
    
    @Autowired
    private SysSeqDAO sysSeqDAO;
    
    @Autowired
    private GroupsServiceImpl groupsServiceImpl;

    /**
     * Check Login Status
     * @param userId
     * @param password
     * @return NULL: login success, String: error message
     */
    public String checkLoginStatus(String userId, User user){

        if(user == null) {  // User not found by account and password.
            // Find an user by account.
            List<User> users = userDAO.queryByAccount(userId);
            user = ((users != null && !users.isEmpty()) ? users.get(0) : null);

            if(user != null) {  // Login failed, and set retry count.
                long retryCount = user.getRetryCount();

                if(retryCount >= 4){    // retry 5 times, and lockout.
                    logger.info("User retry 5 times. User lockout status. User:{}", user.toString());

                    setBlockedByAccount(userId);

                    return "此門號已被鎖定，請尋求系統管理員協助";

                } else {    // Login failed, and set retry count.

                    addRetryCountByAccount(userId, ++retryCount);

                    logger.info("User not found. User retry {} times. User:{}"
                            , new Object[] { retryCount, user.toString() });

                    return "您已於此帳號登入失敗 " + retryCount + " 次";
                }

            } else {    // User not found by user id.
                logger.info("User not found by user id:{}.", userId);
            }

            return "錯誤的帳號或密碼!!";

        } else if(user.getBlocked() == 1){
            logger.info("User is blocked, userId:{}", userId);
            return "此門號已被鎖定，請尋求系統管理員協助";

        } else {    // Find an user, and not blocked. check success.
            return null;
        }
    }

    /**
     * 取得使用者資料
     * @param msisdn
     * @return
     */
    public User queryByAccount(String account, String md5pw) {

        List<User> datas = userDAO.queryByAccoundAndPw(account, EncryptUtility.toMd5(md5pw));
        if(datas != null && !datas.isEmpty()){
            return datas.get(0);
        }

        return null;
    }

    /**
     * 檢查使用者是否存在
     * @param msisdn
     * @return
     */
    public boolean checkUser(String account, String md5pw) {
        User user = queryByAccount(account, md5pw);
        if(user != null){
            return true;
        }

        return false;
    }

    /**
     * Add new user
     * @param inserData
     * @throws Exception
     */
    @Transactional
    public void saveNewUser(Map<String,Object> userData, String[] reqRoleIds, Long modifyUserId) throws Exception {
        //convert to Long[]
        
        Long[] roleIds = new Long[0];
        if(reqRoleIds != null){
            roleIds = new Long[reqRoleIds.length];
            int i=0;
            for(String roleId : reqRoleIds){
                roleIds[i] = Long.parseLong(roleId);
                i++;
            }
        }
        
        Long userId = sysSeqDAO.getSysUserSeq();
        userData.put("USER_ID", userId);
        
        //insert user
        userDAO.insert(userData);
        
        //insert user role
        groupsServiceImpl.saveSysRoleUserByRoleIds(roleIds, userId, modifyUserId);
    }

    /**
     * Update User
     * @param updateData
     * @throws Exception
     */
    @Transactional
    public void updateUser(Map<String, Object> userData, Long userId,
            String[] reqRoleIds, Long modifyUserId) throws Exception {
        
        Long[] roleIds = new Long[0];
        if(reqRoleIds != null){
            roleIds = new Long[reqRoleIds.length];
            int i=0;
            for(String roleId : reqRoleIds){
                roleIds[i] = Long.parseLong(roleId);
                i++;
            }
        }
        
        //update user
        userDAO.updatePassword(userData);
        //insert user role
        groupsServiceImpl.saveSysRoleUserByRoleIds(roleIds, userId, modifyUserId);
    }
    
    /**
     * Update Password
     * @param updateData
     * @throws Exception
     */
    @Transactional
    public void updatePassword(Map<String,Object> updateData) throws Exception {
        userDAO.updatePassword(updateData);
    }

    /**
     * Delete user
     * @param deleteData
     * @throws Exception
     */
    @Transactional
    public void deleteUser(Map<String,Object> deleteData, Long userId) throws Exception {
        // delete sysRoleUser
        Map<String, Object> userIdDeleteMap = new HashMap<String, Object>();
        userIdDeleteMap.put("USER_ID", userId);
        groupsServiceImpl.deleteSysRoleUserByUserId(userIdDeleteMap);

        //Get roleId from SysRoleUser by userId
        List<SysRoleUser> sysRoleUsers = groupsServiceImpl.findSysRoleUserByUserId(userId);
        Set<Long> roleIdSet = new HashSet<Long>();
        for(SysRoleUser sysRoleUser : sysRoleUsers) {
            roleIdSet.add(sysRoleUser.getRoleId());
        }

        if(roleIdSet != null && !roleIdSet.isEmpty()) {
            for(Long roleId : roleIdSet) {
                /*
                 * 1. delete SysRoleBtn
                 * 2. delete SysRoleUser
                 */
                groupsServiceImpl.deleteSysRoleBtnByRoleId(roleId);
                groupsServiceImpl.deleteSysRoleUserByRoleId(roleId);
            }
        }

        userDAO.deleteUser(deleteData);
    }
    
//    @Transactional
//    public void saveUserRoles(Long userId, Long[] roleIds, Long modifyUserId) throws Exception {
//        groupsServiceImpl.saveSysRoleUserByRoleIds(roleIds, userId, modifyUserId);
//    }


    public List<User> queryOther(String account) {

        return userDAO.queryOther(account);
    }

    /**
     * get accounts in db
     */
//    public List<User> listAccount(PageControler pageControler, String account, String md5pw, boolean isAdmin) {
//        List<User> datas = isAdmin ?
//                userDAO.queryAllAccount(pageControler) : userDAO.queryByAccoundAndPw(account,md5pw);
//
//        return datas;
//    }


    public List<User> listAllAccount(PageControler pageControler) {
        return userDAO.queryAllAccount(pageControler);
    }

    /**
     * add retry count
     * @param msisdn
     * @return
     */
    @Transactional
    public void addRetryCountByAccount(String account, Long retryCount) {
        userDAO.addRetryCount(account,retryCount);
    }

    @Transactional
    public void setBlockedByAccount(String account) {
        userDAO.setBlocked(account);
    }


    @Transactional
    public void resetRetryCountByAccount(String account) {

        userDAO.resetRetryCount(account);
    }

    @Transactional
    public void adminUpdatePassword(Map<String,Object> updateData) throws Exception {
        userDAO.adminUpdatePassword(updateData);
    }


    public List<User> queryByAccount(String account) {
        return userDAO.queryByAccount(account);
    }

    public void blockReset(Map<String,Object> updateData) throws Exception {
        userDAO.blockReset(updateData);
    }

    public User queryById(Long id) {
        List<User> datas = userDAO.queryById(id);
        if(datas != null){
            return datas.get(0);
        }
        return new User();
    }

    public Long getUserIdByAccount(String account) {
        List<User> datas = userDAO.queryByAccount(account);
        if(datas != null){
            return datas.get(0).getUser_id();
        }
        return null;
    }
    
    /**
     * Get role id from SYS_ROLE_USER by user id
     * Get function id(Edit password button) & Btn id from SYS_FUNC_BTN by BTN_URL
     * Get SYS_ROLE_BTN by ROLE_ID & BTN_ID & FUNC_ID
     * if SYS_ROLE_BTN == NULL, and the user don't have access to edit user profile
     *
     * @param userId
     * @return
     */
    public boolean getAccess(Long userId, String btnUrl) {
        return true;
        /*
        logger.info("[getAccess] userId:{}, btnUrl:{}", new Object[]{ userId, btnUrl });

        boolean result = false;

        //Get Button id
        List<SysFuncBtn> sysFuncBtns = sysFuncBtnDAO.findByBtnUrl(btnUrl);
        List<Long> btnIdList = new ArrayList<Long>();
        if(sysFuncBtns != null && !sysFuncBtns.isEmpty()) {
            for(SysFuncBtn sysFuncBtn : sysFuncBtns) {
                Long btnId = sysFuncBtn.getBtnId();
                btnIdList.add(btnId);
            }
        } else { return result; }

        //Get Role id
        List<SysRoleUser> sysRoleUsers = sysRoleUserDAO.findByUserId(userId);
        List<Long> roleIdList = new ArrayList<Long>();
        if(sysRoleUsers != null && !sysRoleUsers.isEmpty()) {
            for(SysRoleUser sysRoleUser : sysRoleUsers) {
                Long roleId = sysRoleUser.getRoleId();
                roleIdList.add(roleId);
            }
        } else { return result; }

        if(btnIdList != null && !btnIdList.isEmpty()) {
            for(Long btnId : btnIdList) {
                if(result) { break; }

                List<SysRoleBtn> sysRoleBtns = sysRoleBtnDAO.findByBtnId(btnId);

                if(sysRoleBtns != null && sysRoleBtns.size() > 0) {

                    //是否有權限
                    for(SysRoleBtn sysRoleBtn : sysRoleBtns) {
                        if(result) { break; }

                        for(Long roleId : roleIdList) {
                            if(sysRoleBtn.getRoleId().equals(roleId)) {
                                logger.info("[getAccess] roleId:{}", roleId);

                                result = true;
                                break;
                            }
                        }
                    }

                }
            }
        } else { return result; }

        return result;
        */
    }
}
