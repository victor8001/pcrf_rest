package com.cht.pcrf.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.dao.SapcInfoDao;
import com.cht.pcrf.dao.SysSeqDAO;
import com.cht.pcrf.model.SapcInfo;


/**
 * <code>SapcInfoServiceImpl</code>:
 * @version $Id: SapcInfoServiceImpl.java 365 2016-03-02 03:01:31Z weilinchu $
 * @author $Author: weilinchu $
 */
@Service
public class SapcInfoServiceImpl {

    private static final Logger logger = LoggerFactory
            .getLogger(SapcInfoServiceImpl.class);

    @Autowired
    private SapcInfoDao sapcInfoDao;


    
    @Autowired
    private SysSeqDAO sysSeqDAO;

    /**
     * find all SAPC
     * @param pageControler 
     * @return
     */
    public List<SapcInfo> findAll() { return findAll(null); }
    public List<SapcInfo> findAll(PageControler pageControler) { return sapcInfoDao.findAll(pageControler); }

    /**
     * find SAPC by nodeName
     * @param nodeName
     * @return
     */
    public SapcInfo findByNodeName(String nodeName) {
        return sapcInfoDao.findByNodeName(nodeName);
    }
    
    /**
     * find SAPC by id
     * @param nodeName
     * @return
     */
    public SapcInfo findById(String id) {
        return sapcInfoDao.findById(id);
    }

    
    /**
     * save SAPC
     * @param sapcInfo
     */
    @Transactional
    public void saveSapc(SapcInfo sapcInfo) {
        sapcInfoDao.saveSapc(sapcInfo);
    }

    /**
     * update SAPC
     * @param sapcInfo
     */
    @Transactional
    public void updateSapc(SapcInfo sapcInfo) {
        sapcInfoDao.updateSapc(sapcInfo);
    }
    
    /**
     * update SAPC status
     * @param sapcInfo
     */
    @Transactional
    public void updateSapcStatusById(Long id, int status) {
        sapcInfoDao.updateSapcStatusById(id, status);
    }
    

    /**
     * find SAPC seq
     * @param nodeName
     * @return
     * @throws Exception 
     */
    public Long findPcrfInfoIdSeq() throws Exception {
        return sysSeqDAO.getPcrfInfoIdSeq();
    }
    
}
