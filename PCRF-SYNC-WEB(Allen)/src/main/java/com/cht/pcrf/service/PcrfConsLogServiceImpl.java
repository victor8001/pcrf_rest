package com.cht.pcrf.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cht.pcrf.api.common.CommonConstant;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.dao.PcrfConsLogDAO;
import com.cht.pcrf.exception.ServiceException;
import com.cht.pcrf.utils.DateUtil;

@Service()
public class PcrfConsLogServiceImpl {

	private static final Logger logger = LoggerFactory.getLogger(PcrfConsLogServiceImpl.class);

	@Autowired
	private PcrfConsLogDAO pcrfConsLogDAO;

	public List<Map> findByLocStartTimeResult(PageControler pageControler, String location, String startTime, String endTime, String result) {
		
		startTime = startTime + " 00:00:00";
		endTime = endTime + " 23:59:59";
		
		return pcrfConsLogDAO.findByLocStartTimeResult(pageControler, location, startTime, endTime, result);

		/*
		List<Map> rtDatas = new ArrayList<Map>();
		Map<String, Map> pIdMap = new LinkedHashMap<String, Map>();
		Map model = null;
		if (datas != null) {
			for (Map data : datas) {
				String id = StringUtils.trimToEmpty((String) data.get("ID"));
				String loc = StringUtils.trimToEmpty((String) data.get("LOCATION"));
				String nodeName = StringUtils.trimToEmpty((String) data.get("NODE_NAME"));
				String execResult = StringUtils.trimToEmpty((String) data.get("EXEC_RESULT"));
				String execStartTime = StringUtils.trimToEmpty((String) data.get("EXEC_START_TIME"));
				String execEndTime = StringUtils.trimToEmpty((String) data.get("EXEC_END_TIME"));
				String rptPath = StringUtils.trimToEmpty((String) data.get("RPT_PATH"));
				String replacePath = rptPath.replaceAll("\\\\","_");
				
				model = new HashMap();
				model.put("ID", id);
				model.put("LOCATION", loc);
				model.put("NODE_NAME", nodeName);
				model.put("EXEC_RESULT", execResult);
				model.put("EXEC_START_TIME", execStartTime);
				model.put("EXEC_END_TIME", execEndTime);
				model.put("RPT_PATH", replacePath);

				model.put("SUB_DATA", new ArrayList());
				((List) model.get("SUB_DATA")).add(data);

				pIdMap.put(id, model);
			}
			for (Entry<String, Map> entry : pIdMap.entrySet()) {
				rtDatas.add(entry.getValue());
			}

		}
		return rtDatas;
        */

	}

    public Map<String,Object> findById(Long id) {
        return pcrfConsLogDAO.findById(id);
    }
    
    public List<Map<String, String>> getRptDatas(Long rptId, String csvPath) throws ServiceException{
        BufferedReader buf = null;
        List<Map<String, String>> dataList = new ArrayList<Map<String, String>>();
        Map<String, String> dataMap = null;
        int dataNum = 0;//資料筆數
        try {
            //2.回補資料
            //type:1 只存在在pcrf
            //type:2 只存在在db sync
            //type:3 pcrf與db sync資料不一致
            logger.info("getRptDatas csvPath:{} ", csvPath);

            Map<String, StringBuffer> logMap = readResultToFile(Long.toString(rptId));
            
            String[] tmpDataTitle = {"NODE", "TYPE", "MSISDN", "OBJECTCLASS", "INFO1", "INFO2"};
            StringBuffer data = new StringBuffer();
            String sCurrentLine;
            buf = new BufferedReader(new FileReader(csvPath));
            
            while ((sCurrentLine = buf.readLine()) != null) {
                if (sCurrentLine == null || sCurrentLine.length() == 0)
                    continue;
                
                if(dataNum > 200) throw new ServiceException("text.data.over200","");

                String location = sCurrentLine.split(",")[0].split(" - ")[0];
                if(CommonConstant.locations.contains(location)){
                    logger.debug("data:" + data);
                    if(data.length() > 0){
                        dataMap = new HashMap<String, String>();
                        String[] tmpDataArr = data.toString().split(",", 6);
                        int i=0;
                        for(String tmpData : tmpDataArr){
                            dataMap.put(tmpDataTitle[i++], tmpData);
                        }
                        dataMap.put("R_DATA", convertToCSVFormat(data.toString()));
                        
                        String key = tmpDataArr[1] + "_" + tmpDataArr[2];
                        if(logMap.get(key) != null){
                            dataMap.put("RESULT", logMap.get(key).toString());
                        }
                        
                        dataNum++;
                        dataList.add(dataMap);
                    }
                    
                    data = new StringBuffer();
                }

                data.append(sCurrentLine + CommonConstant.BR);
            }
            if(data.length() > 0){
                dataMap = new HashMap<String, String>();
                String[] tmpDataArr = data.toString().split(",", 6);
                int i=0;
                for(String tmpData : tmpDataArr){
                    dataMap.put(tmpDataTitle[i++], tmpData);
                }
                dataMap.put("R_DATA", convertToCSVFormat(data.toString()));
                
                String key = tmpDataArr[1] + "_" + tmpDataArr[2];
                if(logMap.get(key) != null){
                    dataMap.put("RESULT", logMap.get(key).toString());
                }
                
                dataNum++;
                dataList.add(dataMap);
            }

        } catch(ServiceException e1){
            throw e1;
        } catch (Exception e) {
            logger.info("getRptDatas error. csvPath: {}, ex: {}", csvPath, e.getMessage());
            logger.info(e.getMessage(), e);
            throw new ServiceException("error.view.fail", "", e);
        } finally{
            logger.info("getRptDatas data finish, csvPath:{} ", csvPath);
            
            try {
                if(buf != null)
                    buf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            
        }
        return dataList;
    }

    private String convertToCSVFormat(String data) {
        //TP - 2L3PilotPCRF1,2,88694400008781,epc-subscriberqualification,INFO1,INFO2
        
        StringBuffer sb = new StringBuffer();
        String[] tmpDataArr = data.split(",", 6);
        
        int i=0;
        for(String tmpData : tmpDataArr){
            
            if(i != 0){
                sb.append(",");
            }
            
            if(i == 0){
                //TP - 2L3PilotPCRF1
                tmpData = tmpData.split(" - ")[0];
            }else if(i == 4 || i == 5){
                tmpData = tmpData.replaceAll("\"", "");
            }
            i++;
            sb.append(tmpData);
        }
        
        return sb.toString();
    }


    public void writeResultToFile(String type, String msisdn, String result,
            String id, String username) {
        
        PrintWriter writer = null;
        try{
            String fileDir = System.getProperty("dir.log") + File.separator + "pcrf-web";
            String filePath = fileDir + File.separator + "sync_" + id + ".log";
            
            File file = new File(fileDir);
            file.mkdirs();
            
//            writer = new PrintWriter(filePath, "UTF-8");
            writer = new PrintWriter(new BufferedWriter(new FileWriter(filePath, true)));
            writer.println(type + CommonConstant.COMMON 
                    + msisdn + CommonConstant.COMMON 
                    + result + CommonConstant.COMMON
                    + DateUtil.formatDate(new Date())+ CommonConstant.COMMON
                    + username);
            
        } catch (Exception e) {
           logger.error(e.getMessage(), e);
        } finally {
            if(writer != null)
                writer.close();
        }
        
    }
    
    @SuppressWarnings("unused")
    public HashMap<String, StringBuffer> readResultToFile(String id) throws ServiceException {
        
        BufferedReader buf = null;
        HashMap<String, StringBuffer> map = new HashMap<String, StringBuffer>();
        try {
            String filePath = System.getProperty("dir.log") + File.separator
                    + "pcrf-web" + File.separator + "sync_" + id + ".log";
            //logger.info("filePath:" + filePath);
            
            File file = new File(filePath);
            if(!file.exists()) return map;
            
            StringBuffer data = new StringBuffer();
            String sCurrentLine;
            buf = new BufferedReader(new FileReader(filePath));
            
            while ((sCurrentLine = buf.readLine()) != null) {
                if (sCurrentLine == null)
                    continue;

                //type,msisdn,result,date,username
                String[] dataArr = sCurrentLine.split(CommonConstant.COMMON);
                String key = dataArr[0] + "_" + dataArr[1];
                
                if(map.get(key) == null){
                    map.put(key, new StringBuffer());
                }
                
                map.get(key).append(("1".equals(dataArr[2]) ? "成功" : "失敗") + ":" + dataArr[3] + " " + dataArr[4]);
                map.get(key).append("<BR>");
                
            }
            //logger.info("map:" + map);
            return map;
        } catch (Exception e) {
            logger.info("readResultToFile error. id: {}, ex: {}", id, e.getMessage());
            logger.info(e.getMessage(), e);
            throw new ServiceException("error.view.fail", "", e);
        } finally{
            logger.info("readResultToFile data finish, id:{} ", id);
            
            try {
                if(buf != null)
                    buf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            
        }
        
    }
    
}
