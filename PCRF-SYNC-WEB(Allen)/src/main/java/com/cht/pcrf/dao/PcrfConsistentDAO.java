package com.cht.pcrf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cht.pcrf.utils.SystemUtility;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.model.PcrfConsistent;
import com.cht.pcrf.model.SapcInfo;
import com.cht.pcrf.model.User;


@Repository()
public class PcrfConsistentDAO extends BaseDAO<PcrfConsistent>{
	
	public PcrfConsistent find(){
		Boolean isRest = (SystemUtility.getConfig("db.isRestSuffix")==null)
				? false : Boolean.parseBoolean(SystemUtility.getConfig("db.isRestSuffix"));
		String consist = "PCRF_CONSISTENT"+(isRest?"_REST":"");
		final String sql = "SELECT * FROM " + consist ;
        List<PcrfConsistent> resultList = super.queryForList(sql);
        return (resultList != null && !resultList.isEmpty()) ? resultList.get(0) : null;
	}
	
	 public void update(PcrfConsistent pcrfConsistent) {
		 Boolean isRest = (SystemUtility.getConfig("db.isRestSuffix")==null)
				 ? false : Boolean.parseBoolean(SystemUtility.getConfig("db.isRestSuffix"));
		 String consist = "PCRF_CONSISTENT"+(isRest?"_REST":"");
		 final StringBuffer sql = new StringBuffer("UPDATE "+consist+" SET "
	                + "  EXEC_WEEK=:EXEC_WEEK ,EXEC_MONTH=:EXEC_MONTH "
	                + ", EXEC_DAY=:EXEC_DAY , EXEC_HOUR=:EXEC_HOUR "
	                + ", CREATE_TIME=:CREATE_TIME, MODIFY_TIME=:MODIFY_TIME"
	                + ", CREATE_USER=:CREATE_USER, MODIFY_USER=:MODIFY_USER"
	                + ", SYNC_TYPE=:SYNC_TYPE , AUTO_RECOVERY_MIN =:AUTO_RECOVERY_MIN ");

	        Map paramMap = new HashMap<String, Object>();
	        
	        paramMap.put("EXEC_WEEK", pcrfConsistent.getExecWeek());
	        paramMap.put("EXEC_MONTH", pcrfConsistent.getExecMonth());
	        paramMap.put("EXEC_DAY", pcrfConsistent.getExecDay());
	        paramMap.put("EXEC_HOUR", pcrfConsistent.getExecHour());
	        paramMap.put("CREATE_USER", pcrfConsistent.getCreateUser());
	        paramMap.put("MODIFY_USER", pcrfConsistent.getModifyUser());
	        paramMap.put("CREATE_TIME", pcrfConsistent.getCreateTime());
	        paramMap.put("MODIFY_TIME", pcrfConsistent.getModifyTime());
	        paramMap.put("SYNC_TYPE", pcrfConsistent.getSyncType());
	        paramMap.put("AUTO_RECOVERY_MIN", pcrfConsistent.getAutoRecoveryMin());

	        sql.append(" WHERE ID=:ID");
	        paramMap.put("ID", pcrfConsistent.getId());

	        super.update(sql.toString(), paramMap);
	    }
   


    @Override
    public BasicRowMapper<PcrfConsistent> getRowMapper() {
        return new PcrfConsistentRowMapper();
    }

    private class PcrfConsistentRowMapper extends BasicRowMapper<PcrfConsistent> {

        @Override
        public PcrfConsistent mapColumnValue(ResultSet rs) throws SQLException {

        	PcrfConsistent model = new PcrfConsistent();
            model.setId(rs.getLong("ID"));
            model.setExecWeek(rs.getString("EXEC_WEEK"));
            model.setExecMonth(rs.getString("EXEC_MONTH"));
            model.setExecDay(rs.getString("EXEC_DAY"));
            model.setExecHour(rs.getString("EXEC_HOUR"));
            model.setSyncType(rs.getString("SYNC_TYPE"));
            model.setCreateUser(rs.getLong("CREATE_USER"));
            model.setCreateTime(rs.getTimestamp("CREATE_TIME"));
            model.setModifyUser(rs.getLong("MODIFY_USER"));
            model.setModifyTime(rs.getTimestamp("MODIFY_TIME"));
            model.setAutoRecoveryMin(rs.getInt("AUTO_RECOVERY_MIN"));

            return model;
        }
    }

}

