package com.cht.pcrf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.SysFunc;

/**
 * <code></code>:
 * @author $Author: weilinchu $
 * @version $Id: SysFuncDAO.java 305 2016-01-27 05:49:53Z weilinchu $
 */
@Repository
public class SysFuncDAO extends BaseDAO<SysFunc> {
    private static final Logger logger = LoggerFactory
            .getLogger(SysFuncDAO.class);

    public List<SysFunc> findAll(PageControler pageControler) {
        final String sql = "SELECT row_number() OVER (ORDER BY s.SEQ_NO) AS r, s.* FROM SYS_FUNC s ORDER BY s.SEQ_NO ";
        return super.queryForList(pageControler, sql);
    }

    private class SysFuncRowMapper extends BasicRowMapper<SysFunc> {
        @Override
        public SysFunc mapColumnValue(ResultSet rs) throws SQLException {

            SysFunc model = new SysFunc();
            model.setFuncId(rs.getLong("FUNC_ID"));
            model.setFuncName(rs.getString("FUNC_NAME"));
            model.setFuncCname(rs.getString("FUNC_CNAME"));
            model.setFuncUrl(rs.getString("FUNC_URL"));
            model.setMenuId(rs.getInt("MENU_ID"));
            model.setSeqNo(rs.getInt("SEQ_NO"));
            model.setUsed(rs.getBoolean("IS_USED"));
            model.setCreateUser(rs.getLong("CREATE_USER"));
            model.setCreateTime(rs.getTimestamp("CREATE_TIME"));
            model.setModifyUser(rs.getLong("MODIFY_USER"));
            model.setModifyTime(rs.getTimestamp("MODIFY_TIME"));

            return model;
        }
    }

    @Override
    public BasicRowMapper<SysFunc> getRowMapper() {
        return new SysFuncRowMapper();
    }

}
