package com.cht.pcrf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.SysFuncBtn;

/**
 * <code></code>:
 * @author $Author: weilinchu $
 * @version $Id: SysFuncBtnDAO.java 305 2016-01-27 05:49:53Z weilinchu $
 */
@Repository
public class SysFuncBtnDAO extends BaseDAO<SysFuncBtn> {


    public List<SysFuncBtn> findAll(PageControler pageControler) {
        final String sql = "SELECT row_number() OVER (ORDER BY s.FUNC_ID,s.BTN_NAME) AS r, s.* FROM SYS_FUNC_BTN s ORDER BY s.FUNC_ID,s.BTN_NAME ";
        return super.queryForList(pageControler, sql);
    }

    public List<SysFuncBtn> findByFuncId(Long id) {
        final String sql = "SELECT * FROM SYS_FUNC_BTN WHERE FUNC_ID = :FUNC_ID";
        Map paramMap = new HashMap<String, Object>();
        paramMap.put("FUNC_ID", id);
        return super.queryForList(sql, paramMap);
    }

    public SysFuncBtn findByBtnId(Long id) {
        final String sql = "SELECT * FROM SYS_FUNC_BTN WHERE BTN_ID = :BTN_ID";
        Map paramMap = new HashMap<String, Object>();
        paramMap.put("BTN_ID", id);
        List<SysFuncBtn> resultList = super.queryForList(sql, paramMap);
        return (resultList != null && !resultList.isEmpty()) ? resultList.get(0) : null;
    }

    public List<SysFuncBtn> findByBtnUrl(String url) {
        final String sql = "SELECT * FROM SYS_FUNC_BTN WHERE BTN_URL = :BTN_URL";
        Map paramMap = new HashMap<String, Object>();
        paramMap.put("BTN_URL", url);
        return super.queryForList(sql, paramMap);
    }

    private class SysFuncBtnRowMapper extends BasicRowMapper<SysFuncBtn> {
        @Override
        public SysFuncBtn mapColumnValue(ResultSet rs) throws SQLException {

            SysFuncBtn model = new SysFuncBtn();
            model.setFuncId(rs.getLong("FUNC_ID"));
            model.setBtnId(rs.getLong("BTN_ID"));
            model.setBtnName(rs.getString("BTN_NAME"));
            model.setBtnCname(rs.getString("BTN_CNAME"));
            model.setBtnUrl(rs.getString("BTN_URL"));
            model.setUsed(rs.getBoolean("IS_USED"));
            model.setCreateUser(rs.getLong("CREATE_USER"));
            model.setCreateTime(rs.getTimestamp("CREATE_TIME"));
            model.setModifyUser(rs.getLong("MODIFY_USER"));
            model.setModifyTime(rs.getTimestamp("MODIFY_TIME"));

            return model;
        }
    }

    @Override
    public BasicRowMapper<SysFuncBtn> getRowMapper() {
        return new SysFuncBtnRowMapper();
    }

}
