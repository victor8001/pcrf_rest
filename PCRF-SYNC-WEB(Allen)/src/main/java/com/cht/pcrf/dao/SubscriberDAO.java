package com.cht.pcrf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.Subscriber;

/**
 * <code>SubscriberDAO</code>:
 * @author $Author: rayliao $
 * @version $Id: SubscriberDAO.java 66 2015-12-03 04:28:50Z rayliao $
 */
@Repository
public class SubscriberDAO extends BaseDAO<Subscriber> {
    private static final Logger logger = LoggerFactory.getLogger(SubscriberDAO.class);



    public Subscriber findById(long id) {
        final String sql = "SELECT * FROM SUBSCRIBER WHERE ID = :ID";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ID", id);

        List<Subscriber> resultList = queryForList(sql, paramMap);

        return (resultList != null && !resultList.isEmpty()) ? resultList.get(0) : null;
    }


    public List<Subscriber> findByMsisdn(String msisdn, PageControler pageControler,String location) {
    	String loc ="";
    	if(location.equals("TP")){
    		loc = "1";
    	}
    	if(location.equals("KH")){
    		loc = "2";
    	}
        final String sql = "SELECT " +loc+" as nodeLocation,row_number() OVER (ORDER BY subs.id) AS r, subs.* FROM SUBSCRIBER_"+location+" subs WHERE MSISDN = :MSISDN ORDER BY subs.ID";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("MSISDN", msisdn);
        return super.queryForList(pageControler, sql, paramMap);
    }

    private class SubscriberRowMapper extends BasicRowMapper<Subscriber> {
        @Override
        public Subscriber mapColumnValue(ResultSet rs) throws SQLException {

            Subscriber model = new Subscriber();
            model.setId(rs.getLong("ID"));
            model.setMsisdn(rs.getString("MSISDN"));
            model.setObjectClass(rs.getString("OBJECT_CLASS"));
            model.setDn(rs.getString("DN"));
            model.setCol1_1(rs.getString("COL1_1"));
            model.setCol2_1(rs.getString("COL2_1"));
            model.setCol3_1(rs.getString("COL3_1"));
            model.setCol4_1(rs.getString("COL4_1"));
            model.setCol5_1(rs.getString("COL5_1"));
            model.setCol6_1(rs.getString("COL6_1"));
            model.setCol7_1(rs.getString("COL7_1"));
            model.setCol8_1(rs.getString("COL8_1"));
            model.setCol9_1(rs.getString("COL9_1"));
            model.setCol10_1(rs.getString("COL10_1"));
            model.setCol11_1(rs.getString("COL11_1"));
            model.setCol11_2(rs.getString("COL11_2"));
            model.setCol11_3(rs.getString("COL11_3"));
            model.setCol11_4(rs.getString("COL11_4"));
            model.setCol11_5(rs.getString("COL11_5"));
            model.setCol11_6(rs.getString("COL11_6"));
            model.setCol11_7(rs.getString("COL11_7"));
            model.setCol11_8(rs.getString("COL11_8"));
            model.setCol11_9(rs.getString("COL11_9"));
            model.setCol11_10(rs.getString("COL11_10"));
            model.setCol99_1(rs.getString("COL99_1"));
            model.setInfo(rs.getString("INFO"));
            model.setCreateTime(rs.getTimestamp("CREATE_TIME"));
            model.setModifyTime(rs.getTimestamp("MODIFY_TIME"));
            model.setNodeLocation(rs.getString("NODELOCATION"));
        	
            return model;
        	
        }
    }

    @Override
    public BasicRowMapper<Subscriber> getRowMapper() {
        return new SubscriberRowMapper();
    }
}
