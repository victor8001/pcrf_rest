package com.cht.pcrf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.model.User;


@Repository()
public class UserControlDAO extends BaseDAO<User>{

    public List<User> queryByAccoundAndPw(String account,String md5pw) {
        final String sql =
                " SELECT * FROM SYS_USER "+
                " WHERE ACCOUNT = :ACCOUNT "+
                " AND PASSWORD = :PASSWORD ";

        Map<String,Object> params = new HashMap<String, Object>();
        params.put("ACCOUNT", account);
        params.put("PASSWORD", md5pw);

        return super.queryForList(sql,params);

    }

    public void updatePassword(Map<String,Object> updateData) {

        String sql =
            " update SYS_USER set PASSWORD = :PASSWORD , MODIFY_TIME = :MODIFY_TIME where ACCOUNT = :ACCOUNT";

        super.update(sql, updateData);
    }


    public void deleteUser(Map<String,Object> deleteData) {

        String sql =
            " DELETE FROM SYS_USER WHERE ACCOUNT = :ACCOUNT";

        super.delete(sql, deleteData);
    }


    @Override
    public BasicRowMapper<User> getRowMapper() {
        return new UserRowMapper();
    }

    private class UserRowMapper extends BasicRowMapper<User> {

        @Override
        public User mapColumnValue(ResultSet rs) throws SQLException {

            User model = new User();
//            model.setId(rs.getLong("ID"));
//            model.setAccount(rs.getString("ACCOUNT"));
//            model.setCreateTime(rs.getTimestamp("CREATE_TIME"));
//            model.setModifyTime(rs.getTimestamp("MODIFY_TIME"));
            model.setUser_id(rs.getLong("USER_ID"));
            model.setAccount(rs.getString("ACCOUNT"));
            model.setPassword(rs.getString("PASSWORD"));
            model.setCreateUser(rs.getLong("CREATE_USER"));
            model.setCreateTime(rs.getTimestamp("CREATE_TIME"));
            model.setModifyUser(rs.getLong("MODIFY_USER"));
            model.setModifyTime(rs.getTimestamp("MODIFY_TIME"));
            model.setBlocked(rs.getLong("BLOCKED"));
            model.setRetryCount(rs.getLong("RETRY"));
            model.setPassword_first(rs.getString("PASSWORD_FIRST"));
            model.setPassword_second(rs.getString("PASSWORD_SECOND"));
            model.setPassword_third(rs.getString("PASSWORD_THIRD"));
            model.setPassword_fourth(rs.getString("PASSWORD_FOURTH"));

            return model;
        }

    }

}
