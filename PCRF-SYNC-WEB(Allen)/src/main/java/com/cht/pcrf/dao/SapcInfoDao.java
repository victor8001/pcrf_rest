package com.cht.pcrf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cht.pcrf.utils.SystemUtility;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.SapcInfo;

/**
 * <code>SapcInfoDao</code>:
 * @author $Author: weilinchu $
 * @version $Id: SapcInfoDao.java 365 2016-03-02 03:01:31Z weilinchu $
 */
@Repository
public class SapcInfoDao extends BaseDAO<SapcInfo> {
    private static Logger logger = LoggerFactory.getLogger(SapcInfoDao.class);

    public List<SapcInfo> findAll(PageControler pageControler) {
        String pcrf_info = getTableName();
        final String sql = "SELECT row_number() OVER (ORDER BY PCRF_INFO.ID ASC) AS r, "
        				 + "PCRF_INFO.* FROM "+pcrf_info+" PCRF_INFO "
        				 + "WHERE PCRF_INFO.STATUS !='2' "
        				 + "ORDER BY PCRF_INFO.ID ASC";
        logger.debug("Sql Statement: "+sql);
        return super.queryForList(pageControler, sql, new HashMap());
    }

    public SapcInfo findByNodeName(String nodeName) {
        String pcrf_info = getTableName();
        final String sql = "SELECT * FROM "+pcrf_info+" WHERE NODE_NAME = :NODE_NAME";
        Map paramMap = new HashMap<String, Object>();
        paramMap.put("NODE_NAME", nodeName);
        List<SapcInfo> resultList = super.queryForList(sql, paramMap);
        return (resultList != null && !resultList.isEmpty()) ? resultList.get(0) : null;
    }
    
    public SapcInfo findById(String id) {
        String pcrf_info = getTableName();
        final String sql = "SELECT * FROM "+pcrf_info+" WHERE ID =:ID ";
        Map paramMap = new HashMap<String, Object>();
        paramMap.put("ID", Long.parseLong(id));
        List<SapcInfo> resultList = super.queryForList(sql, paramMap);
        return (resultList != null && !resultList.isEmpty()) ? resultList.get(0) : null;
    }
    

    public void updateSapc(SapcInfo sapcInfo) {
        String pcrf_info = getTableName();
        final StringBuffer sql = new StringBuffer("UPDATE "+pcrf_info+" SET "
                + " NODE_HOST=:NODE_HOST, NODE_PORT=:NODE_PORT"
                + ", LOGIN_ID=:LOGIN_ID, STATUS=:STATUS, LAST_SYNC_TIME=:LAST_SYNC_TIME"
                + ", CREATE_USER=:CREATE_USER, MODIFY_USER=:MODIFY_USER"
                + ", CREATE_TIME=:CREATE_TIME, MODIFY_TIME=:MODIFY_TIME"
                + ", SEQUENCE=:SEQUENCE");

        Map paramMap = new HashMap<String, Object>();
        paramMap.put("NODE_HOST", sapcInfo.getNodeHost());
        paramMap.put("NODE_PORT", sapcInfo.getNodePort());
        paramMap.put("LOGIN_ID", sapcInfo.getLoginId());
        paramMap.put("STATUS", sapcInfo.getStatus());
        paramMap.put("LAST_SYNC_TIME", sapcInfo.getLastSyncTime());
        paramMap.put("CREATE_USER", sapcInfo.getCreateUser());
        paramMap.put("MODIFY_USER", sapcInfo.getModifyUser());
        paramMap.put("CREATE_TIME", sapcInfo.getCreateTime());
        paramMap.put("MODIFY_TIME", sapcInfo.getModifyTime());
        paramMap.put("SEQUENCE", sapcInfo.getSequence());

        if(StringUtils.isNotBlank(sapcInfo.getLoginPw())) {
            sql.append(", LOGIN_PW=:LOGIN_PW");
            paramMap.put("LOGIN_PW", sapcInfo.getLoginPw());
        }

        sql.append(" WHERE NODE_NAME=:NODE_NAME");
        paramMap.put("NODE_NAME", sapcInfo.getNode_name());

        super.update(sql.toString(), paramMap);
    }

    public void saveSapc(SapcInfo sapcInfo) {
        String pcrf_info = getTableName();
        final String sql = "INSERT INTO "+pcrf_info+" (ID, LOCATION, NODE_NAME, NODE_HOST, NODE_PORT"
                + ", LOGIN_ID, LOGIN_PW, STATUS, LAST_SYNC_TIME, CREATE_USER, MODIFY_USER"
                + ", CREATE_TIME, MODIFY_TIME, SEQUENCE) "
                + "VALUES (:ID, :LOCATION, :NODE_NAME, :NODE_HOST, :NODE_PORT, :LOGIN_ID, :LOGIN_PW"
                + ", :STATUS, :LAST_SYNC_TIME, :CREATE_USER, :MODIFY_USER"
                + ", :CREATE_TIME, :MODIFY_TIME, :SEQUENCE)";

        Map paramMap = new HashMap<String, Object>();
        paramMap.put("ID", sapcInfo.getId());
        paramMap.put("LOCATION", sapcInfo.getLocation());
        paramMap.put("NODE_NAME", sapcInfo.getNode_name());
        paramMap.put("NODE_HOST", sapcInfo.getNodeHost());
        paramMap.put("NODE_PORT", sapcInfo.getNodePort());
        paramMap.put("LOGIN_ID", sapcInfo.getLoginId());
        paramMap.put("LOGIN_PW", sapcInfo.getLoginPw());
        paramMap.put("STATUS", sapcInfo.getStatus());
        paramMap.put("LAST_SYNC_TIME", sapcInfo.getLastSyncTime());
        paramMap.put("CREATE_USER", sapcInfo.getCreateUser());
        paramMap.put("MODIFY_USER", sapcInfo.getModifyUser());
        paramMap.put("CREATE_TIME", sapcInfo.getCreateTime());
        paramMap.put("MODIFY_TIME", sapcInfo.getModifyTime());
        paramMap.put("SEQUENCE", sapcInfo.getSequence());

        super.insert(sql, paramMap);
    }

    
    public void updateSapcStatusById(Long id, int status) {
        String pcrf_info = getTableName();
        final String sql = "UPDATE "+pcrf_info+" SET STATUS=:STATUS WHERE ID = :ID";
        Map paramMap = new HashMap<String, Object>();

        paramMap.put("ID", id);
        paramMap.put("STATUS", status);

        super.update(sql, paramMap);
    }


    private class SapcInfoRowMapper extends BasicRowMapper<SapcInfo> {
        @Override
        public SapcInfo mapColumnValue(ResultSet rs) throws SQLException {

            SapcInfo model = new SapcInfo();
            model.setId(rs.getLong("ID"));
            model.setLocation(rs.getString("LOCATION"));
            model.setNode_name(rs.getString("NODE_NAME"));
            model.setNodeHost(rs.getString("NODE_HOST"));
            model.setNodePort(rs.getString("NODE_PORT"));
            model.setLoginId(rs.getString("LOGIN_ID"));
            model.setLoginPw(rs.getString("LOGIN_PW"));
            model.setStatus(rs.getInt("STATUS"));
            model.setLastSyncTime(rs.getTimestamp("LAST_SYNC_TIME"));
            model.setCreateUser(rs.getLong("CREATE_USER"));
            model.setModifyUser(rs.getLong("MODIFY_USER"));
            model.setCreateTime(rs.getTimestamp("CREATE_TIME"));
            model.setModifyTime(rs.getTimestamp("MODIFY_TIME"));
            model.setSequence(rs.getInt("SEQUENCE"));

            return model;
        }
    }

    @Override
    public BasicRowMapper<SapcInfo> getRowMapper() {
        return new SapcInfoRowMapper();
    }

    private String getTableName(){
        Boolean isRest = (SystemUtility.getConfig("db.isRestSuffix")==null)
                ? false : Boolean.parseBoolean(SystemUtility.getConfig("db.isRestSuffix"));
        String pcrf_info = "PCRF_INFO"+(isRest?"_REST":"");
        return pcrf_info;
    }
}
