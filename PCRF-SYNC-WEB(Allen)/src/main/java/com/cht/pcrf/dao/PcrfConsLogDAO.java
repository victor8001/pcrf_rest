package com.cht.pcrf.dao;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cht.pcrf.utils.SystemUtility;
import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.PcrfConsistentLog;

@Repository
public class PcrfConsLogDAO extends BaseDAO {
	
	public List<Map> findByLocStartTimeResult(PageControler pageControler, String location,String startTime,String endTime ,String result){
	    Boolean isRest = (SystemUtility.getConfig("db.isRestSuffix")==null)
                ? false : Boolean.parseBoolean(SystemUtility.getConfig("db.isRestSuffix"));
        String consistent = "PCRF_CONSISTENT_LOG"+(isRest?"_REST":"");
        String pcrfInfo = "PCRF_INFO"+(isRest?"_REST":"");
		String sql ="SELECT row_number() OVER (ORDER BY L.ID DESC) AS r,"
		            +"L.ID, P.LOCATION , P.NODE_NAME, "
					+"L.EXEC_START_TIME,L.EXEC_END_TIME,L.EXEC_RESULT,L.RPT_PATH "
		            +",L.SYNC_RESULT, L.IS_QCI "
					+"FROM "+consistent+" L "
					+"     LEFT JOIN "+pcrfInfo+" P "
					+"     ON P.ID=L.PCRF_ID "
					+"WHERE L.EXEC_START_TIME BETWEEN :START and :END "
                    +"AND exec_result != 4 ";
		
	    
        if(location != null && !"".equals(location) && !"-1".equals(location))
            sql += "AND P.LOCATION = :LOCATION ";
        
        
		if(result != null && !"".equals(result) && !"-1".equals(result))
		    sql += "AND L.EXEC_RESULT =:EXEC_RESULT ";

		sql += " ORDER BY L.ID DESC ";

		Map params = new HashMap();
		params.put("START", Timestamp.valueOf(startTime));
		params.put("END", Timestamp.valueOf(endTime));


        if(location != null && !"".equals(location) && !"-1".equals(location))
            params.put("LOCATION", location);

        if(result != null && !"".equals(result) && !"-1".equals(result))
            params.put("EXEC_RESULT", Integer.valueOf(result));
        logger.debug("SQL Statement: "+sql);

		return queryForList(pageControler, sql,params);
	}


    public Map<String,Object> findById(Long id) {
        Boolean isRest = (SystemUtility.getConfig("db.isRestSuffix")==null)
                ? false : Boolean.parseBoolean(SystemUtility.getConfig("db.isRestSuffix"));
        String consistent_log = "PCRF_CONSISTENT_LOG"+(isRest?"_REST":"");

        String sql = "SELECT * FROM "+consistent_log+" P WHERE P.ID = :ID ";

        Map<String,Object> params = new HashMap<String,Object>();
        params.put("ID", id);
        
        List<Map<String,Object>> datas = queryForList(sql, params);
        if(datas != null && datas.size() > 0)
            return datas.get(0);
        
        return null;
    }

    
    
    @Override
    public BasicRowMapper getRowMapper() {
        
        return null;
    }
}
