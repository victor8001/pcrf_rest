package com.cht.pcrf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.model.SysRoleUser;

/**
 * <code>SysRoleUserDAO</code>:
 * @author $Author: weilinchu $
 * @version $Id: SysRoleUserDAO.java 313 2016-01-27 17:33:39Z weilinchu $
 */
@Repository
public class SysRoleUserDAO extends BaseDAO<SysRoleUser> {

    private static final Logger logger = LoggerFactory
            .getLogger(SysRoleUserDAO.class);

    public List<SysRoleUser> findByRoleId(Long roleId) {
        final String sql = "SELECT * FROM SYS_ROLE_USER WHERE ROLE_ID = :ROLE_ID ORDER BY ROLE_ID,USER_ID ";

        Map paramMap = new HashMap<String, Object>();
        paramMap.put("ROLE_ID", roleId);

        return super.queryForList(sql, paramMap);
    }

    public List<SysRoleUser> findByUserId(Long userId) {
        final String sql = "SELECT * FROM SYS_ROLE_USER WHERE USER_ID = :USER_ID";

        Map paramMap = new HashMap<String, Object>();
        paramMap.put("USER_ID", userId);

        return super.queryForList(sql, paramMap);
    }

    public void saveSysRoleUser(List<SysRoleUser> sysRoleUsers) {
        final String sql = "INSERT INTO SYS_ROLE_USER (ROLE_ID, USER_ID, MODIFY_USER, MODIFY_TIME) "
                + "VALUES (?, ?, ?, ?)";

        List<Object[]> dataList = new ArrayList<Object[]>();

        for(SysRoleUser sysRoleUser : sysRoleUsers) {
//            Map paramMap = new HashMap<String, Object>();
//            paramMap.put("ROLE_ID", sysRoleUser.getRoleId());
//            paramMap.put("USER_ID", sysRoleUser.getUserId());
//            paramMap.put("MODIFY_USER", sysRoleUser.getModifyUser());
//            paramMap.put("MODIFY_TIME", sysRoleUser.getModifyTime());

            dataList.add(new Object[]{sysRoleUser.getRoleId(), sysRoleUser.getUserId()
                    , sysRoleUser.getModifyUser(), sysRoleUser.getModifyTime()});
        }

        super.batchUpdate(sql, dataList);
    }

    public void updateSysRole(SysRoleUser sysRoleUser) {
        final String sql = "UPDATE SYS_ROLE_USER SET USER_ID = :USER_ID"
                + ", MODIFY_USER = :MODIFY_USER, MODIFY_TIME = :MODIFY_TIME"
                + " WHERE ROLE_ID = :ROLE_ID";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ROLE_ID", sysRoleUser.getRoleId());
        paramMap.put("USER_ID", sysRoleUser.getUserId());
        paramMap.put("MODIFY_USER", sysRoleUser.getModifyUser());
        paramMap.put("MODIFY_TIME", sysRoleUser.getModifyTime());

        super.update(sql, paramMap);
    }

    public void deleteSysRoleUserByRoleId(Long roleId) {
        final String sql = "DELETE FROM SYS_ROLE_USER WHERE ROLE_ID = :ROLE_ID";

        Map<String,Object> deleteMap = new HashMap<String, Object>();
        deleteMap.put("ROLE_ID", roleId);

        super.delete(sql, deleteMap);
    }

    public void deleteSysRoleUserByUserId(Map<String,Object> deleteUserData) {
        final String sql = "DELETE FROM SYS_ROLE_USER WHERE USER_ID = :USER_ID";
        super.delete(sql, deleteUserData);
    }

    public void deleteSysRoleUserByUserId(Long userId) {
        final String sql = "DELETE FROM SYS_ROLE_USER WHERE USER_ID = :USER_ID";

        Map<String,Object> deleteMap = new HashMap<String, Object>();
        deleteMap.put("USER_ID", userId);

        super.delete(sql, deleteMap);
    }

    private class SysRoleUserRowMapper extends BasicRowMapper<SysRoleUser> {
        @Override
        public SysRoleUser mapColumnValue(ResultSet rs) throws SQLException {

            SysRoleUser model = new SysRoleUser();
            model.setRoleId(rs.getLong("ROLE_ID"));
            model.setUserId(rs.getLong("USER_ID"));
            model.setModifyUser(rs.getLong("MODIFY_USER"));
            model.setModifyTime(rs.getTimestamp("MODIFY_TIME"));

            return model;
        }
    }

    @Override
    public BasicRowMapper<SysRoleUser> getRowMapper() {
        return new SysRoleUserRowMapper();
    }

}
