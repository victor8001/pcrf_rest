package com.cht.pcrf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.model.SysRoleFunc;

/**
 * <code>SysRoleFuncDAO</code>:
 * @author $Author: weilinchu $
 * @version $Id: SysRoleFuncDAO.java 305 2016-01-27 05:49:53Z weilinchu $
 */
@Repository
public class SysRoleFuncDAO extends BaseDAO<SysRoleFunc> {

    public List<SysRoleFunc> findAll() {
        final String sql = "SELECT * FROM SYS_ROLE_FUNC";
        return super.queryForList(sql);
    }

    public List<SysRoleFunc> findByRoleId(Long roleId) {
        final String sql = "SELECT * FROM SYS_ROLE_FUNC WHERE ROLE_ID = :ROLE_ID ORDER BY ROLE_ID,FUNC_ID ";

        Map<String,Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ROLE_ID", roleId);

        return super.queryForList(sql, paramMap);
    }

    public void saveSysRoleFunc(List<SysRoleFunc> sysRoleFuncs) {
        final String sql = "INSERT INTO SYS_ROLE_FUNC (ROLE_ID, FUNC_ID, MODIFY_USER, MODIFY_TIME) "
                + "VALUES (?, ?, ?, ?)";

        List<Object[]> dataList = new ArrayList<Object[]>();

        for(SysRoleFunc sysRoleFunc : sysRoleFuncs) {

            dataList.add(new Object[]{sysRoleFunc.getRoleId(), sysRoleFunc.getFuncId()
                    , sysRoleFunc.getModifyUser(), sysRoleFunc.getModifyTime()});
        }

        super.batchUpdate(sql, dataList);
    }

    public void deleteSysRoleFuncByRoleId(Long roleId) {
        final String sql = "DELETE FROM SYS_ROLE_FUNC WHERE ROLE_ID = :ROLE_ID";

        Map<String,Object> deleteMap = new HashMap<String, Object>();
        deleteMap.put("ROLE_ID", roleId);

        super.delete(sql, deleteMap);
    }

    private class SysRoleFuncRowMapper extends BasicRowMapper<SysRoleFunc> {
        @Override
        public SysRoleFunc mapColumnValue(ResultSet rs) throws SQLException {

            SysRoleFunc model = new SysRoleFunc();
            model.setRoleId(rs.getLong("ROLE_ID"));
            model.setFuncId(rs.getLong("FUNC_ID"));
            model.setModifyUser(rs.getLong("MODIFY_USER"));
            model.setModifyTime(rs.getTimestamp("MODIFY_TIME"));

            return model;
        }
    }

    @Override
    public BasicRowMapper<SysRoleFunc> getRowMapper() {
        return new SysRoleFuncRowMapper();
    }

}
