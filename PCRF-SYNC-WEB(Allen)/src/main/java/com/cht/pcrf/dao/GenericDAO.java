package com.cht.pcrf.dao;

import com.cht.pcrf.common.BasicRowMapper;


public interface GenericDAO<T> {
    
    public BasicRowMapper<T> getRowMapper();
    
}
