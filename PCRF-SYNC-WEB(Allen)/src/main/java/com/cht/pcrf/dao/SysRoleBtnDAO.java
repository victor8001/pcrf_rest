package com.cht.pcrf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.model.SysRoleBtn;

/**
 * <code>SysRoleBtnDAO</code>:
 * @author $Author: weilinchu $
 * @version $Id: SysRoleBtnDAO.java 305 2016-01-27 05:49:53Z weilinchu $
 */
@Repository
public class SysRoleBtnDAO extends BaseDAO<SysRoleBtn> {

    public List<SysRoleBtn> findAll() {
        final String sql = "SELECT * FROM SYS_ROLE_BTN";
        return super.queryForList(sql);
    }

    public List<SysRoleBtn> findByRoleId(Long roleId) {
        final String sql = "SELECT * FROM SYS_ROLE_BTN WHERE ROLE_ID = :ROLE_ID ORDER BY ROLE_ID,FUNC_ID,BTN_ID ";

        Map<String,Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ROLE_ID", roleId);

        return super.queryForList(sql, paramMap);
    }

    public List<SysRoleBtn> findByBtnId(Long btnId) {
        final String sql = "SELECT * FROM SYS_ROLE_BTN WHERE BTN_ID = :BTN_ID";

        Map<String,Object> paramMap = new HashMap<String, Object>();
        paramMap.put("BTN_ID", btnId);

        return super.queryForList(sql, paramMap);
    }

    public void saveSysRoleBtn(List<SysRoleBtn> sysRoleBtns) {
        final String sql = "INSERT INTO SYS_ROLE_BTN (ROLE_ID, FUNC_ID, BTN_ID, MODIFY_USER, MODIFY_TIME) "
                + "VALUES (?, ?, ?, ?, ?)";

        List<Object[]> dataList = new ArrayList<Object[]>();

        for(SysRoleBtn sysRoleBtn : sysRoleBtns) {

            dataList.add(new Object[]{sysRoleBtn.getRoleId(), sysRoleBtn.getFuncId(), sysRoleBtn.getBtnId()
                    , sysRoleBtn.getModifyUser(), sysRoleBtn.getModifyTime()});
        }

        super.batchUpdate(sql, dataList);
    }

    public void deleteSysRoleBtnByRoleId(Long roleId) {
        final String sql = "DELETE FROM SYS_ROLE_BTN WHERE ROLE_ID = :ROLE_ID";

        Map<String,Object> deleteMap = new HashMap<String, Object>();
        deleteMap.put("ROLE_ID", roleId);

        super.delete(sql, deleteMap);
    }

    private class SysRoleBtnRowMapper extends BasicRowMapper<SysRoleBtn> {
        @Override
        public SysRoleBtn mapColumnValue(ResultSet rs) throws SQLException {

            SysRoleBtn model = new SysRoleBtn();
            model.setRoleId(rs.getLong("ROLE_ID"));
            model.setFuncId(rs.getLong("FUNC_ID"));
            model.setBtnId(rs.getLong("BTN_ID"));
            model.setModifyUser(rs.getLong("MODIFY_USER"));
            model.setModifyTime(rs.getTimestamp("MODIFY_TIME"));

            return model;
        }
    }

    @Override
    public BasicRowMapper<SysRoleBtn> getRowMapper() {
        return new SysRoleBtnRowMapper();
    }

}
