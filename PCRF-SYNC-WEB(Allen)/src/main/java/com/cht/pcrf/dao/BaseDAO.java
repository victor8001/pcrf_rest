package com.cht.pcrf.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.common.MapRowMapper;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.core.common.AlarmConstants;


@Component
public abstract class BaseDAO<T> implements GenericDAO<T>{

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
	public BaseDAO() {}
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<T> queryForList(String sqlStatement) {
		return queryForList(null,sqlStatement);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> queryForList(PageControler pageControler,String sqlStatement) {

	    try{
    		// 列印SQL參數
    		printSqlParam(sqlStatement);
    		
    		BasicRowMapper rowMapper = getBasicRowMapper(pageControler);
    
    		// 無查詢分頁時處理
    		if (rowMapper.isAllRead()) {
    			return (List<T>) getJdbcTemplate().query(sqlStatement,new RowMapperResultSetExtractor(rowMapper));
    		}
    		// 有查詢分頁時處理
    		else {
    
    			//查詢頁數
    			queryPageCount(pageControler, rowMapper, sqlStatement, null);
    			
    			logger.debug("CurrentPageStartRow: "+pageControler.getCurrentPageStartRow());
    			logger.debug("CurrentPageEndRow: "+pageControler.getCurrentPageEndRow());
    			
    			StringBuilder stbSQL = new StringBuilder()
                    .append(" SELECT * FROM (")
                    .append(sqlStatement)
                    .append(") TAB ")
                    .append(" WHERE r >= "+ pageControler.getCurrentPageStartRow())
                    .append(" AND r <= "+ pageControler.getCurrentPageEndRow());
            
    			List<T> lstResult = (List<T>) getJdbcTemplate().query(stbSQL.toString(),
                      new RowMapperResultSetExtractor(rowMapper));
    
    			// 移除非顯示資料
    			List<Map> listNull = new ArrayList<Map>();
    			listNull.add(null);
    
    			lstResult.removeAll(listNull);
    
    			return lstResult;
    		}
	    }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "e:" + e);
	        throw e;
	    }
		
	}

	private BasicRowMapper getBasicRowMapper(PageControler pageControler) {

        try{
    		BasicRowMapper rowMapper = (BasicRowMapper) getRowMapper();//new BasicRowMapper();
    		if(rowMapper == null)
    		    rowMapper = new MapRowMapper();
    
    		// 分頁處理
    		if (pageControler != null) {
    			rowMapper.setPageStartRow(pageControler.getCurrentPageStartRow() - 1);
    			rowMapper.setPageEndRow(pageControler.getCurrentPageEndRow() - 1);
    		}
    		else {
    			rowMapper.setAllRead(true);
    		}
            
            return rowMapper;
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "e:" + e);
            throw e;
        }
	}

	@SuppressWarnings("unchecked")
	public List<T> queryForList(String sqlStatement,String[] params) {

		printSqlParam(sqlStatement,params);
		return queryForList(null,sqlStatement,params);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> queryForList(PageControler pageControler,String sqlStatement,String[] params) {

        try{
            printSqlParam(sqlStatement,params);
    		
    		BasicRowMapper rowMapper = getBasicRowMapper(pageControler);
    		
    		// 無查詢分頁時處理
    		if (rowMapper.isAllRead()) {
    			return (List<T>) getJdbcTemplate().query(sqlStatement, params, new RowMapperResultSetExtractor(rowMapper)); 
    		}
    		// 有查詢分頁時處理
    		else {
    
    			//查詢頁數
    			queryPageCount(pageControler, rowMapper, sqlStatement, params);
    
                logger.debug("CurrentPageStartRow: "+pageControler.getCurrentPageStartRow());
                logger.debug("CurrentPageEndRow: "+pageControler.getCurrentPageEndRow());
                
                StringBuilder stbSQL = new StringBuilder()
                    .append(" SELECT * FROM (")
                    .append(sqlStatement)
                    .append(") TAB ")
                    .append(" WHERE r >= "+ pageControler.getCurrentPageStartRow())
                    .append(" AND r <= "+ pageControler.getCurrentPageEndRow());
            
    			List<T> lstResult = (List<T>) getJdbcTemplate().query(stbSQL.toString(), params,
                        new RowMapperResultSetExtractor(rowMapper));
    			
    			// 移除非顯示資料
    			List<Map> listNull = new ArrayList<Map>();
    			listNull.add(null);
    
    			lstResult.removeAll(listNull);
    
    			return lstResult;
    		}
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "e:" + e);
            throw e;
        }
		
	}

	@SuppressWarnings("unchecked")
	public List<T> queryForList(String sqlStatement,Map params) {
		return queryForList(null,sqlStatement,params);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> queryForList(PageControler pageControler,String sqlStatement,Map params) {

	    try{
    		// 列印SQL參數
    		printSqlParam(sqlStatement,params);
    		
    		BasicRowMapper rowMapper = getBasicRowMapper(pageControler);
    		NamedParameterJdbcTemplate namedParameterJdbcTemplate = 
    			new NamedParameterJdbcTemplate(getJdbcTemplate().getDataSource());
    
    		// 無查詢分頁時處理
    		if (rowMapper.isAllRead()) {
    			return (List<T>) namedParameterJdbcTemplate.query(sqlStatement, params, new RowMapperResultSetExtractor(rowMapper)); 
    		}
    		// 有查詢分頁時處理
    		else {
    			//查詢頁數
    			queryPageCount(pageControler, rowMapper, sqlStatement, params);
    
    	        StringBuilder stbSQL = new StringBuilder()
    	            .append(" SELECT * FROM (")
    	            .append(sqlStatement)
    	            .append(") TAB ")
    	            .append(" WHERE r >= "+ pageControler.getCurrentPageStartRow())
                    .append(" AND r <= "+ pageControler.getCurrentPageEndRow());
    	        
    			
    			List<T> lstResult = (List<T>) namedParameterJdbcTemplate.query(stbSQL.toString(), params,
    					new RowMapperResultSetExtractor(rowMapper));
    			
    			// 移除非顯示資料
    			List<Map> listNull = new ArrayList<Map>();
    			listNull.add(null);
    
    			lstResult.removeAll(listNull);
    
    			return lstResult;
    		}
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "e:" + e);
            throw e;
        }
	}

	private void queryPageCount(PageControler pageControler,BasicRowMapper rowMapper,
			String sqlStatement, Object params) {

	    try{
    		String strSqlStatementTemp = sqlStatement;
    		
    		// 因SQL Server 2005不支援[ORDER BY 子句在檢視、內嵌函數、衍生資料表、子查詢及一般資料表運算式中均為無效。]
    		// 因此統計筆數時，先將ORDER BY去除
    		// 刪除原排序欄位
    		if (sqlStatement.toUpperCase().lastIndexOf("ORDER BY") > 0) {
    
    			strSqlStatementTemp = sqlStatement.substring(0, sqlStatement
    				.toUpperCase().lastIndexOf("ORDER BY"));
    		}
    		
    		// 統計查詢結果總筆數
    		StringBuilder stbSQL = new StringBuilder()
    			.append(" SELECT COUNT(*) FROM (")
    			.append(strSqlStatementTemp)
    			.append(") TAB ");
    
    		logger.debug("＊＊ 查詢總筆數 SqlStatement ＊＊-->" + stbSQL);
    
    		// 查詢資料總筆數
    		int intTotalCount = 0;
    		
    		if(params instanceof Map){
    			intTotalCount = ((Integer) queryForInt(stbSQL.toString(),
    				(Map) params));
    		}else if(params instanceof String[]){
    //			intTotalCount = ((Integer) queryForInt(stbSQL.toString(),
    //					(String[]) params));
    		}else if(params == null){
                intTotalCount = ((Integer) queryForInt(stbSQL.toString(),null));
    		}
    
    		// 取得當前頁數
    //		int pageNo = pageControler.getCurrentPage();
    
    		// 設定總筆數
    		pageControler.setTotalRowCount(intTotalCount);
    		
    		rowMapper.setPageStartRow(pageControler.getCurrentPageStartRow() - 1);
    		rowMapper.setPageEndRow(pageControler.getCurrentPageEndRow() - 1);
    		
    		logger.debug("＊＊ 查詢總筆數 totalCount ＊＊-->" + intTotalCount);

        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "e:" + e);
            throw e;
        }
	}


	public Integer queryForInt(String sql, Map<String, Object> params) {

		Integer result;

		try {

			// 列印SQL參數
			printSqlParam(sql,params);

			// 執行查詢sql
			NamedParameterJdbcTemplate namedParameterJdbcTemplate = 
				new NamedParameterJdbcTemplate(getJdbcTemplate().getDataSource());
			
			result = namedParameterJdbcTemplate.queryForInt(sql,params);

		}catch (DataAccessException dae) {
		    logger.error(AlarmConstants.DB_EX + "e:" + dae);
			throw dae;
		}catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "e:" + e);
            throw e;
        }

		return result;
	}

	public void delete(String sql,Object[] params) {
		execUpdate(sql,params);
	}
	
	public int delete(String sql,Map<String,Object> params) {
		return execUpdate(sql,params);
	}

	public void insert(String sql,Map<String,Object> params) {
		execUpdate(sql,params);
	}
	
	public void update(String sql,Map<String,Object> params) {
		execUpdate(sql,params);
	}

	public void execUpdate(String sql,Object[] params) {

        try {
    		// 列印SQL參數
    		printSqlParam(sql,params);
    		
    		getJdbcTemplate().update(sql, params);
        }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "e:" + e);
            throw e;
        }
	}
	
	public int execUpdate(String sql,Map<String,Object> params) {
	    try{
    		// 列印SQL參數
    		printSqlParam(sql,params);
    		
    		NamedParameterJdbcTemplate namedParameterJdbcTemplate = 
    			new NamedParameterJdbcTemplate(getJdbcTemplate().getDataSource());
    		return namedParameterJdbcTemplate.update(sql, params);
    	}catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "e:" + e);
            throw e;
        }
	}

	private void printSqlParam(String sql) {
		// TODO Auto-generated method stub

		logger.debug(" ＊＊＊ SQL ＊＊＊ = " + sql);

	}
	
	private void printSqlParam(String sql,Map<String, Object> params) {
		// TODO Auto-generated method stub

		logger.debug(" ＊＊＊ SQL ＊＊＊ = " + sql);

		logger.debug(" ＊＊＊ SQL Param Start ＊＊＊  ");
		
		if(params != null){
			for (String sqlParam : params.keySet()) {
				
				logger.debug("Param " + sqlParam + " := " + params.get(sqlParam));
	
			}
		}
		
		logger.debug(" ＊＊＊ SQL Param End ＊＊＊  ");
	}

	private void printSqlParam(String sql,Object[] params) {

		logger.debug(" ＊＊＊ SQL ＊＊＊ = " + sql);

		logger.debug(" ＊＊＊ SQL Param Start ＊＊＊  ");
		
		if(params != null){
			int i=0;
			for (Object sqlParam : params) {
				
				logger.debug("Param " + i + " := " + sqlParam.toString());
				i++;
			}
		}
		
		logger.debug(" ＊＊＊ SQL Param End ＊＊＊  ");
	}

	public void batchUpdate(String sql, List<Object[]> dataList) {

		execBatchUpdate(sql,dataList);
	}
	
	@SuppressWarnings("unchecked")
	private void execBatchUpdate(String sql, List<Object[]> dataList) {

	    try{
    //		// 變數宣告
    //		boolean returnValue = false;
    //
    //		try {
    
    	    JdbcTemplate jdbc = new JdbcTemplate(getJdbcTemplate()
    					.getDataSource());
    			
    			// 列印SQL參數
    
    			logger.debug("＊＊ SqlStatement ＊＊-->" + sql);
    
    			//LOG 印好久
    			/*
    			for(Object[] sqlParams:dataList){
    				
    				logger.debug(" ＊＊＊ SQL Param "+intParamLine+" ＊＊＊  ");
    				
    				int intParam = 0;
    				
    				for (Object sqlParam : sqlParams) {
    
    					logger.debug("Param " + intParam + " := " + sqlParam);
    
    					intParam++;
    				}
    				logger.debug(" ＊＊＊ SQL Param "+intParamLine+" ＊＊＊  ");
    			}
    			*/
    
    			// 執行Sql update
    			int[] result = jdbc.batchUpdate(sql, dataList);
    			
    //		}
    //		catch (DataAccessException dae) {
    //			returnValue = false;
    //		}
    
    //		return returnValue;
	    }catch(Exception e){
            logger.error(AlarmConstants.DB_EX + "e:" + e);
            throw e;
        }
	}
	
}
