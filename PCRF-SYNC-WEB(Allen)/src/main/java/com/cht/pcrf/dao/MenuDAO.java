/*
 * File Name       : MenuDAO.java 											<p>
 * Function Number : MenuDAO.java											<p>
 * Module          : com.tiis.login.dao										<p>
 * Description     : 														<p>
 * Company         : 											<p>
 * Copyright       : Copyright c 2012									<p>
 * Author          : LauraChu												<p>
 * History         : V1.00 2012/7/30 LauraChu 									<p>
 * Version         : 1.00													<p>
 */
package com.cht.pcrf.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;

@Repository
public class MenuDAO extends BaseDAO{
	@SuppressWarnings("unchecked")
	public List<Map> getMenuData(Long userId){
		
		String sql =
			" SELECT DISTINCT FUNC.*,MENU.MENU_NAME "+
		    //" ,ROLE_USER.ROLE_ID "+
			" FROM SYS_ROLE_USER ROLE_USER "+
			" ,SYS_ROLE_FUNC ROLE_FUNC "+
			" ,SYS_FUNC FUNC "+
			" ,SYS_MENU MENU "+
			" WHERE "+
			" ROLE_FUNC.ROLE_ID = ROLE_USER.ROLE_ID "+
			" AND FUNC.FUNC_ID = ROLE_FUNC.FUNC_ID "+
			" AND FUNC.MENU_ID = MENU.MENU_ID "+
			" AND FUNC.IS_USED = 1 "+
			" AND MENU.IS_USED = 1 "+
			" AND ROLE_USER.USER_ID = :USER_ID "+
			" ORDER BY FUNC.SEQ_NO";
	
		Map params = new HashMap();
		params.put("USER_ID", userId);
		return queryForList(sql,params);
	}

    @SuppressWarnings("unchecked")
    public List<Map> getRoleFuncBtnDataByUserId(Long userId){
        
        String sql =
            " SELECT FUNC.FUNC_NAME,FUNC_BTN.BTN_NAME,FUNC_BTN.BTN_CNAME,FUNC_BTN.BTN_URL FROM SYS_ROLE_BTN ROLE_BTN "+
            " ,SYS_FUNC_BTN FUNC_BTN,SYS_FUNC FUNC,SYS_ROLE_USER ROLE_USER  "+
            " WHERE  ROLE_BTN.FUNC_ID = FUNC_BTN.FUNC_ID "+
            " AND  ROLE_BTN.BTN_ID = FUNC_BTN.BTN_ID "+
            " AND ROLE_BTN.FUNC_ID = FUNC.FUNC_ID "+
            " AND FUNC.IS_USED = 1 "+
            //" AND ROLE_BTN.ROLE_ID = :ROLE_ID "+
            " AND ROLE_BTN.ROLE_ID = ROLE_USER.ROLE_ID "+
            " AND FUNC_BTN.IS_USED = 1 "+
            //" AND ROLE_USER.ROLE_ID = :ROLE_ID "+
            " AND ROLE_USER.ROLE_ID = ROLE_USER.ROLE_ID "+
            " AND ROLE_USER.USER_ID = :USER_ID ";
    
        Map params = new HashMap();
        //params.put("ROLE_ID", roleId);
        params.put("USER_ID", userId);
        return queryForList(sql,params);
    }
    
    @Override
    public BasicRowMapper getRowMapper() {
        // TODO Auto-generated method stub
        return null;
    }
}
