package com.cht.pcrf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.model.ConfValueInfo;
import com.cht.pcrf.model.User;

@Repository
public class CommonDAO extends BaseDAO {

	@SuppressWarnings("unchecked")
    public List<ConfValueInfo> getConfValue(String nodeLocation){
        
        String sql ="SELECT v.* "
        			+"FROM CONF_CODE_INFO c,CONF_VALUE_INFO v "
        			+"WHERE c.CODE_ID = v.CODE_ID "
        			+"AND c.CODE_TYPE=:CODE_TYPE ORDER BY v.VALUE_TYPE";
    
        Map<String,Object> params = new HashMap<String, Object>();
        params.put("CODE_TYPE", nodeLocation);
        return super.queryForList(sql,params);
    }
	
	@SuppressWarnings("unchecked")
    public List<ConfValueInfo> getConfValueAndTypeNotNegativeOne(String nodeLocation){
        
        String sql ="SELECT v.* "
        			+"FROM CONF_CODE_INFO c,CONF_VALUE_INFO v "
        			+"WHERE c.CODE_ID = v.CODE_ID "
        			+"AND c.CODE_TYPE=:CODE_TYPE AND v.VALUE_TYPE != '-1' "
        			+"ORDER BY v.VALUE_TYPE";
    
        Map<String,Object> params = new HashMap<String, Object>();
        params.put("CODE_TYPE", nodeLocation);
        return super.queryForList(sql,params);
    }
	
	@Override
	public BasicRowMapper<ConfValueInfo> getRowMapper() {
		// TODO Auto-generated method stub
		return new ConfValueInfoRowMapper();
	}
    private class ConfValueInfoRowMapper extends BasicRowMapper<ConfValueInfo> {

        @Override
        public ConfValueInfo mapColumnValue(ResultSet rs) throws SQLException {

        	ConfValueInfo model = new ConfValueInfo();
        	model.setCodeId(rs.getLong("CODE_ID"));
        	model.setValueId(rs.getLong("VALUE_ID"));
        	model.setValueName(rs.getString("VALUE_NAME"));
        	model.setValueType(rs.getString("VALUE_TYPE"));
            

            return model;
        }

    }
	

}
