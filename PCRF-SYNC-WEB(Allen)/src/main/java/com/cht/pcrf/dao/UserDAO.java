package com.cht.pcrf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.User;

@Repository
public class UserDAO extends BaseDAO<User>{

    public List<User> queryByAccoundAndPw(String account, String md5pw) {
        final String sql =
                " SELECT * FROM SYS_USER "+
                " WHERE ACCOUNT = :ACCOUNT "+
                " AND PASSWORD = :PASSWORD ";

        Map<String,Object> params = new HashMap<String, Object>();
        params.put("ACCOUNT", account);
        params.put("PASSWORD", md5pw);

        return super.queryForList(sql,params);

    }


    public List<User> queryById(Long id) {
        final String sql =
                " SELECT * FROM SYS_USER "+
                " WHERE ID = :ID ";

        Map<String,Object> params = new HashMap<String, Object>();

        params.put("ID", id);

        return super.queryForList(sql,params);
    }


    public List<User> queryByAccount(String account) {
        final String sql =
                " SELECT * FROM SYS_USER "+
                " WHERE ACCOUNT = :ACCOUNT ";

        Map<String,Object> params = new HashMap<String, Object>();

        params.put("ACCOUNT", account);

        return super.queryForList(sql,params);
    }


    public List<User> queryOther(String account) {
        final String sql =
                " SELECT * FROM SYS_USER "+
                " WHERE ACCOUNT != :ACCOUNT ";

        Map<String,Object> params = new HashMap<String, Object>();

        params.put("ACCOUNT", account);

        return super.queryForList(sql,params);
    }


    public List<User> queryAllAccount(PageControler pageControler)
    {
        final String sql = " SELECT row_number() OVER (ORDER BY ACCOUNT) AS r,users.* FROM SYS_USER users ORDER BY ACCOUNT ";

        return super.queryForList(pageControler,sql);
    }


    @Override
    public BasicRowMapper<User> getRowMapper() {
        return new UserRowMapper();
    }

    public void addRetryCount(String account, Long retryCount) {
        final String sql = "update SYS_USER set RETRY = :RETRY where ACCOUNT = :ACCOUNT";
        Map<String,Object> params = new HashMap<String, Object>();

        params.put("ACCOUNT", account);
        params.put("RETRY", retryCount);

        super.update(sql, params);;
    }


    public void setBlocked(String account) {
        final String sql = "update SYS_USER set BLOCKED = 1 where ACCOUNT = :ACCOUNT";
        Map<String,Object> params = new HashMap<String, Object>();

        params.put("ACCOUNT", account);

        super.update(sql, params);;
    }


    public void resetRetryCount(String account) {
        final String sql = "update SYS_USER set RETRY = 0 where ACCOUNT = :ACCOUNT";
        Map<String,Object> params = new HashMap<String, Object>();

        params.put("ACCOUNT", account);

        super.update(sql, params);;
    }

    public void blockReset(Map<String,Object> updateData) {
        String sql = "update SYS_USER set BLOCKED = 0 where ACCOUNT = :ACCOUNT";
        super.update(sql, updateData);
    }


    private class UserRowMapper extends BasicRowMapper<User> {

        @Override
        public User mapColumnValue(ResultSet rs) throws SQLException {

            User model = new User();
            model.setUser_id(rs.getLong("USER_ID"));
            model.setAccount(rs.getString("ACCOUNT"));
            model.setPassword(rs.getString("PASSWORD"));
            model.setBlocked(rs.getLong("BLOCKED"));
            model.setRetryCount(rs.getLong("RETRY"));
            model.setPassword_first(rs.getString("PASSWORD_FIRST"));
            model.setPassword_second(rs.getString("PASSWORD_SECOND"));
            model.setPassword_third(rs.getString("PASSWORD_THIRD"));
            model.setPassword_fourth(rs.getString("PASSWORD_FOURTH"));
            model.setCreateUser(rs.getLong("CREATE_USER"));
            model.setCreateTime(rs.getTimestamp("CREATE_TIME"));
            model.setModifyUser(rs.getLong("MODIFY_USER"));
            model.setModifyTime(rs.getTimestamp("MODIFY_TIME"));

            return model;
        }

    }


    public void insert(Map<String,Object> inserData){
        
        String sql = " INSERT INTO SYS_USER (USER_ID, ACCOUNT, PASSWORD, CREATE_USER, CREATE_TIME, MODIFY_USER, MODIFY_TIME, BLOCKED )"
                + " VALUES (:USER_ID, :ACCOUNT, :PASSWORD, :CREATE_USER, :CREATE_TIME, :MODIFY_USER, :MODIFY_TIME, :BLOCKED ) ";

        super.insert(sql, inserData);
        
    }


    public void updatePassword(Map<String,Object> updateData) {

        String sql_first_change =
                "update SYS_USER set PASSWORD = :PASSWORD, PASSWORD_FIRST = :USER_PASSWORD ,MODIFY_TIME = :MODIFY_TIME, MODIFY_USER = :MODIFY_USER "
                + "where ACCOUNT = :ACCOUNT";
        String sql_second_change =
                "update SYS_USER set PASSWORD = :PASSWORD, PASSWORD_FIRST = :USER_PASSWORD, "
                + "PASSWORD_SECOND = :PASSWORD_FIRST, MODIFY_TIME = :MODIFY_TIME, MODIFY_USER = :MODIFY_USER where ACCOUNT = :ACCOUNT";
        String sql_third_change =
                "update SYS_USER set PASSWORD = :PASSWORD, PASSWORD_FIRST = :USER_PASSWORD, "
                + "PASSWORD_SECOND = :PASSWORD_FIRST, PASSWORD_THIRD = :PASSWORD_SECOND, "
                + "MODIFY_TIME = :MODIFY_TIME, MODIFY_USER = :MODIFY_USER where ACCOUNT = :ACCOUNT";
        String sql_fourth_change =
                "update SYS_USER set PASSWORD = :PASSWORD, PASSWORD_FIRST = :USER_PASSWORD, "
                + "PASSWORD_SECOND = :PASSWORD_FIRST, PASSWORD_THIRD = :PASSWORD_SECOND, "
                + "PASSWORD_FOURTH = :PASSWORD_THIRD, MODIFY_TIME = :MODIFY_TIME, MODIFY_USER = :MODIFY_USER where ACCOUNT = :ACCOUNT";

        if(null==updateData.get("PASSWORD_FIRST") || "".equals(updateData.get("PASSWORD_FIRST")))
            super.update(sql_first_change, updateData);
        else if(null==updateData.get("PASSWORD_SECOND") || "".equals(updateData.get("PASSWORD_SECOND")))
            super.update(sql_second_change, updateData);
        else if(null==updateData.get("PASSWORD_THIRD") || "".equals(updateData.get("PASSWORD_THIRD")))
            super.update(sql_third_change, updateData);
        else if(null==updateData.get("PASSWORD_FOURTH") || "".equals(updateData.get("PASSWORD_FOURTH")))
            super.update(sql_fourth_change, updateData);
        else
            super.update(sql_fourth_change, updateData);
    }


    public void deleteUser(Map<String,Object> deleteData) {
        String sql = "DELETE FROM SYS_USER WHERE ACCOUNT = :ACCOUNT";
        super.delete(sql, deleteData);
    }

    public void adminUpdatePassword(Map<String,Object> updateData) {

        String sql_first_change =
                "update SYS_USER set PASSWORD = :PASSWORD, PASSWORD_FIRST = :USER_PASSWORD where ACCOUNT = :ACCOUNT";
        String sql_second_change =
                "update SYS_USER set PASSWORD = :PASSWORD, PASSWORD_FIRST = :USER_PASSWORD, "
                + "PASSWORD_SECOND = :PASSWORD_FIRST where ACCOUNT = :ACCOUNT";
        String sql_third_change =
                "update SYS_USER set PASSWORD = :PASSWORD, PASSWORD_FIRST = :USER_PASSWORD, "
                + "PASSWORD_SECOND = :PASSWORD_FIRST, PASSWORD_THIRD = :PASSWORD_SECOND where ACCOUNT = :ACCOUNT";
        String sql_fourth_change =
                "update SYS_USER set PASSWORD = :PASSWORD, PASSWORD_FIRST = :USER_PASSWORD, "
                + "PASSWORD_SECOND = :PASSWORD_FIRST, PASSWORD_THIRD = :PASSWORD_SECOND, "
                + "PASSWORD_FOURTH = :PASSWORD_THIRD where ACCOUNT = :ACCOUNT";


        if(null==updateData.get("PASSWORD_FIRST") || "".equals(updateData.get("PASSWORD_FIRST")))
            super.update(sql_first_change, updateData);
        else if(null==updateData.get("PASSWORD_SECOND") || "".equals(updateData.get("PASSWORD_SECOND")))
            super.update(sql_second_change, updateData);
        else if(null==updateData.get("PASSWORD_THIRD") || "".equals(updateData.get("PASSWORD_THIRD")))
            super.update(sql_third_change, updateData);
        else if(null==updateData.get("PASSWORD_FOURTH") || "".equals(updateData.get("PASSWORD_FOURTH")))
            super.update(sql_fourth_change, updateData);
        else
            super.update(sql_fourth_change, updateData);
    }

}
