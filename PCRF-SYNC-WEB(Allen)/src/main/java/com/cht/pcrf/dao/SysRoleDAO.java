package com.cht.pcrf.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.SysRole;

/**
 * <code></code>:
 * @author $Author: weilinchu $
 * @version %Id$
 */
@Repository
public class SysRoleDAO extends BaseDAO<SysRole> {
    private static final Logger logger = LoggerFactory.getLogger(SysRoleDAO.class);

    public List<SysRole> findAll(PageControler pageControler) {
        final String sql = "SELECT row_number() OVER (ORDER BY s.ROLE_NAME) AS r, s.* FROM SYS_ROLE s ORDER BY s.ROLE_NAME ";
        return super.queryForList(pageControler, sql);
    }

    public SysRole findById(Long id) {
        final String sql = "SELECT * FROM SYS_ROLE WHERE ROLE_ID = :ROLE_ID ORDER BY ROLE_NAME ";
        Map paramMap = new HashMap<String, Object>();
        paramMap.put("ROLE_ID", id);
        List<SysRole> resultList = super.queryForList(sql, paramMap);
        return (resultList != null && !resultList.isEmpty()) ? resultList.get(0) : null;
    }

    public List<SysRole> findByRoleName(String roleName, PageControler pageControler) {
        final String sql = "SELECT row_number() OVER (ORDER BY ROLE_NAME) AS r, sys.* FROM SYS_ROLE sys WHERE ROLE_NAME like :ROLE_NAME ORDER BY ROLE_NAME ";
        Map paramMap = new HashMap<String, Object>();
        paramMap.put("ROLE_NAME", "%" + roleName + "%");
        List<SysRole> resultList = super.queryForList(pageControler, sql, paramMap);
        return resultList;
    }

    public void saveSysRole(SysRole sysRole) {
        final String sql = "INSERT INTO SYS_ROLE (ROLE_NAME, IS_USED, CREATE_USER, CREATE_TIME, MODIFY_USER, MODIFY_TIME) "
                + "VALUES (:ROLE_NAME, :IS_USED, :CREATE_USER, :CREATE_TIME, :MODIFY_USER, :MODIFY_TIME)";

        Map paramMap = new HashMap<String, Object>();
        paramMap.put("ROLE_NAME", sysRole.getRoleName());
//        paramMap.put("IS_USED", sysRole.isUsed());
        paramMap.put("IS_USED", sysRole.getIsUsedNumber());
        paramMap.put("CREATE_USER", sysRole.getCreateUser());
        paramMap.put("CREATE_TIME", sysRole.getCreateTime());
        paramMap.put("MODIFY_USER", sysRole.getModifyUser());
        paramMap.put("MODIFY_TIME", sysRole.getModifyTime());

        super.insert(sql, paramMap);
    }

    public void updateSysRole(SysRole sysRole) {
        final String sql = "UPDATE SYS_ROLE SET ROLE_NAME = :ROLE_NAME, IS_USED = :IS_USED"
                + ", CREATE_USER = :CREATE_USER, CREATE_TIME = :CREATE_TIME, MODIFY_USER = :MODIFY_USER, MODIFY_TIME = :MODIFY_TIME"
                + " WHERE ROLE_ID = :ROLE_ID";

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ROLE_ID", sysRole.getRoleId());
        paramMap.put("ROLE_NAME", sysRole.getRoleName());
//        paramMap.put("IS_USED", sysRole.isUsed());
        paramMap.put("IS_USED", sysRole.getIsUsedNumber());
        paramMap.put("CREATE_USER", sysRole.getCreateUser());
        paramMap.put("CREATE_TIME", sysRole.getCreateTime());
        paramMap.put("MODIFY_USER", sysRole.getModifyUser());
        paramMap.put("MODIFY_TIME", sysRole.getModifyTime());

        super.update(sql, paramMap);
    }

    public void deleteSysRoleByRoleId(Long roleId) {
        final String sql = "DELETE FROM SYS_ROLE WHERE ROLE_ID = :ROLE_ID";

        Map<String,Object> deleteMap = new HashMap<String, Object>();
        deleteMap.put("ROLE_ID", roleId);

        super.delete(sql, deleteMap);
    }

    private class SysRoleRowMapper extends BasicRowMapper<SysRole> {
        @Override
        public SysRole mapColumnValue(ResultSet rs) throws SQLException {

            SysRole model = new SysRole();
            model.setRoleId(rs.getLong("ROLE_ID"));
            model.setRoleName(rs.getString("ROLE_NAME"));
            model.setUsed(rs.getBoolean("IS_USED"));
            model.setCreateUser(rs.getLong("CREATE_USER"));
            model.setCreateTime(rs.getTimestamp("CREATE_TIME"));
            model.setModifyUser(rs.getLong("MODIFY_USER"));
            model.setModifyTime(rs.getTimestamp("MODIFY_TIME"));

            return model;
        }
    }

    @Override
    public BasicRowMapper<SysRole> getRowMapper() {
        return new SysRoleRowMapper();
    }

}
