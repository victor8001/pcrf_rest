/**
 * Project Name : PG-WEB
 * File Name 	: SysSeqDAO.java
 * Package Name : com.fet.pg.dao
 * Date 		: 2016年1月29日 
 * Author 		: LauraChu
 * Copyright (c) 2016 All Rights Reserved.
 */
package com.cht.pcrf.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.cht.pcrf.common.BasicRowMapper;

@Repository
public class SysSeqDAO extends BaseDAO{

    @SuppressWarnings("unchecked")
    public Long getSysUserSeq() throws Exception{
        
        String getUserIdSql = "SELECT  nextval('sys_user_user_id_seq') as ID ";
        List<Map> datas = queryForList(getUserIdSql);
        
        if(datas == null || datas.size() == 0)
            throw new Exception("GET SYS_USER_SEQ error !!");

        Map data = datas.get(0);
        return Long.parseLong((String) data.get("ID"));
    }
    
    @SuppressWarnings("unchecked")
    public Long getPcrfInfoIdSeq() throws Exception{
        
        String getUserIdSql = "SELECT  nextval('pcrf_info_id_seq') as ID ";
        List<Map> datas = queryForList(getUserIdSql);
        
        if(datas == null || datas.size() == 0)
            throw new Exception("GET PCRF_INFO_ID error !!");

        Map data = datas.get(0);
        return Long.parseLong((String) data.get("ID"));
    }
    
    @Override
    public BasicRowMapper getRowMapper() {
        // TODO Auto-generated method stub
        return null;
    }
}
