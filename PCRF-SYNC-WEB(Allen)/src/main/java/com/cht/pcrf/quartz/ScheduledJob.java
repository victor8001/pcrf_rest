package com.cht.pcrf.quartz;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.cht.pcrf.common.Constants;
import com.cht.pcrf.utils.CommonUtil;
import com.cht.pcrf.utils.SysParameterPropsReader;


public class ScheduledJob extends QuartzJobBean{
    private static final Logger logger = LoggerFactory.getLogger(ScheduledJob.class);
	
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		
		try {
			CommonUtil.invokeShell(SysParameterPropsReader.getConfig(Constants.DATA_CONSISTENT_AUTO_SHELL), false, "");
		} catch (Exception e) {
            logger.error("ScheduledJob fail, e:" + e);
            logger.error(e.getMessage(), e);
		}
	}
}
