/**
 * Project Name : PG-WEB
 * File Name 	: ButtonTag.java
 * Package Name : com.fet.pg.tag
 * Date 		: 2015年12月15日 
 * Author 		: LauraChu
 * Copyright (c) 2015 All Rights Reserved.
 */
package com.cht.pcrf.tag;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.common.Constants;

public class ButtonTag extends BodyTagSupport{

    private static final Logger logger = LoggerFactory.getLogger(ButtonTag.class);
    private String funcId;
    private String btnId; // button權限

    @SuppressWarnings("unchecked")
    @Override
    public int doEndTag() throws JspException {

        HttpServletRequest request = getRequest();

        try {

//            logger.debug("funcId:" + funcId);
//            logger.debug("btnId:" + btnId);
            
            // 取得權限
            boolean isShowButton = checkBtnAuth(request,funcId,btnId);
            
            // 若顯示，則將功能Button顯示於畫面中
            if (isShowButton) {

                pageContext.getOut().println(getBodyContent().getString());
                
            }

        }
        catch (Exception e) {

            logger.error(">>>>>Button " + funcId + ":" + btnId + "權限設定有問題>>>>>", e);

            request.getSession().setAttribute(Constants.SYS_EXCEPTION_MESSAGE, e);

            RequestDispatcher requestDispatcher = request
                .getRequestDispatcher("/WEB-INF/jsp/common/ExceptionHandlePG.jsp");

            try {

                requestDispatcher.forward(request, getResponse());
            }
            catch (ServletException se) {
                se.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        return super.doEndTag();
    }
    
    private boolean checkBtnAuth(HttpServletRequest request, String funcId, String btnId) {
        
        Map<String, String> authMap = (Map<String, String>) request.getSession().getAttribute(
                Constants.USER_AUTHED_BTN);
        
        if(!"".equals(funcId) && !"".equals(btnId)){
            
            String key = funcId + Constants.SPLIT_KEY + btnId;
            
            if(authMap.containsKey(key)){
                return true;
            }
        }
        
        return false;
        
    }

    private HttpServletRequest getRequest() {
        return (HttpServletRequest) (this.pageContext).getRequest();
    }
    
    private HttpServletResponse getResponse() {
    
        return (HttpServletResponse) (this.pageContext).getResponse();
    }
    
//    private HttpServletRequest getRequest() {
//
//        return (HttpServletRequest) ((PageContext) this.getJspContext()).getRequest();
//    }
//    
//    private HttpServletResponse getResponse() {
//
//        return (HttpServletResponse) ((PageContext) this.getJspContext()).getResponse();
//    }

    public String getFuncId() {
        return funcId;
    }

    public void setFuncId(String funcId) {
        this.funcId = funcId;
    }

    public String getBtnId() {
        return btnId;
    }

    public void setBtnId(String btnId) {
        this.btnId = btnId;
    }
    
}
