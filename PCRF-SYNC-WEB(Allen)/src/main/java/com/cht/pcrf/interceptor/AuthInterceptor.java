package com.cht.pcrf.interceptor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.cht.pcrf.common.Constants;

public class AuthInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(AuthInterceptor.class);

    @Autowired(required=true)
    private MessageSource messageSource = null;

    private static final String LOGIN_PATH = "/login.do";
    private static final String LOGIN_PAGE = "/login.jsp";
    private static final String WELCOME_PAGE = "/welcome.jsp";
    private static final String LOGOUT_PAGE = "/logout.do";

    private static final String PERSONAL_URL_CONTROLLER_PREFIX = "/personalInfo";
    private static final String PERSONAL_URL = PERSONAL_URL_CONTROLLER_PREFIX + Constants.URL_ENTITY_EDIT + ".do";
    private static final String MESSAGE = "message";

//    /**
//     * <code>AdminAuthUrls</code>: 管理者權限PATH
//     * @version $Id$
//     */
//    public enum AdminAuthUrls {
//        // @(#) STATIC ATTRIBUTES
//        USERCONTROLPATH("/userControl");
//
//        // @(#) ATTRIBUTES
//        private static volatile Map<String, AdminAuthUrls> registry = new HashMap<String, AdminAuthUrls>();
//
//        private String code;
//
//        // @(#) INNER CLASSES
//        // @(#) CONSTRUCTORS
//        private AdminAuthUrls(String code)  {
//            this.code = code;
//        }
//
//        // @(#) PUBLIC METHODS
//        public static AdminAuthUrls valueOfCode (String code) {
//            if(0 == registry.size()) {
//                synchronized (registry) {
//                    if(0 == registry.size()) {
//                        for(AdminAuthUrls o: AdminAuthUrls.values()) {
//                            registry.put(o.code, o);
//                        }
//                    }
//                }
//            }
//
//            AdminAuthUrls url = registry.get(code);
//            return url;
//        }
//
//        public String getCode () {  return code;  }
//
//        @Override
//        public String toString () {  return getCode();  }
//
//        // @(#) PROTECTED METHODS
//        // @(#) PRIVATE METHODS
//    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.getSession().removeAttribute(MESSAGE);

        String requestUri = request.getRequestURI();
        //========= Auth Interceptor Pre-handle url: /PG-WEB/groups/list.do =========
        logger.info("========= Auth Interceptor Pre-handle url: " + requestUri + " =========");

        // Avoid a redirect loop for some urls
        if ( !requestUri.contains(LOGIN_PATH) ) {
            Map<String, Object> userData = (Map<String, Object>) request.getSession().getAttribute(Constants.USER_DATA);
            if (userData == null) {
                //response.sendRedirect(request.getContextPath() + LOGIN_PAGE);
                response.sendRedirect(request.getContextPath() + "/timeout.jsp");
                return false;
            }
        }

        if(requestUri.contains(LOGIN_PATH) || requestUri.contains(LOGIN_PAGE) 
                || requestUri.contains(WELCOME_PAGE) || requestUri.contains(LOGOUT_PAGE)) {
            //skip
            return true;
        }else if(requestUri.contains(request.getContextPath() + PERSONAL_URL_CONTROLLER_PREFIX)) {
            String url = request.getContextPath() + PERSONAL_URL.replace("{" + Constants.ATTRIBUTE_ID + "}", getUserAccount(request));
            if(!requestUri.equals(url)){
                String errorCode = messageSource.getMessage("permission.denied", null, "權限不足!", null);
                logger.warn("Permission denied. errorMsg:{}", errorCode);

//                request.getSession().setAttribute(MESSAGE, errorCode);
//                response.sendRedirect(request.getContextPath() + "/redirPage/main.do");

                return false;
            }
            return true;
        }
        
        if(!checkUserIsAdmin(request)) {

            String errorCode = messageSource.getMessage("permission.denied", null, "權限不足!", null);
            logger.warn("Permission denied. errorMsg:{}", errorCode);

//                request.getSession().setAttribute(MESSAGE, errorCode);
//                response.sendRedirect(request.getContextPath() + "/redirPage/main.do");

            return false;
        }

        return true;

    }

    private boolean checkUserIsAdmin(HttpServletRequest request) throws Exception {

        //TODO
        //目前只判斷isNewQuery=Y的，之後需補上所有的link判斷
        if(!"Y".equals(request.getParameter("isNewQuery"))){
            return true;
        }

        Map<String, String> funcList = getFuncList(request);

        String strRequestURI = request.getRequestURI();

        if (strRequestURI.indexOf(request.getContextPath()) == 0) {

            strRequestURI = strRequestURI.substring(request.getContextPath().length());
        }

        //strRequestURI:/userControl/admin/edit.do
        logger.info("strRequestURI:"+strRequestURI);

        if(funcList.containsKey(strRequestURI)){
            return true;
        }
        return false;
    }

    private Map<String, String> getFuncList(HttpServletRequest request) {
        @SuppressWarnings("unchecked")
        Map<String, String> funcAuthedMap = (Map<String, String>) request
                .getSession().getAttribute(Constants.USER_AUTHED_FUNC);
        return funcAuthedMap;
    }

//    public static int findIndexOfAny(Object value, Object[] values) {
//
//        int index = -1;
//
//        for (int i = 0; i < values.length; i++) {
//
//            if (value.equals(values[i])) {
//
//                index = i;
//
//                break;
//            }
//        }
//
//        return index;
//    }
    
    /**
     * 取得使用者account
     * @return
     */
    protected String getUserAccount(HttpServletRequest request) {
        Map userData = getUserData(request);
        return (String)userData.get(Constants.USER_ACCOUNT);
    }

    protected Map getUserData(HttpServletRequest request) {
        return (Map) request.getSession().getAttribute(Constants.USER_DATA);
    }
}
