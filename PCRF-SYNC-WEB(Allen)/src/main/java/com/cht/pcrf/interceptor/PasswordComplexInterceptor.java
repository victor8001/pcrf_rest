package com.cht.pcrf.interceptor;

import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.cht.pcrf.common.Constants;


/**
 *
 * @author steven
 *
 */
public class PasswordComplexInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(PasswordComplexInterceptor.class);

    // /{0}/edit.do
//    private static final String EDIT_PATH = "/userControl/edit";

    private static final String EDIT_PAGE = "/personalInfo/{0}/edit.do";

    private static final String TITLE_PATH = "/redirPage/title";

    private static final String MENU_PATH = "/redirPage/menu";

    private static final String LOGIN_PATH = "/login.do";

    private static final String LOGOUT_PATH = "/logout.do";

    private static final String LOGIN_PAGE = "/login.jsp";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String url = request.getRequestURI();

        logger.debug("========= Pre-handle url: " + url + " =========");

        // Avoid a redirect loop for some urls
        if (!editUrlMatchesResult(url) && !url.contains(TITLE_PATH)
                && !url.contains(MENU_PATH) && !url.contains(LOGIN_PATH)
                && !url.contains(LOGOUT_PATH) && !url.contains(LOGIN_PAGE))
        {
            Map<String, Object> userData = (Map<String, Object>) request.getSession().getAttribute(Constants.USER_DATA);

            // 重新設定密碼
            if (userData != null &&
                    ((userData.get("MODIFY_TIME") != null
                        && (new Date().getTime()-((Date)userData.get("MODIFY_TIME")).getTime())>7776000000L)
                    || userData.get("MODIFY_TIME") == null
                    )) {
                response.sendRedirect(request.getContextPath() +
                        StringUtils.replace(EDIT_PAGE, "{0}", (String) userData.get(Constants.USER_ACCOUNT)));

                logger.info("Edit page replace result:{}", StringUtils.replace(EDIT_PAGE, "{0}", (String)userData.get(Constants.USER_ACCOUNT)));

                return false;
            }
        }
        return true;

    }

    private boolean editUrlMatchesResult(String url) {
//        return true;
        boolean result = false;
        Pattern pattern = Pattern.compile("^(.*)(" + Constants.URL_EDIT + ".do" + "){1}$");
        Matcher matcher = pattern.matcher(url);

        if(matcher.matches()) { result = true; }

        return result;
    }

}
