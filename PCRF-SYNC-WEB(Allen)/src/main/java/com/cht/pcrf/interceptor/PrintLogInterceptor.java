package com.cht.pcrf.interceptor;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.cht.pcrf.common.Constants;

/**
 *
 * @author rich
 * @version $Id: PrintLogInterceptor.java 332 2016-01-31 09:03:43Z weilinchu $
 */
public class PrintLogInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(Constants.LOGGER_PCRF_WEB);

    @Autowired(required=true)
    private MessageSource messageSource = null;

    private static final String LOGIN_PATH = "/login.do";

    private static final String METHOD_GET = "GET";
    private static final String METHOD_POST = "POST";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String requestUri = request.getRequestURI();
        logger.debug("========= Print Log Interceptor Pre-handle url: " + requestUri + " =========");

        boolean result = true;

        String path = request.getServletPath();

        if(path.contains(LOGIN_PATH)) {
            String actionLoginStr = messageSource.getMessage("label.login", null, Locale.TAIWAN);

            logger.info("{},{},{}"
                    , new Object[] {request.getParameter("userID"), actionLoginStr, actionLoginStr});
        } else {
            String httpMethod = request.getMethod();
            Map<String, Object> userData = (Map<String, Object>) request.getSession().getAttribute(Constants.USER_DATA);
            printActionLog(httpMethod, path, userData);
        }

        return result;
    }

    /**
     * 印出當前行為至LOG
     * log pattern : user,func,operation,time
     * @param httpMethod: request.getMethod();
     * @param path: request.getServletPath();
     * @param userData: login user info from session
     */
    public void printActionLog(String httpMethod, String path, Map<String, Object> userData) {
        if(!path.startsWith("/redirPage")) {
            String user = (String) userData.get(Constants.USER_ACCOUNT);

            logger.info("{},{},{}", new Object[] {user, path, httpMethod});
            
            /*
            String userControl = FunctionUrlEnum.USER_CONTROL.getCode();
            String sapc = FunctionUrlEnum.SAPC.getCode();
            String subscriber = FunctionUrlEnum.SUBSCRIBER.getCode();
            String txLog = FunctionUrlEnum.TX_LOG.getCode();
            String groups = FunctionUrlEnum.GROUPS.getCode();
            String personalInfo = FunctionUrlEnum.PERSONAL_INFORMATION.getCode();
            String sysLog = FunctionUrlEnum.SYS_LOG.getCode();
            
            if(path.contains(userControl)) {
                printMatchResult(path, userControl, UserControlOperationEnum.values(), user, httpMethod);
            } else if(path.contains(sapc)) {
                printMatchResult(path, sapc, SapcOperationEnum.values(), user, httpMethod);
            } else if(path.contains(subscriber)) {
                printMatchResult(path, subscriber, SubscriberOperationEnum.values(), user, httpMethod);
            } else if(path.contains(txLog)) {
                printMatchResult(path, txLog, TxLogOperationEnum.values(), user, httpMethod);
            } else if(path.contains(groups)) {
                printMatchResult(path, groups, GroupsOperationEnum.values(), user, httpMethod);
            } else if(path.contains(personalInfo)) {
                printMatchResult(path, personalInfo, PersonalInfoOperationEnum.values(), user, httpMethod);
            } else if(path.contains(sysLog)) {
                printMatchResult(path, sysLog, SysLogOperationEnum.values(), user, httpMethod);
            }
            */
        }

    }

    /**
     * 如果有對應到 ENUM 內的 URL 清單，就依照 LOG 格式輸出
     * @param path
     * @param func
     * @param enumEntrys
     * @param user
     * @param method
     * @return
     */
    private boolean printMatchResult(String path, String func, Enum[] enumEntrys, String user, String method) {
        boolean result = false;

        for(Enum operation : enumEntrys) {
            boolean matchResult = path.matches(".*" + func + operation.toString() + ".do");

            if(matchResult) {
                result = true;
                logger.info("{},{},{}", new Object[] {
                        user, funcMsgMapping(func), operationMsgMapping(operation.toString(), method)});
                break;
            }
        }

        return result;
    }

    /**
     * Function 對應訊息
     * @param func
     * @return
     */
    private String funcMsgMapping(String func) {
        String result = "";

        if(func.equals(FunctionUrlEnum.USER_CONTROL.toString())) {
            result = messageSource.getMessage("text.UserControl", null, Locale.TAIWAN);

        } else if(func.equals(FunctionUrlEnum.SAPC.toString())) {
            result = messageSource.getMessage("text.SAPC", null, Locale.TAIWAN);

        } else if(func.equals(FunctionUrlEnum.SUBSCRIBER.toString())) {
            result = messageSource.getMessage("text.Subscriber", null, Locale.TAIWAN);

        } else if(func.equals(FunctionUrlEnum.TX_LOG.toString())) {
            result = messageSource.getMessage("text.TxLog", null, Locale.TAIWAN);

        } else if(func.equals(FunctionUrlEnum.PERSONAL_INFORMATION.toString())) {
            result = messageSource.getMessage("text.personalInfo", null, Locale.TAIWAN);

        } else if(func.equals(FunctionUrlEnum.GROUPS.toString())) {
            result = messageSource.getMessage("text.group", null, Locale.TAIWAN);

        } else if(func.equals(FunctionUrlEnum.SYS_LOG.toString())) {
            result = messageSource.getMessage("text.sysLog", null, Locale.TAIWAN);

        } else {
            result = func;
        }

        return result;
    }

    /**
     * Operation 對應訊息
     * @param operation
     * @param method
     * @return
     */
    private String operationMsgMapping(String operation, String method) {
        String result = "";

        if(operation.equals(Constants.URL_LIST)) {
            result = messageSource.getMessage("label.search", null, Locale.TAIWAN);

        } else if(operation.equals(Constants.URL_VIEW)) {
            result = messageSource.getMessage("label.view", null, Locale.TAIWAN);

        } else if(operation.equals(Constants.URL_NEW)) {

            if(method.equals(METHOD_POST)) {
                result = messageSource.getMessage("label.new.save", null, Locale.TAIWAN);
            } else if (method.equals(METHOD_GET)) {
                result = messageSource.getMessage("label.new", null, Locale.TAIWAN);
            }

        } else if(operation.equals(Constants.URL_ENTITY_EDIT.replace("{id}", ".*"))) {

            if(method.equals(METHOD_POST)) {
                result = messageSource.getMessage("label.edit.save", null, Locale.TAIWAN);
            } else if (method.equals(METHOD_GET)) {
                result = messageSource.getMessage("label.edit", null, Locale.TAIWAN);
            }

        } else if(operation.equals(Constants.URL_ENTITY_DELETE.replace("{id}", ".*"))) {
            result = messageSource.getMessage("label.delete", null, Locale.TAIWAN);

        } else if(operation.equals(UserControlOperationEnum.ADD_PAGE.toString())) {
            result = messageSource.getMessage("label.new", null, Locale.TAIWAN);

        } else if(operation.equals(UserControlOperationEnum.ADD.toString())) {
            result = messageSource.getMessage("label.new.save", null, Locale.TAIWAN);

        }  else if(operation.equals(SapcOperationEnum.START_SAPC.toString())
                || operation.equals(SapcOperationEnum.START_SAPC_FORCE.toString())) {
            result = messageSource.getMessage("label.start.sapc", null, Locale.TAIWAN);

        } else if(operation.equals(SapcOperationEnum.STOP_SAPC.toString())) {
            result = messageSource.getMessage("label.stop.sapc", null, Locale.TAIWAN);

        } else if(operation.equals(TxLogOperationEnum.QUERY.toString())) {
            result = messageSource.getMessage("label.search", null, Locale.TAIWAN);

        } else if(operation.equals(TxLogOperationEnum.QUERY_BY_TXID.toString())) {
            result = messageSource.getMessage("label.search", null, Locale.TAIWAN);

        } else if(operation.equals(TxLogOperationEnum.EXPORT.toString())) {
            result = messageSource.getMessage("label.export", null, Locale.TAIWAN);

        } else if(operation.equals(TxLogOperationEnum.RETRY_TXLOG.toString())) {
            result = messageSource.getMessage("label.retry.txLog", null, Locale.TAIWAN);

        } else if(operation.equals(GroupsOperationEnum.FUNCTION_SETTING_PAGE.toString())) {
            result = messageSource.getMessage("label.groups.function.setting", null, Locale.TAIWAN);

        } else if(operation.equals(GroupsOperationEnum.USER_SETTING_PAGE.toString())) {
            result = messageSource.getMessage("label.groups.user.setting", null, Locale.TAIWAN);

        } else if(operation.equals(GroupsOperationEnum.FUNCTION_SETTING.toString())) {
            result = messageSource.getMessage("label.groups.function.setting.save", null, Locale.TAIWAN);

        }  else if(operation.equals(GroupsOperationEnum.USER_SETTING.toString())) {
            result = messageSource.getMessage("label.groups.user.setting.save", null, Locale.TAIWAN);

        } else {
            result = operation;
        }

        return result;
    }

    /**
     * <code>FunctionUrlEnum</code>:FunctionUrl list
     * @author $Author: weilinchu $
     * @version $Id: PrintLogInterceptor.java 332 2016-01-31 09:03:43Z weilinchu $
     */
    public enum FunctionUrlEnum {
        USER_CONTROL("/userControl"),
        SAPC("/sapcManagement"),
        SUBSCRIBER("/subscriber"),
        GROUPS("/groups"),
        TX_LOG("/apiLog"),
        PERSONAL_INFORMATION("/personalInfo"),
        SYS_LOG("/sysLog");

        private String code;

        private FunctionUrlEnum(String code) {
            this.code = code;
        }

        public String getCode() { return this.code; }

        @Override
        public String toString() {
            return getCode();
        }
    }

    /**
     * <code>UserControlOperationEnum</code>:UserControl url list
     * @author $Author: weilinchu $
     * @version $Id: PrintLogInterceptor.java 332 2016-01-31 09:03:43Z weilinchu $
     */
    public enum UserControlOperationEnum {
        LIST(Constants.URL_LIST),
        BLOCK("/block_reset"),
        EDIT_PAGE(Constants.URL_ENTITY_EDIT.replace("{id}", ".*")),
        ADD("/add"),
        ADD_PAGE("/add_before"),
        DELETE(Constants.URL_ENTITY_DELETE.replace("{id}", ".*"));

        private String code;

        private UserControlOperationEnum(String code) {
            this.code = code;
        }

        public String getCode() { return this.code; }

        @Override
        public String toString() {
            return getCode();
        }
    }

    /**
     * <code>SapcOperationEnum</code>:SAPC url list
     * @author $Author: weilinchu $
     * @version $Id: PrintLogInterceptor.java 332 2016-01-31 09:03:43Z weilinchu $
     */
    public enum SapcOperationEnum {
        LIST(Constants.URL_LIST),
        NEW(Constants.URL_NEW),
        EDIT_PAGE(Constants.URL_ENTITY_EDIT.replace("{id}", ".*")),
        START_SAPC("/.*/startSapc"),
        START_SAPC_FORCE("/.*/forceOnlineSapc"),
        STOP_SAPC("/.*/stopSapc"),
        DELETE(Constants.URL_ENTITY_DELETE.replace("{id}", ".*"));

        private String code;

        private SapcOperationEnum(String code) {
            this.code = code;
        }

        public String getCode() { return this.code; }

        @Override
        public String toString() {
            return getCode();
        }
    }

    /**
     * <code>SubscriberOperationEnum</code>:Subscriber url list
     * @author $Author: weilinchu $
     * @version $Id: PrintLogInterceptor.java 332 2016-01-31 09:03:43Z weilinchu $
     */
    public enum SubscriberOperationEnum {
        LIST(Constants.URL_LIST),
        EDIT_PAGE(Constants.URL_ENTITY_EDIT.replace("{id}", ".*"));

        private String code;

        private SubscriberOperationEnum(String code) {
            this.code = code;
        }

        public String getCode() { return this.code; }

        @Override
        public String toString() {
            return getCode();
        }
    }

    /**
     * <code>TxLogOperationEnum</code>:TransactionLog url list
     * @author $Author: weilinchu $
     * @version $Id: PrintLogInterceptor.java 332 2016-01-31 09:03:43Z weilinchu $
     */
    public enum TxLogOperationEnum {
        LIST(Constants.URL_LIST),
        QUERY("/query"),
        QUERY_BY_TXID("/queryByTxId"),
        EXPORT("/exp"),
        RETRY_TXLOG("/retryTxLog");

        private String code;

        private TxLogOperationEnum(String code) {
            this.code = code;
        }

        public String getCode() { return this.code; }

        @Override
        public String toString() {
            return getCode();
        }
    }

    /**
     * <code>GroupsOperationEnum</code>:Groups url list
     * @author $Author: weilinchu $
     * @version $Id: PrintLogInterceptor.java 332 2016-01-31 09:03:43Z weilinchu $
     */
    public enum GroupsOperationEnum {
        LIST(Constants.URL_LIST),
        NEW(Constants.URL_NEW),
        EDIT_PAGE(Constants.URL_ENTITY_EDIT.replace("{id}", ".*")),
        FUNCTION_SETTING_PAGE("/.*/functionSetting"),
        USER_SETTING_PAGE("/.*/userSetting"),
        FUNCTION_SETTING("/functionSetting"),
        USER_SETTING("/userSetting"),
        DELETE(Constants.URL_ENTITY_DELETE.replace("{id}", ".*"));

        private String code;

        private GroupsOperationEnum(String code) {
            this.code = code;
        }

        public String getCode() { return this.code; }

        @Override
        public String toString() {
            return getCode();
        }
    }


    /**
     * <code>PersonalInfoOperationEnum</code>:Groups url list
     * @author $Author: weilinchu $
     * @version $Id: PrintLogInterceptor.java 332 2016-01-31 09:03:43Z weilinchu $
     */
    public enum PersonalInfoOperationEnum {
        EDIT_PAGE(Constants.URL_ENTITY_EDIT.replace("{id}", ".*"));

        private String code;

        private PersonalInfoOperationEnum(String code) {
            this.code = code;
        }

        public String getCode() { return this.code; }

        @Override
        public String toString() {
            return getCode();
        }
    }

    /**
     * <code>SysLogOperationEnum</code>:UserControl url list
     * @author $Author: weilinchu $
     * @version $Id: PrintLogInterceptor.java 332 2016-01-31 09:03:43Z weilinchu $
     */
    public enum SysLogOperationEnum {
        LIST(Constants.URL_LIST),
        VIEW(Constants.URL_VIEW);

        private String code;

        private SysLogOperationEnum(String code) {
            this.code = code;
        }

        public String getCode() { return this.code; }

        @Override
        public String toString() {
            return getCode();
        }
    }
}
