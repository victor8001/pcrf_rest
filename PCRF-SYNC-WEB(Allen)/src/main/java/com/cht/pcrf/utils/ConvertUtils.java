package com.cht.pcrf.utils;


import com.cht.pcrf.json.SubDataPlan;
import com.cht.pcrf.json.SubRestInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class ConvertUtils {


    /**
     *
     * @param msisdn subscriberId
     * @param infos ldap or json format (will be deprecated aurguemnts)
     * @param col11_1 4G OR 5G Identifier
     * @return
     * @throws JsonProcessingException
     */
    public String toJSONString(String msisdn,String infos,String col11_1) throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        JSONObject json = null;

        if (infos.startsWith("{") && infos.endsWith("}")){
            // infos is JSON format
            json = new JSONObject(infos);
        }else{
            // infos in LDAP format
            Object jsonObject = infoStrToJSON(infos,false);
            if(jsonObject instanceof JSONObject){
                json = (JSONObject) jsonObject;
            }else{
                json = new JSONObject(jsonObject.toString());
            }
        }
        // returned data payload
        String result = "";
        // set msisdn
        SubRestInfo restInfo = new SubRestInfo();
        restInfo.setSubscriberId(msisdn);

        // setting Qualification Data
        setQualificationData(restInfo,json);

        // setting DataPlans --> if policyID is 5G_* set this fields
        setDataPlan(restInfo,json,col11_1);

        result = om.writeValueAsString(restInfo);

        return result;
    }

    private Object infoStrToJSON(String info,boolean isChild){
        String cvtStr = "";
        // create JSON Object
        JSONObject jsonObject = new JSONObject();

        if(!isChild && info.contains("\r\n")){
            cvtStr = info.replace("\r\n",",");
            String[] jsonEntry = cvtStr.split(",");
            for (String jsonObj : jsonEntry){
                String[] json = null;
                String key = "";
                String val = "" ;
                if(jsonObj.contains(": ")){
                    json = jsonObj.split(": ");
                    key = json[0];
                    val = json[1];
                    if (StringUtils.isBlank(key)){
                        continue;
                    }
                    // result value
                    Object result = new Object();

                    if(val.contains(":") && !key.contains("PolicyIds")){
                        result = infoStrToJSON(val,true);
                    }else{
                        if(key.contains("PolicyIds")){
                            result = processPolicyIds(val);
                        }else{
                            result = val.trim();
                        }
                    }
                    jsonObject.put(key,result);

                }
            }
            return jsonObject.toString();
        }else if(isChild && info.contains(":")){
            cvtStr = info;
            String[] jsonEntry = cvtStr.split(":");
            if(jsonEntry.length >= 2){
                jsonObject.put(jsonEntry[0],jsonEntry[1]);
            }
            return jsonObject;
        }else{
            // otherwise return empty jsonObject
            return "{}";
        }
    }

    /**
     * 預先處理 dataplan 去除特定字串
     * @param dataPlan
     * @return
     */
    private String processPolicyIds(String dataPlan){
        if(dataPlan.contains("0:p_")){
            dataPlan = dataPlan.replaceFirst("0:p_","");
        }
        if(dataPlan.contains(":")){
            dataPlan = dataPlan.substring(0,dataPlan.indexOf(":"));
        }
        return dataPlan;
    }


    private void setQualificationData(SubRestInfo restInfo, JSONObject json){
        JSONObject subsQual = null;
        HashMap<String,Object> map = new HashMap<>();
        if(json.has("EPC-SubscriberQualificationData")){
            subsQual = json.getJSONObject("EPC-SubscriberQualificationData");
        }
        if(subsQual != null && subsQual.has("SubscriberChargingSystemName")){
            map.put("onlineChargingSystemProfileId",subsQual.getString("SubscriberChargingSystemName"));
        }else{
            map.put("onlineChargingSystemProfileId","ChtOcs");
        }
        restInfo.setStaticQualification(map);
    }

    // only when policyid beginwith 5G_ set dataplan
    private void setDataPlan(SubRestInfo restInfo, JSONObject json,String col11_1){
        String dtaplan = "";
        // if JSON not contain policyIds use col11_1 instead
        dtaplan = json.has("EPC-PolicyIds")? json.getString("EPC-PolicyIds") : col11_1 ;

        // 2022 01 11 -> 5G_* -> set dataplan
        if(dtaplan.contains("5G_")){
            List<SubDataPlan> plans = restInfo.getDataplans();
            SubDataPlan plan = new SubDataPlan();
            plan.setDataplanName(dtaplan);
            plans.add(plan);
        }
    }



}
