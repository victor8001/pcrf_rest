package com.cht.pcrf.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	private static final String DATETIME_SIMPLE_FORMAT = "yyyyMMddHHmmss";
	
	private static final String DATETIME_FORMAT = "yyyy/MM/dd HH:mm:ss";
	
    private static final ThreadLocal<SimpleDateFormat> sdfLocal = new ThreadLocal<SimpleDateFormat>();
    
    private static final ThreadLocal<SimpleDateFormat> sdfSimpleLocal = new ThreadLocal<SimpleDateFormat>();
	
	
	/**
	 * Format date to pattern yyMMdd.
	 * @param date
	 * @return String
	 * 
	 */
	public static String formatDate(Date date) {
		return getSDFSimpleFormat().format(date);
    }
	
	/**
     * Format date to pattern yyyy/MM/dd HH:mm:ss.
     * @param date
     * @return String
     * 
     */
	public static String format(Date date) {
	    return getSDFLocal().format(date);
	}
	
	/**
	 * Adds or subtracts the specified amount of minute to the Now time.
	 * 
	 * @param minute
	 * @return Date
	 */
	public static Date operateMinute(int minute) {
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MINUTE, minute);
		
		return now.getTime();
	}
	
	/**
	 * Adds or subtracts the specified amount of minute to the input date.
	 * 
	 * @param date
	 * @param minute
	 * @return Date
	 * 
	 */
	public static Date operateMinute(Date date, int minute) {
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minute);
        
        return calendar.getTime();
    }
	
	/**
	 * Adds or subtracts the specified hour to the Now time.
	 * 
	 * @param date
	 * @param hour
	 * @return Date
	 * 
	 */
	public static Date operateHour(Date date, int hour) {
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, hour);
        
        return calendar.getTime();
    }
	
	/**
	 * Adds or subtracts the specified day to the Now time.
	 * @param date
	 * @param day
	 * @return
	 */
	public static Date operateDay(Date date, int day) {
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, day);
        
        return calendar.getTime();
    }
	
	private static int getDayOfMonth(Date date) {
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        calendar.setTime(date);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        
        return dayOfMonth;
    }
	
	/**
	 * Check whether the day of month are equal or not between two date
	 * 
	 * @param now
	 * @param date
	 * @return boolean true: equal | false: not equal
	 * 
	 */
	public static boolean isEqualDayOfMonth(Date now, Date date) {
	    return getDayOfMonth(now) == getDayOfMonth(date);
	}
	
	/**
	 * Get millisecond time difference between the two date.
	 * 
	 * @param startdate
	 * @param enddate
	 * @return
	 */
	public static long getTimeInterval(Date startdate, Date enddate) {

		long interval_times = 0;
		try {
			Calendar sc = (Calendar) Calendar.getInstance().clone();
			sc.setTime(startdate);
			Calendar ec = (Calendar) Calendar.getInstance().clone();
			ec.setTime(enddate);
			interval_times = getTimeInterval(sc, ec);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return interval_times;
	}


	/**
	 * Get millisecond time difference between the two calendar.
	 * 
	 * @param startdate
	 * @param enddate
	 * @return long
	 */
	public static long getTimeInterval(Calendar startdate, Calendar enddate) {

		long interval_times = 0;
		try {
			interval_times = enddate.getTimeInMillis()
					- startdate.getTimeInMillis();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return interval_times;

	}
	
    private static final SimpleDateFormat getSDFLocal() {
        if (sdfLocal.get() == null) {
            sdfLocal.set(new SimpleDateFormat(DATETIME_FORMAT));
        }
        return sdfLocal.get();
    }
    
    private static final SimpleDateFormat getSDFSimpleFormat() {
        if (sdfSimpleLocal.get() == null) {
        	sdfSimpleLocal.set(new SimpleDateFormat(DATETIME_SIMPLE_FORMAT));
        }
        return sdfSimpleLocal.get();
    }
    
    private static final SimpleDateFormat getSDFLocal(String pattern) {
        sdfLocal.set(new SimpleDateFormat(pattern));
        return sdfLocal.get();
    }

    public static Date parseDate(String dd) {

        if (dd != null) {
            try {
                return getSDFLocal().parse(dd);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }
    
    public static Date parseDate(String dd, String pattern) {

        if (dd != null) {
            try {
                return getSDFLocal(pattern).parse(dd);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }
    
    public static Date operate(Date now, int hour, int min, int sec, int mile) {
        Calendar date = Calendar.getInstance();
        date.setTime(now);
        
        // reset hour, minutes, seconds and millis
        date.set(Calendar.HOUR_OF_DAY, hour);
        date.set(Calendar.MINUTE, min);
        date.set(Calendar.SECOND, sec);
        date.set(Calendar.MILLISECOND, mile);
        return date.getTime();
    }
    
    public static Date operate(Date date, int hour) {
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        calendar.setTime(date);
        
        calendar.add(Calendar.HOUR, hour);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        
        return calendar.getTime();
    }
    
    /**
     * 取得現在日期
     * @return
     */
    public static Timestamp getNowTimestamp(){
    
        return new Timestamp(System.currentTimeMillis());
    }
    
    /**
     * 取得現在日期，並以ISO8601格式("yyyyMMdd'T'HH:mm:ssZ")回傳
     * @return
     */
    public static String getISO8601Format4() { 
        Date d = new Date();
        DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ssZ");
        return df.format(d);
    }
    
    /**
     * 將傳入ts，轉成formatType格式
     * @return
     */
    public static String formate(Timestamp ts,String formatType) { 
        //"yyyy-MM-dd HH:mm:ss"
        
        try{
            return new SimpleDateFormat(formatType).format(ts);
        }catch(Exception e){
            return "";
        }
    }
    
    /**  
     * 計算兩個日期之間相差之天數
     * @param smdate 较小的时间 
     * @param bdate  较大的时间 
     * @return 相差天数 
     * @throws ParseException  
     */    
    public static int daysBetween(Date smdate,Date bdate) throws ParseException {    
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        smdate=sdf.parse(sdf.format(smdate));  
        bdate=sdf.parse(sdf.format(bdate));  
        Calendar cal = Calendar.getInstance();    
        cal.setTime(smdate);    
        long time1 = cal.getTimeInMillis();                 
        cal.setTime(bdate);    
        long time2 = cal.getTimeInMillis();         
        long between_days=(time2-time1)/(1000*3600*24);  
        
       return Integer.parseInt(String.valueOf(between_days));           
    }

}
