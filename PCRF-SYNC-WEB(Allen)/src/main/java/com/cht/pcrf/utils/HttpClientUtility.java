/**
 * Project Name : PG-LDAP
 * File Name    : HttpClientUtility.java
 * Package Name : com.fet.pg.utils
 * Date         : 2015年11月30日 
 * Author       : LauraChu
 * Copyright (c) 2015 All Rights Reserved.
 */
package com.cht.pcrf.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.Consts;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.exception.HttpClientException;

public class HttpClientUtility {
    
    private static final Logger logger = LoggerFactory.getLogger(HttpClientUtility.class);
    
    private static final String HEADER_ACCEPT = "Accept";
    
    /**
     * HTTP POST method.
     * @param url
     * @param requestData
     * @param requestTimeout
     * @return String Response Body data
     * @throws Exception 
     */
    public static String httpPost(String url, String username, String password,
            String requestData, int connectTimeout, int readTimeout) throws HttpClientException {
        StringBuffer result = new StringBuffer();
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);
        
        logger.info("url:{} connectTimeout:{}, readTimeout:{} ", url, connectTimeout, readTimeout);
        
        String authHeader = getAuthHeader(username, password);
        post.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
         
        post.setConfig(RequestConfig.custom()
                .setConnectionRequestTimeout(connectTimeout)
                .setConnectTimeout(connectTimeout)
                .setSocketTimeout(readTimeout)
                .build());
        
        
        // Add header
        post.addHeader(HEADER_ACCEPT, MediaType.APPLICATION_JSON); //application/xml
        
        StringEntity requestEntity = new StringEntity(requestData, ContentType.create(
                MediaType.APPLICATION_JSON, Consts.UTF_8));
        
        try {
            post.setEntity(requestEntity);
            HttpResponse response = client.execute(post);
            logger.info("Response status code : " + response.getStatusLine().getStatusCode());
         
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            
            logger.info("Response result : " + result);
        }catch (Exception ex) {
            logger.error("POST request error, ex:{} , url:{}  ", ex.getMessage(), url);
            throw new HttpClientException(ex);
        }
        
        return result.toString();
    }
    
    private static String getAuthHeader(String username, String password) {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("UTF-8")));
        String authHeader = "";
        try {
            authHeader = "Basic " + new String(encodedAuth,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("UnsupportedEncodingException error, ex:{} ", e.getMessage());
            e.printStackTrace();
        }
        return authHeader;
    }

    /**
     * HTTP GET method.
     * @param url
     * @return String
     * @throws HttpClientException 
     */
    public static String httpGet(String url) throws HttpClientException {
        StringBuffer result = new StringBuffer();
        
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);

        try {
            HttpResponse response = client.execute(request);
            logger.info("Response Code : " + response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));

            
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            logger.info("Response result : " + result);
        } catch (Exception ex) {
            logger.error("Get request error, ex:{} , url:{}  ", ex.getMessage(), url);
            throw new HttpClientException(ex);
        }
        
        return result.toString();
    }
    
    public static String httpGet(String url, String username, String password)
            throws HttpClientException {
        
        HttpHost targetHost = new HttpHost("localhost", 8080, "http");
        HttpGet request = new HttpGet(url);
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(AuthScope.ANY, 
          new UsernamePasswordCredentials(username, password));
         
        AuthCache authCache = new BasicAuthCache();
        authCache.put(targetHost, new BasicScheme());
         
        // Add AuthCache to the execution context
        final HttpClientContext context = HttpClientContext.create();
        context.setCredentialsProvider(credsProvider);
        context.setAuthCache(authCache);
        
        HttpClient client = HttpClientBuilder.create().build();

        StringBuffer result = new StringBuffer();
        try {
            HttpResponse response = client.execute(request, context);
            logger.info("Response Code : " + response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            logger.info("Response result : " + result);
        } catch (Exception ex) {
            logger.error("Get request error, ex:{} , url:{}  ", ex.getMessage(), url);
            throw new HttpClientException(ex);
        }
        
        return result.toString();
    }
    
    public static String httpGetAuthHeader(String url, String username,
            String password) throws HttpClientException {
        HttpGet request = new HttpGet(url);
        String authHeader = getAuthHeader(username, password);
        request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
         
        HttpClient client = HttpClientBuilder.create().build();
        
        StringBuffer result = new StringBuffer();
        try {
            HttpResponse response = client.execute(request);
            logger.info("Response Code : " + response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));

            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            logger.info("Response result : " + result);
        } catch (Exception ex) {
            logger.error("Get request error, ex:{} , url:{}  ", ex.getMessage(), url);
            throw new HttpClientException(ex);
        }
        
        return result.toString();
    }
    

}
