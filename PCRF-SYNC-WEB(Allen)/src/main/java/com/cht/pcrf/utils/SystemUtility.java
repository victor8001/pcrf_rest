package com.cht.pcrf.utils;

import java.io.InputStream;
import java.net.InetAddress;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SystemUtility {
	
	private static final Logger logger = LoggerFactory.getLogger(SystemUtility.class);

	/**
	 * 取得設定檔
	 * @param configName
	 * @return
	 */
	public static String getConfig(String configName) {

		return SysParameterPropsReader.getConfig(configName);
	}

    /**
     * 取得設定檔
     * @param configName
     * @param defaultValue
     * @return
     */
    public static String getConfig(String configName,String defaultValue) {

        return StringUtils.defaultString(
                StringUtils.trimToNull(SysParameterPropsReader.getConfig(configName)),defaultValue);
    }
    
    public static String getHostName(){
        InetAddress serverAddress;
        try {
            serverAddress = InetAddress.getLocalHost();
            return serverAddress.getHostName();
        } catch (Exception e) {
            logger.error("Get server hostname error ", e);
            Process proc;
            try {
                proc = Runtime.getRuntime().exec("hostname");
                try (InputStream stream = proc.getInputStream()) {
                    try (Scanner s = new Scanner(stream,"UTF-8").useDelimiter("\\A")) {
                        return s.hasNext() ? s.next() : "";
                    }
                }
            } catch (Exception e1) {
                logger.error("Get server hostname use os command error ", e1);
            }
            
            return "VoLTE-MCHSI02P";
        }
    }
    
    
}
