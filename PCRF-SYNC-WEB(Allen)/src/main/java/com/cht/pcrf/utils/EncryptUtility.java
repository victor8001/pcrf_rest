package com.cht.pcrf.utils;

import java.security.MessageDigest;

public class EncryptUtility {
    /**
     * 常用函數 - MD5加密<br>
     * @source_string
     * @return md5String
     */
    public static String toMd5(String source){
        String md5String=null;
        try{
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source.getBytes("UTF-8"));
            byte[] digest = md.digest();
            md5String = toHex(digest);
        }catch(Exception e){
            e.printStackTrace();
        }
        return md5String;
    }

    /** 
     * 常用函數 - 轉16進制<br>
     * @ byte[] digest
     * @return 16進制
     */
    public static String toHex(byte[] digest){
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < digest.length; ++i){
                byte b = digest[i];
                int value = (b & 0x7F) + (b < 0 ? 128 : 0);
                buffer.append(value < 16 ? "0" : "");
                buffer.append(Integer.toHexString(value));
        }
        return buffer.toString();
    }

    public static void main(String[] args) {
        String str = "steven";
        System.out.println(toMd5(str)); // 輸出 E4F58A805A6E1FD0F6BEF58C86F9CEB3
    }
}
