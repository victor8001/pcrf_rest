package com.cht.pcrf.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * <code></code>:
 * @author $Author: rayliao $
 * @version $Id: SysRole.java 253 2016-01-15 10:11:29Z rayliao $
 */
public class SysRole extends BaseModel{
    private Long roleId;
    private String roleName;
    private boolean isUsed;
    
    private int isUsedNumber;

    public int getIsUsedNumber() {
    	int result = 0;
    	if(isUsed == true){
    		result = 1;
    	}
    	return result ;
	}
	
	public Long getRoleId() {
        return roleId;
    }
    @NotEmpty(message="NotEmpty.entity.roleName")
    public String getRoleName() {
        return roleName;
    }
    public boolean isUsed() {
        return isUsed;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    public void setUsed(boolean isUsed) {
        this.isUsed = isUsed;
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
