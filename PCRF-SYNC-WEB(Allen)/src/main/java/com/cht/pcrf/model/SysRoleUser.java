package com.cht.pcrf.model;

import java.sql.Timestamp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * <code>SysRoleUser</code>:
 * @author $Author: rayliao $
 * @version $Id: SysRoleUser.java 148 2015-12-16 09:54:11Z rayliao $
 */
public class SysRoleUser {
    private Long roleId;
    private Long userId;
    private Long modifyUser;
    private Timestamp modifyTime;

    public Long getRoleId() {
        return roleId;
    }
    public Long getUserId() {
        return userId;
    }
    public Long getModifyUser() {
        return modifyUser;
    }
    public Timestamp getModifyTime() {
        return modifyTime;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public void setModifyUser(Long modifyUser) {
        this.modifyUser = modifyUser;
    }
    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
