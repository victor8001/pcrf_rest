package com.cht.pcrf.model;

import java.sql.Timestamp;

/**
 * <code>BaseModel</code>:
 * @author $Author: rayliao $
 * @version $Id: BaseModel.java 253 2016-01-15 10:11:29Z rayliao $
 */
public class BaseModel {

    private Long createUser;
    private Timestamp createTime;
    private Long modifyUser;
    private Timestamp modifyTime;

    private String modifyUserName;
    private String createUserName;

    public Long getCreateUser() {
        return createUser;
    }
    public Timestamp getCreateTime() {
        return createTime;
    }
    public Long getModifyUser() {
        return modifyUser;
    }
    public Timestamp getModifyTime() {
        return modifyTime;
    }
    public String getModifyUserName() {
        return modifyUserName;
    }
    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }
    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
    public void setModifyUser(Long modifyUser) {
        this.modifyUser = modifyUser;
    }
    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }
    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }
    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }


}
