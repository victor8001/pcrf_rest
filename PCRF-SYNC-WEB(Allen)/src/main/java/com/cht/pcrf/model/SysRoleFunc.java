package com.cht.pcrf.model;

import java.sql.Timestamp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * <code>SysRoleFunc</code>:
 * @author $Author: rayliao $
 * @version $Id: SysRoleFunc.java 170 2015-12-21 10:35:36Z rayliao $
 */
public class SysRoleFunc {
    private Long roleId;
    private Long funcId;
    private Long modifyUser;
    private Timestamp modifyTime;

    public SysRoleFunc() { super(); }

    public SysRoleFunc(Long roleId, Long funcId, Long modifyUser,
            Timestamp modifyTime) {
        super();
        this.roleId = roleId;
        this.funcId = funcId;
        this.modifyUser = modifyUser;
        this.modifyTime = modifyTime;
    }

    public Long getRoleId() {
        return roleId;
    }
    public Long getFuncId() {
        return funcId;
    }
    public Long getModifyUser() {
        return modifyUser;
    }
    public Timestamp getModifyTime() {
        return modifyTime;
    }
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public void setFuncId(Long funcId) {
        this.funcId = funcId;
    }
    public void setModifyUser(Long modifyUser) {
        this.modifyUser = modifyUser;
    }
    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
