package com.cht.pcrf.model;

import java.sql.Timestamp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PcrfConsistentLog extends BaseModel{
	
	private Long id;
	private Long pcrfId;
	private Timestamp execStartTime;
	private Timestamp execEndTime;
	private Integer execResult;
	private Long execUser;
	private String logPath;
	private String rptPath;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPcrfId() {
		return pcrfId;
	}
	public void setPcrfId(Long pcrfId) {
		this.pcrfId = pcrfId;
	}
	public Timestamp getExecStartTime() {
		return execStartTime;
	}
	public void setExecStartTime(Timestamp execStartTime) {
		this.execStartTime = execStartTime;
	}
	public Timestamp getExecEndTime() {
		return execEndTime;
	}
	public void setExecEndTime(Timestamp execEndTime) {
		this.execEndTime = execEndTime;
	}
	public Integer getExecResult() {
		return execResult;
	}
	public void setExecResult(Integer execResult) {
		this.execResult = execResult;
	}
	public Long getExecUser() {
		return execUser;
	}
	public void setExecUser(Long execUser) {
		this.execUser = execUser;
	}
	public String getLogPath() {
		return logPath;
	}
	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}
	
	public String getRptPath() {
		return rptPath;
	}
	public void setRptPath(String rptPath) {
		this.rptPath = rptPath;
	}
	@Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
	
	
}
