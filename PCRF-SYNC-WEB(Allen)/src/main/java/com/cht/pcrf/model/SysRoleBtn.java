package com.cht.pcrf.model;

import java.sql.Timestamp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * <code>SysRoleBtn</code>:
 * @author $Author: rayliao $
 * @version $Id: SysRoleBtn.java 170 2015-12-21 10:35:36Z rayliao $
 */
public class SysRoleBtn {
    private Long roleId;
    private Long funcId;
    private Long btnId;
    private Long modifyUser;
    private Timestamp modifyTime;

    public SysRoleBtn() { super(); }

    public SysRoleBtn(Long roleId, Long funcId, Long btnId, Long modifyUser,
            Timestamp modifyTime) {
        super();
        this.roleId = roleId;
        this.funcId = funcId;
        this.btnId = btnId;
        this.modifyUser = modifyUser;
        this.modifyTime = modifyTime;
    }

    public Long getRoleId() {
        return roleId;
    }
    public Long getFuncId() {
        return funcId;
    }
    public Long getBtnId() {
        return btnId;
    }
    public Long getModifyUser() {
        return modifyUser;
    }
    public Timestamp getModifyTime() {
        return modifyTime;
    }
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
    public void setFuncId(Long funcId) {
        this.funcId = funcId;
    }
    public void setBtnId(Long btnId) {
        this.btnId = btnId;
    }
    public void setModifyUser(Long modifyUser) {
        this.modifyUser = modifyUser;
    }
    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
