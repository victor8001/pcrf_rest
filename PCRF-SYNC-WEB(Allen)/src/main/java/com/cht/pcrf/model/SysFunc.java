package com.cht.pcrf.model;

import java.sql.Timestamp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * <code>SysFunc</code>:
 * @author $Author: rayliao $
 * @version $Id: SysFunc.java 165 2015-12-18 10:23:08Z rayliao $
 */
public class SysFunc {
    private Long funcId;
    private String funcName;
    private String funcCname;
    private String funcUrl;
    private Integer menuId;
    private Integer seqNo;
    private boolean isUsed;
    private Long createUser;
    private Timestamp createTime;
    private Long modifyUser;
    private Timestamp modifyTime;

    public Long getFuncId() {
        return funcId;
    }
    public String getFuncName() {
        return funcName;
    }
    public String getFuncCname() {
        return funcCname;
    }
    public String getFuncUrl() {
        return funcUrl;
    }
    public Integer getMenuId() {
        return menuId;
    }
    public Integer getSeqNo() {
        return seqNo;
    }
    public boolean isUsed() {
        return isUsed;
    }
    public Long getCreateUser() {
        return createUser;
    }
    public Timestamp getCreateTime() {
        return createTime;
    }
    public Long getModifyUser() {
        return modifyUser;
    }
    public Timestamp getModifyTime() {
        return modifyTime;
    }


    public void setFuncId(Long funcId) {
        this.funcId = funcId;
    }
    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }
    public void setFuncCname(String funcCname) {
        this.funcCname = funcCname;
    }
    public void setFuncUrl(String funcUrl) {
        this.funcUrl = funcUrl;
    }
    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }
    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }
    public void setUsed(boolean isUsed) {
        this.isUsed = isUsed;
    }
    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }
    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
    public void setModifyUser(Long modifyUser) {
        this.modifyUser = modifyUser;
    }
    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
