package com.cht.pcrf.model;

import java.sql.Timestamp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

public class Subscriber {

	private Long id;
	private String msisdn;
	private String objectClass;
	private String dn;
	private String col1_1;
	private String col2_1;
	private String col3_1;
	private String col4_1;
	private String col5_1;
	private String col6_1;
	private String col7_1;
	private String col8_1;
	private String col9_1;
	private String col10_1;
	private String col11_1;
	private String col11_2;
	private String col11_3;
	private String col11_4;
	private String col11_5;
	private String col11_6;
	private String col11_7;
	private String col11_8;
	private String col11_9;
	private String col11_10;
	private String col99_1;
	private Timestamp createTime;
	private Timestamp modifyTime;
	private String info;

	private String nodeLocation;

	public String getNodeLocation() {
		return nodeLocation;
	}

	public void setNodeLocation(String nodeLocation) {
		if (nodeLocation.equals("1")) {
			this.nodeLocation = "台北";
		}
		if (nodeLocation.equals("2")) {
			this.nodeLocation = "高雄";
		}

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getObjectClass() {
		return objectClass;
	}

	public void setObjectClass(String objectClass) {
		this.objectClass = objectClass;
	}

	public String getDn() {
		return dn;
	}

	public void setDn(String dn) {
		this.dn = dn;
	}

	public String getCol1_1() {
		return col1_1;
	}

	public void setCol1_1(String col1_1) {
		this.col1_1 = col1_1;
	}

	public String getCol2_1() {
		return col2_1;
	}

	public void setCol2_1(String col2_1) {
		this.col2_1 = col2_1;
	}

	public String getCol3_1() {
		return col3_1;
	}

	public void setCol3_1(String col3_1) {
		this.col3_1 = col3_1;
	}

	public String getCol4_1() {
		return col4_1;
	}

	public void setCol4_1(String col4_1) {
		this.col4_1 = col4_1;
	}

	public String getCol5_1() {
		return col5_1;
	}

	public void setCol5_1(String col5_1) {
		this.col5_1 = col5_1;
	}

	public String getCol6_1() {
		return col6_1;
	}

	public void setCol6_1(String col6_1) {
		this.col6_1 = col6_1;
	}

	public String getCol7_1() {
		return col7_1;
	}

	public void setCol7_1(String col7_1) {
		this.col7_1 = col7_1;
	}

	public String getCol8_1() {
		return col8_1;
	}

	public void setCol8_1(String col8_1) {
		this.col8_1 = col8_1;
	}

	public String getCol9_1() {
		return col9_1;
	}

	public void setCol9_1(String col9_1) {
		this.col9_1 = col9_1;
	}

	public String getCol10_1() {
		return col10_1;
	}

	public void setCol10_1(String col10_1) {
		this.col10_1 = col10_1;
	}

	public String getCol11_1() {
		return col11_1;
	}

	public void setCol11_1(String col11_1) {
		this.col11_1 = col11_1;
	}

	public String getCol11_2() {
		return col11_2;
	}

	public void setCol11_2(String col11_2) {
		this.col11_2 = col11_2;
	}

	public String getCol11_3() {
		return col11_3;
	}

	public void setCol11_3(String col11_3) {
		this.col11_3 = col11_3;
	}

	public String getCol11_4() {
		return col11_4;
	}

	public void setCol11_4(String col11_4) {
		this.col11_4 = col11_4;
	}

	public String getCol11_5() {
		return col11_5;
	}

	public void setCol11_5(String col11_5) {
		this.col11_5 = col11_5;
	}

	public String getCol11_6() {
		return col11_6;
	}

	public void setCol11_6(String col11_6) {
		this.col11_6 = col11_6;
	}

	public String getCol11_7() {
		return col11_7;
	}

	public void setCol11_7(String col11_7) {
		this.col11_7 = col11_7;
	}

	public String getCol11_8() {
		return col11_8;
	}

	public void setCol11_8(String col11_8) {
		this.col11_8 = col11_8;
	}

	public String getCol11_9() {
		return col11_9;
	}

	public void setCol11_9(String col11_9) {
		this.col11_9 = col11_9;
	}

	public String getCol11_10() {
		return col11_10;
	}

	public void setCol11_10(String col11_10) {
		this.col11_10 = col11_10;
	}

	public String getCol99_1() {
		return col99_1;
	}

	public void setCol99_1(String col99_1) {
		this.col99_1 = col99_1;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Timestamp modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
