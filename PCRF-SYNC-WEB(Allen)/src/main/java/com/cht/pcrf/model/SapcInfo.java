package com.cht.pcrf.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

/**
 * <code>SapcInfo</code>:SAPC model
 * 
 * @author $Author: weilinchu $
 * @version $Id: SapcInfo.java 305 2016-01-27 05:49:53Z weilinchu $
 */
public class SapcInfo extends BaseModel {

	private Long id;
	private String location;
	private String node_name;
	private String nodeHost;
	private String nodePort;
	private String loginId;
	private String loginPw;
	private int status; // 1：online 2：offline (default) 3：處理中 4:new
	private Timestamp lastSyncTime;
	
	private String nodeLocation;
	// private Long createUser;
	// private Long modifyUser;
	// private Date createTime;
	// private Date modifyTime;

	@NumberFormat(style = Style.NUMBER)
	private int sequence;

	/**
	 * <code>StatusCode</code>: SAPC 目前狀態 1：online 2：offline (default) 3：處理中
	 * 4:new
	 * 
	 * @author $Author: weilinchu $
	 * @version $Id: SapcInfo.java 305 2016-01-27 05:49:53Z weilinchu $
	 */
	public enum SapcStatusCode {
		ONLINE(1), OFFLINE(2), PROCESSING(3), FORCE_ONLINE(1), // TODO:暫定，狀態碼未確認
		NEW(4);

		private Integer code;

		private SapcStatusCode(Integer code) {
			this.code = code;
		}

		public Integer getCode() {
			return code;
		}
	}

	
	public String getNode_name() {
		return node_name;
	}

	@NotEmpty(message = "NotEmpty.entity.nodehost")
	@Pattern(regexp = "[0-9]*.[0-9]*.[0-9]*.[0-9]*", message = "Pattern.entity.nodehost")
	public String getNodeHost() {
		return nodeHost;
	}

	@NotEmpty(message = "NotEmpty.entity.nodeport")
	@Pattern(regexp = "0|[1-9][0-9]*", message = "Pattern.entity.nodeport")
	public String getNodePort() {
		return nodePort;
	}

	@NotEmpty(message = "NotEmpty.entity.loginId")
	public String getLoginId() {
		return loginId;
	}

	@NotEmpty(message = "NotEmpty.entity.loginPw")
	public String getLoginPw() {
		return loginPw;
	}

	public int getStatus() {
		return status;
	}

	public Timestamp getLastSyncTime() {
		return lastSyncTime;
	}

	
	@Min(value = 0, message = "Size.entity.sequence")
	@Max(value = 100, message = "Size.entity.sequence")
	public int getSequence() {
		return sequence;
	}

	public void setNode_name(String node_name) {
		this.node_name = node_name;
	}

	public void setNodeHost(String nodeHost) {
		this.nodeHost = nodeHost;
	}

	public void setNodePort(String nodePort) {
		this.nodePort = nodePort;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public void setLoginPw(String loginPw) {
		this.loginPw = loginPw;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setLastSyncTime(Timestamp lastSyncTime) {
		this.lastSyncTime = lastSyncTime;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	
	
	public String getNodeLocation() {
		String result="";
		if(location.equals("TP")){
			result = "台北";
		}
		if(location.equals("KH")){
			result = "高雄";
		}
		return result;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
