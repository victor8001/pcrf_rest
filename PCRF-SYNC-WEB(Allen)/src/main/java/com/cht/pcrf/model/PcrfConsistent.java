package com.cht.pcrf.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PcrfConsistent extends BaseModel {

	private Long id;
	private String execWeek;
	private String execMonth;
	private String execDay;
	private String execHour;
	private String syncType;
	private int autoRecoveryMin;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExecWeek() {
		return execWeek;
	}

	public void setExecWeek(String execWeek) {
		this.execWeek = execWeek;
	}

	public String getExecMonth() {
		return execMonth;
	}

	public void setExecMonth(String execMonth) {
		this.execMonth = execMonth;
	}

	public String getExecDay() {
		return execDay;
	}

	public void setExecDay(String execDay) {
		this.execDay = execDay;
	}

	public String getExecHour() {
		return execHour;
	}

	public void setExecHour(String execHour) {
		this.execHour = execHour;
	}

	public String getSyncType() {
		return syncType;
	}

	public void setSyncType(String syncType) {
		this.syncType = syncType;
	}

	public int getAutoRecoveryMin() {
		return autoRecoveryMin;
	}

	public void setAutoRecoveryMin(int autoRecoveryMin) {
		this.autoRecoveryMin = autoRecoveryMin;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
