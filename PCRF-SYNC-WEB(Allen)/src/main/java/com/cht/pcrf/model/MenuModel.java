/*
 * File Name       : MenuModel.java 											<p>
 * Function Number : MenuModel.java											<p>
 * Module          : com.tiis.login.model										<p>
 * Description     : 														<p>
 * Company         : 											<p>
 * Copyright       : Copyright c 2012									<p>
 * Author          : LauraChu												<p>
 * History         : V1.00 2012/7/30 LauraChu 									<p>
 * Version         : 1.00													<p>
 */
package com.cht.pcrf.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MenuModel{
	private String pId;
	private String pName;
	private List<Map> subMenuList;
	
	public MenuModel(){
		subMenuList = new ArrayList();
	}
	
	public String getPId() {
		return pId;
	}
	public void setPId(String id) {
		pId = id;
	}
	public String getPName() {
		return pName;
	}
	public void setPName(String name) {
		pName = name;
	}
	public List<Map> getSubMenuList() {
		return subMenuList;
	}
	public void setSubMenuList(List<Map> subMenuList) {
		this.subMenuList = subMenuList;
	}
	
}
