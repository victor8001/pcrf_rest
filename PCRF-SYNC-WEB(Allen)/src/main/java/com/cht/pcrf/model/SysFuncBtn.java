package com.cht.pcrf.model;

import java.sql.Timestamp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * <code>SysFunc</code>:
 * @author $Author: rayliao $
 * @version $Id: SysFuncBtn.java 165 2015-12-18 10:23:08Z rayliao $
 */
public class SysFuncBtn {

    private Long funcId;
    private Long btnId;
    private String btnName;
    private String btnCname;
    private String btnUrl;
    private boolean isUsed;
    private Long createUser;
    private Timestamp createTime;
    private Long modifyUser;
    private Timestamp modifyTime;

    public Long getFuncId() {
        return funcId;
    }
    public Long getBtnId() {
        return btnId;
    }
    public String getBtnName() {
        return btnName;
    }
    public String getBtnCname() {
        return btnCname;
    }
    public String getBtnUrl() {
        return btnUrl;
    }
    public boolean isUsed() {
        return isUsed;
    }
    public Long getCreateUser() {
        return createUser;
    }
    public Timestamp getCreateTime() {
        return createTime;
    }
    public Long getModifyUser() {
        return modifyUser;
    }
    public Timestamp getModifyTime() {
        return modifyTime;
    }


    public void setFuncId(Long funcId) {
        this.funcId = funcId;
    }
    public void setBtnId(Long btnId) {
        this.btnId = btnId;
    }
    public void setBtnName(String btnName) {
        this.btnName = btnName;
    }
    public void setBtnCname(String btnCname) {
        this.btnCname = btnCname;
    }
    public void setBtnUrl(String btnUrl) {
        this.btnUrl = btnUrl;
    }
    public void setUsed(boolean isUsed) {
        this.isUsed = isUsed;
    }
    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }
    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
    public void setModifyUser(Long modifyUser) {
        this.modifyUser = modifyUser;
    }
    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
