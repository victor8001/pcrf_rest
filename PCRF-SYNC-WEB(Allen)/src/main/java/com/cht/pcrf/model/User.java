package com.cht.pcrf.model;

import java.sql.Timestamp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class User {

    private Long user_id;
    private String account;
    private String password;
    private Long blocked;
    private Long retryCount;
    private String password_first;
    private String password_second;
    private String password_third;
    private String password_fourth;
    private Long createUser;
    private Timestamp createTime;
    private Long modifyUser;
    private Timestamp modifyTime;
    

	
	public Long getUser_id() {
        return user_id;
    }
    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }
    public String getAccount() {
        return account;
    }
    public void setAccount(String account) {
        this.account = account;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public Long getBlocked() {
		return blocked;
	}
	public void setBlocked(Long blocked) {
		this.blocked = blocked;
	}
	public Long getRetryCount() {
        return retryCount;
    }
    public void setRetryCount(Long retryCount) {
        this.retryCount = retryCount;
    }
    public String getPassword_first() {
        return password_first;
    }
    public void setPassword_first(String password_first) {
        this.password_first = password_first;
    }
    public String getPassword_second() {
        return password_second;
    }
    public void setPassword_second(String password_second) {
        this.password_second = password_second;
    }
    public String getPassword_third() {
        return password_third;
    }
    public void setPassword_third(String password_third) {
        this.password_third = password_third;
    }
    public String getPassword_fourth() {
        return password_fourth;
    }
    public void setPassword_fourth(String password_fourth) {
        this.password_fourth = password_fourth;
    }
    public Long getCreateUser() {
        return createUser;
    }
    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }
    public Timestamp getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
    public Long getModifyUser() {
        return modifyUser;
    }
    public void setModifyUser(Long modifyUser) {
        this.modifyUser = modifyUser;
    }
    public Timestamp getModifyTime() {
        return modifyTime;
    }
    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
