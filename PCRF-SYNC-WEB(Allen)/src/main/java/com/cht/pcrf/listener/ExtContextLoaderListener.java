/** 
 * Project Name:PG-WEB 
 * File Name:ExtContextLoaderListener.java 
 * Package Name:com.fet.pg.listener 
 * Date:2015年11月16日上午10:54:24 
 * 
*/  
  
package com.cht.pcrf.listener;  

import java.text.ParseException;

import javax.servlet.ServletContextEvent;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;


import com.cht.pcrf.common.Constants;
import com.cht.pcrf.model.PcrfConsistent;
import com.cht.pcrf.service.PcrfConsistentServiceImpl;
import com.cht.pcrf.service.QuartzServiceImpl;


/** 
 * ClassName:ExtContextLoaderListener <br/> 
 * Date:     2015年11月16日 上午10:54:24 <br/> 
 * @author   rich 
 * @version   
 * @since    
 * @see       
 */
public class ExtContextLoaderListener extends ContextLoaderListener{
	
    @Override
    public void contextInitialized(ServletContextEvent event) {
    	

        //取得server name
        Constants.setSERVER_NAME(event.getServletContext().getInitParameter("server-name"));
        
        super.contextInitialized(event);
        
        
    	WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(event.getServletContext());
    	PcrfConsistentServiceImpl pcrfConsistentServiceImpl = (PcrfConsistentServiceImpl) ctx.getBean("pcrfConsistentServiceImpl");
    	PcrfConsistent pcrfConsistent = pcrfConsistentServiceImpl.find();
        
    	QuartzServiceImpl quartzServiceImpl = (QuartzServiceImpl)ctx.getBean("quartzServiceImpl");
    	
    	String week = pcrfConsistent.getExecWeek();
    	String month= pcrfConsistent.getExecMonth();
    	String day= pcrfConsistent.getExecDay();
    	String hour= pcrfConsistent.getExecHour();
    	
    	if(month.equals("")){
    		month = "*";
    	}
    	
    	if(day.equals("")){
    		day = "*";
    	}
    	
    	if(hour.equals("")){
    		hour = "*";
    	}
    	
    	
    	try {
			quartzServiceImpl.setJobTime(week, month, day, hour);
		} catch (SchedulerException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    } 

}
  