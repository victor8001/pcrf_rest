package com.cht.pcrf.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cht.pcrf.api.JobExecute;
import com.cht.pcrf.common.BaseAction;
import com.cht.pcrf.common.Constants;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.exception.ServiceException;
import com.cht.pcrf.model.ConfValueInfo;
import com.cht.pcrf.service.CommonServiceImpl;
import com.cht.pcrf.service.PcrfConsLogServiceImpl;
import com.cht.pcrf.service.PcrfConsistentServiceImpl;
import com.cht.pcrf.utils.CommonUtil;
import com.cht.pcrf.utils.DateUtil;
import com.cht.pcrf.utils.SysParameterPropsReader;
import com.cht.pcrf.utils.SystemUtility;


@Controller
@RequestMapping(PcrfConsLogController.URL_CONTROLLER_PREFIX)
public class PcrfConsLogController extends BaseAction {
    private static final Logger logger = LoggerFactory.getLogger(PcrfConsLogController.class);
//    private static final Logger operateLogger = LoggerFactory.getLogger(Constants.LOGGER_PG_WEB);

    protected static final String URL_CONTROLLER_PREFIX = "/pcrfConsLog";
    private static final String URL_DOWNLOAD = "/downLoadFile";
    private static final String URL_DATA_SYNC = "/dataSync";
    
    protected static final String PAGE_URL_PREFIX = "/pcrfConsLog";
    private static final String PAGE_LIST = "/list";
    private static final String PAGE_VIEW = "/view";
    
    @Autowired
    private PcrfConsistentServiceImpl pcrfConsistentServiceImpl;
    
    @Autowired
    private PcrfConsLogServiceImpl pcrfConsLogServiceImpl;
    
    @Autowired
    private CommonServiceImpl commonServiceImpl;
    
    protected Map<String, Object> viewMap;

    public PcrfConsLogController(){
        viewMap = new HashMap<String, Object>();

        viewMap.put(Constants.LIST_PAGE, PAGE_URL_PREFIX + PAGE_LIST);
        viewMap.put(Constants.VIEW_PAGE, PAGE_URL_PREFIX + PAGE_VIEW);
    }

    /**
     * pcrfConsLog 清單頁
     * @param request
     * @return
     */
    @RequestMapping(value=URL_LIST, method={RequestMethod.GET, RequestMethod.POST})
    public ModelAndView list(
    		HttpServletRequest request,
    		ModelMap model,
    		@RequestParam(value="node_location", required=false) String nodeLocation ,
    		@RequestParam(value="startTime", required=false) String startTime ,
    		@RequestParam(value="endTime", required=false) String endTime ,
    		@RequestParam(value="execResult", required=false) String execResult
    		) {
        try {
        	List<ConfValueInfo>  commons =commonServiceImpl.queryConfValue("node_location");
            logger.info("list all pcrfConsLog to page.");
            PageControler pageControler = getPageControler(request);
            
           
            List<Map> listResult = null;
            if( nodeLocation != null){
            	listResult = pcrfConsLogServiceImpl.findByLocStartTimeResult(pageControler, nodeLocation, startTime, endTime, execResult);
            }
            
            
            ModelAndView mav = getModelAndView(viewMap, LIST_PAGE);
            mav.addObject("commons", commons);
            mav.addObject("listResult", listResult);
            mav.addAllObjects(model);
            mav.addObject(PAGES, pageControler);
            
            mav.addObject("node_location", nodeLocation);
            mav.addObject("startTime", startTime);
            mav.addObject("endTime", endTime);
            mav.addObject("execResult", execResult);
            
            setPageControler(request,pageControler);

            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    @RequestMapping(value=URL_DOWNLOAD, method={RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<byte[]> download(
    		@RequestParam(value="path", required=false) String path
    		)throws IOException { 
    	String[] paths = path.split(",");
    	
    	String[] filename =paths[0].split("/") ;
    	String strName ="";
    	for(int i=0 ;i <filename.length ;i++){
    		strName = filename[i];
    	}
    	
    	File file = new File(paths[0]); 
    	HttpHeaders headers = new HttpHeaders(); 
    	String fileName = new String(strName.getBytes("UTF-8"), "iso-8859-1");
    	headers.setContentDispositionFormData("attachment", fileName); 
    	headers.setContentType(MediaType.APPLICATION_OCTET_STREAM); 
    	return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), 
    			headers, HttpStatus.CREATED); 
    } 

    @RequestMapping(value=URL_DATA_SYNC, method={RequestMethod.GET, RequestMethod.POST})
    public ModelAndView dataSync(
            HttpServletRequest request) {
        try {
            String syncType = request.getParameter("syncType");//all:所有 one:一筆門號
            
            if("one".equals(syncType)){
                return dataSyncOne(request);
            }else{
                return dataSyncAll(request);
            }
            
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    private ModelAndView dataSyncAll(HttpServletRequest request) {
        String id = request.getParameter("logId");
        logger.info("Data Sync LogId:{}", id);
        ModelMap modelMap = new ModelMap();

        //call shell，傳入Long logId, long user, String csvPath
        Map<String,Object> logInfo = pcrfConsLogServiceImpl.findById(Long.parseLong(id));
        
        if(logInfo == null) {
            modelMap.put(MESSAGE_CODE, "data.not.found");
        }else{
            String rptPath = (String) logInfo.get("RPT_PATH");
            
            if(rptPath == null || "".equals(rptPath)){
                modelMap.put(MESSAGE_CODE, "data.not.found");
            }else{
                rptPath = rptPath.split(",")[0];
//                CommonUtil.invokeShell(false, SysParameterPropsReader.getConfig(Constants.DATA_SYNC_SHELL)
//                        , new String[]{id, Long.toString(getUserId(request)), rptPath});
                
                execDataSyncTask(id, Long.toString(getUserId(request)), rptPath);
                modelMap.put(MESSAGE_CODE, "text.executing");
            }
        }

        
        return this.list(request, modelMap
                , request.getParameter("node_location")
                , request.getParameter("startTime")
                , request.getParameter("endTime")
                , request.getParameter("execResult"));
    }
    
    private ModelAndView dataSyncOne(HttpServletRequest request) throws Exception {

        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        
        try {
            ModelMap model = new ModelMap();
            String msisdn1 = request.getParameter("msisdn");
            String type1 = request.getParameter("type");
            String rptData = request.getParameter("rptRawData");
            String id = request.getParameter("logId");
            logger.info("dataSyncOne msisdn:{}", msisdn1);

            String fileName1 = "CSV_" 
                     + (msisdn1.equals("") ? "ALL" : msisdn1) + "_" 
                     + DateUtil.formatDate(new Date())+".csv";
            
            final String spileWord = ",";
             
            String path = SystemUtility.getConfig(Constants.SUBSCRIBER_FILL_PATH);
            String csvPath = path+fileName1;
            
            File file1 = new File(csvPath);
            
            
            fos = new FileOutputStream(file1);
            osw = new OutputStreamWriter(fos, "UTF-8");
            osw.write(0xFEFF);
            
            String data =
                    //title
                    "node"+spileWord+"type"+spileWord+"subscriber"+spileWord+"col1_1"+spileWord+"info"+"\n"
                    //data
                    +rptData
                    ;
            
            osw.write(data);
            osw.flush();
            
            String result = "0";
            try {
                JobExecute.getInstance().getSyncRestService().syncBySub(getUserAccount(request), csvPath);
                model.put(MESSAGE_CODE,"text.sync.success");
                result = "1";
            } catch (Exception e) {
                logger.error(e.getMessage(),e);
                model.put(MESSAGE_CODE,"text.sync.fail");
            } finally{
                try{
                    pcrfConsLogServiceImpl.writeResultToFile(type1, msisdn1,
                            result, id,
                            (String) getUserData(request).get(Constants.USER_ACCOUNT));
                }catch(Exception e){
                    logger.info("saveResultToFile error. e:", e.getMessage());
                }
            }

            ModelAndView mav = this.detail(request);
            
            mav.addAllObjects(model);
            
            return mav;
        } catch (Exception ex) {
            throw ex;
        }finally {
            try {
                if(osw != null)
                    osw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
    }

    /**
     * pcrfConsLog 明細頁
     * @param request
     * @return
     */
    @RequestMapping(value=URL_VIEW, method={RequestMethod.GET, RequestMethod.POST})
    public ModelAndView detail(HttpServletRequest request) {
        try {
            logger.info("detail pcrfConsLog to page.");
            ModelMap modelMap = new ModelMap();
            ModelAndView mav = getModelAndView(viewMap, VIEW_PAGE);
            
            Long logId = Long.parseLong(request.getParameter("logId"));
            Map<String,Object> logInfo = pcrfConsLogServiceImpl.findById(logId);
            
            if(logInfo == null) {
                modelMap.put(MESSAGE_CODE, "data.not.found");
            }else{
                String rptPath = (String) logInfo.get("RPT_PATH");
                
                if(rptPath == null || "".equals(rptPath)){
                    modelMap.put(MESSAGE_CODE, "data.not.found");
                }else{
                    try {
                        List<Map<String, String>> rptResult = pcrfConsLogServiceImpl.getRptDatas(logId, rptPath.split(",")[0]);
                        mav.addObject("rptResult", rptResult);
                    } catch (ServiceException ex) {
                        modelMap.put(MESSAGE_CODE, ex.getRtnCode());
                    }
                    
                    mav.addObject("logData", logInfo);
                }
            }
            mav.addObject("logId", logId);
            mav.addAllObjects(modelMap);
            
            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }
    
    /** 自動產生特定之 HTTP request attributes。 */
    @ModelAttribute
    protected void prepareModelAttribute (HttpServletRequest request, Map<String, Object> model) {
        try {
            String path = request.getServletPath();
            logger.info("prepareModelAttribute path:{}", path);
            if(path.startsWith(URL_CONTROLLER_PREFIX)) {
                String url = path.substring(URL_CONTROLLER_PREFIX.length());

                logger.debug("substring url:{}", url);

                String newUrl = URL_NEW + ".do";
                String editUrl = URL_EDIT + ".do";
                

                Pattern pattern = Pattern.compile("^(.*)(" + newUrl + "|" + editUrl +"){1}$");
                Matcher matcher = pattern.matcher(url);

                logger.info("matcher.matches result:{}", matcher.matches());

                if(!matcher.matches()) {
                    logger.info(String.format("No model attributes needed for the incoming request, %s.", path));
                } else {
//                    PcrfConsistent entity = pcrfConsistentServiceImpl.find();
//
//                    model.put(ATTRIBUTE_ENTITY, entity);
//                    model.put("execType",entity.getExecType());
                }
            }

        } catch(Throwable cause)  {
            logger.error(cause.getMessage(), cause);
        }
    }

    private void execDataSyncTask(final String logId,final String userId, final String rptPath) {
        class DataSyncTask implements Runnable {
            String logId;
            String userId;
            String rptPath;
            
            DataSyncTask(final String logId,final String userId, final String rptPath) { 
                this.logId = logId;
                this.userId = userId;
                this.rptPath = rptPath;}
            
            public void run() {
                try {
                    CommonUtil.invokeShell(SysParameterPropsReader.getConfig(Constants.DATA_SYNC_SHELL)
                            ,false ,new String[]{logId, userId, rptPath});
                } catch (Exception cause) {
                    logger.error(cause.getMessage(), cause);
                }
            }
        }
        
        Thread t = new Thread(new DataSyncTask(logId, userId, rptPath));
        t.start();
    }
    
}
