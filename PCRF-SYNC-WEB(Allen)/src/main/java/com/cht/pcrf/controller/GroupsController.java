package com.cht.pcrf.controller;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cht.pcrf.common.BaseAction;
import com.cht.pcrf.common.Constants;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.SapcInfo;
import com.cht.pcrf.model.SysFunc;
import com.cht.pcrf.model.SysFuncBtn;
import com.cht.pcrf.model.SysRole;
import com.cht.pcrf.model.SysRoleBtn;
import com.cht.pcrf.model.SysRoleFunc;
import com.cht.pcrf.model.SysRoleUser;
import com.cht.pcrf.model.User;
import com.cht.pcrf.service.GroupsServiceImpl;
import com.cht.pcrf.service.UserServiceImpl;

/**
 * <code>GroupsController</code>:群組管理
 * @author $Author: weilinchu $
 * @version $Id: GroupsController.java 323 2016-01-29 15:19:18Z weilinchu $
 */
@Controller
@RequestMapping(value=GroupsController.URL_CONTROLLER_PREFIX)
public class GroupsController extends BaseAction {

    private static final Logger logger = LoggerFactory
            .getLogger(GroupsController.class);

    protected static final String URL_CONTROLLER_PREFIX = "/groups";
    private static final String URL_FUNCTION_SETTING = "/functionSetting";
    private static final String URL_FUNCTION_SETTING_PAGE = "/{" + ATTRIBUTE_ID + "}" + URL_FUNCTION_SETTING;
    private static final String URL_USER_SETTING = "/userSetting";
    private static final String URL_USER_SETTING_PAGE = "/{" + ATTRIBUTE_ID + "}" + URL_USER_SETTING;

    private static final String VIEW_MAP_USER_SETTING = "USER_SETTING";
    private static final String VIEW_MAP_FUNCTION_SETTING = "FUNCTION_SETTING";

    protected static final String PAGE_URL_PREFIX = "/groups";
    private static final String PAGE_LIST = "/list";
    private static final String PAGE_NEW = "/new";
    private static final String PAGE_EDIT = "/edit";
    private static final String PAGE_DELETE = "/delete";
    private static final String PAGE_USER_SETTING = "/userSetting";
    private static final String PAGE_FUNCTION_SETTING = "/functionSetting";

    protected Map<String, Object> viewMap;

    @Autowired
    private GroupsServiceImpl groupsServiceImpl;

    @Autowired
    private UserServiceImpl userServiceImpl;

    public GroupsController() {
        viewMap = new HashMap<String, Object>();

        viewMap.put(Constants.LIST_PAGE, PAGE_URL_PREFIX + PAGE_LIST);
        viewMap.put(Constants.ADD_PAGE, PAGE_URL_PREFIX + PAGE_NEW);
        viewMap.put(Constants.EDIT_PAGE, PAGE_URL_PREFIX + PAGE_EDIT);
        viewMap.put(Constants.DELETE_PAGE, PAGE_URL_PREFIX + PAGE_DELETE);
        viewMap.put(VIEW_MAP_USER_SETTING, PAGE_URL_PREFIX + PAGE_USER_SETTING);
        viewMap.put(VIEW_MAP_FUNCTION_SETTING, PAGE_URL_PREFIX + PAGE_FUNCTION_SETTING);
    }

    /**
     * 清單頁
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value=URL_LIST, method={RequestMethod.GET})
    public ModelAndView list(HttpServletRequest request, Map<String, Object>  model) {
        try {
            logger.info("list all groups to page.");

            ModelAndView mav = getModelAndView(viewMap, LIST_PAGE);
            PageControler pageControler = getPageControler(request);

            List<SysRole> result = groupsServiceImpl.findAll(pageControler);

            logger.info("Groups query result:{}", (result != null?result.size():null));

            result = (List<SysRole>) getUserNameByUserId(userServiceImpl.listAllAccount(null), result);
            /*
            List<User> users = userServiceImpl.listAllAccount(null);
            if(users != null && !users.isEmpty()) {
                for(SysRole sysRole : result) {
                    for(User user : users) {
                        if(sysRole.getModifyUser().equals(user.getId())) {
                            sysRole.setModifyUserName(user.getAccount());
                        }
                    }
                }
            }
            */

            mav.addAllObjects(model);
            mav.addObject(ATTRIBUTE_ENTITY, result);
            mav.addObject(PAGES, pageControler);
            setPageControler(request,pageControler);

            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 查詢
     * @param roleName
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value=URL_LIST, method=RequestMethod.POST)
    public ModelAndView queryList(
            @RequestParam(value="roleName") String roleName,
            HttpServletRequest request,
            Map<String, Object> model) {
        try {
            logger.info("query SysRole to page by roleName.");

            if(StringUtils.isBlank(roleName)) {
                return this.list(request, model);
            }

            ModelAndView mav = getModelAndView(viewMap, LIST_PAGE);
            PageControler pageControler = getPageControler(request);

            List<SysRole> result = groupsServiceImpl.findByRoleName(roleName, pageControler);
            logger.info("SysRole query result:{}", (result != null?result.size():null));

            mav.addAllObjects(model);
            mav.addObject(ATTRIBUTE_ENTITY, result);
            mav.addObject("roleName", roleName);    //查詢條件
            mav.addObject(PAGES, pageControler);
            setPageControler(request,pageControler);

            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 前往新增頁
     * @param sysRole
     * @param request
     * @param sysRoleBindingResult
     * @param model
     * @return
     */
    @RequestMapping(value=URL_NEW, method={RequestMethod.GET})
    public ModelAndView addPage(
            @ModelAttribute(ATTRIBUTE_ENTITY) final SysRole sysRole,
            HttpServletRequest request,
            BindingResult sysRoleBindingResult,
            ModelMap model) {
        try {
            logger.info("Go to new a groups page.");

            ModelAndView mav = getModelAndView(viewMap, ADD_PAGE);
            mav.addAllObjects(model);
            logger.info("addpage messagecode:{}", mav.getModelMap().get(MESSAGE_CODE));
            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 新增群組
     * @param sysRole
     * @param sysRoleBindingResult
     * @param request
     * @return
     */
    @RequestMapping(value=URL_NEW, method={RequestMethod.POST})
    public ModelAndView add(
            @Valid @ModelAttribute(ATTRIBUTE_ENTITY) final SysRole sysRole,
            BindingResult sysRoleBindingResult,
            HttpServletRequest request) {
        logger.info("New a groups page.");
        logger.info("Print new groups info:{}", sysRole.toString());

        ModelMap modelMap = new ModelMap();
        try {

            sysRole.setRoleName(sysRole.getRoleName().toUpperCase());   //轉大寫存入

            if(sysRoleBindingResult.hasErrors()) {
                logger.info("Groups validate failed! SysRole:{}", sysRole.toString());
                return this.addPage(sysRole, request, sysRoleBindingResult, modelMap);
            }

            Long userId = getUserId(request);
            Timestamp time = new Timestamp(new Date().getTime());
            sysRole.setUsed(true);
            sysRole.setCreateUser(userId);
            sysRole.setCreateTime(time);
            sysRole.setModifyUser(userId);
            sysRole.setModifyTime(time);

            // save SysRole
            groupsServiceImpl.saveSysRole(sysRole);

            modelMap.put(MESSAGE_CODE, "text.new.groups.success");

            return this.list(request, modelMap);
        } catch (DuplicateKeyException dupex) {
            modelMap.put(MESSAGE_CODE, "text.new.groups.duplicate");
            return this.list(request, modelMap);
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 前往修改頁面
     * @param roleId
     * @param sysRole
     * @param request
     * @return
     */
    @RequestMapping(value=URL_ENTITY_EDIT, method={RequestMethod.GET})
    public ModelAndView editPage(
            @PathVariable(ATTRIBUTE_ID) final String roleId,
            @ModelAttribute(ATTRIBUTE_ENTITY) final SysRole sysRole,
            HttpServletRequest request) {
        try {
            logger.info("Go to edit page by roleId:{}", roleId);
            logger.info("SysRole:{}", sysRole.toString());

            ModelAndView mav = new ModelAndView();
            mav = getModelAndView(viewMap, EDIT_PAGE);
            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 修改群組
     * @param sysRole
     * @param request
     * @return
     */
    @RequestMapping(value=URL_ENTITY_EDIT, method={RequestMethod.POST})
    public ModelAndView edit(
            @ModelAttribute(ATTRIBUTE_ENTITY) final SysRole sysRole,
            HttpServletRequest request) {

        logger.info("Edit SysRole:{}, action:{}", new Object[]{ sysRole });
        ModelMap modelMap = new ModelMap();

        try {
            sysRole.setRoleName(sysRole.getRoleName().toUpperCase());   //轉大寫存入

            sysRole.setModifyUser(getUserId(request));
            sysRole.setModifyTime(new Timestamp(new Date().getTime()));

            // update SysRole
            groupsServiceImpl.updateSysRole(sysRole);


            modelMap.put(MESSAGE_CODE, "text.update.groups.success");

            return this.list(request, modelMap);
        } catch (DuplicateKeyException dupex) {
            modelMap.put(MESSAGE_CODE, "text.new.groups.duplicate");
            return this.list(request, modelMap);
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }

    }

    /**
     * 刪除群組
     * @param roleId
     * @param request
     * @return
     */
    @RequestMapping(value=URL_ENTITY_DELETE, method={RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE})
    public ModelAndView delete(
            @PathVariable(ATTRIBUTE_ID) final Long roleId,
            HttpServletRequest request) {
        try {
            logger.info("Delete Groups:{}", roleId);

            groupsServiceImpl.deleteGroups(roleId);

            ModelMap modelMap = new ModelMap();
            modelMap.put(MESSAGE_CODE, "text.delete.groups.success");

            return this.list(request, modelMap);
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 群組功能頁
     * @param roleId
     * @param sysRole
     * @param request
     * @return
     */
    @RequestMapping(value=URL_FUNCTION_SETTING_PAGE, method=RequestMethod.GET)
    public ModelAndView functionSettingPage(
            @PathVariable(ATTRIBUTE_ID) final String roleId,
            @ModelAttribute(ATTRIBUTE_ENTITY) final SysRole sysRole,
            HttpServletRequest request) {
        try {
            logger.info("Go to functionSettingPage page by roleId:{}", roleId);
            logger.info("SysRole:{}", sysRole.toString());

            ModelAndView mav = new ModelAndView();
            PageControler pageControler = getPageControler(request);
            /*
             * select function_name from sys_func by role_id
             * select btn from sys_func_btn by role_id
             */
            List<SysFunc> sysFuncs = groupsServiceImpl.findAllFuncs(pageControler); // all functions
            List<SysFuncBtn> sysFuncBtns = groupsServiceImpl.findAllSysFuncBtns(pageControler); // all buttons by function

            List<SysRoleBtn> sysRoleBtns = groupsServiceImpl.findSysRoleBtnByRoleId(Long.valueOf(roleId)); // checked role btns
            List<SysRoleFunc> sysRoleFuncs = groupsServiceImpl.findSysRoleFuncByRoleId(Long.valueOf(roleId));

            mav = getModelAndView(viewMap, VIEW_MAP_FUNCTION_SETTING);
            mav.addObject("funcs", sysFuncs);
            mav.addObject("funcBtns", sysFuncBtns);
            mav.addObject("sysRoleBtns", sysRoleBtns);
            mav.addObject(ATTRIBUTE_ENTITY, sysRole);
//            mav.addObject(PAGES, pageControler);
//            setPageControler(request,pageControler);

            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 群組功能更新
     * @param roleId
     * @param btnIds
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value=URL_FUNCTION_SETTING, method=RequestMethod.POST)
    public ModelAndView funcSetting(
            @RequestParam("roleId") final Long roleId,
            @RequestParam(value="btnIds",required=false) final Long[] btnIds,
            HttpServletRequest request, Map<String, Object>  model) {
        try {
            logger.info("Save FunctionSetting Result by roleId:{}, btnCheckBox:{}"
                    , new Object[]{roleId, Arrays.deepToString(btnIds)});
            
            if(btnIds == null){
            	groupsServiceImpl.deleteSysRoleBtnByRoleId(roleId);
            	groupsServiceImpl.deleteSysRoleFuncByRoleId(roleId);
            }else{
            	groupsServiceImpl.updateFunctionSetting(roleId, getUserId(request), btnIds);
            }

            model.put(MESSAGE_CODE, "text.update.groups.function.setting.success");

            return this.list(request, model);
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 群組使用者頁
     * @param roleId
     * @param sysRole
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value=URL_USER_SETTING_PAGE, method=RequestMethod.GET)
    public ModelAndView userSettingPage(
            @PathVariable(ATTRIBUTE_ID) final Long roleId,
            @ModelAttribute(ATTRIBUTE_ENTITY) final SysRole sysRole,
            HttpServletRequest request,
            Map<String, Object> model) {
        try {
            logger.info("Go to userSettingPage page by roleId:{}", roleId);
            logger.info("SysRole:{}", sysRole.toString());

            ModelAndView mav = new ModelAndView();
            PageControler pageControler = getPageControler(request);

            List<User> users = userServiceImpl.listAllAccount(pageControler);
            List<SysRoleUser> sysRoleUsers = groupsServiceImpl.findSysRoleUserByRoleId(roleId);

            // Get Checked user
            List<String> userIds = groupsServiceImpl.getCheckedUsers(users, sysRoleUsers);

            mav = getModelAndView(viewMap, VIEW_MAP_USER_SETTING);
            mav.addObject("SysRoleEntity", sysRole);
            mav.addObject("sysRoleUsers", sysRoleUsers);
            mav.addObject("userIdChecked", userIds);
            mav.addObject(ATTRIBUTE_ENTITY, users);
            mav.addObject(PAGES, pageControler);
            setPageControler(request,pageControler);
            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 群組使用者更新
     * @param roleId
     * @param userIds
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value=URL_USER_SETTING, method=RequestMethod.POST)
    public ModelAndView userSetting(
            @RequestParam("roleId") final Long roleId,
            @RequestParam(value="userId",required = false) final Long[] userIds,
            HttpServletRequest request, Map<String, Object>  model) {
        try {
            logger.info("Save UserSetting Result by roleId:{}, userId:{}"
                    , new Object[]{roleId, Arrays.deepToString(userIds)});
        	
        	if(userIds == null){
        		groupsServiceImpl.updateSysRoleUserNullUserIds(roleId);
        	}else{
        		groupsServiceImpl.updateSysRoleUser(roleId, getUserId(request), userIds);
        	}

            model.put(MESSAGE_CODE, "text.update.groups.user.setting.success");

            return this.list(request, model);
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /** 自動產生特定之 HTTP request attributes。 */
    @ModelAttribute
    protected void prepareModelAttribute (HttpServletRequest request, Map<String, Object> model) {
        try {
            String path = request.getServletPath();
            logger.info("prepareModelAttribute path:{}", path);
            if(path.startsWith(URL_CONTROLLER_PREFIX)) {
                String url = path.substring(URL_CONTROLLER_PREFIX.length());

                logger.debug("substring url:{}", url);

                String newUrl = URL_NEW + ".do";
                String editUrl = URL_EDIT + ".do";
                String functionSettingUrl = "/functionSetting" + ".do";
                String userSettingUrl = "/userSetting" + ".do";

                Pattern pattern = Pattern.compile("^(.*)(" + newUrl + "|" + editUrl + "|"
                                        + functionSettingUrl + "|" + userSettingUrl + "){1}$");
                Matcher matcher = pattern.matcher(url);

                logger.info("matcher.matches result:{}", matcher.matches());

                if(!matcher.matches()) {
                    logger.info(String.format("No model attributes needed for the incoming request, %s.", path));
                } else {
                    // /XXXX/edit 取出的 id 為 /XXXX，故此真實的 id 要 substring(1)
                    String id = matcher.group(1);
                    logger.info("sub id:{}", id);
                    SysRole entity = StringUtils.isBlank(id) ?
                            new SysRole() : groupsServiceImpl.findById(Long.valueOf(id.substring(1)));

                    logger.info(String.format("url = %s, id = %s, entity = %s", url, id, entity));

                    model.put(ATTRIBUTE_ENTITY, entity);
                }
            }

        } catch(Throwable cause)  {
            logger.error(cause.getMessage(), cause);
        }
    }
}
