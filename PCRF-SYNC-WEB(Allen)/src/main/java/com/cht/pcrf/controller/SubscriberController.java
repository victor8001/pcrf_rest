package com.cht.pcrf.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cht.pcrf.utils.ConvertUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cht.pcrf.api.JobExecute;
import com.cht.pcrf.common.BaseAction;
import com.cht.pcrf.common.Constants;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.ConfValueInfo;
import com.cht.pcrf.model.Subscriber;
import com.cht.pcrf.service.CommonServiceImpl;
import com.cht.pcrf.service.SubscriberServiceImpl;
import com.cht.pcrf.utils.DateUtil;
import com.cht.pcrf.utils.SystemUtility;

/**
 * <code>SubscriberController</code>:門號管理 Controller
 * @author $Author: rayliao $
 * @version $Id: SubscriberController.java 282 2016-01-21 09:19:09Z rayliao $
 */
@Controller
@RequestMapping(SubscriberController.URL_CONTROLLER_PREFIX)
public class SubscriberController extends BaseAction{
    private static final Logger logger = LoggerFactory
            .getLogger(SubscriberController.class);

    protected static final String URL_CONTROLLER_PREFIX = "/subscriber";

    protected static final String PAGE_URL_PREFIX = "/subscriber";
    private static final String PAGE_LIST = "/list";

    
    @Autowired
    private SubscriberServiceImpl subscriberServiceImpl;
    
    @Autowired
    private CommonServiceImpl commonServiceImpl;

    @Autowired
    private ConvertUtils convertUtils;


    protected Map<String, Object> viewMap;

    public SubscriberController() {
        viewMap = new HashMap<String, Object>();

        viewMap.put(Constants.LIST_PAGE, PAGE_URL_PREFIX + PAGE_LIST);

    }

    /**
     * 清單頁
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value=URL_LIST, method=RequestMethod.GET)
    public ModelAndView list(HttpServletRequest request, Map<String, Object> model) {
        try {
            logger.info("list all subscriber to page.");

            ModelAndView mav = getModelAndView(viewMap, LIST_PAGE);
//            PageControler pageControler = getPageControler(request);

//            List<Subscriber> result = subscriberServiceImpl.findAll(pageControler);
//            logger.info("Subscriber query result:{}", (result != null?result.size():null));
            List<ConfValueInfo>  commons =commonServiceImpl.queryConfValue("node_location");
            System.out.println("list entity:" + model.get(ATTRIBUTE_ENTITY));
            
            mav.addObject("commons", commons);
            mav.addAllObjects(model);
            mav.addObject(ATTRIBUTE_ENTITY, new ArrayList<Subscriber>());
//            mav.addObject(PAGES, pageControler);
//            setPageControler(request,pageControler);

            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * Query Subscriber by MSISDN
     * @param msisdn
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value=URL_LIST, method=RequestMethod.POST)
    public ModelAndView queryList(
            @RequestParam(value="msisdn") String msisdn,
            @RequestParam(value="node_location") String location,
            HttpServletRequest request,
            Map<String, Object> model) {
        try {
        	
        	String OriginalLocation = location;
            logger.info("query subscriber to page by msisdn.");

            if(StringUtils.isBlank(msisdn)) {
                return this.list(request, model);
            }
            String message = (String) model.get("message");
           
            
            ModelAndView mav = getModelAndView(viewMap, LIST_PAGE);
            List<Subscriber> result = new ArrayList<Subscriber>();
            List<Subscriber> resultKH = new ArrayList<Subscriber>();
            List<Subscriber> resultTP = new ArrayList<Subscriber>();
            
            PageControler pageControler = getPageControler(request);
            
            if(location.equals("KH") || location.equals("TP")){
            	result = subscriberServiceImpl.findByMsisdn(msisdn, pageControler,location);
            	logger.info("Subscriber query result:{}", (result != null?result.size():null));
            }
            
            if(location.equals("-1")){
            	for(int i=0 ; i<2 ;i++){
            		if(i == 0){
            			location = "KH";
            			resultKH = subscriberServiceImpl.findByMsisdn(msisdn, pageControler,location);
            		}
            		if(i == 1){
            			location = "TP";
            			resultTP = subscriberServiceImpl.findByMsisdn(msisdn, pageControler,location);
            		}
            	}
            }
            
            List<ConfValueInfo>  commons =commonServiceImpl.queryConfValue("node_location");
            
            mav.addObject("commons", commons);
            mav.addAllObjects(model);
            mav.addObject(ATTRIBUTE_ENTITY, result);
            mav.addObject("resultKH", resultKH);
            mav.addObject("resultTP", resultTP);
            mav.addObject("OriginalLocation", OriginalLocation);
            mav.addObject(PAGES, pageControler);
            mav.addObject("msisdn", msisdn);    //保留查詢參數
            mav.addObject("message", message);  
            setPageControler(request,pageControler);

            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }
    
    @RequestMapping(value="/exp", method=RequestMethod.POST)
    public ModelAndView exp(HttpServletRequest request,
    		HttpServletResponse response,
    		Map<String, Object> model) throws JsonProcessingException {
    	
    	String OriginalLocation = StringUtils.trimToEmpty(request.getParameter("OriginalLocation"));
    	String msisdn1 = StringUtils.trimToEmpty(request.getParameter("msisdn"));
        String location = StringUtils.trimToEmpty(request.getParameter("location"));
        String type = StringUtils.trimToEmpty(request.getParameter("type"));
        String info = StringUtils.trimToEmpty(request.getParameter("info"));
        String col1_1 = StringUtils.trimToEmpty(request.getParameter("col1_1")) ;
        String col11_1 = StringUtils.trimToEmpty(request.getParameter("col11_1"));

        String jsonStr = convertUtils.toJSONString(msisdn1,info,col11_1);
        String fileName1 = "CSV_" 
        		 + (msisdn1.equals("") ? "ALL" : msisdn1) + "_" 
        		 + DateUtil.formatDate(new Date())+".csv";
        
        final String spileWord = ",";
         
        if (location.equals("台北")) {
            location = "TP";
        } else {
            location = "KH";
        }
        
        String path = SystemUtility.getConfig(Constants.SUBSCRIBER_FILL_PATH);
    	String csvPath = path+fileName1;
    	
    	File file1 = new File(csvPath);
    	
    	FileOutputStream fos = null;
    	OutputStreamWriter osw = null;
    	
		try {
			fos = new FileOutputStream(file1);
			osw = new OutputStreamWriter(fos, "UTF-8");
			osw.write(0xFEFF);
			
			String data = "node"+spileWord+"type"+spileWord+"subscriber"+spileWord+"col1_1"+spileWord+"info"+"\n"
                    +location+spileWord+type+spileWord+msisdn1+spileWord+col1_1+spileWord+jsonStr
                    ;
		
			
			osw.write(data);
			osw.flush();
			
			try {
			    JobExecute.getInstance().getSyncRestService().syncBySub(getUserAccount(request), csvPath);
			} catch (Exception e) {
			    logger.error(e.getMessage(),e);
			    model.put("message","回補差異失敗");
			}
			if(model.get("message") == null){
				 model.put("message","回補差異成功");
			}
			
			
		} catch (Exception ex) {
            return exceptionProcess(ex);
        }finally {
			try {
			    if(osw != null)
			    	osw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
        return queryList(msisdn1, OriginalLocation, request, model);

    }

    /** 自動產生特定之 HTTP request attributes。 */
    @ModelAttribute
    protected void prepareModelAttribute (HttpServletRequest request, Map<String, Object> model) {
        try {
            String path = request.getServletPath();
            logger.info("prepareModelAttribute path:{}", path);
            if(path.startsWith(URL_CONTROLLER_PREFIX)) {
                String url = path.substring(URL_CONTROLLER_PREFIX.length());

                logger.debug("substring url:{}", url);

                String newUrl = URL_NEW + ".do";
                String editUrl = URL_EDIT + ".do";
                String expUrl ="/exp.do";

                Pattern pattern = Pattern.compile("^(.*)(" + newUrl + "|" + editUrl + "|"+ expUrl + "){1}$");
                Matcher matcher = pattern.matcher(url);

                logger.info("matcher.matches result:{}", matcher.matches());

                if(!matcher.matches()) {
                    logger.info(String.format("No model attributes needed for the incoming request, %s.", path));
                } 
            }

        } catch(Throwable cause)  {
            logger.error(cause.getMessage(), cause);
        }
    }
}
