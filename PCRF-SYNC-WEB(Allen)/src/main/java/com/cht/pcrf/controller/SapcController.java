package com.cht.pcrf.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cht.pcrf.common.BaseAction;
import com.cht.pcrf.common.Constants;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.ConfValueInfo;
import com.cht.pcrf.model.SapcInfo;
import com.cht.pcrf.service.CommonServiceImpl;
import com.cht.pcrf.service.SapcInfoServiceImpl;
import com.cht.pcrf.service.UserServiceImpl;
import com.cht.pcrf.utils.CommonUtil;
import com.cht.pcrf.utils.EncryptUtility;
import com.cht.pcrf.utils.SysParameterPropsReader;

/**
 * <code>SPACController</code>: SPAC 控制器
 * @author $Author: weilinchu $
 * @Id $Id: SapcController.java 365 2016-03-02 03:01:31Z weilinchu $
 */
@Controller
@RequestMapping(SapcController.URL_CONTROLLER_PREFIX)
public class SapcController extends BaseAction {
    private static final Logger logger = LoggerFactory.getLogger(SapcController.class);
//    private static final Logger operateLogger = LoggerFactory.getLogger(Constants.LOGGER_PG_WEB);

    protected static final String URL_CONTROLLER_PREFIX = "/pcrfMGMT";
    private static final String URL_DATA_CONSISTENT = "/{" + ATTRIBUTE_ID + "}/"+"/dataConsistent";

    protected static final String PAGE_URL_PREFIX = "/sapcManagement";
    private static final String PAGE_LIST = "/list";
    private static final String PAGE_NEW = "/new";
    private static final String PAGE_EDIT = "/edit";
    private static final String PAGE_DELETE = "/delete";
   
    
    @Autowired
    private CommonServiceImpl commonServiceImpl;
    
    @Autowired
    private SapcInfoServiceImpl sapcInfoServiceImpl;

    @Autowired
    private UserServiceImpl userServiceImpl;
    
    protected Map<String, Object> viewMap;

    public SapcController(){
        viewMap = new HashMap<String, Object>();

        viewMap.put(Constants.LIST_PAGE, PAGE_URL_PREFIX + PAGE_LIST);
        viewMap.put(Constants.ADD_PAGE, PAGE_URL_PREFIX + PAGE_NEW);
        viewMap.put(Constants.EDIT_PAGE, PAGE_URL_PREFIX + PAGE_EDIT);
        viewMap.put(Constants.DELETE_PAGE, PAGE_URL_PREFIX + PAGE_DELETE);
        
    }

    /**
     * SAPC 清單頁
     * @param request
     * @return
     */
    @RequestMapping(value=URL_LIST, method={RequestMethod.GET, RequestMethod.POST})
    public ModelAndView list(HttpServletRequest request, ModelMap model) {
        try {
            logger.info("list all spac to page.");
            PageControler pageControler = getPageControler(request);
            List<SapcInfo> result = sapcInfoServiceImpl.findAll(pageControler);
            result = (List<SapcInfo>) getUserNameByUserId(userServiceImpl.listAllAccount(null), result);

            logger.info("SAPC query result:{}", (result != null?result.size():null));

            ModelAndView mav = getModelAndView(viewMap, LIST_PAGE);
            mav.addAllObjects(model);
            mav.addObject(ATTRIBUTE_ENTITY, result);
            mav.addObject(PAGES, pageControler);
            setPageControler(request,pageControler);

            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 前往新增頁
     * @param request
     * @return
     */
    @RequestMapping(value=URL_NEW, method={RequestMethod.GET})
    public ModelAndView addPage(
            @ModelAttribute(ATTRIBUTE_ENTITY) final SapcInfo sapcInfo,
            HttpServletRequest request,
            BindingResult sapcinfoBindingResult,
            ModelMap model) {
        try {
            logger.info("Go to new a sapc page.");
            
            List<ConfValueInfo>  commons =commonServiceImpl.queryConfValueAndTypeNotNegativeOne("node_location");
            
            ModelAndView mav = getModelAndView(viewMap, ADD_PAGE);
            mav.addAllObjects(model);
            mav.addObject("commons", commons);
            logger.info("addpage messagecode:{}", mav.getModelMap().get(MESSAGE_CODE));
            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 新增 SAPC
     * @param sapcInfo
     * @param request
     * @return
     */
    @RequestMapping(value=URL_NEW, method={RequestMethod.POST})
    public ModelAndView add(
    		@RequestParam(value="node_location") String location,
            @Valid @ModelAttribute(ATTRIBUTE_ENTITY) final SapcInfo sapcInfo,
            BindingResult sapcinfoBindingResult,
            HttpServletRequest request) {
        try {
            logger.info("New a sapc page.");
            logger.info("Print new SAPC info:{}", sapcInfo.toString());

            ModelMap modelMap = new ModelMap();

            // validate
            if(sapcinfoBindingResult.hasErrors()) {
                logger.info("SAPC validate failed! SAPC:{}", sapcInfo.toString());
                return this.addPage(sapcInfo, request, sapcinfoBindingResult, modelMap);
            }

            // validate duplicate
            SapcInfo isSapcDuplicate = sapcInfoServiceImpl.findByNodeName(sapcInfo.getNode_name());
            if(isSapcDuplicate != null) {
                logger.info("isSapcDuplicate:{}", isSapcDuplicate.toString());
                logger.info("The SAPC:{} already exists.", sapcInfo.toString());
                modelMap.put(MESSAGE_CODE, "text.sapc.duplicate");
                return this.addPage(sapcInfo, request, null, modelMap);
            }
            
            sapcInfo.setLocation(location);
            sapcInfo.setCreateUser(getUserId(request));
            sapcInfo.setCreateTime(getModifyTime());
            sapcInfo.setModifyUser(getUserId(request));
            sapcInfo.setModifyTime(getModifyTime());

            // PASSWORD to MD5
            //TODO:修改加密規則
            //String loginPasswdToMd5 = EncryptUtility.toMd5(sapcInfo.getLoginPw());
            //sapcInfo.setLoginPw(loginPasswdToMd5);

            // set default status(4:new)
            sapcInfo.setStatus(1);
            sapcInfo.setSequence(0);
            
            Long id = sapcInfoServiceImpl.findPcrfInfoIdSeq();
            sapcInfo.setId(id);
            
            // save SAPC
            sapcInfoServiceImpl.saveSapc(sapcInfo);

            modelMap.put(MESSAGE_CODE, "text.new.sapc.success");

            return this.list(request, modelMap);
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 前往編輯頁
     * @param sapcNodeName
     * @param sapcInfo
     * @param request
     * @return
     */
    @RequestMapping(value=URL_ENTITY_EDIT, method={RequestMethod.GET})
    public ModelAndView editPage(
            @PathVariable(ATTRIBUTE_ID) final String sapcNodeName,
            @ModelAttribute(ATTRIBUTE_ENTITY) final SapcInfo sapcInfo,
            HttpServletRequest request,
            BindingResult sapcinfoBindingResult) {
        try {
            logger.info("Go to edit page by SAPC:{}", sapcNodeName);
            logger.info("sapcInfo:{}", sapcInfo.toString());

            ModelAndView mav = new ModelAndView();
            mav = getModelAndView(viewMap, EDIT_PAGE);
//            mav.addObject("statusMap", getStatusMap());
            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * 編輯 SAPC
     * @param sapcInfo
     * @param request
     * @return
     */
    @RequestMapping(value=URL_ENTITY_EDIT, method={RequestMethod.POST})
    public ModelAndView edit(
            @Valid @ModelAttribute(ATTRIBUTE_ENTITY) final SapcInfo sapcInfo,
            BindingResult sapcinfoBindingResult,
            HttpServletRequest request) {
        try {
            logger.info("Edit SAPC:{}", sapcInfo);

            // validate
            if(sapcinfoBindingResult.hasErrors()) {
                logger.info("SAPC validate failed! SAPC:{}", sapcInfo.toString());
                return this.editPage(sapcInfo.getNode_name(), sapcInfo, request, sapcinfoBindingResult);
            }

            sapcInfo.setModifyUser(getUserId(request));
            sapcInfo.setModifyTime(getModifyTime());

            // PASSWORD to MD5
            // TODO:修改加密規則
            /*
            String loginPw = sapcInfo.getLoginPw();
            if(StringUtils.isNotBlank(loginPw)) {
                String loginPasswdToMd5 = EncryptUtility.toMd5(loginPw);
                sapcInfo.setLoginPw(loginPasswdToMd5);
                logger.info("SAPC passwd to md5:{}", sapcInfo.getLoginPw());
               // sapcInfo.setLoginPw(loginPw);
            }
            */

            // save SAPC
            sapcInfoServiceImpl.updateSapc(sapcInfo);

            ModelMap modelMap = new ModelMap();
            modelMap.put(MESSAGE_CODE, "text.update.sapc.success");

            return this.list(request, modelMap);
//        } catch (AdaptorException ex) {
//            logger.error(ex.getMessage(),ex);
//            ModelMap modelMap = new ModelMap();
//            modelMap.put(MESSAGE_CODE, "MSG_0011");
//            return this.editPage(sapcInfo.getNode_name(), sapcInfo, request, sapcinfoBindingResult);
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }

    }

    /**
     * 刪除SAPC
     * @param nodeName
     * @param request
     * @return
     */
    @RequestMapping(value=URL_ENTITY_DELETE, method={RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE})
    public ModelAndView delete(
            @PathVariable(ATTRIBUTE_ID) final Long id,
            HttpServletRequest request) {
        try {
            logger.info("Delete SAPC:{}", id);

            // delete SAPC
            //sapcInfoServiceImpl.deleteSapc(nodeName);
            
            //delete 將STATUS update 為 2
            sapcInfoServiceImpl.updateSapcStatusById(id, 2);

            ModelMap modelMap = new ModelMap();
            modelMap.put(MESSAGE_CODE, "text.delete.sapc.success");

            return this.list(request, modelMap);
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }
    
    /**
     * 資料比對
     * @param nodeName
     * @param request
     * @return
     */
    @RequestMapping(value=URL_DATA_CONSISTENT, method={RequestMethod.GET, RequestMethod.POST})
    public ModelAndView dataConsistent(
            @PathVariable(ATTRIBUTE_ID) final Long id,
            HttpServletRequest request) {
        try {
            logger.info("Data Consistent SAPC:{}", id);
            ModelMap modelMap = new ModelMap();

            //call shell，傳入pcrf_name userId
            SapcInfo sapcInfo = sapcInfoServiceImpl.findById(Long.toString(id));
            
            if(sapcInfo == null) {
                modelMap.put(MESSAGE_CODE, "data.not.found");
            }else{
                execPcrfConsTask(Long.toString(getUserId(request)), sapcInfo.getNode_name());
                
//                CommonUtil.invokeShell(false, SysParameterPropsReader.getConfig(Constants.DATA_SYNC_SHELL)
//                        , new String[]{Long.toString(getUserId(request)), sapcInfo.getNode_name()});
                
                modelMap.put(MESSAGE_CODE, "text.executing");
            }

            return this.list(request, modelMap);
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /** 自動產生特定之 HTTP request attributes。 */
    @ModelAttribute
    protected void prepareModelAttribute (HttpServletRequest request, Map<String, Object> model) {
        try {
            String path = request.getServletPath();
            logger.info("prepareModelAttribute path:{}", path);
            if(path.startsWith(URL_CONTROLLER_PREFIX)) {
                String url = path.substring(URL_CONTROLLER_PREFIX.length());

                logger.debug("substring url:{}", url);

                String newUrl = URL_NEW + ".do";
                String editUrl = URL_EDIT + ".do";

                Pattern pattern = Pattern.compile("^(.*)(" + newUrl + "|" + editUrl + "){1}$");
                Matcher matcher = pattern.matcher(url);

                logger.info("matcher.matches result:{}", matcher.matches());

                if(!matcher.matches()) {
                    logger.info(String.format("No model attributes needed for the incoming request, %s.", path));
                } else {
                    // /XXXX/edit 取出的 id 為 /XXXX，故此真實的 id 要 substring(1)
                    String id = matcher.group(1);
                    logger.info("sub id:{}", id);
                    SapcInfo entity = StringUtils.isBlank(id) ?
                            new SapcInfo() : sapcInfoServiceImpl.findById(id.substring(1));

                    logger.info(String.format("url = %s, id = %s, entity = %s", url, id, entity));

                    model.put(ATTRIBUTE_ENTITY, entity);
                }
            }

        } catch(Throwable cause)  {
            logger.error(cause.getMessage(), cause);
        }
    }
    

    private void execPcrfConsTask(final String userId, final String nodeName) {
        class PcrfConsTask implements Runnable {
            String userId;
            String nodeName;
            
            PcrfConsTask(final String userId, final String nodeName) { 
                this.userId = userId;
                this.nodeName = nodeName;}
            
            public void run() {
                try {
                    CommonUtil.invokeShell(SysParameterPropsReader.getConfig(Constants.DATA_CONSISTENT_SHELL)
                            ,false , new String[]{userId, nodeName});
                } catch (Exception cause) {
                    logger.error(cause.getMessage(), cause);
                }
            }
        }
        
        Thread t = new Thread(new PcrfConsTask(userId, nodeName));
        t.start();
    }
}
