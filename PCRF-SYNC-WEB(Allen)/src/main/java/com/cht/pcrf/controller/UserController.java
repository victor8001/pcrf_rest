package com.cht.pcrf.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cht.pcrf.common.BaseAction;
import com.cht.pcrf.common.CommonValidation;
import com.cht.pcrf.common.Constants;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.SysRole;
import com.cht.pcrf.model.User;
import com.cht.pcrf.service.GroupsServiceImpl;
import com.cht.pcrf.service.UserServiceImpl;
import com.cht.pcrf.utils.EncryptUtility;

/**
 * <code>UserController</code>: 「使用者管理」控制器。
 * @version $Id$
 */
@Controller
@RequestMapping(UserController.URL_CONTROLLER_PREFIX)
public class UserController extends BaseAction{

    protected Map<String, Object> viewMap;

    @Autowired(required=true)
    private UserServiceImpl userServiceImpl;

    @Autowired
    private GroupsServiceImpl groupsServiceImpl;

    @Autowired
    private MessageSource messageSource = null;

    public UserController(){
        viewMap = new HashMap<String, Object>();

        viewMap.put(Constants.LIST_PAGE, PAGE_URL_PREFIX + PAGE_LIST);
        viewMap.put(Constants.ADD_BEFORE_PAGE, PAGE_URL_PREFIX + PAGE_ADD_BEFORE);
        viewMap.put(Constants.EDIT_PAGE, PAGE_URL_PREFIX + PAGE_EDIT);
        viewMap.put(Constants.DELETE_PAGE, PAGE_URL_PREFIX + PAGE_DELETE);
    }

    protected static final String URL_CONTROLLER_PREFIX = "/user";
    protected static final String URL_BLOCK = "/{" + ATTRIBUTE_ID + "}" + "/block_reset";

    protected static final String PAGE_URL_PREFIX = "/userControl";
    private static final String PAGE_LIST = "/list";
    private static final String PAGE_ADD_BEFORE = "/add_before";
    private static final String PAGE_EDIT = "/edit";
    private static final String PAGE_DELETE = "/delete";

    private static final String MESSAGE_CODE = "messageCode";

    @RequestMapping(value=URL_LIST, method={RequestMethod.GET, RequestMethod.POST})
    public ModelAndView list(
            HttpServletRequest request) {
        try
        {
            ModelAndView modelAndView;
            PageControler pageControler = getPageControler(request);
            Map userData = getUserData(request);
            List<User> datas = userServiceImpl.listAllAccount(pageControler);

            modelAndView = getModelAndView(viewMap,Constants.LIST_PAGE);
            modelAndView.addObject("USER_RS", datas);
            modelAndView.addObject("PAGES", pageControler);
            setPageControler(request,pageControler);

            return modelAndView;

        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }
    }

    /**
     * 鎖定
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = URL_BLOCK,method = { RequestMethod.GET, RequestMethod.POST })
    @SuppressWarnings("rawtypes")
    public ModelAndView block_reset(
            @PathVariable(ATTRIBUTE_ID) final String accountId,
            HttpServletRequest request,
            HttpServletResponse response) {
        try
        {
            ModelAndView modelAndView;
            PageControler pageControler = getPageControler(request);
            Map userData = getUserData(request);
            Map<String,Object> update = new HashMap<String,Object>();
            List<User> datas;

            if(StringUtils.isNotBlank(accountId)) {
                logger.info("Reset blocked and retry count. userId:{}", accountId);
                update.put("ACCOUNT", accountId);
                userServiceImpl.blockReset(update);
                userServiceImpl.resetRetryCountByAccount(accountId);
            }

            datas = userServiceImpl.listAllAccount(pageControler);

            modelAndView = getModelAndView(viewMap,Constants.LIST_PAGE);
            modelAndView.addObject("USER_RS", datas);
            modelAndView.addObject("PAGES", pageControler);
            setPageControler(request,pageControler);

            return modelAndView;

        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }
    }

    /**
     * 修改密碼頁
     * @param accountId
     * @param user
     * @param request
     * @return
     */
    @RequestMapping(value=URL_ENTITY_EDIT, method = { RequestMethod.GET })
    public ModelAndView editPage(
            @PathVariable(ATTRIBUTE_ID) final String accountId,
//            @ModelAttribute(ATTRIBUTE_ENTITY) final User user,
            HttpServletRequest request,
            ModelMap model) {
        try {
            logger.info("ready to edit account:{}", accountId);
//            logger.info("edit user:{}", user.toString());

            ModelAndView modelAndView = getModelAndView(viewMap,Constants.EDIT_PAGE);

            //select all group
            List<SysRole> sysRoleList = groupsServiceImpl.findAll(null);
            
            //get user role
            Long userId = userServiceImpl.getUserIdByAccount(accountId);
            Map<Long,String> userRolesMap = groupsServiceImpl.getUserRole(userId);
            
            modelAndView.addObject(ATTRIBUTE_ENTITY, model.get(ATTRIBUTE_ENTITY));
            modelAndView.addObject("ROLE_RS", sysRoleList);
            modelAndView.addObject("USER_ROLES_MAP", userRolesMap);

            return modelAndView;
        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }
    }

    /**
     * 修改密碼
     * @param user
     * @param request
     * @return
     */
    @RequestMapping(value=URL_ENTITY_EDIT, method = { RequestMethod.POST })
    @SuppressWarnings({"unchecked","rawtypes"})
    public ModelAndView edit(
//            @ModelAttribute(ATTRIBUTE_ENTITY) final User user,
            @RequestParam(value="password", required=true) String passwd,
            @RequestParam(value="userId", required=true) String reqUserId,
            //@RequestParam("roleIds") final Long[] roleIds,
            HttpServletRequest request,
            ModelMap model) {
        try {
            User user = (User) model.get(ATTRIBUTE_ENTITY);
            user.setPassword(passwd);
            logger.info("User account:" + user.getAccount());
            //logger.info("User password:" + user.getPassword());

            ModelAndView modelAndView = getModelAndView(viewMap,Constants.EDIT_PAGE);;
            PageControler pageControler = getPageControler(request);
            String[] roleIds = request.getParameterValues("roleIds");
            
            // Get user info
            Map userData = getUserData(request);
            Date nowTime = new Date();
            Date modify_time = user.getModifyTime();
            //String loginUserID = getUserAccount(request);
            String changePasswordUserID = user.getAccount();
            String password = user.getPassword();
            String encryptPassword = EncryptUtility.toMd5(password);
            User changePasswordUser = userServiceImpl.queryByAccount(changePasswordUserID).get(0);

            // Set Page datas
            List<User> datas = userServiceImpl.listAllAccount(pageControler);
            modelAndView.addObject("USER_RS", datas);
            modelAndView.addObject("PAGES", pageControler);
            setPageControler(request,pageControler);
            
            //select all group
            List<SysRole> sysRoleList = groupsServiceImpl.findAll(null);
            //get user role
            Map<Long,String> userRolesMap = getUserRolesMap(roleIds);

            modelAndView.addObject("ROLE_RS", sysRoleList);
            modelAndView.addObject("USER_ROLES_MAP", userRolesMap);

            // validate
            if(!CommonValidation.complyWithPasswordRules(password)) {
                //MSG_0004=密碼需依照規則建立，請您重新輸入
                modelAndView.addObject("messageCode", "MSG_0004");
                return modelAndView;
            }
            if(CommonValidation.passwdHasBeenUsed(encryptPassword, changePasswordUser)) {

                logger.info("destPasswdEncrypted:{}, user passwd:{}, first_passwd:{}, second_passwd:{}, third_passwd:{}, fourth_passwd:{}",
                        new Object[]{encryptPassword, changePasswordUser.getPassword(),
                                changePasswordUser.getPassword_first(), changePasswordUser.getPassword_second(),
                                changePasswordUser.getPassword_third(), changePasswordUser.getPassword_fourth()});
                //MSG_0009=新密碼不可與前五次曾使用之密碼相同，請重新輸入
                modelAndView.addObject("messageCode", "MSG_0009");
                return modelAndView;
            }

            // Set data to update
            Map<String,Object> update = new HashMap();
            update.put("ACCOUNT", changePasswordUserID);
            update.put("PASSWORD", encryptPassword);
            update.put("USER_PASSWORD", changePasswordUser.getPassword());
            update.put("PASSWORD_FIRST", changePasswordUser.getPassword_first());
            update.put("PASSWORD_SECOND", changePasswordUser.getPassword_second());
            update.put("PASSWORD_THIRD", changePasswordUser.getPassword_third());
            update.put("PASSWORD_FOURTH", changePasswordUser.getPassword_fourth());

            String btnUrl = URL_CONTROLLER_PREFIX + URL_ENTITY_EDIT + ".do";
            if(userServiceImpl.getAccess(getUserId(request), btnUrl.replace("{" + ATTRIBUTE_ID + "}", "{0}"))) {
                if(modify_time == null
                        || (modify_time != null && (nowTime.getTime()-modify_time.getTime())>604800000L)) {
                    // Success to update User

                    update.put("MODIFY_USER", getUserId(request));
                    update.put("MODIFY_TIME", nowTime);
                    
                    
                    //userServiceImpl.updatePassword(update);

                    //save user & user roles
                    userServiceImpl.updateUser(update, Long.parseLong(reqUserId), roleIds, getUserId(request));
                    
                    modelAndView.setViewName(getViewPage(viewMap,Constants.LIST_PAGE));
                    modelAndView.addObject("messageCode", "MSG_0003");
                } else {
                    //MSG_0007=更新密碼失敗!七日內已修改過密碼!
                    modelAndView.addObject("messageCode", "MSG_0007");
                }
            } else {    // 權限不足
                modelAndView.addObject("messageCode", "MSG_0008");
            }

            User queryAccount = userServiceImpl.queryByAccount(user.getAccount(), password);

            if(queryAccount != null) {
                logger.info("Change password ok. Account info:" + queryAccount.toString());
                Map<String, Object> newUserData = new HashMap();
                newUserData.put(Constants.USER_ID, queryAccount.getUser_id());
                newUserData.put(Constants.USER_ACCOUNT, queryAccount.getAccount());
                newUserData.put("BLOCKED", queryAccount.getBlocked());
                newUserData.put("MODIFY_TIME", queryAccount.getModifyTime());

                request.getSession().setAttribute(Constants.USER_DATA, newUserData);
            }

            return modelAndView;
        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }
    }

    /**
     * Delete account
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value=URL_ENTITY_DELETE, method = {RequestMethod.GET, RequestMethod.DELETE})
    public ModelAndView deleteAccount(
            @PathVariable(ATTRIBUTE_ID) final String id,
            HttpServletRequest request) {
        try {
            logger.info("Delete id:{}", id);

            Map userData = getUserData(request);
            // Set redirect page
            ModelAndView modelAndView = getModelAndView(viewMap,Constants.LIST_PAGE);

            // Validate
            String userAccount = (String)userData.get(Constants.USER_ACCOUNT);
            if(userAccount.equals(id)) {
                logger.info("Can't delete self. id:{}", id);
                logger.info("errorMsg:{}", messageSource.getMessage("text.cant.delete.self", null, null));
                //modelAndView.addObject(messageSource.getMessage("text.cant.delete.self", null, "不可刪除正在使用的帳號!", null));
                modelAndView.addObject(MESSAGE_CODE, "text.cant.delete.self");
            }else{

                List<User> users = userServiceImpl.queryByAccount(id);
                User user = null;
                if(users != null && !users.isEmpty()) {
                    user = users.get(0);
                }
    
                if(user == null) {
                    throw new Exception("User not fuound by accountName:" + id);
                }
    
                // Delete account
                Map<String,Object> deleteData = new HashMap();
                deleteData.put("ACCOUNT", id);
                userServiceImpl.deleteUser(deleteData, user.getUser_id());

                modelAndView.addObject(MESSAGE_CODE, "text.deletion.success");
                
                logger.info("msg code:{}", messageSource.getMessage("text.deletion.success", null, null));
                logger.info("delete id:{} over.", id);
            }

            // Get datas
            PageControler pageControler = getPageControler(request);
            List<User> datas = userServiceImpl.listAllAccount(pageControler);

            // Set Datas
            modelAndView.addObject("USER_RS", datas);
            modelAndView.addObject("PAGES", pageControler);
            setPageControler(request,pageControler);

            return modelAndView;

        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }
    }


    @RequestMapping(value="/add_before")
    public ModelAndView add_before(HttpServletRequest request,
            HttpServletResponse response) {
        try
        {
            ModelAndView modelAndView = getModelAndView(viewMap,Constants.ADD_BEFORE_PAGE);
            PageControler pageControler = getPageControler(request);
            List<User> datas = userServiceImpl.listAllAccount(pageControler);
            
            //select all group
            List<SysRole> sysRoleList = groupsServiceImpl.findAll(null);
            
            modelAndView.addObject("USER_RS", datas);
            modelAndView.addObject("ROLE_RS", sysRoleList);

            return modelAndView;

        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }
    }


    @RequestMapping(value="/add")
    @SuppressWarnings({"unchecked","rawtypes"})
    public ModelAndView add(HttpServletRequest request,
            HttpServletResponse response) {
        try
        {

            Map<String,Object> inserData = new HashMap();
            List<User> datas;
            String password = request.getParameter("password");
            String userID = request.getParameter("userID");
            String[] roleIds = request.getParameterValues("roleIds");

            ModelAndView modelAndView = null;

            if(null!=password && password.length()>=6
                    && password.matches(".*[A-Za-z]{1,}.*[A-Za-z]{1,}.*")
                    && password.matches(".*[^A-Za-z]{1,}.*"))
            {                                       
                inserData.put("ACCOUNT", userID);
                inserData.put("PASSWORD", EncryptUtility.toMd5(password));
                inserData.put("CREATE_USER", getUserId(request));
                inserData.put("CREATE_TIME", getModifyDate());
                inserData.put("MODIFY_USER", getUserId(request));
                inserData.put("MODIFY_TIME", getModifyDate());
//                inserData.put("ADMIN", request.getParameter("isAdmin"));
                inserData.put("BLOCKED", 0);

                userServiceImpl.saveNewUser(inserData, roleIds, getUserId(request));
                
                modelAndView = getModelAndView(viewMap,Constants.LIST_PAGE);
                modelAndView.addObject("messageCode", "MSG_0005");
            }
            else {
                modelAndView = getModelAndView(viewMap,Constants.ADD_BEFORE_PAGE);
                modelAndView.addObject("messageCode", "MSG_0004");
                modelAndView.addObject("userID", userID);
                

                //select all group
                List<SysRole> sysRoleList = groupsServiceImpl.findAll(null);
                //get user role
                Map<Long,String> userRolesMap = getUserRolesMap(roleIds);

                modelAndView.addObject("ROLE_RS", sysRoleList);
                modelAndView.addObject("USER_ROLES_MAP", userRolesMap);
            }


            PageControler pageControler = getPageControler(request);

            datas = userServiceImpl.listAllAccount(pageControler);
            modelAndView.addObject("USER_RS", datas);
            modelAndView.addObject("PAGES", pageControler);
            setPageControler(request,pageControler);

            return modelAndView;

        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }
    }

    private Map<Long, String> getUserRolesMap(Object[] roleIds) {
        Map<Long,String> userRolesMap = new HashMap<Long,String>();
        if(roleIds != null){
            if(roleIds instanceof String[]){
                for(String roleId : (String[]) roleIds){
                    userRolesMap.put(Long.parseLong(roleId), "Y");
                }
            }else if(roleIds instanceof Long[]){
                for(Long roleId : (Long[]) roleIds){
                    userRolesMap.put(roleId, "Y");
                }
            }
                
        }
        
        return userRolesMap;
    }

    /** 自動產生特定之 HTTP request attributes。 */
    @ModelAttribute
    protected void prepareModelAttribute (HttpServletRequest request, Map<String, Object> model) {
        try {
            String path = request.getServletPath();
            logger.info("prepareModelAttribute path:{}", path);
            if(path.startsWith(URL_CONTROLLER_PREFIX)) {
                String url = path.substring(URL_CONTROLLER_PREFIX.length());

                logger.info("substring url:{}", url);

//                Pattern pattern = Pattern.compile("^(.*)(" + URL_NEW + "|" + URL_EDIT + "|" + URL_UPDATE + "){1}$");
                Pattern pattern = Pattern.compile("^(.*)(" + URL_EDIT + ".do" + "){1}$");
                Matcher matcher = pattern.matcher(url);

                logger.info("matcher.matches result:{}", matcher.matches());

                if(!matcher.matches()) {
                    logger.info(String.format("No model attributes needed for the incoming request, %s.", path));
                } else {
                    // /XXXX/edit 取出的 id 為 /XXXX，故此真實的 id 要 substring(1)
                    String id = matcher.group(1);
                    logger.info("sub id:{}", id);
                    User entity = (StringUtils.isBlank(id) ?  new User() :
                        (userServiceImpl.queryByAccount(id.substring(1)) != null ?
                                userServiceImpl.queryByAccount(id.substring(1)).get(0) : null));

                    logger.info(String.format("url = %s, id = %s, entity = %s", url, id, entity.toString()));

                    model.put(ATTRIBUTE_ENTITY, entity);
                }
            }

        } catch(Throwable cause)  {
            logger.error(cause.getMessage(), cause);
        }
    }
}

