package com.cht.pcrf.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdScheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cht.pcrf.common.BaseAction;
import com.cht.pcrf.common.Constants;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.PcrfConsistent;
import com.cht.pcrf.service.PcrfConsistentServiceImpl;
import com.cht.pcrf.service.QuartzServiceImpl;


@Controller
@RequestMapping(PcrfSyncConfController.URL_CONTROLLER_PREFIX)
public class PcrfSyncConfController extends BaseAction {
    private static final Logger logger = LoggerFactory.getLogger(PcrfSyncConfController.class);
//    private static final Logger operateLogger = LoggerFactory.getLogger(Constants.LOGGER_PG_WEB);

    protected static final String URL_CONTROLLER_PREFIX = "/pcrfSyncConf";

    protected static final String PAGE_URL_PREFIX = "/pcrfSyncConf";
    private static final String PAGE_LIST = "/list";
    
    @Autowired
    private PcrfConsistentServiceImpl pcrfConsistentServiceImpl;
    
//    @Autowired
//    private ApplicationContext context;
    
    @Autowired
    private QuartzServiceImpl quartzServiceImpl;

    protected Map<String, Object> viewMap;

    public PcrfSyncConfController(){
        viewMap = new HashMap<String, Object>();

        viewMap.put(Constants.LIST_PAGE, PAGE_URL_PREFIX + PAGE_LIST);
    }

    /**
     * pcrfSyncConf 清單頁
     * @param request
     * @return
     */
    @RequestMapping(value=URL_LIST, method={RequestMethod.GET, RequestMethod.POST})
    public ModelAndView list(HttpServletRequest request, ModelMap model) {
        try {
            logger.info("list all pcrfSyncConf to page.");
            PageControler pageControler = getPageControler(request);
           
            ModelAndView mav = getModelAndView(viewMap, LIST_PAGE);
            mav.addAllObjects(model);
            mav.addObject("execTypeSelect", model.get("execType"));
            mav.addObject(PAGES, pageControler);
            setPageControler(request,pageControler);

            return mav;
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }
    }

    /**
     * @param pcrfConsistent
     * @param syncType
     * @param execYear
     * @param execMonth
     * @param execDay
     * @param execHour
     * @param execMin
     * @param pcrfConsistentBindingResult
     * @param request
     * @return
     */
    @RequestMapping(value=URL_ENTITY_EDIT, method={RequestMethod.POST})
    public ModelAndView edit(
            @ModelAttribute(ATTRIBUTE_ENTITY) final PcrfConsistent pcrfConsistent,
            @RequestParam(value="syncType", required=false) String syncType,
            @RequestParam(value="execWeek", required=false) String execWeek,
            @RequestParam(value="execMonth", required=false) String execMonth,
            @RequestParam(value="execDay", required=false) String execDay,
            @RequestParam(value="execHour", required=false) String execHour,
            BindingResult pcrfConsistentBindingResult,
            HttpServletRequest request) {
        try {
        	ModelMap model = new ModelMap();
            logger.info("Edit PCRF_SYNC_CONF:{}", pcrfConsistent);

            // validate
            if(pcrfConsistentBindingResult.hasErrors()) {
                logger.info("SAPC validate failed! SAPC:{}", pcrfConsistent.toString());
                return this.list(request, model);
            }
            
            if(execWeek.trim().equals(""))
            	execWeek = "";
            if(execMonth.trim().equals(""))
            	execMonth = "";
            if(execDay.trim().equals(""))
            	execDay = "";
            if(execHour.trim().equals(""))
            	execHour = "";
            
            
            pcrfConsistent.setSyncType(syncType);
            pcrfConsistent.setExecWeek(execWeek);
            pcrfConsistent.setExecMonth(execMonth);
            pcrfConsistent.setExecDay(execDay);
            pcrfConsistent.setExecHour(execHour);
            pcrfConsistent.setModifyUser(getUserId(request));
            pcrfConsistent.setModifyTime(getModifyTime());

            // UPDATE PCRF_SYNC_CONF
            pcrfConsistentServiceImpl.update(pcrfConsistent);
            
            
        	String week = pcrfConsistent.getExecWeek();
        	String month= pcrfConsistent.getExecMonth();
        	String day= pcrfConsistent.getExecDay();
        	String hour= pcrfConsistent.getExecHour();
        	
            quartzServiceImpl.setJobTime(week,month,day,hour);
            
            

            ModelMap modelMap = new ModelMap();
            modelMap.put(MESSAGE_CODE, "text.update.pcrfSyncConf.success");

            return this.list(request, modelMap);
//        } catch (AdaptorException ex) {
//            logger.error(ex.getMessage(),ex);
//            ModelMap modelMap = new ModelMap();
//            modelMap.put(MESSAGE_CODE, "MSG_0011");
//            return this.editPage(sapcInfo.getNode_name(), sapcInfo, request, sapcinfoBindingResult);
        } catch (Exception ex) {
            return exceptionProcess(ex);
        }

    }

    /** 自動產生特定之 HTTP request attributes。 */
    @ModelAttribute
    protected void prepareModelAttribute (HttpServletRequest request, Map<String, Object> model) {
        try {
            String path = request.getServletPath();
            logger.info("prepareModelAttribute path:{}", path);
            if(path.startsWith(URL_CONTROLLER_PREFIX)) {
                String url = path.substring(URL_CONTROLLER_PREFIX.length());

                logger.debug("substring url:{}", url);

                String newUrl = URL_NEW + ".do";
                String editUrl = URL_EDIT + ".do";
                String listUrl = URL_LIST + ".do";

                Pattern pattern = Pattern.compile("^(.*)(" + newUrl + "|" + editUrl + "|" +listUrl+ "){1}$");
                Matcher matcher = pattern.matcher(url);

                logger.info("matcher.matches result:{}", matcher.matches());

                if(!matcher.matches()) {
                    logger.info(String.format("No model attributes needed for the incoming request, %s.", path));
                } else {
                    PcrfConsistent entity = pcrfConsistentServiceImpl.find();

                    model.put(ATTRIBUTE_ENTITY, entity);
                    
                }
            }

        } catch(Throwable cause)  {
            logger.error(cause.getMessage(), cause);
        }
    }
    
}
