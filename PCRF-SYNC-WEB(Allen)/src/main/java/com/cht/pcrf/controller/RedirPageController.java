package com.cht.pcrf.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cht.pcrf.common.BaseAction;
import com.cht.pcrf.common.Constants;
import com.cht.pcrf.service.RedirPageServiceImpl;



@Controller()
@RequestMapping("/redirPage")
public class RedirPageController extends BaseAction {

    @Autowired
    private RedirPageServiceImpl redirPageServiceImpl;
    protected Map<String, Object> viewMap; // 頁面轉向Map
    
    public RedirPageController(){
        viewMap = new HashMap<String, Object>();
        
        viewMap.put("TITLE_PAGE", "/login/title");
        viewMap.put("MENU_PAGE", "/login/menu");
        viewMap.put("MAIN_PAGE", "/login/main");
    }

	@RequestMapping("/title")
	public ModelAndView title(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView modelAndView = getModelAndView(viewMap,"TITLE_PAGE");
		return modelAndView;

	}

    @RequestMapping("/menu")
	public ModelAndView menu(HttpServletRequest request,
			HttpServletResponse response) {
        ModelAndView modelAndView = getModelAndView(viewMap,"MENU_PAGE");
        
        try {
            Long userId = super.getUserId(request);
            List<Map> menuList = redirPageServiceImpl.getMenuData(userId);
            request.getSession().setAttribute(Constants.MENU_DATA, menuList);
            modelAndView.addObject("menuList", menuList);
            //getButtonDatas
            List<Map> funcBtnList = redirPageServiceImpl.getFuncBtnData(userId);
            request.getSession().setAttribute(Constants.FUNC_BTN_DATA, funcBtnList);

            //get functionlist
            redirPageServiceImpl.getAuthFunc(request);
            
            return modelAndView;
        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }
        
	}

    @RequestMapping("/main")
	public ModelAndView main(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView modelAndView = getModelAndView(viewMap,"MAIN_PAGE");
		return modelAndView;

	}
	

}
