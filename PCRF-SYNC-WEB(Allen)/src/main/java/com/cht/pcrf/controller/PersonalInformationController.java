package com.cht.pcrf.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cht.pcrf.common.BaseAction;
import com.cht.pcrf.common.CommonValidation;
import com.cht.pcrf.common.Constants;
import com.cht.pcrf.common.PageControler;
import com.cht.pcrf.model.User;
import com.cht.pcrf.service.UserServiceImpl;
import com.cht.pcrf.utils.EncryptUtility;

/**
 * <code>PersonalInformationController</code>:個人資料管理控制器
 * @author $Author: weilinchu $
 * @version $Id: PersonalInformationController.java 314 2016-01-27 18:01:48Z weilinchu $
 */
@Controller
@RequestMapping(PersonalInformationController.URL_CONTROLLER_PREFIX)
public class PersonalInformationController extends BaseAction{

    protected static final String URL_CONTROLLER_PREFIX = "/personalInfo";

    protected Map<String, Object> viewMap;

    @Autowired(required=true)
    private UserServiceImpl userServiceImpl;

    @Autowired
    private MessageSource messageSource = null;

    public PersonalInformationController(){
        viewMap = new HashMap<String, Object>();

        viewMap.put(Constants.EDIT_PAGE, PAGE_URL_PREFIX + PAGE_EDIT);
    }

    protected static final String PAGE_URL_PREFIX = "/personalInfo";
    private static final String PAGE_EDIT = "/edit";

    /**
     * 修改密碼頁
     * @param accountId
     * @param user
     * @param request
     * @return
     */
    @RequestMapping(value=URL_ENTITY_EDIT, method = { RequestMethod.GET })
    public ModelAndView editPage(
            @PathVariable(ATTRIBUTE_ID) final String accountId,
            HttpServletRequest request,
            ModelMap model) {
        try {
            logger.info("ready to edit account:{}", accountId);

            ModelAndView modelAndView = getModelAndView(viewMap,Constants.EDIT_PAGE);

            modelAndView.addObject(ATTRIBUTE_ENTITY, model.get(ATTRIBUTE_ENTITY));

            return modelAndView;
        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }
    }

    /**
     * 修改密碼
     * @param user
     * @param request
     * @return
     */
    @RequestMapping(value=URL_ENTITY_EDIT, method = { RequestMethod.POST })
    @SuppressWarnings({"unchecked","rawtypes"})
    public ModelAndView edit(
            @RequestParam(value="password", required=true) String passwd,
            HttpServletRequest request,
            ModelMap model) {
        try {
            User user = (User) model.get(ATTRIBUTE_ENTITY);
            user.setPassword(passwd);
            logger.info("User account:" + user.getAccount());
            logger.info("User password:" + user.getPassword());

            ModelAndView modelAndView = getModelAndView(viewMap,Constants.EDIT_PAGE);;
            PageControler pageControler = getPageControler(request);

            // Get user info
            Map userData = getUserData(request);
            Date nowTime = new Date();
            Date modify_time =
                    (userData.get("MODIFY_TIME") == null ? null : (Date)userData.get("MODIFY_TIME"));
            String loginUserID = getUserAccount(request);
            String changePasswordUserID = user.getAccount();
            String password = user.getPassword();
            String encryptPassword = EncryptUtility.toMd5(password);
            User changePasswordUser = userServiceImpl.queryByAccount(changePasswordUserID).get(0);

            // Set Page datas
//            List<User> datas = userServiceImpl.listAllAccount(pageControler);
//            modelAndView.addObject("USER_RS", datas);
            modelAndView.addObject("PAGES", pageControler);
            setPageControler(request,pageControler);

            // validate
            if(!CommonValidation.complyWithPasswordRules(password)) {
                //MSG_0004=密碼需依照規則建立，請您重新輸入
                modelAndView.addObject("messageCode", "MSG_0004");
                return modelAndView;
            }
            if(CommonValidation.passwdHasBeenUsed(encryptPassword, changePasswordUser)) {

                logger.info("destPasswdEncrypted:{}, user passwd:{}, first_passwd:{}, second_passwd:{}, third_passwd:{}, fourth_passwd:{}",
                        new Object[]{encryptPassword, changePasswordUser.getPassword(),
                                changePasswordUser.getPassword_first(), changePasswordUser.getPassword_second(),
                                changePasswordUser.getPassword_third(), changePasswordUser.getPassword_fourth()});
                //MSG_0009=新密碼不可與前五次曾使用之密碼相同，請重新輸入
                modelAndView.addObject("messageCode", "MSG_0009");
                return modelAndView;
            }

            // Set data to update
            Map<String,Object> update = new HashMap();
            update.put("ACCOUNT", changePasswordUserID);
            update.put("PASSWORD", encryptPassword);
            update.put("USER_PASSWORD", changePasswordUser.getPassword());
            update.put("PASSWORD_FIRST", changePasswordUser.getPassword_first());
            update.put("PASSWORD_SECOND", changePasswordUser.getPassword_second());
            update.put("PASSWORD_THIRD", changePasswordUser.getPassword_third());
            update.put("PASSWORD_FOURTH", changePasswordUser.getPassword_fourth());

            String btnUrl = URL_CONTROLLER_PREFIX + URL_ENTITY_EDIT + ".do";
            if(userServiceImpl.getAccess(getUserId(request), btnUrl.replace("{" + ATTRIBUTE_ID + "}", "{0}"))) {
                if(modify_time == null
                        || (modify_time != null && (nowTime.getTime()-modify_time.getTime())>604800000L)) {
                    // Success to update User

                    update.put("MODIFY_USER", getUserId(request));
                    update.put("MODIFY_TIME", nowTime);
                    userServiceImpl.updatePassword(update);

                    modelAndView.setViewName(getViewPage(viewMap,Constants.EDIT_PAGE));
                    modelAndView.addObject("messageCode", "MSG_0003");
                } else {
                    //MSG_0007=更新密碼失敗!七日內已修改過密碼!
                    modelAndView.addObject("messageCode", "MSG_0007");
                }
            } else {    // 權限不足
                modelAndView.addObject("messageCode", "MSG_0008");
            }

            User queryAccount = userServiceImpl.queryByAccount(user.getAccount(), password);

            if(queryAccount != null) {
                logger.info("Change password ok. Account info:" + queryAccount.toString());
                Map<String, Object> newUserData = new HashMap();
                newUserData.put(Constants.USER_ID, queryAccount.getUser_id());
                newUserData.put(Constants.USER_ACCOUNT, queryAccount.getAccount());
                newUserData.put("BLOCKED", queryAccount.getBlocked());
                newUserData.put("MODIFY_TIME", queryAccount.getModifyTime());

                request.getSession().setAttribute(Constants.USER_DATA, newUserData);
            }

            return modelAndView;
        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }
    }

    /** 自動產生特定之 HTTP request attributes。 */
    @ModelAttribute
    protected void prepareModelAttribute (HttpServletRequest request, Map<String, Object> model) {
        try {
            String path = request.getServletPath();

            logger.info("prepareModelAttribute path:{}", path);

            if(path.startsWith(URL_CONTROLLER_PREFIX)) {
                String url = path.substring(URL_CONTROLLER_PREFIX.length());

                logger.info("substring url:{}", url);

                Pattern pattern = Pattern.compile("^(.*)(" + URL_EDIT + ".do" + "){1}$");
                Matcher matcher = pattern.matcher(url);

                logger.debug("matcher.matches result:{}", matcher.matches());

                if(!matcher.matches()) {
                    logger.info(String.format("No model attributes needed for the incoming request, %s.", path));
                } else {
                    // /XXXX/edit 取出的 id 為 /XXXX，故此真實的 id 要 substring(1)
                    String id = matcher.group(1);
                    User entity = (StringUtils.isBlank(id) ?  new User() :
                        (userServiceImpl.queryByAccount(id.substring(1)) != null ?
                                userServiceImpl.queryByAccount(id.substring(1)).get(0) : null));

                    logger.info(String.format("url = %s, id = %s, entity = %s", url, id, entity.toString()));

                    model.put(ATTRIBUTE_ENTITY, entity);
                }
            }

        } catch(Throwable cause)  {
            logger.error(cause.getMessage(), cause);
        }
    }
}
