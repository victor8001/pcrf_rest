package com.cht.pcrf.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cht.pcrf.common.BaseAction;
import com.cht.pcrf.common.Constants;
import com.cht.pcrf.model.User;
import com.cht.pcrf.service.UserServiceImpl;

@Controller
@RequestMapping("/login")
public class LoginController extends BaseAction{

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired(required=true)
    private UserServiceImpl userServiceImpl;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView login(HttpServletRequest request, HttpServletResponse response) {

        try {
            @SuppressWarnings("unchecked")
            Map<String, Object> userData = (Map<String, Object>) request.getSession().getAttribute(Constants.USER_DATA);
            if (userData != null) {
                ModelAndView modelAndView = new ModelAndView("/login/welcome");
                return modelAndView;
            }

            return new ModelAndView("redirect:/login.jsp");
        } catch (Exception ex) {
            logger.error("Login render error!", ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }

    }

    @RequestMapping(method = RequestMethod.POST)
    @SuppressWarnings({"unchecked","rawtypes"})
    public ModelAndView loginAuth(HttpServletRequest request, HttpServletResponse response) {
        try {
            String userId = request.getParameter("userID");
            String password = request.getParameter("password");
            User user = userServiceImpl.queryByAccount(userId, password);

            // check account and password
            String errorMsg = userServiceImpl.checkLoginStatus(userId, user);
            if(errorMsg != null){ // Login error, and return error msg to login.jsp
                setMessage(request, "MSG_0001");
                Map<String, String> message = new HashMap<String, String>();
                message.put("error_msg", errorMsg);

                ModelAndView modelAndView = new ModelAndView("redirect:/login.jsp", message);
                return modelAndView;
            }

            if(user != null && user.getRetryCount() != 0) {
                logger.info("Login success. and reset retry account.");
                userServiceImpl.resetRetryCountByAccount(userId); // login success, and reset retry count.
            }

            Map<String, Object> userData = new HashMap();
            userData.put(Constants.USER_ID, user.getUser_id());
            userData.put(Constants.USER_ACCOUNT, user.getAccount());
            userData.put("BLOCKED", user.getBlocked());
            userData.put("MODIFY_TIME", user.getModifyTime());

            request.getSession().setAttribute(Constants.USER_DATA, userData);
            ModelAndView modelAndView = new ModelAndView("/login/welcome");
            return modelAndView;

        } catch (Exception ex) {
            logger.error("Show login page error!", ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }

    }


    @RequestMapping(value="/logout")
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
        try {
            cleanLoginUserData(request);
            return new ModelAndView("redirect:/login.jsp");
        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            return new ModelAndView(Constants.ERROR_PAGE).addObject(
                    Constants.SYS_EXCEPTION_MESSAGE, ex);
        }
    }

    public UserServiceImpl getUserServiceImpl() {
        return userServiceImpl;
    }

    public void setUserServiceImpl(UserServiceImpl userServiceImpl) {
        this.userServiceImpl = userServiceImpl;
    }

}
