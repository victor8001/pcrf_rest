package com.cht.pcrf.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <code>StatusController</code>:PG-WEB 狀態檢查API
 * @author $Author: rayliao $
 * @version $Id: StatusController.java 263 2016-01-19 10:09:21Z rayliao $
 */
@RestController
@RequestMapping(value=StatusController.URL_CONTROLLER_PREFIX)
public class StatusController {
    private static final Logger logger = LoggerFactory
            .getLogger(StatusController.class);

    protected static final String URL_CONTROLLER_PREFIX = "/api";

    /**
     * 回傳目前PG-WEB是否正常執行
     * @return
     */
    @RequestMapping(value="/l4/checkWebService", method={RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<String> helthCheck() {
        logger.info("PG-WEB helthCheck.");
        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
