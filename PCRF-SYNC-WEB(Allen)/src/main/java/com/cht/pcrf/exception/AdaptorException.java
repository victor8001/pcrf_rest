/**
 * Project Name : PG-WEB
 * File Name 	: AdaptorException.java
 * Package Name : com.fet.pg.exception
 * Date 		: 2015年12月18日 
 * Author 		: LauraChu
 * Copyright (c) 2015 All Rights Reserved.
 */
package com.cht.pcrf.exception;

public class AdaptorException  extends Exception{
    private static final long serialVersionUID = -8655323159358889379L;

    private String rtnCode;
    private String rtnMsg;
    
    public AdaptorException(Exception ex){
        super(ex);
    }
    
    public AdaptorException(String rtnCode, String rtnMsg) {
        this.rtnCode = rtnCode;
        this.rtnMsg = rtnMsg;
    }
    
    public AdaptorException(String rtnCode, String rtnMsg, Exception ex) {
        super(ex);
        this.rtnCode = rtnCode;
        this.rtnMsg = rtnMsg;
    }
    

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }
    
}