/**
 * Project Name : PG-WEB
 * File Name 	: HttpClientException.java
 * Package Name : com.fet.pg.exception
 * Date 		: 2015年12月17日 
 * Author 		: LauraChu
 * Copyright (c) 2015 All Rights Reserved.
 */
package com.cht.pcrf.exception;

public class HttpClientException extends Exception{
    private static final long serialVersionUID = 2254406841782182810L;

    public HttpClientException(Exception ex){
        super(ex);
    }
}