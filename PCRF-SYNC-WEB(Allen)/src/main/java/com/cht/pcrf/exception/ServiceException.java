/**
 * Project Name : PG-WEB
 * File Name 	: ServiceException.java
 * Package Name : com.fet.pg.exception
 * Date 		: 2015年12月18日 
 * Author 		: LauraChu
 * Copyright (c) 2015 All Rights Reserved.
 */
package com.cht.pcrf.exception;

public class ServiceException  extends Exception{

    private static final long serialVersionUID = -7772557985246111131L;

    private String rtnCode;
    private String rtnMsg;
    
    public ServiceException(Exception ex){
        super(ex);
    }
    
    public ServiceException(String rtnCode, String rtnMsg) {
        this.rtnCode = rtnCode;
        this.rtnMsg = rtnMsg;
    }
    
    public ServiceException(String rtnCode, String rtnMsg, Exception ex) {
        super(ex);
        this.rtnCode = rtnCode;
        this.rtnMsg = rtnMsg;
    }
    

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }
}