/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: SyncServiceTest.java
 * Package Name : com.cht.pcrf.api.service
 * Date 		: 2016年8月8日 
 * Author 		: LauraChu
 * Copyright (c) 2016 All Rights Reserved.
 */
package com.cht.pcrf.api.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cht.pcrf.api.exception.ApiException;

//@RunWith(SpringJUnit4ClassRunner.class)

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/api-spring-bean.xml"
        //,"file:src/main/resources/logback.xml" 
        })
public class SyncServiceTest {
    protected Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    /*@Autowired(required = true)
    private SyncService syncService;*/
    
    @Before
    public void setUp() throws Exception {
    }

    /**
     */
    @Test
    public void testSyncBySub() throws Exception {
        
        System.setProperty("api.config.path", "C:/__ForWork/_CASE/CHT_PCRF_DB_SYNC/source/PCRF-SYNC-API/src/main/resources");
        System.setProperty("config.path", "C:/__ForWork/_CASE/CHT_PCRF_DB_SYNC/source/PCRF-SYNC-API/src/main/resources");
//        syncService.syncBySub("", "d:/aa.csv");
//        syncService.syncBySystem("2017-06-24 17:58:41", 0L, 0L, "d:/DB_DIFF_KH_7L2PCRF1_2_20171116111000.csv");
        
//        syncService.syncByTransationTime("TP", "pcrf1", 0L, "2017-04-21 13:30:55");
        
//        syncService.syncDataFromPcrfToDbSync("d:/DB_DIFF_KH_7L2PCRF1_2_20171116111000.csv", "");
    }

}
