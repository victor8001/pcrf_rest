/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: EPCService.java
 * Package Name : com.cht.pcrf.api.service.ldap
 * Date 		: 2017年4月26日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api.service.rest;

import java.util.List;

import javax.ws.rs.client.Client;

import com.cht.pcrf.api.util.CurlUtil;
import com.cht.pcrf.api.util.SystemUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.api.common.CommonConstant;
import com.cht.pcrf.core.model.PcrfInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public abstract class EPCRestService {
    protected final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);
    protected CurlUtil curlUtil;
    protected String beta;
    @Autowired
    public final void setCurlUtil(CurlUtil curlUtil) {
        this.curlUtil = curlUtil;
    }

    public void setBeta(String beta) {
        this.beta = beta;
    }

    abstract void setSapcRestService(SapcRestService sapcRestService);
    abstract SapcRestService getSapcRestService();
    abstract void setSapcRestConfService(SapcRestConfService sapcRestConfService);
    abstract SapcRestConfService getSapcRestConfService();
    
    public List<PcrfInfo> getPcrfInfoByLocationOrNodeName(String location, String nodename) throws Exception{
        List<PcrfInfo> sapcInfoList = null;
        
        if(nodename == null){
            sapcInfoList = getSapcRestService().getPcrfInfoByLocation(location);
        }else{
            sapcInfoList = getSapcRestService().getPcrfInfoByNode(location, nodename);
        }
        
        if(sapcInfoList == null)
            throw new Exception("PcrfInfo can't find info, location : " + location);
        
        return sapcInfoList;
    }
    
    public boolean modifyEntry(String location, String msisdn, String infos) throws Exception {
        return modifyEntry(location, null, msisdn, infos);
    }
    
    public boolean modifyEntry(String location, String nodename, String msisdn, String infos) throws Exception {
        boolean result = true;
        long startTime = System.currentTimeMillis();

        
        try {
            logger.info("ModifyEntry start ");
            
            List<PcrfInfo> sapcInfoList = getPcrfInfoByLocationOrNodeName(location, nodename);
            
            for(PcrfInfo sapcInfo : sapcInfoList){
            	
            	String baseUrl = sapcInfo.getNodeHost() + ":" + sapcInfo.getNodePort();
            	Client restClient = getSapcRestConfService().createSingleRestClient(sapcInfo);
            	if("Y".equals(beta)){
                    logger.info("Run curl shell here: ");
                    result = curlUtil.createAndModify(sapcInfo.getLoginId(),sapcInfo.getLoginPw(),infos,baseUrl,msisdn);
                }else{
                    logger.info("Run javax.ws.jersy https here: ");
                    getSapcRestService().put(baseUrl, msisdn, infos, restClient);
                }

                //extraModifyEntry(sapcInfo.getLdapEnv(), sapcInfo.getNodeName(), msisdnEntryDN, infos, isExist);
            }
            logger.info("ModifyEntry " + msisdn + " successful");
        } catch (Exception e) {
            result = false;
            logger.error(e.getMessage(), e);
            logger.error("ModifyEntry " + msisdn + " fail. e:" + e);
            throw new Exception("ModifyEntry " + msisdn + " fail. e:" + e);
        } finally{
            logger.info("ModifyEntry execute time:"+(System.currentTimeMillis()-startTime));    
        }
        
        return result; 
    }
    
    public boolean deleteEntry(String location, String msisdn) throws Exception {
        return deleteEntry(location, null, msisdn);
    }
    
    public boolean deleteEntry(String location, String nodename, String msisdn) throws Exception {
        //ldapdelete -h "172.20.160.245" -p "7323" 
        //-D "administratorName=provgw,nodeName=TPS1PCRF" 
        //-w "fetErt567" 
        //"EPC-SubscriberId=886944302023,EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=TPS1PCRF"
        
        boolean result = true;
        long startTime = System.currentTimeMillis();
        try {
            logger.info("DeleteEntry start ");
            
            List<PcrfInfo> sapcInfoList = getPcrfInfoByLocationOrNodeName(location, nodename);
            
            for(PcrfInfo sapcInfo : sapcInfoList){
            	String baseUrl = sapcInfo.getNodeHost() + ":" + sapcInfo.getNodePort();
            	Client restClient = getSapcRestConfService().createSingleRestClient(sapcInfo);

                if("Y".equals(beta)){
                    result = curlUtil.deleteMsisdn(sapcInfo.getLoginId(),sapcInfo.getLoginPw(),baseUrl,msisdn);
                }else {
                    getSapcRestService().delete(baseUrl, msisdn, restClient);
                }
            }
            
            logger.info("DeleteEntry " + msisdn + " successful");   
        } catch (Exception e) {
            result = false;
            logger.error(e.getMessage(), e);
            logger.error("DeleteEntry " + msisdn + " fail. e:" + e);
            throw new Exception("DeleteEntry " + msisdn + " fail. e:" + e);
        } finally{
            logger.info("DeleteEntry execute time:"+(System.currentTimeMillis()-startTime));    
        }
        
        return result; 
        
    }

}
