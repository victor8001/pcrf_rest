/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: DateUtil.java
 * Package Name : com.cht.pcrf.consistent.util
 * Date 		: 2017年4月27日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.api.common.CommonConstant;

public class DateUtil {
    private final static Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);
    
    private static final String DATE_TIME_PATTERN = "yyyyMMddHHmmss";
    private static final String DATETIME_FORMAT = "yyyy/MM/dd HH:mm:ss";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd");
    private static final ThreadLocal<SimpleDateFormat> sdfPatternLocal = new ThreadLocal<SimpleDateFormat>();
    
    private static final SimpleDateFormat getSDFPatternLocal() {
        if(sdfPatternLocal.get() == null) {
            sdfPatternLocal.set(new SimpleDateFormat());
        }
        return sdfPatternLocal.get();
    }
    
    /**
     * Get the current date time, format is yyyyMMddHHmmss.
     * 
     * @return String 
     */
    public static String getNowDateTime() {
        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = getSDFPatternLocal();
        sdf.applyPattern(DATE_TIME_PATTERN);
        
        return sdf.format(now.getTime());
    }
    
    public static String getNowDateTime(Date now) {
        SimpleDateFormat sdf = getSDFPatternLocal();
        sdf.applyPattern(DATE_TIME_PATTERN);
        
        return sdf.format(now.getTime());
    }
    
    
    /**
     * Adds or subtracts the specified amount of minute to the Now time.
     * 
     * @param minute
     * @return Date
     */
    public static Date operateMinute(int minute) {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, minute);
        
        return now.getTime();
    }
    
    public static Date operateMinute(Date date, int minute) {
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minute);
        
        return calendar.getTime();
    }
    /**
     * Get millisecond time difference between the two date.
     * 
     * @param startdate
     * @param enddate
     * @return
     */
    public static long getTimeInterval(Date startdate, Date enddate) {

        long interval_times = 0;
        try {
            Calendar sc = (Calendar) Calendar.getInstance().clone();
            sc.setTime(startdate);
            Calendar ec = (Calendar) Calendar.getInstance().clone();
            ec.setTime(enddate);
            interval_times = getTimeInterval(sc, ec);
        } catch (Exception e) {
            logger.error("DateUtil_getTimeInterval got error!", e);
        }
        return interval_times;
    }
    
    public static long getTimeIntervalWithNow(Date startdate) {
        Calendar sc = (Calendar) Calendar.getInstance().clone();
        sc.setTime(startdate);
        
        return getTimeInterval(sc, Calendar.getInstance());
    }


    /**
     * Get millisecond time difference between the two calendar.
     * 
     * @param startdate
     * @param enddate
     * @return long
     */
    public static long getTimeInterval(Calendar startdate, Calendar enddate) {

        long interval_times = 0;
            interval_times = enddate.getTimeInMillis() 
                    - startdate.getTimeInMillis();

        return interval_times;

    }

    public static String formatDate(Date d, String format) {
        if (d != null) {
            SimpleDateFormat sdf = getSDFPatternLocal();
            sdf.applyPattern(format);
            return sdf.format(d);
        } else {
            return null;
        }
    }
    
    public static Date parseDate(String dd) {

        if (dd != null) {
            try {
                return getSDFLocal().parse(dd);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }
    
    public static Date parseDate(String dd, String formate) {

        if (dd != null) {
            try {
                SimpleDateFormat sdf=new SimpleDateFormat(formate);  
                return sdf.parse(dd);  
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }
    
    private static final SimpleDateFormat getSDFLocal() {
        if (sdfPatternLocal.get() == null) {
            sdfPatternLocal.set(new SimpleDateFormat(DATETIME_FORMAT));
        }
        return sdfPatternLocal.get();
    }
    
    public static int getDayOfMonth(Date date) {
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        calendar.setTime(date);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        
        return dayOfMonth;
    }
    
    public static Integer getHowMuchDaysOfMonth(int year, int month) {
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);//Java?�份??�??�?
        Integer dateOfMonth = calendar.getActualMaximum(Calendar.DATE);
        
        return dateOfMonth;
    }
    
    private static int getMonth(Date date) {
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH);
        
        return month;
    }
    
    public static int getYear(Date date) {
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        
        return year;
    }
    
    public static Date getBillCycleDate(Integer billCycle) {
        int[] yearMonthDateNow = getYearAndMonthAndDate(new Date());
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        
        calendar.set(yearMonthDateNow[0], yearMonthDateNow[1], billCycle);
        
        return calendar.getTime();
    }
    
    public static int[] getYearAndMonthAndDate(Date date) {
        int[] yearAndMonthAndDate = new int[3];
        
        yearAndMonthAndDate[0] = getYear(date);
        yearAndMonthAndDate[1] = getMonth(date);
        yearAndMonthAndDate[2] = getDayOfMonth(date);
        
        return yearAndMonthAndDate;
      }
    
    public static Date operateSeconds(Date date, Long seconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, seconds.intValue());
        
        return calendar.getTime();
    }
    
    public static Date operateDays(Date date, int days) {
        if(null!=date)
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, days);
            
            return calendar.getTime();
        }
        else
            return null;
    }
    
    /**  
     * 计�?两个?��?之间?�差?�天?? 
     * @param smdate 较�??�时??
     * @param bdate  较大?�时??
     * @return ?�差天数 
     * @throws ParseException  
     */    
    public static int daysBetween(Date smdate,Date bdate) throws ParseException    
    {    
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        smdate=sdf.parse(sdf.format(smdate));  
        bdate=sdf.parse(sdf.format(bdate));  
        Calendar cal = Calendar.getInstance();    
        cal.setTime(smdate);    
        long time1 = cal.getTimeInMillis();                 
        cal.setTime(bdate);    
        long time2 = cal.getTimeInMillis();         
        long between_days=(time2-time1)/(1000*3600*24);  
            
       return Integer.parseInt(String.valueOf(between_days));           
    }
    
    public static String getByPattern(Date date, String pattern){

        SimpleDateFormat sdf=new SimpleDateFormat(pattern);  
        
        return sdf.format(date);
    }
}