/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: DeleteSubService.java
 * Package Name : com.cht.pcrf.api.service.ldap
 * Date 		: 2017年4月25日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api.service.rest;


import com.cht.pcrf.api.util.CurlUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.api.common.CommonConstant;
import org.springframework.beans.factory.annotation.Autowired;

public class SubRestService extends EPCRestService{
    protected final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);

    private SapcRestService sapcRestService;
    
    private SapcRestConfService sapcRestConfService;

    @Override
	public SapcRestService getSapcRestService() {
		return sapcRestService;
	}

    @Override
	public void setSapcRestService(SapcRestService sapcRestService) {
		this.sapcRestService = sapcRestService;
	}

    @Override
	public SapcRestConfService getSapcRestConfService() {
		return sapcRestConfService;
	}

    @Override
	public void setSapcRestConfService(SapcRestConfService sapcRestConfService) {
		this.sapcRestConfService = sapcRestConfService;
	}
   
    

    

    /*@Override
    protected void extraModifyEntry(LdapTemplate ldapEnv, String nodeName,
            String msisdnEntryDN, String[] infos, boolean isExist) throws Exception {
        //存在，且EPC-GroupIds是null，則刪除group
        logger.debug(" check extraModifyEntry");
        boolean hasGroupId = false;
        if(isExist){
            for(String info : infos){
                if(info == null || info.trim().length() == 0) continue;

                String[] kv = info.split(": ");
                
                String key = StringUtils.trimToEmpty(kv[0]);

                if(LdapConstant.EPC_GROUPIDS_LOWCASE.equalsIgnoreCase(key)){
                    hasGroupId = true;
                    break;
                }
            }
            logger.debug("dn:{}, hasGroupId:{} ", msisdnEntryDN, hasGroupId);
            if(!hasGroupId){
                String[] delKeys = {LdapConstant.EPC_GROUPIDS};
                getLdapService().removeAttribute(ldapEnv, nodeName, msisdnEntryDN, delKeys);
            }
        }
    }*/

}
