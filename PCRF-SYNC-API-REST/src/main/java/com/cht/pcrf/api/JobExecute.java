/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: JobExecute.java
 * Package Name : com.cht.pcrf.api
 * Date 		: 2017年4月26日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cht.pcrf.api.common.CommonConstant;
import com.cht.pcrf.api.service.SyncRestService;

public class JobExecute {
    protected static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);
    private static ClassPathXmlApplicationContext ctx;
    public static JobExecute instance;
    private SyncRestService syncRestService;
    
    public static JobExecute getInstance(){
        if(instance == null)
            instance = new JobExecute();
        
        return instance;
    }
    
    private JobExecute(){
        ctx = new ClassPathXmlApplicationContext("api-spring-bean.xml");
        logger.info("Initialize spring config finish!");
        syncRestService = ctx.getBean("syncRestService", SyncRestService.class);
    }

	public SyncRestService getSyncRestService() {
		return syncRestService;
	}

}
