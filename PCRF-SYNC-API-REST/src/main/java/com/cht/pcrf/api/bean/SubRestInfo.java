package com.cht.pcrf.api.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubRestInfo {
    private String subscriberId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<SubDataPlan> dataplans = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private HashMap<String,Object> staticQualification;

    public String getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(String subscriberId) {
        this.subscriberId = subscriberId;
    }

    public List<SubDataPlan> getDataplans() {
        return dataplans;
    }

    public void setDataplans(List<SubDataPlan> dataplans) {
        this.dataplans = dataplans;
    }

    public HashMap<String, Object> getStaticQualification() {
        return staticQualification;
    }

    public void setStaticQualification(HashMap<String, Object> staticQualification) {
        this.staticQualification = staticQualification;
    }

    @Override
    public String toString() {
    	/*{
		        "dataplans" : [
		                {
		                        "dataplanName" : "4G_volte"
		                }
		        ],
		        "subscriberId" : "34122000000001"
		}
		*/
        return "SubscriberRestInfo [subscriberId=" + subscriberId
                + ", dataplans=" + dataplans + "]";
    }
}

