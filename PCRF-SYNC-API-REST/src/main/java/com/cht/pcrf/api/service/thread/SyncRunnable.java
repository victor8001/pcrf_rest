/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: SyncRunnable.java
 * Package Name : com.cht.pcrf.api.service.thread
 * Date 		: 2017年5月7日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api.service.thread;

import com.cht.pcrf.api.util.RestfulUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.api.common.CommonConstant;
import com.cht.pcrf.api.common.SyncDataType;
import com.cht.pcrf.api.service.SyncRestService;
import com.cht.pcrf.api.service.rest.SubRestService;
import com.cht.pcrf.core.common.LdapConstant;

public class SyncRunnable implements Runnable{
    private final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);
    
    private String data;
    private SubRestService subRestService;
    private RestfulUtil restfulUtil;
    
    public SyncRunnable(SubRestService subRestService,RestfulUtil restfulUtil ,String data) {
        this.data = data;
        this.subRestService = subRestService;
        this.restfulUtil = restfulUtil;
    }
    
    public void run() {
//        try {
            syncDataByNodename(data);
//        } catch (Exception e) {
//            logger.error(e.getMessage());
//        }
    }
    
    private boolean syncDataByNodename(String data) {
        try{
            //node,type,subscriber,col1_1,info
            String[] dataArray = data.split(",");
            if(dataArray != null && dataArray.length > 4){
                //'"+priLocation + " - " + nodename+"'"
                String location = dataArray[0].split(" - ")[0];
                String nodename = dataArray[0].split(" - ")[1];
                String msisdn = dataArray[2];
                // ex: EPC-Subscriber
                String classObject = StringUtils.trimToEmpty(dataArray[3]);
                classObject = classObject.toLowerCase();
                // subscriber info 欄位
                String info = dataArray[4];

                logger.info("========== info = " + info);
                String JSONStr = restfulUtil.convertJSON(msisdn,classObject);

                if(SyncDataType.TYPE_2.getId().equals(dataArray[1])
                        || SyncDataType.TYPE_3.getId().equals(dataArray[1])){
                	subRestService.modifyEntry(location, nodename, msisdn, JSONStr);
                }else if(SyncDataType.TYPE_1.getId().equals(dataArray[1])){
                	subRestService.deleteEntry(location, nodename, msisdn);
                }
            }
            return true;
        }catch(Exception e){
        	SyncRestService.syncResult = false;
            throw new RuntimeException("syncDataByNodename fail. e:" + e + ", data:" + data);
//            return false;
        }
    }
    
    private String convertToCSVFormat(String data) {
        //TP - 2L3PilotPCRF1,2,88694400008781,epc-subscriberqualification,INFO1,INFO2
        
        StringBuffer sb = new StringBuffer();
        
        data = data.toString().replaceAll("\"\"", "\"");
        String replaceString = "," + CommonConstant.BR + "  \"dataplans";
        String reductionString = "，" + CommonConstant.BR + "  \"dataplans";
        String replaceString2 = "}," + CommonConstant.BR + "  \\{";
        String reductionString2 = "}，" + CommonConstant.BR + "  \\{";
        data = data.replaceAll(replaceString, reductionString);
        data = data.replaceAll(replaceString2, reductionString2);
        String[] tmpDataArr = data.split(",", 6);
        
        int i=0;
        for(String tmpData : tmpDataArr){
            
        	if(i == 5){
        		break;
        	}
        	
            if(i != 0){
                sb.append(",");
            }
            
            if(i == 0){
                //TP - 2L3PilotPCRF1
                tmpData = tmpData.split(" - ")[0];
            }else if(i == 4 || i == 5){
            	tmpData = "\"" + tmpData + "\"";
                tmpData = tmpData.replaceAll("\"\"", "");
                tmpData = tmpData.replaceAll("\"" + CommonConstant.BR + "\"", "");
                if(tmpData.indexOf(reductionString) != -1){
            		tmpData = tmpData.replaceAll(reductionString, replaceString);
            	}
                tmpData = tmpData.replaceAll(reductionString2, replaceString2);
            }
            i++;
            sb.append(tmpData);
        }
        
        return sb.toString();
    }
}
