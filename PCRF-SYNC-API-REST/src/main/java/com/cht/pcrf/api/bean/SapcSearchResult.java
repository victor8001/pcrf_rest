/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: SapcSearchResult.java
 * Package Name : com.cht.pcrf.api.bean
 * Date 		: 2017年5月19日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SapcSearchResult {

    private Map<String, List<String>> attributes = new HashMap<>();

    public Map<String, List<String>> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, List<String>> attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return "SapcSearchResult [attributes=" + attributes + "]";
    }
    
}
  