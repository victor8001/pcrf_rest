/**
 * Project Name : PG-Adaptor
 * File Name 	: SapcRestService.java
 * Package Name : com.fet.pg.adaptor.rest
 * Date 		: 2017年12月13日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api.service.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.directory.BasicAttribute;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.client.ClientProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.CommunicationException;
import org.springframework.stereotype.Service;

import com.cht.pcrf.api.common.CommonConstant;
import com.cht.pcrf.api.exception.ApiException;
import com.cht.pcrf.core.common.LdapConstant;
import com.cht.pcrf.core.model.PcrfInfo;
import com.cht.pcrf.core.service.CoreDaoFactory;

@Service
public class SapcRestService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SapcRestService.class);
    private static final int NO_SUCH_OBJECT = 404;
    
    private Map<String, List<PcrfInfo>> sapcInfoMapByLC = null;
    private Map<String, List<PcrfInfo>> sapcInfoMapByNode = null;
    
    public SapcRestService() throws Exception{
        setPcrfInfoMapByLC();
    }
    
    @Value("${sapc.rest.url.protocol}") 
    String URL_PROTOCOL;
    @Value("${sapc.rest.sub.put.url}") 
    String SUB_PUT_URL;
    @Value("${sapc.rest.sub.del.url}") 
    String SUB_DEL_URL;
    @Value("${sapc.rest.sub.get.url}") 
    String SUB_GET_URL;
    @Value("${sapc.rest.group.get.url}") 
    String GROUP_GET_URL;
    @Value("${sapc.rest.retry.code}") 
    String RETY_CODE;
    @Value("${sapc.rest.put.succ.code}") 
    String PUT_SUCC_CODE;
    @Value("${sapc.rest.del.succ.code}") 
    String DEL_SUCC_CODE;
    @Value("${sapc.rest.get.succ.code}") 
    String GET_SUCC_CODE;
    
    public void moidfy(final String baseUrl, String subId, String infos, Client restClient) throws ApiException{
        put(baseUrl, subId, infos, restClient);
    }
//    @Override
    public void put(final String baseUrl, String subId, String infos, Client restClient) throws ApiException{
            LOGGER.debug("Put subId: {}", subId);
        try{
            String url = URL_PROTOCOL + "://" + baseUrl + SUB_PUT_URL;
//            Map<String, Object> infoMap = SapcRestUtil.convertSubLdapAttributeToJsonMap(subId, attributeMap);
            
            url = url.replace("{id}", subId);
            LOGGER.info("Put subId: {} , {}, {}", subId, url, infos);

            LOGGER.info("Put subId: {} , {}, {}" + subId +","+ url +","+ infos + "\n");
            LOGGER.info("JSON Data:"+Entity.entity(infos, MediaType.APPLICATION_JSON).getEntity()+ "\n");
            LOGGER.info("Request PCRF URL: {} ",url);

            WebTarget webTarget = restClient.target(url); //.path("employees");
            
            Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
            invocationBuilder.property(ClientProperties.CONNECT_TIMEOUT,500);
            invocationBuilder.property(ClientProperties.READ_TIMEOUT,500);
            Response response = invocationBuilder.put(Entity.entity(infos, MediaType.APPLICATION_JSON));
            LOGGER.info("response Status: "+response.getStatus());
            if(PUT_SUCC_CODE.contains(Integer.toString(response.getStatus()))) {
                LOGGER.info("Create Subscriber success, DN: {}", subId);
            } else {
                LOGGER.info("Create Subscriber fail, DN: {}, result: {}", subId, response.getStatus());
                String errmsg = Integer.toString(response.getStatus()) + response.getStatusInfo();
                throw new ApiException(CommonConstant.PROCCESS_REQUIRED, errmsg);
            }
            response.readEntity(HashMap.class);
            
        } catch (CommunicationException ex){
            LOGGER.warn("Connect Rest failed: {}", ex.getMessage(), ex);
            throw new ApiException(CommonConstant.PROCCESS_REQUIRED, ex.getMessage(), ex);
            
        } catch (ApiException ex) {
            throw ex;
        } catch (Exception ex){
            LOGGER.error("Create Subscriber error: {}", ex.getMessage(), ex);
            LOGGER.error(ex.getMessage(), ex);
            
            throw new ApiException(CommonConstant.PROCCESS_REQUIRED, ex.getMessage(), ex);
        }
        LOGGER.info("Put subId: {} success!", subId);
    }
    

//    @Override
    public void delete(final String baseUrl, String subId, Client restClient) throws ApiException{
        LOGGER.debug("Delete subId: {}", subId);
        
        try{
            String url = URL_PROTOCOL + "://" + baseUrl + SUB_DEL_URL;

            url = url.replace("{id}", subId);
            LOGGER.debug("Delete subId: {} , {}", subId, url);
            LOGGER.info("Delete subId: {} , {}", subId, url);

            WebTarget webTarget = restClient.target(url); //.path("employees");
            
            Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
            
            Response response = invocationBuilder.delete();

            if(DEL_SUCC_CODE.contains(Integer.toString(response.getStatus()))) {
                LOGGER.info("Delete Subscriber success, DN: {}", subId);
            } else {
                LOGGER.info("Delete Subscriber fail, DN: {}, result: {}", subId, response.getStatus());
                String errmsg = Integer.toString(response.getStatus()) + response.getStatusInfo();
                throw new ApiException(CommonConstant.PROCCESS_REQUIRED, errmsg);
            }
            response.readEntity(HashMap.class);
            
        } catch (CommunicationException ex){
            LOGGER.warn("Connect Rest failed: {}", ex.getMessage(), ex);
            throw new ApiException(CommonConstant.PROCCESS_REQUIRED, ex.getMessage(), ex);
            
        } catch (ApiException ex) {
            throw ex;
        } catch (Exception ex){
            LOGGER.error("Delete Subscriber error: {}", ex.getMessage(), ex);
            LOGGER.error(ex.getMessage(), ex);
            
            throw new ApiException(CommonConstant.PROCCESS_REQUIRED, ex.getMessage(), ex);
        }
        
        LOGGER.info("Delete subId: {} success!", subId);
    }
    
//    @SuppressWarnings("unchecked")
//    public SapcSearchResult get(final String nodeName, final String baseUrl, String subId, String dn, String filter, Client restClient) throws ApiException, SapcLdapEntryNotExistsException{
//        LOGGER.debug("Get subId: {}", subId);
//        try{
//            String url = URL_PROTOCOL + "://" + baseUrl + SUB_GET_URL;
//            String objectClass = SapcRestUtil.LDAP_OBJ_CLA_EPC_SUBSCRIBERID;
//            
//            LOGGER.debug("dn: {}", dn);
//            LOGGER.debug("filter: {}", filter);
//            
//            if(dn.indexOf(SapcRestUtil.LDAP_SERACH_EPC_SUBSCRIBERGROUPID) != -1
//                    || filter.indexOf(SapcRestUtil.LDAP_SERACH_EPC_SUBSCRIBERGROUPID) != -1){
//                url = URL_PROTOCOL + "://" + baseUrl + GROUP_GET_URL;
//                objectClass = SapcRestUtil.LDAP_OBJ_CLA_EPC_GROUPID;
//                //url = url.replace("{id}", subId);
//                
//                //get group id
//                //dn:EPC-SubscriberGroupId=Sy,applicationName=EPC-EpcNode,nodeName=TPS1PCRF //filter:(objectclass=*) 
//                //dn:applicationName=EPC-EpcNode,nodeName=RestSrv1 // filter:EPC-SubscriberGroupId=Sy
//                
//                String groupId = "";
//                if(dn.indexOf(SapcRestUtil.LDAP_SERACH_EPC_SUBSCRIBERGROUPID) != -1){
//                	groupId = dn.substring(dn.indexOf(Constants.SAPC_SUBSCRIBERGROUPID_PREFIX), dn.indexOf(","))
//                        .replace(Constants.SAPC_SUBSCRIBERGROUPID_PREFIX, "");
//                }else{
//                	groupId = filter.replace(Constants.SAPC_SUBSCRIBERGROUPID_PREFIX, "");
//                }
//                
//            	url = url.replace("{id}", groupId);
//                LOGGER.debug("Get groupId: {} , {}, {}", subId, url, objectClass);
//            }else{
//            	url = url.replace("{id}", subId);
//                LOGGER.debug("Get subId: {} , {}, {}", subId, url, objectClass);
//            }
//
//            
//            WebTarget webTarget = restClient.target(url); //.path("employees");
//            
//            Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
//            
//            Response response = invocationBuilder.get();
//            HashMap<String, Object> info = response.readEntity(HashMap.class);
//
//            if(response.getStatus() == NO_SUCH_OBJECT) {
//                throw new SapcLdapEntryNotExistsException("Search object not found, DN: " + dn);
//            } else if(GET_SUCC_CODE.contains(Integer.toString(response.getStatus()))) {
//                LOGGER.info("Get Subscriber success, subId: {}", subId);
//                
//                //TODO CONVERT
//                SapcSearchResult searchResult = SapcRestUtil.convertJsonMapToLdapAttribute(nodeName, objectClass, info);
//                
//                return searchResult;
//            } else {
//                LOGGER.info("Get Subscriber fail, DN: {}, result: {}", subId, response.getStatus());
//                String errmsg = Integer.toString(response.getStatus()) + response.getStatusInfo();
//                throw new ApiException(errmsg, SapcRestUtil.converRestCodeToLdapCode(response.getStatus()));
//            }
//            
//        } catch (CommunicationException ex){
//            LOGGER.warn("Connect Rest failed: {}", ex.getMessage(), ex);
//            throw new ApiException(ex.getMessage(), LdapErrorCodeEnum.UNAVAILABLE.getCode());
//        } catch (SapcLdapEntryNotExistsException ex){
//            throw ex;
//        } catch (ApiException ex) {
//            throw ex;
//        } catch (Exception ex){
//            LOGGER.error("Get Subscriber error: {}", ex.getMessage(), ex);
//            LOGGER.error(ex.getMessage(), ex);
//            
//            throw new ApiException(ex.getMessage(), SapcRestUtil.converRestCodeToLdapCode(errorCode));
//        } finally {
//            LOGGER.info("Get sub: {} success!", subId);
//        }
//    }

    public void resetPcrfInfoMapByLC() throws Exception {
        this.sapcInfoMapByLC = CoreDaoFactory.getInstance().getPcrfInfoRestDao().getPcrfInfoMap();
        setPcrfInfoMapByNode();
    }
    
    protected Map<String, List<PcrfInfo>> getPcrfInfoMapByLC() throws Exception {
        return sapcInfoMapByLC;
    }
    
    private void setPcrfInfoMapByLC() throws Exception {
        if(sapcInfoMapByLC == null){
            this.sapcInfoMapByLC = CoreDaoFactory.getInstance().getPcrfInfoRestDao().getPcrfInfoMap();
            setPcrfInfoMapByNode();
        }
    }
    

    protected List<PcrfInfo> getPcrfInfoByLocation(String location) throws Exception {
        return getPcrfInfoMapByLC().get(location);
    }

    private void setPcrfInfoMapByNode() throws Exception {
        Map<String, List<PcrfInfo>> tmpMap = new HashMap<String, List<PcrfInfo>>();
        getPcrfInfoMapByLC();
        for(String location : sapcInfoMapByLC.keySet()){
            for(PcrfInfo sapcInfo : sapcInfoMapByLC.get(location)){
                String key = location + " - " + sapcInfo.getNodeName();
                if(tmpMap.get(key) == null){
                    tmpMap.put(key, new ArrayList<PcrfInfo>());
                }
                tmpMap.get(key).add(sapcInfo);
            }
        }
        this.sapcInfoMapByNode = tmpMap;
    }
    
    protected List<PcrfInfo> getPcrfInfoByNode(String location, String nodename) throws Exception {
        //String key = location + " - " + sapcInfo.getNodeName();
        return getPcrfInfoMapByNode().get(location + " - " + nodename);
    }
    
    public Map<String, List<PcrfInfo>> getPcrfInfoMapByNode() throws Exception {
        return sapcInfoMapByNode;
    }
	public String getURL_PROTOCOL() {
		return URL_PROTOCOL;
	}
	public void setURL_PROTOCOL(String uRL_PROTOCOL) {
		URL_PROTOCOL = uRL_PROTOCOL;
	}
	public String getSUB_PUT_URL() {
		return SUB_PUT_URL;
	}
	public void setSUB_PUT_URL(String sUB_PUT_URL) {
		SUB_PUT_URL = sUB_PUT_URL;
	}
	public String getSUB_DEL_URL() {
		return SUB_DEL_URL;
	}
	public void setSUB_DEL_URL(String sUB_DEL_URL) {
		SUB_DEL_URL = sUB_DEL_URL;
	}
	public String getSUB_GET_URL() {
		return SUB_GET_URL;
	}
	public void setSUB_GET_URL(String sUB_GET_URL) {
		SUB_GET_URL = sUB_GET_URL;
	}
	public String getGROUP_GET_URL() {
		return GROUP_GET_URL;
	}
	public void setGROUP_GET_URL(String gROUP_GET_URL) {
		GROUP_GET_URL = gROUP_GET_URL;
	}
	public String getRETY_CODE() {
		return RETY_CODE;
	}
	public void setRETY_CODE(String rETY_CODE) {
		RETY_CODE = rETY_CODE;
	}
	public String getPUT_SUCC_CODE() {
		return PUT_SUCC_CODE;
	}
	public void setPUT_SUCC_CODE(String pUT_SUCC_CODE) {
		PUT_SUCC_CODE = pUT_SUCC_CODE;
	}
	public String getDEL_SUCC_CODE() {
		return DEL_SUCC_CODE;
	}
	public void setDEL_SUCC_CODE(String dEL_SUCC_CODE) {
		DEL_SUCC_CODE = dEL_SUCC_CODE;
	}
	public String getGET_SUCC_CODE() {
		return GET_SUCC_CODE;
	}
	public void setGET_SUCC_CODE(String gET_SUCC_CODE) {
		GET_SUCC_CODE = gET_SUCC_CODE;
	}
    
    
}
