/**
 * Project Name : PG-Adaptor
 * File Name 	: RestFactory.java
 * Package Name : com.fet.pg.adaptor.rest
 * Date 		: 2017年12月13日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api.service.rest;

import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.glassfish.jersey.apache.connector.ApacheClientProperties;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cht.pcrf.core.model.PcrfInfo;

@Service
public class SapcRestConfService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SapcRestConfService.class);
    private static final String REST_URL_PREFIX = "https://";
    
//    private static TreeMap<SapcInfo, Client> sapcConf = new TreeMap<>();
//    private SapcInfoRepository sapcInfoRepository;
    
//    @Value("${sapc.conn.timeout}") 
    String sapcConnTimeout;

//    @Value("${sapc.read.timeout}") 
    String sapcReadTimeout;
    
//    @Value("${sapc.rest.maxTotal}") 
    String sapcRestMaxTotal;
    
//    @Autowired
//    public SapcRestConfService(SapcInfoRepository sapcInfoRepository, 
//            @Value("${sapc.conn.timeout}") String connTimeout, 
//            @Value("${sapc.read.timeout}") String readTimeout,
//            @Value("${sapc.rest.maxTotal}") String maxTotal
//            ) throws GeneralSecurityException{
//        this.sapcInfoRepository = sapcInfoRepository;
//        List<SapcInfo> sapcInfoList = sapcInfoRepository.findByStatusAndRestServer(Constants.SAPC_ONLINE);
//        initRestConf(sapcInfoList, connTimeout, readTimeout, maxTotal);
//    }
    

//    public void initRestConf(List<SapcInfo> sapcInfoList, String connTimeout,
//            String readTimeout, String maxTotal) throws GeneralSecurityException {
//        for(SapcInfo sapcInfo : sapcInfoList){
//            LOGGER.info("Initial rest client from SapcInfo: {}, connection timeout:{}, read timeout:{}, maxTotal:{} ", new Object[] { sapcInfo, connTimeout, readTimeout, maxTotal });
//            
//            String url = REST_URL_PREFIX + sapcInfo.getNodeHost() + ":" + sapcInfo.getNodePort();
//            
//            Client restClient = getTrustClient(sapcInfo.getLoginId(), sapcInfo.getLoginPw(),
//                    connTimeout, readTimeout, maxTotal);
//            
//            LOGGER.info("Initial rest client Url:{} ", url);
//            sapcConf.put(sapcInfo, restClient);
//        }
//    }
    
    public Client createSingleRestClient(PcrfInfo pcrfInfo) throws Exception {

        LOGGER.info("Initial rest client from SapcInfo: {}, connection timeout:{}, read timeout:{}, maxTotal:{} ", new Object[] { pcrfInfo, sapcConnTimeout, sapcReadTimeout, 1 });
        
        String url = REST_URL_PREFIX + pcrfInfo.getNodeHost() + ":" + pcrfInfo.getNodePort();
        
        Client restClient = getSingleTrustClient(pcrfInfo.getLoginId(), pcrfInfo.getLoginPw(),
                sapcConnTimeout, sapcReadTimeout, "1");
        
        LOGGER.info("Initial rest client Url:{} ", url);
        return restClient;
        
    }

    private Client getSingleTrustClient(String username, String password,
            String connTimeout, String readTimeout, String maxTotal)
            throws GeneralSecurityException {

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(
                username, password);

        return ClientBuilder.newBuilder()
                .withConfig(getPoolClientConfig(Integer.parseInt(connTimeout), Integer.parseInt(readTimeout), Integer.parseInt(maxTotal)))
                .register(JacksonFeature.class)
                .register(feature).build();
    }
    
    public Client createRestClient(PcrfInfo pcrfInfo) throws Exception {

        LOGGER.info("Initial rest client from SapcInfo: {}, connection timeout:{}, read timeout:{}, maxTotal:{} ", new Object[] { pcrfInfo, sapcConnTimeout, sapcReadTimeout, sapcRestMaxTotal });
        
        String url = REST_URL_PREFIX + pcrfInfo.getNodeHost() + ":" + pcrfInfo.getNodePort();
        
        Client restClient = getTrustClient(pcrfInfo.getLoginId(), pcrfInfo.getLoginPw(),
                sapcConnTimeout, sapcReadTimeout, sapcRestMaxTotal);
        
        LOGGER.info("Initial rest client Url:{} ", url);
        return restClient;
        
    }

    private Client getTrustClient(String username, String password,
            String connTimeout, String readTimeout, String maxTotal)
            throws GeneralSecurityException {

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(
                username, password);

        return ClientBuilder.newBuilder()
                .withConfig(getPoolClientConfig(Integer.parseInt(connTimeout), Integer.parseInt(readTimeout), Integer.parseInt(maxTotal)))
                .register(JacksonFeature.class)
                .register(feature).build();
    }

    private static ClientConfig getPoolClientConfig(int connTimeout, int readTimeout, int maxTotal)
            throws GeneralSecurityException {

        SSLConnectionSocketFactory sslSocketFactory = createSSLFactory();

        final Registry<ConnectionSocketFactory> registry = RegistryBuilder
                .<ConnectionSocketFactory> create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", sslSocketFactory).build();

        ClientConfig clientConfig = new ClientConfig();
        clientConfig.property(ClientProperties.CONNECT_TIMEOUT, connTimeout);
        clientConfig.property(ClientProperties.READ_TIMEOUT, readTimeout);
        //
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
        connectionManager.setMaxTotal(maxTotal); // n max connections when hitting localhost
        // connectionManager.setDefaultMaxPerRoute(10);

        clientConfig.property(ApacheClientProperties.CONNECTION_MANAGER, connectionManager);
        clientConfig.connectorProvider(new ApacheConnectorProvider());

        return clientConfig;
    }

    private static SSLConnectionSocketFactory createSSLFactory()
            throws GeneralSecurityException {

        TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain,
                    String authType) throws CertificateException {
            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain,
                    String authType) throws CertificateException {
            }
        } };

        SSLContext ctx = null;
        try {
            ctx = SSLContext.getInstance("TLSv1.2");
            ctx.init(null, null, null);// SecureRandom random
        } catch (java.security.GeneralSecurityException e) {
            e.printStackTrace();
            throw e;
        }

        HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());

        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(
                ctx, new HostnameVerifier() {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1) {
                        return true;
                    }
                });
        
        return sslSocketFactory;
    }

	public String getSapcConnTimeout() {
		return sapcConnTimeout;
	}

	public void setSapcConnTimeout(String sapcConnTimeout) {
		this.sapcConnTimeout = sapcConnTimeout;
	}

	public String getSapcReadTimeout() {
		return sapcReadTimeout;
	}

	public void setSapcReadTimeout(String sapcReadTimeout) {
		this.sapcReadTimeout = sapcReadTimeout;
	}

	public String getSapcRestMaxTotal() {
		return sapcRestMaxTotal;
	}

	public void setSapcRestMaxTotal(String sapcRestMaxTotal) {
		this.sapcRestMaxTotal = sapcRestMaxTotal;
	}

}
