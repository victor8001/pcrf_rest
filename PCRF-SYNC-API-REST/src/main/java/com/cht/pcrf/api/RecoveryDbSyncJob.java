/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: RecoveryDbSyncJob.java
 * Package Name : com.cht.pcrf.api
 * Date 		: 2017年8月21日 
 * Author 		: LauraChu
 * Description  : PCRF Data Recovery to DB Sync  
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.api.common.CommonConstant;
import com.cht.pcrf.api.service.SyncRestService;

public class RecoveryDbSyncJob {
    protected static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);
    
    public static void main(String[] args){
        RecoveryDbSyncJob jobExecute = new RecoveryDbSyncJob();
        jobExecute.execute(args);
    }
    
    /**
     * args[0] : nodename
     * args[1] : pcrf_name
     * args[2] : 開始時間 ("2017-04-21 13:30:55")
     * @param args
     */
    private void execute(String[] args) {

        final long sTime = System.currentTimeMillis();
        try {
            logger.info("====== RecoveryDbSyncJob start execute ======");

            logger.info("csvPath: " + args[0]);
            logger.info("msisdnPath: " + (args.length >=2 ? args[1] : null));
            
            SyncRestService syncRestService = JobExecute.getInstance().getSyncRestService();
            syncRestService.syncDataFromPcrfToDbSync(args[0], (args.length >=2 ? args[1] : null));

        } catch (Exception e) {
            logger.error("RecoveryDbSyncJob exception, e:", e);
            logger.error(e.getMessage(), e);
        } finally{
            logger.info("====== RecoveryDbSyncJob Execute finished, spend time: "
                    + (System.currentTimeMillis() - sTime) / 1000
                    + " seconds ======");
        }
    }
}
