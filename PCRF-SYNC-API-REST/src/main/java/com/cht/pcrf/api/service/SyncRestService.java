package com.cht.pcrf.api.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.cht.pcrf.api.util.RestfulUtil;
import com.cht.pcrf.core.model.Subscriber;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.api.common.CommonConstant;
import com.cht.pcrf.api.common.SyncDataType;
import com.cht.pcrf.api.exception.ApiException;
import com.cht.pcrf.api.service.rest.SubRestService;
import com.cht.pcrf.api.service.thread.SyncRunnable;
import com.cht.pcrf.api.util.DateUtil;
import com.cht.pcrf.api.util.FileUtil;
import com.cht.pcrf.core.model.PcrfConsistenLog;
import com.cht.pcrf.core.model.TransactionLog;
import com.cht.pcrf.core.service.CoreDaoFactory;

public class SyncRestService extends ServiceExcute{

	private final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);
    private ExecutorService executor; //Executors.newFixedThreadPool(1);
    private static final Date now = new Date();
    public static boolean syncResult = true;
    private SubRestService subRestService;
    private int threadPoolNum;
    private String exportTxLogsPath = "";
    private String locationName = "";
    private RestfulUtil restfulUtil;
    
    public SyncRestService(int threadPoolNum){
        this.threadPoolNum = threadPoolNum;
        executor = Executors.newFixedThreadPool(threadPoolNum);
    }
    
    /**
     * 1.回補資料(由GUI畫面回補)
     * @param csvPath
     * @return
     * @throws ApiException
     */
    public boolean syncBySub(String user, String csvPath) throws ApiException {
        BufferedReader buf = null;
        String typeName = "syncBySub";
        try {
            logger.info("sync data start ({}), csvPath:{} ", typeName, csvPath);
            
            StringBuffer data = new StringBuffer();
            String sCurrentLine;
            buf = new BufferedReader(new FileReader(csvPath));
            
            //第一筆資料會帶標頭，略過第一行
            if ((sCurrentLine = buf.readLine()) != null) {
            }
//            int i=0;
            while ((sCurrentLine = buf.readLine()) != null) {
//                i++;
                if (sCurrentLine == null || sCurrentLine.length() == 0)
                    continue;

                String location = sCurrentLine.split(",")[0];
                if(CommonConstant.locations.contains(location)){
                    logger.debug("data:" + data);
                    syncDataByLocation(data.toString());
                    data = new StringBuffer();
                }

                data.append(sCurrentLine + CommonConstant.BR);
            }
            if(data.length() > 0){
                logger.debug("data:" + data);
                syncDataByLocation(data.toString());
            }

            return true;
        } catch (Exception e) {
            logger.error("syncBySub error ({}). csvPath: {}, ex: {}", typeName, csvPath, e.getMessage());
            logger.error(e.getMessage(), e);
            throw new ApiException(CommonConstant.PROCCESS_REQUIRED, "syncBySub error", e);
        } finally{
            logger.info("sync data finish ({}), csvPath:{} ", typeName, csvPath);
            try {
                if(buf != null)
                    buf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * 由trap tigger
     * 1.取得起迄的request
     * 2.是否系統回補資料
     * 3.回補資料
     * 4.另存發送記錄LOG
     * @return
     * @throws ApiException
     */
    public boolean syncByTransationTime(String location, String nodename, Long user, String syncTime) throws ApiException {
        Long logId = null;
        boolean recBySys = false;
        boolean syncResult = false;
        try {
            logger.info("location:{}, nodename:{}, syncTime:{}", location, nodename, syncTime);
            
            Date syncDate = DateUtils.parseDate(syncTime, ServiceExcute.EXEC_TS_PATTERN);
            
            //save log
            logId = saveExcutingLog(nodename, syncDate.getTime(), user);
            
            //1.取得起迄的request
            String logsFilePath = getExportTxLogsPath()
                    + getCsvFileName("TX_LOGS_" + location + "_" + nodename, now);
            
            String startTime = CoreDaoFactory.getInstance().getPcrfConsistentRestDao().findAutoRecMinStartTime(syncTime);
            CoreDaoFactory.getInstance().getTransactionLogRestDao().findTransactionLog(logsFilePath, location, nodename, startTime, syncTime);
            
            //2.是否系統回補資料
            recBySys = isRecoverBySystem();
            if(!recBySys) {
                logger.info("Recovery by Manul");
                //4.另存發送記錄LOG
                updateRecoveryResult(logId, location, nodename, logsFilePath);
                return true;
            }
            
            //3.回補資料
            updateRecoveryResultExecting(logId);
            syncResult = syncDataByNodenameFromFile(logsFilePath, "syncByTransationTime");
            
            //4.另存發送記錄LOG
            updateRecoveryResult(logId, location, nodename, logsFilePath, syncResult);
            //saveRecoveryResult(location, nodename, user, logsFilePath, syncResult, now.getTime());
            
            return true;
        } catch (Exception e) {
            logger.error("syncByTransationTime error. location:{}, nodename:{}, startTime:{}", location, nodename, syncTime);
            logger.error(e.getMessage(), e);
            try {
                updateRecoveryResult(logId, location, nodename, "", false);
            } catch (Exception e1) {
                logger.error("updateRecoveryResult fail, e:" + e);
                logger.error(e.getMessage(), e);
            }
            throw new ApiException(CommonConstant.PROCCESS_REQUIRED, "syncByTransationTime error", e);
        } finally{
            if(recBySys){
                logger.error("====== Auto_Recovery_Finished (nodename:{}, syncResult:{}) ======", nodename, syncResult);
            }
        }
    }
    
    /**
     * 系統回補資料
     * 1.是否系統回補資料 及db的資料大於temp_dbn的資料
     * 2.回補資料
     * 3.另存發送記錄LOG
     * 
     * PCRF DB sync 與PCRF 資料依據GUI 設定的比對週期進行資料比對,  若有資料異常筆數, 會以
       PCRF DB sync 的資料庫為主, 對 PCRF 進行異常筆數修正的供裝. 若PCRF DB sync 筆數少於PCRF 
                      會停此自動回補機制．
     * @param csvPath
     * @return
     * @throws ApiException
     */
    public boolean syncBySystem(String execTS, Long logId, long user, String csvPath) throws ApiException {
        
        boolean syncResult = true;
        boolean recBySys = false;
        boolean isDBDataMoreThenPcrf = false;
        String location = "";
        try {
            logger.info("syncBySystem start");
            
            //1.是否系統回補資料
            recBySys = isRecoverBySystem();
            if(!recBySys) {
                logger.info("Recovery by Manul");
                return true;
            }
            
            //1.db的資料大於temp_dbn的資料
            File f = new File(csvPath);
            location = f.getName().split("_")[2];
            isDBDataMoreThenPcrf = isDBDataMoreThenPcrf(location, execTS);
            if(!isDBDataMoreThenPcrf) {
                logger.info("DB's data less then Pcrf's data");
                return true;
            }

            //2.回補資料
            updateRecoveryResultExecting(logId);
            syncResult = syncDataByNodenameFromFile(csvPath, "syncBySystem");
            
            return syncResult;
        } catch (Exception e) {
            logger.error("syncBySystem error. csvPath: {}, ex: {}", csvPath, e.getMessage());
            logger.error(e.getMessage(), e);
            throw new ApiException(CommonConstant.PROCCESS_REQUIRED, "syncBySystem error", e);
        } finally{
            if(recBySys && isDBDataMoreThenPcrf){
                //3.另存發送記錄LOG
                updateRecoveryResult(logId, syncResult);
                logger.error("====== Auto_Recovery_Finished (csvPath:{}, syncResult:{}) ======", csvPath, syncResult);
            }
            logger.info("syncBySystem end");
        }
    }

    /**
     * 手動回補資料
     * 2.回補資料
     * 3.另存發送記錄LOG
     * @param csvPath
     * @return
     * @throws Exception 
     */
    public boolean syncByManul(long user, Long logId, String csvPath) throws Exception {

        boolean syncResult = true;
        String location = "";
        String nodename = "";
        try {
            logger.info("csvPath:" + csvPath);
            if(csvPath == null) return true;
            

            File f = new File(csvPath);
            location = f.getName().split("_")[2];
            nodename = f.getName().split("_")[3]; 

            //2.回補資料
            if(logId != null){
                updateRecoveryResultExecting(logId);
            }
            syncResult = syncDataByNodenameFromFile(csvPath, "syncByManul");

            return syncResult;
        } catch (Exception e) {
            logger.error("syncByManul error. csvPath: {}, ex: {}", csvPath, e.getMessage());
            logger.error(e.getMessage(), e);
            throw new ApiException(CommonConstant.PROCCESS_REQUIRED, "syncByManul error", e);
        } finally{
            if(logId != null){
                //3.另存發送記錄LOG
                updateRecoveryResult(logId, syncResult);
            }else{
                //3.另存發送記錄LOG
                saveRecoveryResult(location, nodename, user, csvPath, syncResult, now.getTime());
            }

            logger.error("====== Auto_Recovery_Finished (nodename:{}, syncResult:{}) ======", nodename, syncResult);
        }
    }
    
    /**
     * 回補資料(由pcrf data sync to db sync)
     * @param csvPath
     * @return
     * @throws ApiException
     */
    public boolean syncDataFromPcrfToDbSync(String csvPath, String msisdnPath) throws Exception {
        //TODO
        boolean syncResult = false;
        BufferedReader allDataBuf = null;
        BufferedReader filterMsisdnBuf = null;
//        List<String> csvList = new ArrayList<String>();
        List<String> msisdnList = new ArrayList<String>();
        
        logger.info("csvPath:{}, msisdnPath:{}", csvPath, msisdnPath);
        
//        Date syncDate = DateUtils.parseDate(syncTime, ServiceExcute.EXEC_TS_PATTERN);
        
        //get msisdn List
        try {
            if(msisdnPath != null && !"".equals(msisdnPath)){
                String sCurrentLine;
                filterMsisdnBuf = new BufferedReader(new FileReader(msisdnPath));
                
                while ((sCurrentLine = filterMsisdnBuf.readLine()) != null) {
                    //logger.debug("sCurrentLine:" + sCurrentLine);
                    msisdnList.add(sCurrentLine);
                }
            }
        } catch (Exception e) {
            logger.error("read msisdn file error. msisdnPath: {}, ex: {}", msisdnPath, e.getMessage());
            logger.error(e.getMessage(), e);
            throw new ApiException(CommonConstant.PROCCESS_REQUIRED, "read msisdn file error", e);
        } finally{
            try {
                if(filterMsisdnBuf != null)
                    filterMsisdnBuf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        try {
            //2.回補資料
            //type:1 只存在在pcrf
            //type:2 只存在在db sync
            //type:3 pcrf與db sync資料不一致
            StringBuffer data = new StringBuffer();
            String sCurrentLine;
            allDataBuf = new BufferedReader(new FileReader(csvPath));
            
            while ((sCurrentLine = allDataBuf.readLine()) != null) {
                if (sCurrentLine == null || sCurrentLine.length() == 0)
                    continue;

                String location = sCurrentLine.split(",")[0].split(" - ")[0];
                if(CommonConstant.locations.contains(location) && data.length() != 0){
                    //logger.debug("data:" + data);
                    String msisdn = data.toString().split(",")[2];
                    
                    if(msisdnList.size() == 0 || msisdnList.contains(msisdn)){
                        boolean tempResult = syncDataFromPcrfToDbSync(data.toString());
                        if(!tempResult)
                            syncResult = false;
                    }
                    
                    data = new StringBuffer();
                }

                data.append(sCurrentLine + CommonConstant.BR);
            }
            
            if(data.length() > 0){
                logger.debug("data:" + data);
                String msisdn = data.toString().split(",")[2];
                
                if(msisdnList.size() == 0 || msisdnList.contains(msisdn)){
                    boolean tempResult = syncDataFromPcrfToDbSync(data.toString());
                    if(!tempResult)
                        syncResult = false;
                }
            }
            
            return syncResult;
        } catch (Exception e) {
            logger.error("syncDataFromPcrfToDbSync error ({}). csvPath: {}, msisdnPath:{}, ex: {}", csvPath, msisdnPath, e.getMessage());
            logger.error(e.getMessage(), e);
            throw new ApiException(CommonConstant.PROCCESS_REQUIRED, "sync data error", e);
        } finally{
            logger.info("syncDataFromPcrfToDbSync finish ({}), csvPath:{} ", csvPath, csvPath);
            
            try {
                if(allDataBuf != null)
                    allDataBuf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
    }
    
    private boolean syncDataByNodenameFromFile(String csvPath, String typeName) throws ApiException {
        BufferedReader buf = null;

        try {
            //2.回補資料
            //type:1 只存在在pcrf
            //type:2 只存在在db sync
            //type:3 pcrf與db sync資料不一致
            logger.info("sync data start ({}), csvPath:{} ", typeName, csvPath);
            
            StringBuffer data = new StringBuffer();
            String sCurrentLine;
            buf = new BufferedReader(new FileReader(csvPath));
            
            while ((sCurrentLine = buf.readLine()) != null) {
                if (sCurrentLine == null || sCurrentLine.length() == 0)
                    continue;

                String location = sCurrentLine.split(",")[0].split(" - ")[0];
                if(CommonConstant.locations.contains(location)){
                    logger.debug("data:" + data);
                    boolean tempResult = syncDataByNodename(data.toString());
                    if(!tempResult)
                        syncResult = false;
                    
                    data = new StringBuffer();
                }

                data.append(sCurrentLine + CommonConstant.BR);
            }
            if(data.length() > 0){
                boolean tempResult = syncDataByNodename(data.toString());
                if(!tempResult)
                    syncResult = false;
            }
            
            executor.shutdown(); // 最後記得關閉Thread pool

            // 等待所有作業執行完畢，才繼續執行
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            logger.info("all thread complete");
            
            return syncResult;
        } catch (Exception e) {
            logger.error("syncDataByNodenameFromFile error ({}). csvPath: {}, ex: {}", typeName, csvPath, e.getMessage());
            logger.error(e.getMessage(), e);
            throw new ApiException(CommonConstant.PROCCESS_REQUIRED, "syncBySystem error", e);
        } finally{
            logger.info("sync data finish ({}), csvPath:{} ", typeName, csvPath);
            
            try {
                if(buf != null)
                    buf.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if(executor != null)
                    executor.shutdown(); 
            } catch (Exception e) {
                e.printStackTrace();
            }
            
        }
    }
    
    private void syncDataByLocation(String data) throws Exception {
        //node,type,subscriber,col1_1,info
        String[] dataArray = data.split(",");
        if(dataArray != null && dataArray.length > 4){
            String location = dataArray[0];
            String msisdn = dataArray[2];
            String classObject = dataArray[3];
            String info = dataArray[4];
            
//            if(dataArray.length > 5){
//            	for(int i=5;i<dataArray.length;i++){
//            		info += "," + dataArray[i];
//            	}
//            }
            // modify restful data payload
            logger.info("Sent JSON String as payload");
            String jsonStr = restfulUtil.convertJSON(msisdn,classObject);
            logger.info("JSON String : "+jsonStr);
            if(SyncDataType.TYPE_2.getId().equals(dataArray[1])
                    || SyncDataType.TYPE_3.getId().equals(dataArray[1])){

            	subRestService.modifyEntry(location, msisdn, jsonStr);
            	logger.debug("location:"+location);
            	logger.debug("msisdn:"+msisdn);
            	logger.debug("info:"+jsonStr);
            }else if(SyncDataType.TYPE_1.getId().equals(dataArray[1])){
            	subRestService.deleteEntry(location, msisdn);
            }
            
            /*if(info.startsWith("\"")){
             
                info = info.substring(1,info.length()-1);
            }
            logger.debug("info:" + info);
            if(LdapConstant.EPC_SUBSCRIBER_LOWCASE.equalsIgnoreCase(classObject)){
                if(SyncDataType.TYPE_2.getId().equals(dataArray[1])
                        || SyncDataType.TYPE_3.getId().equals(dataArray[1])){
                	subRestService.modifyEntry(location, msisdn, info.split(CommonConstant.BR));
                }else if(SyncDataType.TYPE_1.getId().equals(dataArray[1])){
                	subRestService.deleteEntry(location, msisdn);
                }
            }else if(LdapConstant.EPC_SUBSCRIBERQUALIFICATION_LOWCASE.equalsIgnoreCase(classObject)){
                if(SyncDataType.TYPE_2.getId().equals(dataArray[1])
                        || SyncDataType.TYPE_3.getId().equals(dataArray[1])){
                	subRestService.modifyEntry(location, msisdn, info.split(CommonConstant.BR));
                }else if(SyncDataType.TYPE_1.getId().equals(dataArray[1])){
                	subRestService.deleteEntry(location, msisdn);
                }
            }*/
        }
    }
    
    private boolean syncDataByNodename(String data) throws Exception {
        try{
            // regroup data with , here
            executor.submit(new SyncRunnable(subRestService, restfulUtil,data));
            return true;
        }catch(Exception e){
            //throw new Exception("DeleteEntry " + msisdn + " fail. e:" + e);
            return false;
        }
    }

    private boolean isRecoverBySystem() throws Exception {
        String type = CoreDaoFactory.getInstance().getPcrfConsistentRestDao().findSyncType();

        //A:自動/M:手動
        if(type != null && "A".equals(type)){
            return true;
        }
        
        return false;
    }
    
    private void updateRecoveryResult(Long logId, boolean result){
        try {
            if(logId != null)
                CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao().updateSyncResult(logId, (result ? PcrfConsistenLog.SYNC_RESULT_2_SUCCESS : PcrfConsistenLog.SYNC_RESULT_1_FAIL));
        } catch (Exception e) {
            logger.error("updateRecoveryResult exception, ", e);
        }
    }

    private void updateRecoveryResultExecting(Long logId){
        try {
            if(logId != null)
                CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao().updateSyncResult(logId, PcrfConsistenLog.SYNC_RESULT_3_EXECTE);
        } catch (Exception e) {
            logger.error("updateRecoveryResult exception, ", e);
        }
    }
    
    private void saveRecoveryResult(String location, String nodename,
            Long user,String logsFilePath, boolean syncResult, long execTime) throws Exception {
        //find pcrf_info id
        //save log
        logger.info("save pcrf consisten log, nodename:{}, syncResult:{} ", location + "_" + nodename, syncResult);

        //3: 成功(不一致)
        int execResult = PcrfConsistenLog.EXEC_RESULT_3;
        
        long pcrfId = CoreDaoFactory.getInstance().getPcrfInfoRestDao().findIdByNodename(nodename);
        long logId = CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao().getNextPK();
        
        PcrfConsistenLog log = new PcrfConsistenLog();
        log.setId(logId);
        log.setPcrfId(pcrfId);
        log.setExecStartTime(new Timestamp(execTime));
        log.setExecEndTime(new Timestamp(System.currentTimeMillis()));
        log.setExecResult(execResult);
        log.setExecUser(CommonConstant.DEFAULT_USER);
        log.setRptPath(logsFilePath);
        log.setSyncResult((syncResult ? PcrfConsistenLog.SYNC_RESULT_2_SUCCESS : PcrfConsistenLog.SYNC_RESULT_1_FAIL));
        
        CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao().save(log);
    }

    private boolean isDBDataMoreThenPcrf(String location, String execTS) throws Exception {
        long subNum = CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao().findSubsriberCount(location, execTS);
        long dbnNum = CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao().findDBNCount();
        logger.debug("dbNum:{}, dbnNum:{}", subNum, dbnNum);
        return (subNum >= dbnNum);
    }
    
    private long saveExcutingLog(String nodename, long execTime, Long execUserId) throws Exception {
        //save log
        logger.info("save pcrf consisten log Excuting , nodename:{}", nodename);

        long logId = CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao().getNextPK();
        long pcrfId = CoreDaoFactory.getInstance().getPcrfInfoRestDao().findIdByNodename(nodename);
        
        PcrfConsistenLog log = new PcrfConsistenLog();
        log.setId(logId);
        log.setPcrfId(pcrfId);
        log.setExecStartTime(new Timestamp(execTime));
        log.setExecResult(PcrfConsistenLog.EXEC_RESULT_0);
        log.setExecUser(execUserId);
        CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao().save(log);
        
        return logId;
    }
    
    private void updateRecoveryResult(long logId,
            String location, String nodename, String logsFilePath) throws Exception {
        
        //find pcrf_info id
        //save log
        logger.info("save pcrf consisten log, nodename:{}, syncResult:{} ", location + "_" + nodename, syncResult);
        
        CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao().updateSyncResult(
                logId, new Timestamp(System.currentTimeMillis())
                , PcrfConsistenLog.EXEC_RESULT_3
                , logsFilePath
                , PcrfConsistenLog.SYNC_RESULT_0);
        
    }
    
    private void updateRecoveryResult(long logId,
            String location, String nodename, String logsFilePath, boolean syncResult) throws Exception {
        
        //find pcrf_info id
        //save log
        logger.info("save pcrf consisten log, nodename:{}, syncResult:{} ", location + "_" + nodename, syncResult);
        
        CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao().updateSyncResult(
                logId, new Timestamp(System.currentTimeMillis())
                , PcrfConsistenLog.EXEC_RESULT_3
                , logsFilePath
                , (syncResult ? PcrfConsistenLog.SYNC_RESULT_2_SUCCESS : PcrfConsistenLog.SYNC_RESULT_1_FAIL));
        
    }
    
    private boolean syncDataFromPcrfToDbSync(String data) {
        logger.info("data:" + data);
        if(data != null){
            String type = data.split(",")[1];
            String msisdn = data.split(",")[2];
            
            if(SyncDataType.TYPE_1.getId().equals(type)){
                //insert data
                /*
                    TP - 2L3PilotPCRF1,1,886944001001,epc-subscriber,""
                */
                try{
                    String groupIds = data.split(",")[4];
                    if(groupIds.startsWith("\"")){
                        groupIds = groupIds.substring(1,groupIds.length());
                    }
                    if(groupIds.lastIndexOf("\"") != -1){
                        groupIds = groupIds.substring(0,groupIds.lastIndexOf("\""));
                    }
                    
                    CoreDaoFactory.getInstance().getTransactionLogRestDao().createTxDataByFn(getLocationName(), msisdn, groupIds);
                    logger.info("insert TransactionLog sucssful , msisdn: {}, type:{} ", msisdn, type);
                } catch(Exception e) {
                    logger.error("insert TransactionLog fail , msisdn: {}", msisdn);
                    logger.error(e.getMessage(), e);
                }
                
                try{
                    Thread.sleep(100);
                }catch(Exception e){
                    
                }
            }else if(SyncDataType.TYPE_2.getId().equals(type)){
                //delete data
                /*
                    TP - 2L3PilotPCRF1,2,886505050520,epc-subscriber,"objectclass: epc-subscriber
                    EPC-SubscriberId: 886505050520
                    permissions: 15
                    groupId: 4003
                    ownerId: 0
                    shareTree: nodeName=PGNode"
                 */
                //CoreDaoFactory.getInstance().getTransactionLogDao().save(location, vo, appendKeySQL, appendValSQL, updateVal);
                String objectClass1 = "EPC-SubscriberId";
                String objectClass2 = "EPC-SubscriberQualification";
                String dn1 = "";
                //String dn1 = "EPC-SubscriberId="+msisdn+",EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=PGNode";
                String dn2 = "EPC-Name=EPC-SubscriberQualification,EPC-SubscriberId="+msisdn+",EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=PGNode";
                String reqParam = "";
                
                saveTransaction(getLocationName(), TransactionLog.OPERATION_DELETE_3, msisdn, objectClass1, dn1, reqParam);
                //saveTransaction(getLocationName(), TransactionLog.OPERATION_DELETE_3, msisdn, objectClass2, dn2, reqParam);
                try{
                    Thread.sleep(100);
                }catch(Exception e){
                    
                }
            }else if(SyncDataType.TYPE_3.getId().equals(type)){
                //modify data
                /*
                    TP - 2L3PilotPCRF1,3,886923988216,epc-subscriber,"objectclass: epc-subscriber
                    EPC-SubscriberId: 886923988216
                    permissions: 15
                    groupId: 4003
                    ownerId: 0
                    shareTree: nodeName=PGNode","4G_volte_vowifi
                    "
                */
                try{
                    String groupIds = data.split(",")[5];
                    if(groupIds.startsWith("\"")){
                        groupIds = groupIds.substring(1,groupIds.length());
                    }
                    if(groupIds.lastIndexOf("\"") != -1){
                        groupIds = groupIds.substring(0,groupIds.lastIndexOf("\""));
                    }
                    
                    CoreDaoFactory.getInstance().getTransactionLogRestDao().createTxDataByFn(getLocationName(), msisdn, groupIds);
                    logger.info("insert TransactionLog sucssful , msisdn: {}, type:{} ", msisdn, type);
                } catch(Exception e) {
                    logger.error("insert TransactionLog fail , msisdn: {}", msisdn);
                    logger.error(e.getMessage(), e);
                }
                
                try{
                    Thread.sleep(100);
                }catch(Exception e){
                    
                }
                
            }
        }
        return false;
    }
    
    public void saveTransaction(String location, String type, String msisdn, 
            String objectClass, String dn, String reqParam){
        TransactionLog log = new TransactionLog();
        try{
            log.setLocation(location);
            log.setMsisdn(msisdn); 
            log.setInterFace(CommonConstant.MANUL_RECOVERY_INTERFACE); 
            log.setOperation(type); 
            log.setRequestDn(dn); 
            log.setRequest(reqParam); 
            log.setResponse(CommonConstant.SUCCESS); 
            log.setStatus(0);
            log.setCreateTime(new Timestamp(System.currentTimeMillis()));
            
            //other params
            StringBuffer appendKeySQL = new StringBuffer("");
            StringBuffer appendValSQL = new StringBuffer("");
            List<String> appendVal = new LinkedList<String>();
            /*
            Map<String, ArrayList<String>> reqMap = notifyReq.getReqParamMap();
            if(reqMap != null){
                Map<String, String> colTypeMap = SystemCodeConfig.getInstance().getColTypeMap().get(objectClass);
                if(colTypeMap == null){
                    colTypeMap = new HashMap<String, String>();
                }
                
                StringBuffer otherVal = new StringBuffer();
                for(Entry<String, ArrayList<String>> entry : reqMap.entrySet()){
                    String key = SystemCodeConfig.getInstance().getOtherInfoCol();
                    if(colTypeMap.get(entry.getKey()) != null){
                        key = colTypeMap.get(entry.getKey());
                    }
                    
                    if(SystemCodeConfig.getInstance().getOtherInfoCol().equals(key)){
                        for(String val : entry.getValue()){
                            otherVal.append(entry.getKey() + LdapConvertUtil.LDAP_DATA_SET_STR + val + LdapConvertUtil.SPLIT_STR);
                        }
                    }else{
                        if(entry.getValue().size() == 0){
                            appendVal.add(entry.getValue().get(0));
                            appendKeySQL.append(", ").append(key+"_1");
                            appendValSQL.append(", ? ");
                        }else{
                            int i=1;
                            for(String val : entry.getValue()){
                                appendVal.add(val);
                                appendKeySQL.append(", ").append(key+"_"+i);
                                appendValSQL.append(", ? ");
                                i++;
                            }
                        }
                    }
                }
                
                if(otherVal.length() > 0){
                    appendVal.add(otherVal.toString());
                    appendKeySQL.append(", ").append(SystemCodeConfig.getInstance().getOtherInfoCol() + "_1");
                    appendValSQL.append(", ? ");
                }
                
            }*/
            
            CoreDaoFactory.getInstance().getTransactionLogRestDao().save(location, log, appendKeySQL, appendValSQL, appendVal);
            logger.info("insert TransactionLog sucssful , msisdn: {}, type:{} ", msisdn, type);
        } catch(Exception e) {
            logger.error("insert TransactionLog fail , msisdn: {}", msisdn);
            logger.error(e.getMessage(), e);
        }
    }
    
	public SubRestService getSubRestService() {
		return subRestService;
	}

	public void setSubRestService(SubRestService subRestService) {
		this.subRestService = subRestService;
	}

	public int getThreadPoolNum() {
		return threadPoolNum;
	}
	
	public void setThreadPoolNum(int threadPoolNum) {
		this.threadPoolNum = threadPoolNum;
	}
    
	public String getExportTxLogsPath() {
        exportTxLogsPath = exportTxLogsPath.replace("{date}", DateUtil.formatDate(now, "yyyyMMddHHmmss"));
        FileUtil.mkDir(exportTxLogsPath);
        return exportTxLogsPath;
    }

    public void setExportTxLogsPath(String exportTxLogsPath) {
        this.exportTxLogsPath = exportTxLogsPath;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public RestfulUtil getRestfulUtil() {
        return restfulUtil;
    }

    public void setRestfulUtil(RestfulUtil restfulUtil) {
        this.restfulUtil = restfulUtil;
    }
}
