package com.cht.pcrf.api.util;

import com.cht.pcrf.api.bean.SubDataPlan;
import com.cht.pcrf.api.bean.SubRestInfo;
import com.cht.pcrf.core.model.Subscriber;
import com.cht.pcrf.core.service.CoreDaoFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@Component
public class RestfulUtil {
    @Value("${location.name}")
    private String location;
    public String convertJSON(String msisdn,String objClass) throws JsonProcessingException {
        List<Subscriber> resultSet = CoreDaoFactory.getInstance().getSubscriberRestDao().findByMsisdnAndObjectClass(msisdn,objClass.toLowerCase(),location);
        Subscriber subInfo = null;
        SubRestInfo restInfo = new SubRestInfo();
        HashMap<String,Object> map = new HashMap<>();
        map.put("onlineChargingSystemProfileId","ChtOcs");
        // set msisdn
        restInfo.setSubscriberId(msisdn);
        // setting Qualification Data
        restInfo.setStaticQualification(map);
        ObjectMapper om = new ObjectMapper();

        if(resultSet != null && resultSet.size()>0){
            subInfo = resultSet.get(0);
            // setting DataPlans --> if policyID is 5G_* set this fields
            setDataPlan(restInfo,subInfo.getCol11());
        }
        // returned data payload
        String result = "";
        result = om.writeValueAsString(restInfo);

        return result;

    }
    // set data plan here
    private void setDataPlan(SubRestInfo restInfo,String col11_1){
        String dtaplan = "";
        // if JSON not contain policyIds use col11_1 instead
        dtaplan = StringUtils.trimToEmpty(col11_1);
        // 2022 01 11 -> 5G_* -> set dataplan
        if(dtaplan.contains("5G_")){
            List<SubDataPlan> plans = restInfo.getDataplans();
            SubDataPlan plan = new SubDataPlan();
            plan.setDataplanName(dtaplan);
            plans.add(plan);
        }
    }
}
