/**
 * Project Name : PCRF-SYNC-WEB
 * File Name 	: CommonUtil.java
 * Package Name : com.cht.pcrf.utils
 * Date 		: 2017年5月12日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

@Component
public class CommonUtil {

    private static final Logger logger = LoggerFactory.getLogger(CommonUtil.class);

    private static final String OS_NAME_WIN = "Windows";
    @Value("${os.name}")
    private String osName;

    public void invokeShell(String shellPath,boolean isShowLog , String... params) throws Exception {
        
        Process proc = null;
        long startTime = System.currentTimeMillis();
        try {
            logger.info("----- invokeShell Started -----");
            Runtime rt = Runtime.getRuntime();
            String command = shellPath;
            if(params != null){
                for(String param : params){
                    command += " " + param;
                }
            }
            logger.info("shell cmd : "+ command);
            
            
            if (OS_NAME_WIN.equalsIgnoreCase(osName)) {
                String[] stringCommandWindows = { "cmd", "/C", command };
                proc = rt.exec(stringCommandWindows);
            } else {
                String[] stringCommandLinux = { "bash", "-c", command };
                proc = rt.exec(stringCommandLinux);
            }

            String line = null;
            /*
            InputStream stderr = proc.getErrorStream ();
            InputStreamReader esr = new InputStreamReader (stderr);
            BufferedReader ebr = new BufferedReader (esr);
            logger.info("<info>");
            while ( (line = ebr.readLine ()) != null)
                logger.info(line);
            logger.info("</info>");
            */
             
            InputStream stdout = proc.getInputStream ();
            InputStreamReader osr = new InputStreamReader (stdout,"UTF-8");
            BufferedReader obr = new BufferedReader (osr);
            //StringBuffer outSB = new StringBuffer();
            logger.info("<output>");
            while ( (line = obr.readLine ()) != null){
                //outSB.append(line+Constants.OS_BR);
                if(isShowLog)
                    logger.info(line);
            }
            logger.info ("</output>");
            
            int exitVal = proc.waitFor ();
            logger.info ("Process exitValue: " + exitVal);
                
            if(exitVal != 0) throw new Exception("invokeShell failed, e:" + line);
        } catch (Exception e) {
            logger.error("invokeShell failed, e:", e);
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            if(proc != null)
                proc.destroy();
            logger.info("invokeShell execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
}
