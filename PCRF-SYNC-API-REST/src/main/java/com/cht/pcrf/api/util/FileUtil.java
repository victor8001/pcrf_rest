/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: FileUtil.java
 * Package Name : com.cht.pcrf.consistent.util
 * Date 		: 2017年4月27日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.api.common.CommonConstant;

public class FileUtil {
    private final static Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);
    
    public static void mkDir(String writeFilePath){

        File folder = new File(writeFilePath);
        if (!folder.isFile())
            folder.mkdirs();
    }
    
    public static String writerFile(String data, String fileName, String writeFilePath)
    {
        BufferedWriter fileWriter = null;
        File folder = null;
        File file = null;
        String fileLocation = writeFilePath + fileName;
        
        try
        {
            //logger.info("CSV File: " + writeFilePath + fileName);
            
            folder = new File(writeFilePath);
            if (!folder.isFile())
                folder.mkdirs();
            
            file = new File(writeFilePath + fileName);
            //fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "UTF-8"));
            fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "Big5"));
            fileWriter.append(data);
            fileWriter.append(System.getProperty("line.separator"));
            fileWriter.flush();
            
        } catch (Exception e) {
            logger.error("writerFile exception!", e);
        } finally {
            if (fileWriter != null) 
            {
                try
                {
                    fileWriter.close();
                    
                } catch (Exception e) {
                    logger.error("fileWriter.close() exception!", e);
                }
            }
        }
        
        return fileLocation;
    }
    
    
    public static void writerEx(String failLog, Exception failEx) {
        PrintStream ps = null;
        try {
            File file = new File(failLog);
            ps = new PrintStream(file);
            failEx.printStackTrace(ps);
            // something
        } catch (Exception ex) {
            logger.info("writerEx exception! e:", ex);
        } finally{
            if(ps != null)
                ps.close();
        }
    }
    
}
//end of class