package com.cht.pcrf.api.util;

import com.cht.pcrf.api.exception.ApiException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class CurlUtil {

    private static Logger logger = LoggerFactory.getLogger(CurlUtil.class);
    @Value("${provision.shellPath}")
    private String filepath;
    @Value("${sapc.rest.url.protocol}")
    private String URL_PROTOCOL;
    @Value("${sapc.rest.sub.put.url}")
    private String SUB_PUT_URL;
    @Value("${sapc.rest.sub.del.url}")
    private String SUB_DEL_URL;
    @Value("${sapc.rest.sub.get.url}")
    private String SUB_GET_URL;
    @Autowired
    private CommonUtil commonUtil;

    public  boolean createAndModify(String username,String password,String dataStr, String url, String msisdn) throws Exception {
        boolean result = false;
        if(StringUtils.isBlank(filepath)){
            throw new ApiException("500","filePath cannot be empty! ");
        }
        url = URL_PROTOCOL + "://" + url + SUB_PUT_URL ;
        url = url.substring(0,url.length()-5);
        logger.info("Request URL: {}",url);
        logger.info("Msisdn: {}",msisdn);
        logger.info("JSON Data: {}",dataStr);

        logger.info("Shell Path: " + filepath);
        commonUtil.invokeShell(filepath+"restful_api_put.sh",true,"\""+msisdn+"\"","\""+username+"\""
                ,"\""+password+"\"","\""+url+"\"","'"+dataStr+"'");
        result = true;
        return result;
    }

    public  boolean deleteMsisdn(String username,String password, String url, String msisdn) throws Exception {
        boolean result = false;
//        String filepath = SystemUtility.getConfig("provision.shellPath","");
        if(StringUtils.isBlank(filepath)){
            throw new ApiException("500","filePath cannot be empty! ");
        }
        url = URL_PROTOCOL + "://" + url + SUB_DEL_URL ;
        url = url.substring(0,url.length()-5);
        logger.info("Request URL: {}",url);
        logger.info("Msisdn: {}",msisdn);

        logger.info("Shell Path: " + filepath);
        commonUtil.invokeShell(filepath+"restful_api_del.sh",true,"\""+msisdn+"\""
                ,"\""+username+"\"","\""+password+"\"","\""+url+"\"");
            result = true;
        return result;

    }

    public  boolean checkMsisdn(String username,String password, String url, String msisdn) throws Exception {
        boolean result = false;
//        String filepath = SystemUtility.getConfig("provision.shellPath","");
        if(StringUtils.isBlank(filepath)){
            throw new ApiException("500","filePath cannot be empty! ");
        }
        url = URL_PROTOCOL + "://" + url + SUB_GET_URL ;
        url = url.substring(0,url.length()-5);
        logger.info("Request URL: {}",url);
        logger.info("Msisdn: {}",msisdn);

        logger.info("Shell Path: " + filepath);
        commonUtil.invokeShell(filepath+"restful_api_get.sh",true,"\""+msisdn+"\""
                ,"\""+username+"\"","\""+password+"\"","\""+url+"\"");
        result = true;
        return result;

    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }
}
