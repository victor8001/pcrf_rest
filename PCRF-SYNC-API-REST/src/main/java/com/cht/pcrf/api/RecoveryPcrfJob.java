/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: RecoveryPcrfJob.java
 * Package Name : com.cht.pcrf.api
 * Date 		: 2017年5月14日 
 * Author 		: LauraChu
 * Description  : zone reload Recovery (DB Sync Data Recovery to PCRF) 
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.api.common.CommonConstant;
import com.cht.pcrf.api.service.SyncRestService;

public class RecoveryPcrfJob {
    protected static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);
    
    public static void main(String[] args){
        RecoveryPcrfJob jobExecute = new RecoveryPcrfJob();
        jobExecute.execute(args);
    }
    
    /**
     * args[0] : nodename
     * args[1] : pcrf_name
     * args[2] : 開始時間 ("2017-04-21 13:30:55")
     * @param args
     */
    private void execute(String[] args) {

        final long sTime = System.currentTimeMillis();
        try {
            logger.info("====== RecoveryPcrfJob start execute ======");

            logger.info("nodename: " + args[0]);
            logger.info("pcrfname: " + args[1]);
            logger.info("startTimes: " + args[2]);
            
            SyncRestService syncRestService = JobExecute.getInstance().getSyncRestService();
            syncRestService.syncByTransationTime(args[0], args[1], CommonConstant.DEFAULT_USER, args[2]);

        } catch (Exception e) {
            logger.error("RecoveryPcrfJob exception, e:", e);
            logger.error(e.getMessage(), e);
        } finally{
            logger.info("====== RecoveryPcrfJob Execute finished, spend time: "
                    + (System.currentTimeMillis() - sTime) / 1000
                    + " seconds ======");
        }
    }
}
