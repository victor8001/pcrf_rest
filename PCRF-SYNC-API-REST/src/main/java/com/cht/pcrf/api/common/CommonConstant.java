/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: SoapMessageCode.java
 * Package Name : com.cht.pcrf.api.common
 * Date 		: 2016年8月10日 
 * Author 		: LauraChu
 * Copyright (c) 2016 All Rights Reserved.
 */
package com.cht.pcrf.api.common;

import java.util.ArrayList;
import java.util.List;

public class CommonConstant {

    public static final String PCRF_SYNC_API = "PCRF-SYNC-API-REST";
    public static final String SUCCESS = "0000";
    public static final String PROCCESS_REQUIRED = "9999";
    public static final String SUCCESS_MSG = "Successful";
    public static final long DEFAULT_USER = -1L;
    public static final long DEFAULT_PCRF = -1L;
    public static final String MANUL_RECOVERY_INTERFACE = "9";
    
    public static List<String> locations = new ArrayList<String>();
    
    public static final String BR = "\n";
    public static final String COMMON = ",";
    
    static {
        locations.add("TP");
        locations.add("KH");
    }
}
