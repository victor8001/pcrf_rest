package com.cht.pcrf.api.util;

import com.cht.pcrf.api.common.Constants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;


public class SysParameterPropsReader implements Serializable{

    private static final long serialVersionUID = 1706402197704449403L;

    private static Logger logger = LoggerFactory.getLogger(SysParameterPropsReader.class);
    
    private static final String CONFIG_PROPERTIES = "config.properties";

	private static final String CONFIG_PROPERTIES_PATH = Constants.SERVER_NAME + ".config.properties.path";
	
    private static Properties props;
    
    private static long lastModified = 0L;
    
    
    static{
        String filePath = getPropertiesFile(); 
        logger.info("propPath:" + filePath);

        if(!"".equals(StringUtils.trimToEmpty(filePath))){
            loadProperties(filePath);
        }else{
            ClassLoader loader = SysParameterPropsReader.class.getClassLoader();
            try(InputStream resourceStream = loader.getResourceAsStream(CONFIG_PROPERTIES)){
                props = new Properties();
                props.load(resourceStream);
            } catch (IOException e) {
                logger.error("Load properties file:{}, error!", CONFIG_PROPERTIES, e);
            }
        }
        
    }
    
    /**
     * 取得config資料
     * @param key
     * @return
     */
    public static String getConfig(String key) {
    	
        /*
        String filePath = getPropertiesFile(); 
        logger.info("propPath:" + filePath);

        if(!"".equals(StringUtils.trimToEmpty(filePath))){
            loadProperties(filePath);
        }else{
            ClassLoader loader = SysParameterPropsReader.class.getClassLoader();
            try(InputStream resourceStream = loader.getResourceAsStream(CONFIG_PROPERTIES)){
                props = new Properties();
                props.load(resourceStream);
            } catch (IOException e) {
                logger.error("Load properties file:{}, error!", CONFIG_PROPERTIES, e);
            }
        }
    	*/
        return props.getProperty(key);
    }
    
    
    /**
     * 載入Properties檔
     * @param filePath
     */
    private static void loadProperties(String filePath) {
    	//區分大小寫
        try {
        	if(chkFile(filePath)){
        		props = new Properties();
        		props.load(new FileInputStream(filePath));

    			lastModified = new File(filePath).lastModified();
        	}
        } catch (FileNotFoundException e) {
             e.printStackTrace();
             logger.error(e.getMessage(),e);
        } catch (IOException e) {
             e.printStackTrace();
             logger.error(e.getMessage(),e);
        }
   }
    
    /**
     * 載入Properties檔的路徑 (從環境變數取得)
     * @return
     */
    private static String getPropertiesFile() {
    	Properties propertes = System.getProperties();
		return propertes.getProperty(CONFIG_PROPERTIES_PATH);
	}

    /**
     * 檢查Properties檔
     * @param filePath
     * @return
     */
	private static boolean chkFile(String filePath){

    	File file = new File(filePath);
    	if (file.isFile()) {
    		if(file.lastModified() > lastModified){
    			return true;
    		}
    	}
    	return false;
    }
	
}
