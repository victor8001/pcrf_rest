/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: LdapService.java
 * Package Name : com.cht.pcrf.api.service
 * Date 		: 2017年4月25日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api.service.ldap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.api.common.CommonConstant;
import com.cht.pcrf.core.common.LdapConstant;
import com.cht.pcrf.core.dao.PcrfInfoDaoImpl;
import com.cht.pcrf.core.model.PcrfInfo;
import com.cht.pcrf.core.service.CoreDaoFactory;

public class LdapServiceBak {
    protected final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);
    protected final int THREAD_SLEEP_MS = 200;
    
//    private LdapService instance = null;
    private Map<String, List<PcrfInfo>> sapcInfoMapByLC = null;
    private Map<String, List<PcrfInfo>> sapcInfoMapByNode = null;
    
    public LdapServiceBak() throws Exception{
        setPcrfInfoMapByLC();
        
    }
//    public LdapService getInstance() throws Exception{
//        if(instance == null){
//            instance = new LdapService();
//            sapcInfoMap = getPcrfInfoMap();
//        }
//        
//        return instance;
//    }
    
    protected Map<String, List<PcrfInfo>> getPcrfInfoMapByLC() throws Exception {
        return sapcInfoMapByLC;
    }
    
    private void setPcrfInfoMapByLC() throws Exception {
        if(sapcInfoMapByLC == null){
            this.sapcInfoMapByLC = CoreDaoFactory.getInstance().getPcrfInfoDao().getPcrfInfoMap();
            setPcrfInfoMapByNode();
        }
    }
    

    protected List<PcrfInfo> getPcrfInfoByLocation(String location) throws Exception {
        return getPcrfInfoMapByLC().get(location);
    }

    private void setPcrfInfoMapByNode() throws Exception {
        Map<String, List<PcrfInfo>> tmpMap = new HashMap<String, List<PcrfInfo>>();
        getPcrfInfoMapByLC();
        for(String location : sapcInfoMapByLC.keySet()){
            for(PcrfInfo sapcInfo : sapcInfoMapByLC.get(location)){
                String key = location + " - " + sapcInfo.getNodeName();
                if(tmpMap.get(key) == null){
                    tmpMap.put(key, new ArrayList<PcrfInfo>());
                }
                tmpMap.get(key).add(sapcInfo);
            }
        }
        this.sapcInfoMapByNode = tmpMap;
    }
    
    protected List<PcrfInfo> getPcrfInfoByNode(String location, String nodename) throws Exception {
        //String key = location + " - " + sapcInfo.getNodeName();
        return getPcrfInfoMapByNode().get(location + " - " + nodename);
    }
    
    public Map<String, List<PcrfInfo>> getPcrfInfoMapByNode() throws Exception {
        return sapcInfoMapByNode;
    }

//    protected Properties createLdapEnv(String url, String username, String password){
//        Properties env = new Properties();
//        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
//        env.put(Context.PROVIDER_URL, url);
//        env.put(Context.SECURITY_PRINCIPAL, username);
//        env.put(Context.SECURITY_CREDENTIALS, password);
//        return env;
//    }

    protected void deleteEntry(Hashtable ldapEnv, String entryDN) throws Exception {
        DirContext ctx = null;
        try {
            logger.info("deleteEntry: deleted entry start, dn: " + entryDN);

            ctx = new InitialDirContext(ldapEnv);
            
            ctx.destroySubcontext(entryDN);

            logger.info("deleteEntry: deleted entry successful, dn:" + entryDN);
        } catch (NamingException e) {
            if(e.getMessage().startsWith(LdapConstant.SAPC_MSG_CODE_32)){
                //[LDAP: error code 32 - Object not found.];
                logger.info("deleteEntry: deleted entry not found skip, dn:" + entryDN);
            }else{
                logger.error("deleteEntry: deleted entry fail, dn:"+entryDN+ ", e:"+e);
                logger.error(e.getMessage(), e);
                throw e;
            }
        } catch (Exception e) {
            logger.error("deleteEntry: deleted entry fail, dn:"+entryDN+ ", e:"+e);
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            try{
                if(ctx != null)
                    ctx.close();
                
                Thread.sleep(THREAD_SLEEP_MS);
            }catch(Exception e){
                logger.error("Close DirContext error, e:"+e);
            }
        }
    }
    
    protected void createOrModifyEntry(Hashtable ldapEnv, String nodeName, String entryDN, String[] infos, boolean isCreate) throws Exception {
        DirContext ctx = null;
        String type = isCreate ? "create" : "Modify";
        try {
            logger.info("createEntry: entry start ("+type+"), dn: " + entryDN);

            ctx = new InitialDirContext(ldapEnv);
            
            Map<String,Attribute> attMap = new HashMap<String,Attribute>();
            Attributes attrs = new BasicAttributes(true); // case-ignore
            for(String info : infos){
                /*
                String[] kv = info.split(": ");
                logger.info("createEntry info:"+ info+","+kv[0]+","+kv[1]);
                
                if (attMap.get(kv[0]) == null) {
                    attMap.put(kv[0], new BasicAttribute(kv[0]));
                }
                if ("sharetree".equalsIgnoreCase(kv[0])) {
                    ((Attribute) attMap.get(kv[0])).add("nodeName=" + nodeName);
                } else {
                    ((Attribute) attMap.get(kv[0])).add(kv[1]);
                }
                */
                
                if(info == null || info.trim().length() == 0) continue;

                String[] kv = info.split(": ");
                if(kv.length < 2){
                    logger.debug("createEntry info:"+ info);
                    continue;
                }
                logger.debug("createEntry info:"+ info+","+kv[0]+","+kv[1]);
                
                String key = StringUtils.trimToEmpty(kv[0]);
                String value = StringUtils.trimToEmpty(kv[1]);

                if(attMap.get(key) == null){
                    attMap.put(key, new BasicAttribute(key));
                }
                
                if(LdapConstant.SHARE_TREE_LOWCASE.equalsIgnoreCase(key)){
                    //nodeName=2L3PilotPCRF1
                    attMap.get(key).add("nodeName="+nodeName);
                }else{
                    attMap.get(key).add(value);
                }
            }
            
            for(Attribute att : attMap.values()){
                attrs.put(att);
            }
            
//            Attribute o = new BasicAttribute("o");
//            o.add("test");
//            attributes.put(o);

            if(isCreate){
                ctx.createSubcontext(entryDN, attrs);
            }else{
                ctx.modifyAttributes(entryDN, DirContext.REPLACE_ATTRIBUTE, attrs);
            }
            
            logger.info("createEntry: entry successful ("+type+"), dn: " + entryDN);
        } catch (NamingException e) {
            logger.error("createEntry: entry fail ("+type+"), dn:"+entryDN+ ", e:"+e);
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error("createEntry: entry fail ("+type+"), dn:"+entryDN+ ", e:"+e);
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            try{
                if(ctx != null)
                    ctx.close();
                
                Thread.sleep(THREAD_SLEEP_MS);
            }catch(Exception e){
                logger.error("Close DirContext error, e:"+e);
            }
        }
    }
    
    protected void removeAttribute(Hashtable ldapEnv, String nodeName, String entryDN, String[] infos) throws Exception {
        DirContext ctx = null;
        try {
            logger.info("removeAttribute: entry start , dn: " + entryDN);

            ctx = new InitialDirContext(ldapEnv);
            
            Map<String,Attribute> attMap = new HashMap<String,Attribute>();
            Attributes attrs = new BasicAttributes(true); // case-ignore
            for(String key : infos){
                if(key == null || key.trim().length() == 0) continue;
                logger.debug("removeAttribute key:"+ key);
                
                attMap.put(key, new BasicAttribute(key));
            }
            
            for(Attribute att : attMap.values()){
                attrs.put(att);
            }
            
            ctx.modifyAttributes(entryDN, DirContext.REMOVE_ATTRIBUTE, attrs);
            
            logger.info("removeAttribute entry successful , dn: " + entryDN);
        } catch (NamingException e) {
            logger.error("removeAttribute entry fail , dn:"+entryDN+ ", e:"+e);
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error("removeAttribute entry fail , dn:"+entryDN+ ", e:"+e);
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            try{
                if(ctx != null)
                    ctx.close();
                
                Thread.sleep(THREAD_SLEEP_MS);
            }catch(Exception e){
                logger.error("Close DirContext error, e:"+e);
            }
        }
    }
    
    protected boolean searchEntry(Hashtable ldapEnv, String entryDN) throws Exception {
        boolean result = false;
        DirContext ctx = null;
        NamingEnumeration<SearchResult> results = null;
        try {
            logger.info("searchEntry: search entry start : " + entryDN);

            ctx = new InitialDirContext(ldapEnv);
            
            String searchFilter = "(objectclass=*)";

            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(SearchControls.OBJECT_SCOPE);//base

            results = ctx.search(entryDN, searchFilter, searchControls);

            if(results.hasMoreElements()) {
                result = true;
            }

            logger.info("searchEntry: search entry successful, dn: " + entryDN);
            return result;
        } catch (NamingException e) {
            if(e.getMessage().startsWith(LdapConstant.SAPC_MSG_CODE_32)){
                //[LDAP: error code 32 - Object not found.];
                logger.error("searchEntry: search entry not found, dn:"+entryDN);
                return false;
            }else{
                logger.error("searchEntry: search entry fail, dn:"+entryDN+ ", e:"+e);
                logger.error(e.getMessage(), e);
                throw e;
            }
        } catch (Exception e) {
            logger.error("searchEntry: search entry fail, dn:"+entryDN+ ", e:"+e);
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            try{
                if(results != null)
                results.close();
            }catch(Exception e){
                logger.error("Close search error, e:"+e);
            }
            try{
                if(ctx != null)
                    ctx.close();
                
                Thread.sleep(THREAD_SLEEP_MS);
            }catch(Exception e){
                logger.error("Close DirContext error, e:"+e);
            }
        }
    }

}
