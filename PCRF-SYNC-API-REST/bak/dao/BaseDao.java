package com.cht.pcrf.api.dao;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.dbcp2.BasicDataSource;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.cht.pcrf.api.common.CommonConstant;

public class BaseDao extends JdbcDaoSupport {
    protected final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);

    protected void copyToFile(
            final String filePath, final String tableOrQuery, final String spiltChar)   
            throws Exception {  
          
        FileOutputStream fileOutputStream = null;  
        Connection conn = null;
        try { 
            String url = ((BasicDataSource) getJdbcTemplate().getDataSource()).getUrl() 
                    + "?user="+ ((BasicDataSource) getJdbcTemplate().getDataSource()).getUsername()
                    + "&password=" + ((BasicDataSource) getJdbcTemplate().getDataSource()).getPassword();
            
            conn = DriverManager.getConnection(url);
            
            CopyManager copyManager = new CopyManager((BaseConnection) conn);  
            fileOutputStream = new FileOutputStream(filePath);  
            copyManager.copyOut("COPY " + tableOrQuery + " TO STDOUT WITH CSV DELIMITER '"+spiltChar+"' ", fileOutputStream);  
        }catch(Exception e){
            logger.error(e.getMessage(), e);
            throw new Exception("PostgreManager copyToFile: " + filePath 
                    + " failed,error: {}" + e.getMessage());
        } finally {  
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            
            if (fileOutputStream != null) {  
                try {  
                    fileOutputStream.close();  
                } catch (IOException e) {  
                    e.printStackTrace();  
                }  
            }  
        }  
    }
    

    protected void copyFromFile(
            final String filePath, final String tableName, final String spiltChar)   
            throws Exception {  
          
        FileInputStream fileInputStream = null;  
        Connection conn = null;
        try {  
            String url = ((BasicDataSource) getJdbcTemplate().getDataSource()).getUrl() 
                    + "?user="+ ((BasicDataSource) getJdbcTemplate().getDataSource()).getUsername()
                    + "&password=" + ((BasicDataSource) getJdbcTemplate().getDataSource()).getPassword();
            
            conn = DriverManager.getConnection(url);
            
            CopyManager copyManager = new CopyManager((BaseConnection)conn);  
            fileInputStream = new FileInputStream(filePath);  
            copyManager.copyIn("COPY " + tableName + " FROM STDIN WITH CSV DELIMITER '"+spiltChar+"' ", fileInputStream);  
        } catch(Exception e){
            logger.error(e.getMessage(), e);
            throw new Exception("PostgreManager copyFromFile: " + filePath 
                    + " failed,error: {}" + e.getMessage());
        } finally {  
            if (conn != null)
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            
            if (fileInputStream != null) {  
                try {  
                    fileInputStream.close();  
                } catch (IOException e) {  
                    e.printStackTrace();  
                }  
            }  
        }  
    }

}
