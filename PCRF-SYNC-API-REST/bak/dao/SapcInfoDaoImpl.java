package com.cht.pcrf.api.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.cht.pcrf.api.common.CommonConstant;
import com.cht.pcrf.api.model.SapcInfo;

public class SapcInfoDaoImpl extends JdbcDaoSupport {
    protected final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);

    public List<SapcInfo> getSapcInfoList(String location) throws Exception {

        List<SapcInfo> result = new ArrayList();
        long startTime = System.currentTimeMillis();
        
        try{
            //TODO
            logger.info(" getSapcInfoList start ");
            
//            OracleManagement.getConnection();
            
//            SapcInfo sapcInfo = OracleManagement.selectSapcInfo(nodeName);

        }catch(Exception e){
            logger.error("getSapcInfoList exception!", e);
            throw e;
        }finally{
            logger.info("getSapcInfoList execute time:"+(System.currentTimeMillis()-startTime));
//            OracleManagement.closeConnection();
        }

        return result;
    }
    
    public Map<String, List<SapcInfo>> getSapcInfoMap() throws Exception {

        long startTime = System.currentTimeMillis();
        Map<String, List<SapcInfo>> sapcMap = new HashMap();
        try{
            logger.info(" getSapcInfoMap start ");

            List<SapcInfo> result = getSapcInfoList();
            
            for(SapcInfo sapcInfo : result){
                String key = sapcInfo.getLocation();
                if(sapcMap.get(key) == null){
                    sapcMap.put(key, new ArrayList());
                }
                sapcMap.get(key).add(sapcInfo);
            }

        }catch(Exception e){
            logger.error("getSapcInfoMap exception!", e);
            throw e;
        }finally{
            logger.info("getSapcInfoMap execute time:"+(System.currentTimeMillis()-startTime));
//            OracleManagement.closeConnection();
        }

        return sapcMap;
    }

    public List<SapcInfo> getSapcInfoList() throws Exception{
        long startTime = System.currentTimeMillis();
        
        try{
            logger.info(" getSapcInfoList start ");
            
            final String sql = "SELECT LOCATION, NODE_NAME, NODE_HOST, NODE_PORT"
                + ", LOGIN_ID, LOGIN_PW FROM PCRF_INFO WHERE STATUS = 1 "; 

            return convertToObj(getJdbcTemplate().queryForList(sql));

        }catch(Exception e){
            logger.error("getSapcInfoList exception!", e);
            throw e;
        }finally{
            logger.info("getSapcInfoList execute time:"+(System.currentTimeMillis()-startTime));
        }

    }

    private List<SapcInfo> convertToObj(List<Map<String, Object>> datas) {
        List<SapcInfo> result = new ArrayList();
        SapcInfo model = null;
        if(datas == null) return result;
        
        for(Map<String, Object> rs : datas){
            model = new SapcInfo();
            model.setLocation((String) rs.get("LOCATION"));
            model.setNodeName((String) rs.get("NODE_NAME"));
            model.setNodeHost((String) rs.get("NODE_HOST"));
            model.setNodePort((String) rs.get("NODE_PORT"));
            model.setLoginId((String) rs.get("LOGIN_ID"));
            model.setLoginPw((String) rs.get("LOGIN_PW"));
            
            model.setLdapEnv();
            result.add(model);
        }
        return result;
    }
}
