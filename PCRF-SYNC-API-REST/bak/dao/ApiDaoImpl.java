package com.cht.pcrf.api.dao;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.pcrf.api.common.CommonConstant;
import com.cht.pcrf.api.common.SyncDataType;
import com.cht.pcrf.api.model.PcrfConsistenLog;

public class ApiDaoImpl extends BaseDao {
    protected final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_API);

//    public long getNextPK() throws Exception {
//         
//        long startTime = System.currentTimeMillis();
//        
//        try{
//            logger.info("getNextPK start ");
//            
//            final String sql = 
//                    " SELECT  nextval('pcrf_consisten_log_id_seq') " ;
//            
//            return getJdbcTemplate().queryForLong(sql);
//            
//        }catch(Exception e){
//            logger.error("getNextPK exception!", e);
//            throw e;
//        }finally{
//            logger.info("getNextPK execute time:"+(System.currentTimeMillis()-startTime));
//        }
//    }
    
    public void saveConsistentLog(PcrfConsistenLog log) throws Exception {

        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("save start ");
            
            final String sql = 
                    " INSERT INTO PCRF_CONSISTEN_LOG( " +
                    "         PCRF_ID, EXEC_START_TIME, EXEC_END_TIME, EXEC_RESULT, EXEC_USER, " + 
                    "         LOG_PATH, RPT_PATH, SYNC_RESULT) " +
                    " VALUES (?, ?, ?, ?, ?, " +
                    "         ?, ?, ?) " ;
            
            Object[] params = {
//                    log.getId(),
                      log.getPcrfId()
                    , log.getExecStartTime()
                    , log.getExecEndTime()
                    , log.getExecResult()
                    , log.getExecUser()
                    , log.getLogPath()
                    , log.getRptPath()
                    , PcrfConsistenLog.SYNC_RESULT_0
            };
            
            getJdbcTemplate().update(sql, params);
            
        }catch(Exception e){
            logger.error("save exception!", e);
            throw e;
        }finally{
            logger.info("save execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    public void updateSyncResult(long logId, boolean result) throws Exception {

        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("updateSyncResult start ");
            
            final String sql = 
                    " UPDATE PCRF_CONSISTEN_LOG " +
                    "   SET SYNC_RESULT = " + (result ? PcrfConsistenLog.SYNC_RESULT_2_SUCCESS : PcrfConsistenLog.SYNC_RESULT_1_FAIL) +
                    " WHERE ID = ? " ;
            
            Object[] params = {
                    logId
            };
            
            getJdbcTemplate().update(sql, params);
            
        }catch(Exception e){
            logger.error("updateSyncResult exception!", e);
            throw e;
        }finally{
            logger.info("updateSyncResult execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    public String findSyncType() throws Exception {

        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("findSyncType start ");
            
            final String sql = 
                    " SELECT SYNC_TYPE FROM PCRF_CONSISTENT ";
            
            Map<String, Object> data = getJdbcTemplate().queryForMap(sql);
            
            if(data == null) return null;
            
            return (String) data.get("SYNC_TYPE");
        }catch(Exception e){
            logger.error("findSyncType exception!", e);
            throw e;
        }finally{
            logger.info("findSyncType execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    public String findAutoRecMinEndTime(String sTime) throws Exception {

        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("findAutoRecMinEndTime start ");
            
            final String sql = 
            " SELECT to_char(to_timestamp("+sTime+",'YYYY-MM-DD HH24:MI:SS') " + 
            "       - (AUTO_RECOVERY_MIN * interval '1 minute'),'YYYY-MM-DD HH24:MI:SS') as END_TIME" + 
            " FROM PCRF_CONSISTENT ";

            
            Map<String, Object> data = getJdbcTemplate().queryForMap(sql);
            
            return (String) data.get("END_TIME");
        }catch(Exception e){
            logger.error("findAutoRecMinEndTime exception!", e);
            throw e;
        }finally{
            logger.info("findAutoRecMinEndTime execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    public void findTransactionLog(String exportPath, String location,String nodename , String sTime, String eTime) throws Exception {

        long startTime = System.currentTimeMillis();
        
        try{
            logger.info("findTransactionLog start ");
            
            final String sql = 
                    " ( SELECT * FROM ( " +
                    "   SELECT " +
                    "       '" + location + " - " + nodename + "', " +
                    "       '" + SyncDataType.TYPE_2 + "', " + 
                    "       MSISDN,COL1_1,REQUEST FROM TRANSACTION_LOG_ " + location + 
                    "   WHERE CREATE_TIME BETWEEN '"+sTime+"' AND '" + eTime + "' " + 
                    "   ORDER BY TX_ID " +
                    " ) T ) ";
            
            copyToFile(exportPath, sql, ",");
            
        }catch(Exception e){
            logger.error("findTransactionLog exception!", e);
            throw e;
        }finally{
            logger.info("findTransactionLog execute time:"+(System.currentTimeMillis()-startTime));
        }
    }

}
