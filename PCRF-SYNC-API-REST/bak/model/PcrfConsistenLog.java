package com.cht.pcrf.api.model;

import java.sql.Timestamp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PcrfConsistenLog {
    //0: 執行中
    //1: 失敗
    //2: 成功(都一致)
    //3: 成功(不一致)
    public static final Integer EXEC_RESULT_0 = 0;
    public static final Integer EXEC_RESULT_1 = 1;
    public static final Integer EXEC_RESULT_2 = 2;
    public static final Integer EXEC_RESULT_3 = 3;
    
    /*
    0: n/a
    1: 失敗
    2: 成功
    */
    public static final Integer SYNC_RESULT_0 = 0;
    public static final Integer SYNC_RESULT_1_FAIL = 1;
    public static final Integer SYNC_RESULT_2_SUCCESS = 2;
	
	private Long id;
	private Long pcrfId;
	private Timestamp execStartTime;
	private Timestamp execEndTime;
	private Integer execResult;
	private Integer syncResult;
	private Long execUser;
	private String logPath;
    private String rptPath;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPcrfId() {
		return pcrfId;
	}
	public void setPcrfId(Long pcrfId) {
		this.pcrfId = pcrfId;
	}
	public Timestamp getExecStartTime() {
		return execStartTime;
	}
	public void setExecStartTime(Timestamp execStartTime) {
		this.execStartTime = execStartTime;
	}
	public Timestamp getExecEndTime() {
		return execEndTime;
	}
	public void setExecEndTime(Timestamp execEndTime) {
		this.execEndTime = execEndTime;
	}
	public Integer getExecResult() {
		return execResult;
	}
	public void setExecResult(Integer execResult) {
		this.execResult = execResult;
	}
	public Long getExecUser() {
		return execUser;
	}
	public void setExecUser(Long execUser) {
		this.execUser = execUser;
	}
	public String getLogPath() {
		return logPath;
	}
	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}
	public String getRptPath() {
        return rptPath;
    }
    public void setRptPath(String rptPath) {
        this.rptPath = rptPath;
    }
    public Integer getSyncResult() {
        return syncResult;
    }
    public void setSyncResult(Integer syncResult) {
        this.syncResult = syncResult;
    }
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
	
	
}
