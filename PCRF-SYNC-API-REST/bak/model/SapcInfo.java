package com.cht.pcrf.api.model;

import java.sql.Timestamp;
import java.util.Properties;

import javax.naming.Context;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.cht.pcrf.api.common.LdapConstant;

/**
 * <code>SapcInfo</code>:SAPC model
 * 
 * @author $Author: weilinchu $
 * @version $Id: SapcInfo.java 305 2016-01-27 05:49:53Z weilinchu $
 */
public class SapcInfo extends BaseModel {

	private Long id;
	private String location;
	private String nodeName;
	private String nodeHost;
	private String nodePort;
	private String loginId;
	private String loginPw;
	private int status; // 1：online 2：offline (default) 3：處理中 
	private Timestamp lastSyncTime;
	private int sequence;

	private Properties ldapEnv;
	/**
	 * <code>StatusCode</code>: SAPC 目前狀態 1：online 2：offline (default) 3：處理中
	 * 
	 * @author $Author: weilinchu $
	 * @version $Id: SapcInfo.java 305 2016-01-27 05:49:53Z weilinchu $
	 */
	public enum SapcStatusCode {
		ONLINE(1), OFFLINE(2), PROCESSING(3);

		private Integer code;

		private SapcStatusCode(Integer code) {
			this.code = code;
		}

		public Integer getCode() {
			return code;
		}
	}

	
	public String getNodeName() {
		return nodeName;
	}

	public String getNodeHost() {
		return nodeHost;
	}

	public String getNodePort() {
		return nodePort;
	}

	public String getLoginId() {
		return loginId;
	}

	public String getLoginPw() {
		return loginPw;
	}

	public int getStatus() {
		return status;
	}

	public Timestamp getLastSyncTime() {
		return lastSyncTime;
	}

	
	public int getSequence() {
		return sequence;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public void setNodeHost(String nodeHost) {
		this.nodeHost = nodeHost;
	}

	public void setNodePort(String nodePort) {
		this.nodePort = nodePort;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public void setLoginPw(String loginPw) {
		this.loginPw = loginPw;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setLastSyncTime(Timestamp lastSyncTime) {
		this.lastSyncTime = lastSyncTime;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	
	
	public String getNodeLocation() {
		String result="";
		if(location.equals("TP")){
			result = "台北";
		}
		if(location.equals("KH")){
			result = "高雄";
		}
		return result;
	}

	public void setLdapEnv(){
	    String url = LdapConstant.LDAP_URL + LdapConstant.IP_PORT.replace("${ip}", getNodeHost())
                .replace("${port}", getNodePort());
        String username = LdapConstant.USERNAME.replace("${username}", getLoginId())
                        .replace("${nodeName}", getNodeName());
        String password = getLoginPw();

        Properties env = new Properties();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, url);
        env.put(Context.SECURITY_PRINCIPAL, username);
        env.put(Context.SECURITY_CREDENTIALS, password);
        
        this.ldapEnv = env;
	}
	
	public Properties getLdapEnv(){
        return ldapEnv;
    }
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
