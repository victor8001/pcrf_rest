/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: LdapConstants.java
 * Package Name : com.cht.pcrf.api.common
 * Date 		: 2017年4月26日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.api.common;

public class LdapConstant {

    public static final String USERNAME = "administratorName=${username},nodeName=${nodeName}";
    public static final String LDAP_URL = "ldap://";
    public static final String IP_PORT = "${ip}:${port}";
    

    public static final String EPC_SUBSCRIBER_LOWCASE = "epc-subscriber";
    public static final String EPC_SUBSCRIBERQUALIFICATION_LOWCASE = "epc-subscriberqualification";
    public static final String SHARE_TREE_LOWCASE = "sharetree";
    
    public static final String SAPC_MSG_CODE_32 = "[LDAP: error code 32";
}
