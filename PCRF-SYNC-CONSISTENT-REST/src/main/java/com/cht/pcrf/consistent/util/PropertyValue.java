/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: PropertyValue.java
 * Package Name : com.cht.pcrf.consistent.util
 * Date 		: 2017年4月27日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.util;

import com.cht.pcrf.consistent.bean.DBNFtpBean;
import com.cht.pcrf.consistent.common.CommonConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;


public class PropertyValue {
    private static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    
    private static final String CONFIG_PATH = "config.path";
    
    private static final ResourceBundle rb;
    
    public static final String OS_NAME;
    
    public static final String SH_DECODE_DBN_PATH;

    public static final String REST_DECODE_PATH;
    
    public static String SOURCE_FILE_PATH;
    
    public static String WRITE_FILE_PATH;
    
    public static Long DBN_PROC_SEC;
    
    public static final String FILE_LANGUAGE;
    
    public static final String SH_ERROR_LOG;

    public static String DBN_SRC_PATH;
    
    public static String DBN_SAPC_PATH;
    
    public static String DBN_CSV_PATH;
    
    public static String DBN_SQL_PATH;
    
    public static String DBN_AUTOPROVCSV_PATH;
    
    public static final String DBN_AUTOPROV_IGNORE;
    
    public static final String DBN_CHECK_QUALIFICATION;
    
    public static final Integer WRITE_FILE_COUNT;

    public static final Integer WRITE_FILE_SLEEP;

    public static final Integer READ_FILE_COUNT;

    public static final Integer READ_FILE_SLEEP;

    public static final Integer PARSE_THREAD_NUM;

    public static final Integer WRITE_THREAD_NUM;

    public static final Integer READ_QUEUE_SIZE;

    public static final Integer DATA_QUEUE_SIZE;

    public static final String LOCATION_NAME_PRI;
    
    public static final String[] LOCATION_NAME_SEC;
    
    public static final String[] DBN_FTP_NODENAMES;

    public static final Integer PARSE_LINE_COUNT;

    public static final Integer PARSE_LINE_SLEEP;

    public static Map<String, DBNFtpBean> dbnFtpMap = new HashMap<String, DBNFtpBean>();
    
    static 
    {
//        String today = DateUtil.getByPattern(new Date(),"yyyyMMddHHmmss");
        rb = ResourceBundle.getBundle(PropKeyConstant.PROP_FILE_NAME, Locale.US, getPropLoader());
//        rb = ResourceBundle.getBundle(PropKeyConstant.PROP_FILE_NAME, Locale.US);
        OS_NAME = rb.getString(PropKeyConstant.OS_NAME);
        SH_DECODE_DBN_PATH = rb.getString(PropKeyConstant.SH_DECODE_DBN_PATH);
        REST_DECODE_PATH = rb.getString(PropKeyConstant.REST_DECODE_PATH);
        SOURCE_FILE_PATH = rb.getString(PropKeyConstant.SOURCE_FILE_PATH);
        WRITE_FILE_PATH = rb.getString(PropKeyConstant.WRITE_FILE_PATH);
        DBN_PROC_SEC = Long.parseLong(rb.getString(PropKeyConstant.DBN_PROC_SEC));
        FILE_LANGUAGE = rb.getString(PropKeyConstant.FILE_LANGUAGE);
        SH_ERROR_LOG = rb.getString(PropKeyConstant.SH_ERROR_LOG);
        DBN_SRC_PATH = rb.getString(PropKeyConstant.DBN_SRC_PATH);
        DBN_SAPC_PATH = rb.getString(PropKeyConstant.DBN_SAPC_PATH);
        DBN_CSV_PATH = rb.getString(PropKeyConstant.DBN_CSV_PATH);
        DBN_SQL_PATH = rb.getString(PropKeyConstant.DBN_SQL_PATH);
        DBN_AUTOPROVCSV_PATH = rb.getString(PropKeyConstant.DBN_AUTOPROVCSV_PATH);
        DBN_AUTOPROV_IGNORE = rb.getString(PropKeyConstant.DBN_AUTOPROV_IGNORE);
        DBN_CHECK_QUALIFICATION = rb.getString(PropKeyConstant.DBN_CHECK_QUALIFICATION);
        WRITE_FILE_COUNT = Integer.parseInt(rb.getString(PropKeyConstant.WRITE_FILE_COUNT));
        WRITE_FILE_SLEEP = Integer.parseInt(rb.getString(PropKeyConstant.WRITE_FILE_SLEEP));
        // 2022.05.04 add by Victor
        READ_FILE_COUNT = Integer.parseInt(rb.getString(PropKeyConstant.READ_FILE_COUNT));
        READ_FILE_SLEEP = Integer.parseInt(rb.getString(PropKeyConstant.READ_FILE_SLEEP));
        PARSE_THREAD_NUM = Integer.parseInt(rb.getString(PropKeyConstant.PARSE_THREAD_NUM));
        WRITE_THREAD_NUM = Integer.parseInt(rb.getString(PropKeyConstant.WRITE_THREAD_NUM));
        READ_QUEUE_SIZE = Integer.parseInt(rb.getString(PropKeyConstant.READ_QUEUE_SIZE));
        DATA_QUEUE_SIZE = Integer.parseInt(rb.getString(PropKeyConstant.DATA_QUEUE_SIZE));
        // 2022 05 16 add by Victor
        PARSE_LINE_COUNT = Integer.parseInt(rb.getString(PropKeyConstant.PARSE_LINE_COUNT));
        PARSE_LINE_SLEEP = Integer.parseInt(rb.getString(PropKeyConstant.PARSE_LINE_SLEEP));
        LOCATION_NAME_PRI = rb.getString(PropKeyConstant.LOCATION_NAME_PRI);
        if(PropKeyConstant.LOCATION_NAME_SEC != null){
            LOCATION_NAME_SEC = rb.getString(PropKeyConstant.LOCATION_NAME_SEC).split(",");
        }else{
            LOCATION_NAME_SEC = new String[0];
        }

        DBN_FTP_NODENAMES = rb.getString(PropKeyConstant.DBN_FTP_NODENAMES).split(",");
        for(String nodename : DBN_FTP_NODENAMES){
            dbnFtpMap.put(nodename, new DBNFtpBean(
                    rb.getString(nodename + PropKeyConstant.DBN_FTP_DIR),
                    rb.getString(nodename + PropKeyConstant.DBN_FTP_HOST),
                    Integer.parseInt(rb.getString(nodename + PropKeyConstant.DBN_FTP_PORT)),
                    rb.getString(nodename + PropKeyConstant.DBN_FTP_ID),
                    rb.getString(nodename + PropKeyConstant.DBN_FTP_PW),
                    Integer.parseInt(rb.getString(nodename + PropKeyConstant.DBN_FTP_BUFSIZE))));
        }
    }

    private static ClassLoader getPropLoader() {
        try{
            File file = new File(System.getProperty(CONFIG_PATH));
            URL[] urls = {file.toURI().toURL()};
            ClassLoader loader = new URLClassLoader(urls);
            return loader;
        }catch(Exception e){
            logger.error("PropertyValue load failed");
        }
        return null;
    }

    public static void mkDefaultDir(String yyyyMMddHHmmss) {
        SOURCE_FILE_PATH = SOURCE_FILE_PATH.replace("${date}", yyyyMMddHHmmss);
        WRITE_FILE_PATH = WRITE_FILE_PATH.replace("${date}", yyyyMMddHHmmss);
        DBN_SRC_PATH = DBN_SRC_PATH.replace("${date}", yyyyMMddHHmmss);
        DBN_SAPC_PATH = DBN_SAPC_PATH.replace("${date}", yyyyMMddHHmmss);
        DBN_CSV_PATH = DBN_CSV_PATH.replace("${date}", yyyyMMddHHmmss);
        DBN_SQL_PATH = DBN_SQL_PATH.replace("${date}", yyyyMMddHHmmss);
        DBN_AUTOPROVCSV_PATH = DBN_AUTOPROVCSV_PATH.replace("${date}", yyyyMMddHHmmss);

        FileUtil.mkDir(SOURCE_FILE_PATH);
        FileUtil.mkDir(WRITE_FILE_PATH);
        FileUtil.mkDir(DBN_SRC_PATH);
        FileUtil.mkDir(DBN_SAPC_PATH);
        FileUtil.mkDir(DBN_CSV_PATH);
        FileUtil.mkDir(DBN_SQL_PATH);
        FileUtil.mkDir(DBN_AUTOPROVCSV_PATH);
        
    }
}
