/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: FileUtil.java
 * Package Name : com.cht.pcrf.consistent.util
 * Date 		: 2017年4月27日
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.util;

import com.cht.pcrf.consistent.common.CommonConstant;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class FileUtil {
    private final static Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    private static final String SOURCE_FILE_PATH = PropertyValue.SOURCE_FILE_PATH;
    private static final String FILE_LANGUAGE = PropertyValue.FILE_LANGUAGE;
    private static final String SMPP_FILE_PATTERN = "PG_DataConsistent_TSSSASA%s.txt";

    public static void mkDir(String writeFilePath) {

        File folder = new File(writeFilePath);
        if (!folder.isFile())
            folder.mkdirs();
    }

    public static String writerFile(String data, String fileName, String writeFilePath) {
        BufferedWriter fileWriter = null;
        File folder = null;
        File file = null;
        String fileLocation = writeFilePath + fileName;

        try {
            //logger.info("CSV File: " + writeFilePath + fileName);

            folder = new File(writeFilePath);
            if (!folder.isFile())
                folder.mkdirs();

            file = new File(writeFilePath + fileName);
            //fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "UTF-8"));
            fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "Big5"));
            fileWriter.append(data);
            fileWriter.append(System.getProperty("line.separator"));
            fileWriter.flush();

        } catch (Exception e) {
            logger.error("writerFile exception!", e);
        } finally {
            if (fileWriter != null) {
                try {
                    fileWriter.close();

                } catch (Exception e) {
                    logger.error("fileWriter.close() exception!", e);
                }
            }
        }

        return fileLocation;
    }


    public static List<String> getCsvFiles() throws Exception {
        File fileFolder = new File(SOURCE_FILE_PATH);
        ArrayList<String> fileList = new ArrayList<String>();

        if (fileFolder.isDirectory()) {
            String[] files = fileFolder.list();

            if (null != files)
                for (String file : files) {
                    File checkFileOrFolder = new File(SOURCE_FILE_PATH + file);

                    if (!checkFileOrFolder.isDirectory())
                        fileList.add(file);
                }
        }

        return fileList;
    }


    public static List<String> getSourceCsv(String inFilePath) throws Exception {

        long startTime = System.currentTimeMillis();
        List<String> fileList = new ArrayList<String>();

        try {
            logger.info("getSourceCsv path : " + inFilePath);

            File fileFolder = new File(inFilePath);

            if (fileFolder.isDirectory()) {
                String[] files = fileFolder.list(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return name.toLowerCase().contains("csv");
                    }
                });
                fileList = Arrays.asList(files);
            }

        } catch (Exception e) {
            logger.error("getSourceCsv exception!", e);
            throw e;
        } finally {
            logger.info("get file execute time:"
                    + (System.currentTimeMillis() - startTime));
        }

        return fileList;
    }

    public static void writerEx(String failLog, Exception failEx) {
        PrintStream ps = null;
        try {
            File file = new File(failLog);
            ps = new PrintStream(file);
            failEx.printStackTrace(ps);
            // something
        } catch (Exception ex) {
            logger.info("writerEx exception! e:", ex);
        } finally {
            if (ps != null)
                ps.close();
        }
    }

    /**
     * 以正規表達式設定要刪除的檔案
     *
     * @param path
     * @param regex
     */
    public static void deleteFileWithPattern(String path, String regex) {
        File folder = new File(path);
        File[] fileAry = new File[]{};
        FileFilter filter = new RegexFileFilter(regex);
        if (folder.isDirectory()) {
            fileAry = folder.listFiles(filter);
            if (fileAry.length > 0) {
                for (File f : fileAry) {
                    f.delete();
                }
            }
        }
    }

    public static void appendToFile(File src, File dist) throws IOException {
        long start = System.currentTimeMillis();
        BufferedReader buf = null;
        String sCurrentLine = null;
        FileWriter writer = null;
        BufferedWriter br = null;
        PrintWriter pr = null;
        // src not exist throw exception
        if (src != null && !src.exists()) {
            logger.debug("src file path : "+src.getPath());
            logger.error("source file does not exist");
        }
        // dist not exist create new file
        if (dist != null && !dist.exists()) {
            logger.info("Create file here");
            dist.createNewFile();
        }

        logger.info("start Append file here");

        try {
            writer = new FileWriter(dist,true);
            br = new BufferedWriter(writer);
            pr = new PrintWriter(br);
        } catch (Exception e) {
            logger.error("Init Writer error", e);
        }
        try{
            buf = new BufferedReader(new FileReader(src));
        }catch (Exception e){
            logger.error("Reader goes wrong !! ",e);
        }

        try {
            while ((sCurrentLine = buf.readLine()) != null) {
                if (sCurrentLine == null || sCurrentLine.trim().length() == 0){
                    continue;
                }
                else{
                    pr.println(sCurrentLine);
                    pr.flush();
                }
            }
        } catch (IOException e) {
            logger.error("Write file failed");
        } finally {
            try {
                buf.close();
                pr.close();
                br.close();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        logger.info("Append file cost : "+((System.currentTimeMillis()-start) / 1000)+" seconds");
    }
}
//end of class