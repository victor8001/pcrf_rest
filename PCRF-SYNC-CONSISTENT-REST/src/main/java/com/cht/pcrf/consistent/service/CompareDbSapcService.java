/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: CompareDbSapcService.java
 * Package Name : com.cht.pcrf.consistent.service
 * Date 		: 2017年4月27日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.service;

import com.cht.pcrf.api.common.SyncDataType;
import com.cht.pcrf.consistent.bean.CompareResultBean;
import com.cht.pcrf.consistent.common.CommonConstant;
import com.cht.pcrf.consistent.util.FileUtil;
import com.cht.pcrf.consistent.util.PropertyValue;
import com.cht.pcrf.core.dao.PcrfConsistenLogRestDaoImpl;
import com.cht.pcrf.core.dao.PcrfInfoRestDaoImpl;
import com.cht.pcrf.core.dao.TempDBNRestDaoImpl;
import com.cht.pcrf.core.model.PcrfConsistenLog;
import com.cht.pcrf.core.service.CoreDaoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class CompareDbSapcService extends ServiceExcute{
    protected static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    private static final Date now = new Date();
//    private static boolean isDbDataDiff = false;
    private final String DBN_TB = "TEMP_DBN_REST";
    
    private TempDBNRestDaoImpl tempDBNDao = CoreDaoFactory.getInstance().getTempDBNRestDao();
    private PcrfInfoRestDaoImpl pcrfInfoRestDao = CoreDaoFactory.getInstance().getPcrfInfoRestDao();
    private PcrfConsistenLogRestDaoImpl pcrfConsistenLogRestDao = CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao();
    
    /**
     * 
     * @param execTS : yyyy-MM-dd HH:mm:ss
     * @param execUserId
     * @param params
     * @return
     * @throws Exception
     */
    @Override
    public Object execute(String execTS, Long execUserId, Object... params) throws Exception {
        final long sTime = System.currentTimeMillis();
        try {
            logger.info("====== start CompareDbSapcService ======");

            long execTime = getExecTime(execTS);
            String dbnExecTs = getDBNExecTime(execTS, PropertyValue.DBN_PROC_SEC);
            
            //20200701 for 5G add POLICY_ID
            //final String tc = " " + DBN_TB + " (MSISDN,HAS_QUAL,POLICY_ID,COL11_1,COL11_2,COL11_3,COL11_4,COL11_5,COL11_6,COL11_7,COL11_8,COL11_9,COL11_10) ";
            //20220110 將第三個欄位POLICY_ID改為COL11_1
            final String tc = " " + DBN_TB + " (MSISDN,HAS_QUAL,COL11_1,POLICY_ID,COL11_2,COL11_3,COL11_4,COL11_5,COL11_6,COL11_7,COL11_8,COL11_9,COL11_10) ";
            //final String tc = " " + DBN_TB + " (MSISDN,HAS_QUAL,COL11_1,COL11_2,COL11_3,COL11_4,COL11_5,COL11_6,COL11_7,COL11_8,COL11_9,COL11_10) ";
//            tempDBNDao.copyFromFile("D:\\work\\tp-source\\CHT\\CHT_PCRF_DB_Sync\\source\\trunk\\PCRF-SYNC-CONSISTENT\\src\\test\\testdata\\convert\\pcrf_dum_file_1.csv", tc, ";");

            boolean dbCompResult = true;
            String dbCompFile = null;
            if(params != null && params.length > 0){
                dbCompFile = (String) params[0];
                long fileLen = getFileLength(dbCompFile);
                if(fileLen > 0){
                    dbCompResult = false;
                }
            }

            CompareResultBean compareResult = new CompareResultBean();
            compareResult.setDbCompResult(dbCompResult);
            
            List<String> dbnCsvList = FileUtil.getSourceCsv(PropertyValue.SOURCE_FILE_PATH);
            
            //read dbn path 
            for(String dbnCsvFile : dbnCsvList){
               // long execTime = System.currentTimeMillis();
                String nodename = "";
                PcrfConsistenLog log = null;
                try{
                    nodename = dbnCsvFile.substring(0, dbnCsvFile.lastIndexOf(CommonConstant.DOT));
                    String pcrfCompFile = PropertyValue.WRITE_FILE_PATH + getCsvFileName("DB_DIFF_" + PropertyValue.LOCATION_NAME_PRI + "_" + nodename, now);
                    String qciPcrfCompFile = PropertyValue.WRITE_FILE_PATH + getCsvFileName("DB_DIFF_" + PropertyValue.LOCATION_NAME_PRI + "_" + nodename + "_QCI", now);
                    
                    //String filePath = "D:\\work\\tp-source\\CHT\\CHT_PCRF_DB_Sync\\source\\trunk\\壓測\\msisdn_info.txt";
                    String filePath = PropertyValue.SOURCE_FILE_PATH + dbnCsvFile;
                    logger.info("dbnCsvFile:" + filePath);
                    
                    //truncat table 
                    tempDBNDao.truncateTB(DBN_TB);
                    
                    //insert dbn to db 
                    tempDBNDao.copyFromFile(filePath, tc, CommonConstant.SEMICOLON);
                    
                    //compare to dbn & subscriber
                    String FilePathPath = getInSecondaryOnly(SyncDataType.TYPE_1.getId(), dbnExecTs, PropertyValue.LOCATION_NAME_PRI, nodename);
                    String pOnlyFilePath = getInPrimaryOnly(SyncDataType.TYPE_2.getId(), dbnExecTs, PropertyValue.LOCATION_NAME_PRI, nodename);
                    String diffMap = getDBDiff(SyncDataType.TYPE_3.getId(), dbnExecTs, PropertyValue.LOCATION_NAME_PRI, nodename);
                    //String noQualPath = getNoQualInTempDBN(SyncDataType.TYPE_2.getId(), PropertyValue.LOCATION_NAME_PRI, nodename); //20220110 取消比對noQual 不再傳送HAS QUAL
                    
                    //20200701 for 5G start
                    //20220111 取消傳送has qual,policy_id temp_dbn只傳送msisdn,col11_1
                    //String QciFilePathPath = getQciInSecondaryOnly(SyncDataType.TYPE_1.getId(), dbnExecTs, PropertyValue.LOCATION_NAME_PRI, nodename);
                    //String QciOnlyFilePath = getQciInPrimaryOnly(SyncDataType.TYPE_2.getId(), dbnExecTs, PropertyValue.LOCATION_NAME_PRI, nodename);
                    //String QciDiffMap = getQciDBDiff(SyncDataType.TYPE_3.getId(), dbnExecTs, PropertyValue.LOCATION_NAME_PRI, nodename);
                    //20200701 for 5G end
                    
                    //dbname,type,msisdn,objectclass,info
                    exportFile(FilePathPath, pcrfCompFile);
                    exportFile(pOnlyFilePath, pcrfCompFile);
                    //exportFile(noQualPath, pcrfCompFile); //20220110 取消比對noQual 不再傳送HAS QUAL
                    exportFile(diffMap, pcrfCompFile);
                    
                    //20200701 for 5G
                    //20220111 取消傳送has qual,policy_id temp_dbn只傳送msisdn,col11_1
                    //exportFile(QciFilePathPath, qciPcrfCompFile);
                    //exportFile(QciOnlyFilePath, qciPcrfCompFile);
                    //exportFile(QciDiffMap, qciPcrfCompFile);
                    
                    //check is same
                    boolean pcrfCompResult = (getFileLength(pcrfCompFile) == 0) ? true : false;
                    
                    //save result to db
                    log = saveLog(execUserId, dbCompResult, pcrfCompResult, nodename, dbCompFile, pcrfCompFile, execTime, null);
                    compareResult.putLogData(log);
/*
                    //20200701 for 5G -> restful use col11_1 as comparison column omit QCI Type
                    pcrfCompResult = (getFileLength(qciPcrfCompFile) == 0) ? true : false;
                    log = saveLog(execUserId, dbCompResult, pcrfCompResult, nodename, dbCompFile, qciPcrfCompFile, execTime, "Y");
                    compareResult.putLogData(log);

 */
                } catch (Exception e) {
                    log = saveFailLog(execUserId, nodename, execTime, e);
                    //compareResult.putLogData(log);
                    
                    logger.error("CompareDbSapcService exception, ", e);
                }
            }
            
            return compareResult;
        } catch (Exception e) {
            logger.error("CompareDbSapcService exception, ", e);
            throw e;
        } finally {
            
            logger.info("====== CompareDbSapcService Execute finished, spend time: "
                    + (System.currentTimeMillis() - sTime) / 1000
                    + " seconds ======");

        }
    }

    private PcrfConsistenLog saveLog(Long execUserId, boolean dbCompResult, boolean pcrfCompResult, String nodename,
            String dbCompFile, String pcrfCompFile, long execTime, String isQci) throws Exception {
        //find pcrf_info id
        //save log
        logger.info("save pcrf consisten log, nodename:{}, dbCompResult:{}, pcrfCompResult:{} ", PropertyValue.LOCATION_NAME_PRI + "_" + nodename, dbCompResult, pcrfCompResult);

        //3: 成功(不一致)
        int execResult = PcrfConsistenLog.EXEC_RESULT_3;
        if(dbCompResult && pcrfCompResult){
            //2: 成功(都一致)
            execResult = PcrfConsistenLog.EXEC_RESULT_2;
        }
        
        if(execResult == PcrfConsistenLog.EXEC_RESULT_3){
            logger.error("PCRF_DB_NO_SYNC,{}", nodename);
        }
        
        long pcrfId = pcrfInfoRestDao.findIdByNodename(nodename);
        long logId = pcrfConsistenLogRestDao.getNextPK();
        
        PcrfConsistenLog log = new PcrfConsistenLog();
        log.setId(logId);
        log.setPcrfId(pcrfId);
        log.setExecStartTime(new Timestamp(execTime));
        log.setExecEndTime(new Timestamp(System.currentTimeMillis()));
        log.setExecResult(execResult);
        log.setExecUser(execUserId);
        log.setRptPath(pcrfCompFile + CommonConstant.COMMA + dbCompFile);
        log.setIsQci(isQci);
        pcrfConsistenLogRestDao.save(log);
        
        return log;
    }
    
    private PcrfConsistenLog saveFailLog(Long execUserId, String nodename, long execTime, 
            Exception failEx) throws Exception {
        //find pcrf_info id
        //save log
        logger.info("save pcrf consisten log, nodename:{}, somthing fail", PropertyValue.LOCATION_NAME_PRI + "_" + nodename);

        String failLog = getCsvFileName("DB_DIFF_" + PropertyValue.LOCATION_NAME_PRI + "_" + nodename + "_FAIL", now);

        try{
            FileUtil.writerFile(
                failEx.getMessage() + failEx, 
                failLog, 
                PropertyValue.WRITE_FILE_PATH);
        } catch (Exception e) {
            logger.error("writerFile exception, ", e);
        }
        
        long pcrfId = pcrfInfoRestDao.findIdByNodename(nodename);
        long logId = pcrfConsistenLogRestDao.getNextPK();
        
        PcrfConsistenLog log = new PcrfConsistenLog();
        log.setId(logId);
        log.setPcrfId(pcrfId);
        log.setExecStartTime(new Timestamp(execTime));
        log.setExecEndTime(new Timestamp(System.currentTimeMillis()));
        log.setExecResult(PcrfConsistenLog.EXEC_RESULT_1);
        log.setExecUser(execUserId);
        log.setRptPath(PropertyValue.WRITE_FILE_PATH + failLog);
        pcrfConsistenLogRestDao.save(log);
        
        return log;
    }

    /**
     * 取得只存在在第一台的資料
     * @param locationNamePri
     * @param IN_DB 
     * @throws Exception 
     */
    private String getInPrimaryOnly(final String type, final String execTS, String locationNamePri, String nodename) throws Exception {
        
        String diffFilePath = PropertyValue.WRITE_FILE_PATH
                + getCsvFileName("DB_DIFF_" + locationNamePri + "_" + nodename + "_" + type, now);
        
        tempDBNDao.selectExistInPri(diffFilePath, type, execTS, locationNamePri, nodename, DBN_TB);
        
        return diffFilePath;
    }

    /**
     * 取得只存在在第二台的資料
     * @param locationNamePri
     * @return 
     * @throws Exception 
     */
    private String getInSecondaryOnly(final String type, final String execTS, String locationNamePri, String nodename) throws Exception {

        String diffFilePath = PropertyValue.WRITE_FILE_PATH
                + getCsvFileName("DB_DIFF_" + locationNamePri + "_" + nodename + "_" + type, now);
        
        tempDBNDao.selectExistInSec(diffFilePath, type, execTS, locationNamePri, nodename, DBN_TB);
        
        return diffFilePath;
    }

    /**
     * 取得差異
     * @param locationNamePri
     * @param locationNameSec
     * @return 
     * @throws Exception 
     */
    private String getDBDiff(final String type, final String execTS, String locationNamePri, String nodename) throws Exception {
        
        String diffFilePath = PropertyValue.WRITE_FILE_PATH
                + getCsvFileName("DB_DIFF_" + locationNamePri + "_" + nodename + "_" + type, now);
        
        tempDBNDao.selectDiff(diffFilePath, type, execTS, locationNamePri, nodename, DBN_TB);
        
        return diffFilePath;
    }
    
    /**
     * 取得沒有qualification的門號
     * @param locationNamePri
     * @param IN_DB 
     * @throws Exception 
     */
    private String getNoQualInTempDBN(final String type, String locationNamePri, String nodename) throws Exception {
        
        String filePath = PropertyValue.WRITE_FILE_PATH
                + getCsvFileName("DB_DIFF_" + locationNamePri + "_" + nodename + "_" + type + "-2", now);
        
        tempDBNDao.findNoQualInTempDBN(filePath, type, locationNamePri, nodename, DBN_TB);
        
        return filePath;
    }
    
    /**
     * 取得只存在在第一台的Qos資料
     * @param locationNamePri
     * @param IN_DB 
     * @throws Exception 
     */
    private String getQciInPrimaryOnly(final String type, final String execTS, String locationNamePri, String nodename) throws Exception {
        
        String diffFilePath = PropertyValue.WRITE_FILE_PATH
                + getCsvFileName("DB_DIFF_" + locationNamePri + "_" + nodename + "_Qci_" + type, now);
        
        tempDBNDao.selectQciExistInPri(diffFilePath, type, execTS, locationNamePri, nodename, DBN_TB);
        
        return diffFilePath;
    }
    
    /**
     * 取得只存在在第二台的Qos資料
     * @param locationNamePri
     * @return 
     * @throws Exception 
     */
    private String getQciInSecondaryOnly(final String type, final String execTS, String locationNamePri, String nodename) throws Exception {
    	
    	String diffFilePath = PropertyValue.WRITE_FILE_PATH
                + getCsvFileName("DB_DIFF_" + locationNamePri + "_" + nodename + "_Qci_" + type, now);
    	
    	tempDBNDao.selectQciExistInSec(diffFilePath, type, execTS, locationNamePri, nodename, DBN_TB);
        
        return diffFilePath;
    }
    
    /**
     * 取得Qos差異
     * @param locationNamePri
     * @param locationNameSec
     * @return 
     * @throws Exception 
     */
    private String getQciDBDiff(final String type, final String execTS, String locationNamePri, String nodename) throws Exception {
        
        String diffFilePath = PropertyValue.WRITE_FILE_PATH
                + getCsvFileName("DB_DIFF_" + locationNamePri + "_" + nodename + "_Qci_" + type, now);
        
        tempDBNDao.selectQciDiff(diffFilePath, type, execTS, locationNamePri, nodename, DBN_TB);
        
        return diffFilePath;
    }

    public TempDBNRestDaoImpl getTempDBNDao() {
        return tempDBNDao;
    }

    public void setTempDBNDao(TempDBNRestDaoImpl tempDBNDao) {
        this.tempDBNDao = tempDBNDao;
    }

    public PcrfInfoRestDaoImpl getPcrfInfoDao() {
        return pcrfInfoRestDao;
    }

    public void setPcrfInfoDao(PcrfInfoRestDaoImpl pcrfInfoDao) {
        this.pcrfInfoRestDao = pcrfInfoDao;
    }

    public PcrfConsistenLogRestDaoImpl getPcrfConsistenLogDao() {
        return pcrfConsistenLogRestDao;
    }

    public void setPcrfConsistenLogDao(PcrfConsistenLogRestDaoImpl pcrfConsistenLogDao) {
        this.pcrfConsistenLogRestDao = pcrfConsistenLogDao;
    }
    
}


