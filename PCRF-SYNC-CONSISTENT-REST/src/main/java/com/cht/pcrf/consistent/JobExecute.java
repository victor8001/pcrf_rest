/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: JobExecute.java
 * Package Name : com.cht.pcrf.consistent
 * Date 		: 2017年4月26日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent;

import com.cht.pcrf.consistent.common.CommonConstant;
import com.cht.pcrf.consistent.common.ConsistentType;
import com.cht.pcrf.consistent.service.*;
import com.cht.pcrf.consistent.util.DateUtil;
import com.cht.pcrf.consistent.util.PropertyValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

public class JobExecute {
    protected static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
//    public static final Date now = new Date();
    private static final String FILE_PATTERN = "%s_%s.csv";
    
    private static ClassPathXmlApplicationContext ctx;
    public static JobExecute instance;
    
    public static JobExecute getInstance(){
        if(instance == null)
            instance = new JobExecute();
        
        return instance;
    }
    
    private JobExecute(){
        ctx = new ClassPathXmlApplicationContext("consistent-spring-bean.xml");
        logger.info("Initialize spring config finish!");
    }
    
    public static void main(String[] args) {
        JobExecute jobExecute = new JobExecute();
        jobExecute.execute(args);
    }
    
    /**
     * args[0] : 執行時間
     * args[1] : 執行種類
     * args[2] : 執行使用者
     * args[3] : 執行pcrf_consistent_log.id
     * @param args
     */
    private void execute(String[] args) {

        final long sTime = System.currentTimeMillis();
        try {
            logger.info("====== JobExecute start execute ======");

            for (Object param : args) {
                logger.info("param:" + (String) param);
            }
            
            ServiceExcute service = null;
            String jobStartYMDHMS = args[0]; //yyyyMMddHHmmss
            String execType = args[1];
            Long execUserId = Long.parseLong(args[2]);
            String ftpYMD = "";
            if(args.length >= 6){
                ftpYMD = args[5];
            }

            logger.info("jobStartYMDHMS: " + jobStartYMDHMS);
            logger.info("execType: " + execType);
            logger.info("execUserId: " + execUserId);
            logger.info("ftpYMD: " + ftpYMD);
            
            String[] execTypeArr = execType.split(CommonConstant.COMMA);
            PropertyValue.mkDefaultDir(jobStartYMDHMS);
            
            Date execDate = DateUtil.parseDate(jobStartYMDHMS, "yyyyMMddHHmmss");
            String execTS = DateUtil.formatDate(execDate, ServiceExcute.EXEC_TS_PATTERN);//2017-04-21 00:00:00
            
            for (String type : execTypeArr) {
                if(ConsistentType.COMPARE_DATAS.getId().equals(type)){
                    service = ctx.getBean("compareDatasService", CompareDatasService.class);
                
                    if (service != null){
                        String nodename = "";
                        boolean isPassDown = false;
                        if(args.length > 3){
                            nodename = args[3];
                        }
                        if(args.length > 4 && "Y".equals(args[4])){
                            isPassDown = true;
                        }
                        //#exec_time 4 userId nodename
                        service.execute(execTS, execUserId, new Object[]{nodename, Boolean.toString(isPassDown), ftpYMD});
                    }
                    
                }else if(ConsistentType.SYNC_PCRF_DATAS_MANUALLY.getId().equals(type)){
                    service = ctx.getBean("recPcrfDataManuallyService", RecPcrfDataManuallyService.class);
                    
                    if (service != null)
                        //#exec_time 5 userId logId csvPath
                        service.execute(execTS, execUserId, new Object[]{args[3], args[4]});
                }else if(ConsistentType.CONVERT_SAPC_DBN_FOR_INIT.getId().equals(type)){
                    service = ctx.getBean("covertRestDBNToCSVService", CovertRestDBNToCSVService.class);
                    
                    if (service != null)
                        service.execute(execTS, execUserId, new Object[]{"Y"});
                }else{
                
                    if (ConsistentType.CONVERT_SAPC_DBN.getId().equals(type))
                        service = ctx.getBean("covertRestDBNToCSVService", CovertRestDBNToCSVService.class);
                        
                    else if(ConsistentType.COMPARE_DBS.getId().equals(type))
                         service = ctx.getBean("compareDBsService", CompareDBsService.class);
                    
                    else if(ConsistentType.COMPARE_DB_SAPC.getId().equals(type))
                         service = ctx.getBean("compareDbSapcService", CompareDbSapcService.class);
                    
                    if (service != null)
                        service.execute(execTS, execUserId);
                }
            }

        } catch (Exception e) {
            logger.error("JobExecute exception, e:", e);
            logger.error(e.getMessage(), e);
        } finally{
            logger.info("====== JobExecute Execute finished, spend time: "
                    + (System.currentTimeMillis() - sTime) / 1000
                    + " seconds ======");
        }
    }

    private static String getCsvFileName(String type, Date now) {
        String csvFileName = "";

        try {
            csvFileName = String.format(FILE_PATTERN, type,
                    DateUtil.getNowDateTime(now));

        } catch (Exception e) {
            logger.error("getCsvFileName error ", e);
        }

        return csvFileName;
    } 

}
