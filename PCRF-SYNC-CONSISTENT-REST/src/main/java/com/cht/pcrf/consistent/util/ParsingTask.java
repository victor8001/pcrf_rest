package com.cht.pcrf.consistent.util;

import com.cht.pcrf.consistent.common.CommonConstant;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 讀取完每一行字串後對資料進行Parsing
 */
public class ParsingTask implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    private static final String OS_NAME_WIN = "Windows";
    private static final String WRITE_SPLITE_STR = ";";
    // data from read file
    private BlockingQueue<String[]> _dataQueue;
    // prepare Parsing file
    private BlockingQueue<String> _writeQueue;
    private String _path, _filenm;

    private static volatile Boolean finished = false;

    public ParsingTask(String path, String filenm, BlockingQueue<String[]> dataQueue, BlockingQueue<String> writeQueue) {
        _path = path;
        _filenm = filenm;
        _dataQueue = dataQueue;
        _writeQueue = writeQueue;
    }

    @Override
    public void run() {
        // 處理中的檔案數量
        int processing = 0;
        while (!getFinished()) {
            logger.info("ParsingTask is running");
            String[] data = new String[0];
            try {
                // add sleep to prevent IOException
                if(processing == PropertyValue.PARSE_LINE_COUNT ){
                    TimeUnit.MILLISECONDS.sleep(PropertyValue.PARSE_LINE_SLEEP);
                }
                if (isNeedWait()) {
                    synchronized (this) {
                        logger.info("Get Queue Size : " + _dataQueue.size());
                        processing ++;
                        data = _dataQueue.take();
                        // get record and put to queue
                        String record = parseData(_path, _filenm, data);
                        _writeQueue.put(record);
                        logger.info("Comsuming data");
                        processing --;
                        continue;
                    }
                }
                if(! _dataQueue.isEmpty()){
                    synchronized (this) {
                        logger.info("Get Queue Size : " + _dataQueue.size());
                        processing ++;
                        data = _dataQueue.take();
                        // get record and put to queue
                        String record = parseData(_path, _filenm, data);
                        _writeQueue.put(record);
                        logger.info("Comsuming data");
                        processing -- ;
                        continue;
                    }
                }
                TimeUnit.SECONDS.sleep(3);
                if (isTerminated()) {
                    break;
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
        setFinished(true);
        logger.info("ParsingTask is ended");
    }

    private String parseData(String dbnDir, String dbnFileName, String[] sub) throws Exception {
        // TODO
        String dbnSrcDir = PropertyValue.DBN_SRC_PATH + dbnFileName;

        String groupPath = dbnDir + dbnFileName + "_" + sub[0] + "_subgroup_dump.log";
        runCmd("dump_instance " + sub[0] + " " + groupPath + " " + dbnSrcDir);
        //	./BackupFormatter /opt/pcrf/data/consistent-volte/${date}/dbn_src/ dump_instance 4815890994626560-0000e80000000008 > /opt/pcrf/data/consistent-volte/${date}/dbn/2L3VoLTEPCRF1_subgroup_dump.log

        return readSubDataplanFile(sub[1], groupPath);
    }

    private void runCmd(String param) throws InterruptedException, IOException {

        String command = PropertyValue.REST_DECODE_PATH.replace("${0}", param);

        String[] stringCommandWindows = {"cmd", "/C", command};

        String[] stringCommandLinux = {"bash", "-c", command};

        Process proc = null;
        long startTime = System.currentTimeMillis();
        try {

            logger.info("----- runSqlldr Started -----");
            Runtime rt = Runtime.getRuntime();

            logger.info("sqlldr cmd : " + command);

            if (OS_NAME_WIN.equalsIgnoreCase(PropertyValue.OS_NAME)) {
                proc = rt.exec(stringCommandWindows);
            } else {
                proc = rt.exec(stringCommandLinux);
            }

            String line = null;

            InputStream stderr = proc.getErrorStream();
            InputStreamReader esr = new InputStreamReader(stderr);
            BufferedReader ebr = new BufferedReader(esr);
            logger.info("<info>");
            while ((line = ebr.readLine()) != null)
                logger.info(line);
            logger.info("</info>");

            InputStream stdout = proc.getInputStream();
            InputStreamReader osr = new InputStreamReader(stdout);
            BufferedReader obr = new BufferedReader(osr);
            logger.info("<output>");
            while ((line = obr.readLine()) != null)
                logger.info(line);
            logger.info("</output>");

            int exitVal = proc.waitFor();
            logger.info("Process exitValue: " + exitVal);

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (NullPointerException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            if (proc != null)
                proc.destroy();
            logger.info("runSqlldr execute time:" + (System.currentTimeMillis() - startTime));
        }
    }

    private String readSubDataplanFile(String msisdn, String inFileName) throws Exception {
        long startTime = System.currentTimeMillis();

        BufferedReader buf = null;
        String sCurrentLine = null;
        String csvStr = null;
        try {
            logger.info(" readData start ");
            buf = new BufferedReader(new FileReader(inFileName));
            String groups = "";
            boolean isMsisdnGroupAttr = false;
            while ((sCurrentLine = buf.readLine()) != null) {
                if (sCurrentLine == null || sCurrentLine.trim().length() == 1)
                    continue;

                // Attr0 [id:S]
                // content:[DATA:[length:16][~~~~0940000218~~][0000000a303934303030303231380000]]

                // Attr16 [dataplans:A0[:S]]
                // content: [DATA:[length:68][~~~~~~~~Sy_MRT_PRA#0##~~~~~~Rx_MRT_PRA#0##~~~~~~RoamingNonVolume#0##][000000030000000e53795f4d52545f5052412330232300000000000e52785f4d52545f50524123302323000000000014526f616d696e674e6f6e566f6c756d6523302323]]

                sCurrentLine = StringUtils.trimToEmpty(sCurrentLine);

                if (sCurrentLine.startsWith("Attr")) {
                    if (sCurrentLine.startsWith("Attr16")) {
                        isMsisdnGroupAttr = true;
                    }
                    continue;
                }

                if (isMsisdnGroupAttr && sCurrentLine.indexOf("content:") != -1) {
                    logger.info(" sCurrentLine = " + sCurrentLine);
                    // groups =
                    // sCurrentLine.split("~~~~~~~~")[1].split("~~")[0];

                    String tmpGroups = sCurrentLine.split("\\[")[3];
                    groups = tmpGroups.substring(0, tmpGroups.length() - 1);

                    String convertGroups = convertGroups(groups);
                    logger.info(" convertGroups = " + convertGroups);
                    // logger.info(msisdn + "," + convertGroups);
                    isMsisdnGroupAttr = false;

                    if (groups.length() != 0){
                        csvStr = msisdn + WRITE_SPLITE_STR + WRITE_SPLITE_STR + convertGroups;
                    }else{
                        csvStr = msisdn + WRITE_SPLITE_STR + WRITE_SPLITE_STR + convertGroups("");
                    }
                    // continue;
                }
            }
        } catch (Exception e) {
            logger.error("readData exception, ", e);
            throw e;
        } finally {
            logger.info("readData execute time:" + (System.currentTimeMillis() - startTime));

            if (buf != null){
                try {
                    buf.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return csvStr;
    }

    private String convertGroups(String groups) {
        int totalGroup = 10;
        StringBuffer groupSB = new StringBuffer();
        if (groups.length() > 0) {
            groups = groups.substring(2, groups.length());
            //~~~~~~~~4G_volte_vowifi#####
            //~~~~~~~~Sy_MRT_PRA#0##~~~~~~Rx_MRT_PRA#0##~~~~~~RoamingNonVolume#0##
            String[] groupArr = groups.split("~~~~~~");
            for (String tgroup : groupArr) {
                if (!"".equals(tgroup)) {
                    String[] grpPri = tgroup.replace("~", "").split("#");
                    String group = grpPri[0];
                    if (grpPri.length >= 2) {
                        group = group + ":" + grpPri[1];
                    }
                    logger.info("group:" + group);
                    totalGroup--;
                    //GroupId1;Priority1;startdate1;enddate1;
                    groupSB.append(group + WRITE_SPLITE_STR);
                }
            }
        }

        for (; totalGroup > 0; totalGroup--) {
            groupSB.append(WRITE_SPLITE_STR);
        }

        return groupSB.toString();
    }

    private synchronized boolean isNeedWait() {
        return !ReadFileTask.getFinished() || (this._dataQueue.size() > 0);
    }

    private synchronized boolean isTerminated() {
        return ReadFileTask.getFinished() && (this._dataQueue.size() == 0);
    }

    public static synchronized Boolean getFinished() {
        return finished;
    }

    public static synchronized void setFinished(Boolean finished) {
        ParsingTask.finished = finished;
    }
}
