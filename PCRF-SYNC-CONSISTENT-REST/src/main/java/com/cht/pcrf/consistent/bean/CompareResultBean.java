/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: CompareResultBean.java
 * Package Name : com.cht.pcrf.consistent.bean
 * Date 		: 2017年5月5日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.bean;

import com.cht.pcrf.core.model.PcrfConsistenLog;

import java.util.ArrayList;
import java.util.List;

public class CompareResultBean {

    private boolean dbCompResult = false;
    private List<PcrfConsistenLog> logList = new ArrayList();
    
    public boolean isDbCompResult() {
        return dbCompResult;
    }
    public void setDbCompResult(boolean dbCompResult) {
        this.dbCompResult = dbCompResult;
    }
    public List<PcrfConsistenLog> getLogList() {
        return logList;
    }
//    public void setLogList(List<PcrfConsistenLog> logList) {
//        this.logList = logList;
//    }
    public void putLogData(PcrfConsistenLog log){
        this.logList.add(log);
    }
    
}
