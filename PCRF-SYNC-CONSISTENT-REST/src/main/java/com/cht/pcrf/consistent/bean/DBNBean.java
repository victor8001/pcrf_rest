/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: DBNModel.java
 * Package Name : com.cht.pcrf.consistent.model
 * Date 		: 2017年4月27日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.bean;

import org.apache.commons.lang3.StringUtils;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class DBNBean {
    private boolean isAutoProvision = false;
    private String msisdn;
    private List<String> tags = new ArrayList<String>(4);
    private List<String> groups = new ArrayList<String>(8);
    public static final String CSV_SPLIT = ";";
    private final int tagsMaxSize = 4;
    private final int groupsMaxSize = 10;
    private boolean hasQualification = true;
    
    //20200701 for 5G
    private String policyId;
    
    public String getMsisdn() {
        return msisdn;
    }
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
    public List<String> getTags() {
        return tags;
    }
    public void setTags(List<String> tags) {
        this.tags = tags;
    }
    public List<String> getGroups() {
        return groups;
    }
    public void setGroups(List<String> groups) {
        this.groups = groups;
    }
    public boolean isAutoProvision() {
        return isAutoProvision;
    }
    public void setAutoProvision(boolean isAutoProvision) {
        this.isAutoProvision = isAutoProvision;
    }
    public boolean isHasQualification() {
        return hasQualification;
    }
    public void setHasQualification(boolean hasQualification) {
        this.hasQualification = hasQualification;
    }
    //20200701 for 5G start
    public String getPolicyId() {
		return policyId;
	}
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	//20200701 for 5G end
	
    public String getInsertSQL() {
//        dn: EPC-SubscriberId=886900000001,EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=PC01PCRF
//        EPC-SubscriberId: 886989446653
//        EPC-GroupIds: Sy:20
//        EPC-GroupIds: Rx:10
                
        //INSERT INTO "SUBSCRIBER" (MSISDN,INFO,CREATE_TIME,MODIFY_TIME) VALUES ('886999001002','',null,now);
        String insSQL = "INSERT INTO \"SUBSCRIBER\" (MSISDN,INFO,CREATE_TIME,MODIFY_TIME) VALUES "
                + " ('V_MSISDN','V_INFO',current_timestamp,current_timestamp);\n";
        
        StringBuffer sb = new StringBuffer();
        //sb.append("dn: EPC-SubscriberId="+msisdn+",EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=PC01PCRF");
        sb.append("EPC-SubscriberId="+msisdn+",EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=PC01PCRF");
        sb.append("' || chr(10) || '");
        sb.append("EPC-SubscriberId: "+msisdn);
        sb.append("' || chr(10) || '");
        
        for(String tag : tags){
            sb.append("EPC-OperatorSpecificInfo: "+tag);
            sb.append("' || chr(10) || '");
        }
        for(String group : groups){
            sb.append("EPC-GroupIds: "+group.replaceAll(";", ""));
            sb.append("' || chr(10) || '");
        }
        
        insSQL = insSQL.replaceFirst("V_MSISDN", msisdn);
        insSQL = insSQL.replaceFirst("V_INFO", sb.toString());
        
        
        return insSQL;
    }
    

    public void getInsertSQLForLoader(PrintWriter pw) {
//        dn: EPC-SubscriberId=886900000001,EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=PC01PCRF
//        EPC-SubscriberId: 886989446653
//        EPC-GroupIds: Sy:20
//        EPC-GroupIds: Rx:10
                
        //INSERT INTO "SUBSCRIBER" (MSISDN,INFO,CREATE_TIME,MODIFY_TIME) VALUES ('886999001002','',null,now);
//        String insSQL = " msisdn;object_class;dn;create_time;col1_1;col2_1;col3_1;col4_1;col5_1;col6_1;info;col11_1;col11_2;col11_3;col11_4;col11_5;col11_6;col11_7;col11_8;col11_9;col11_10 ";
        

        
        StringBuffer gropupSB = new StringBuffer();
        List<String> groups = new ArrayList();
        groups.addAll(this.groups);
        
        int needAddSize = groupsMaxSize - groups.size() - 1;
        if(needAddSize > 0){
            for(int i=0;i<needAddSize;i++){
                //補分號
                groups.add(CSV_SPLIT);
            }
        }
        if(groupsMaxSize == groups.size()){
            String lastGroup = groups.get(groupsMaxSize-1);
            groups.set(groupsMaxSize-1, lastGroup.substring(0, lastGroup.length()-1));
        }
        
        
        StringBuffer baseSB = new StringBuffer();
//        StringBuffer infoSB = new StringBuffer();
        baseSB.append(msisdn);
        baseSB.append(";");
        baseSB.append("EPC-Subscriber");
        baseSB.append(";");
        baseSB.append("EPC-SubscriberId="+msisdn+",EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=PGNode");
        baseSB.append(";");
        baseSB.append("1900-01-01 14:39:46");
        baseSB.append(";");
        //baseSB.append("EPC-Subscriber;0;4003;15;nodeName=PGNode;" + msisdn);
        baseSB.append("EPC-Subscriber;;;15;nodeName=PGNode;" + msisdn);
        baseSB.append(";");
        baseSB.append(";");
        pw.print(baseSB);
        
        //info
        /*
        infoSB.append("objectClass: EPC-Subscriberchr(10)");
        infoSB.append("EPC-SubscriberId: "+msisdn+"chr(10)");
        infoSB.append("permissions: 15chr(10)");
        infoSB.append("groupId: 4003chr(10)");
        infoSB.append("ownerId: 0chr(10)");
        infoSB.append("shareTree: nodeName=PGNode");
        if(gropupSB.length() > 0){
            infoSB.append("chr(10)" + gropupSB.toString());
        }
        infoSB.append(";");
        */
        pw.print("\"");
        pw.print("objectClass: EPC-Subscriber");
        pw.println("");
        pw.print("EPC-SubscriberId: "+msisdn);
        pw.println("");
        pw.print("permissions: 15");
        pw.println("");
        //pw.print("groupId: 4003");
        //pw.println("");
        //pw.print("ownerId: 0");
        //pw.println("");
        pw.print("shareTree: nodeName=PGNode");
        

        for(String group : groups){
            if(group == null || "".equals(group.replaceAll(";", "").trim()))
                continue;
            
            pw.println("");
            pw.print("EPC-GroupIds: "+group.replaceAll(";", ""));
        }
        pw.print("\"");
        pw.print(";");
        

        //group
        String gropupCols = "v1;v2;v3;v4;v5;v6;v7;v8;v9;v10;";
        int i=1;
        for(String group : groups){
            if(group == null) group = ";";
            group = group.trim();
            
            gropupCols = gropupCols.replace("v"+i+";", group);
            i++;
        }
        gropupCols = gropupCols.replace("v10;", ";");
        gropupCols = gropupCols.substring(0, gropupCols.length()-1);
        pw.print(gropupCols);
        pw.println("");
        
//        baseSB.append(infoSB);
//        baseSB.append(gropupCols);
        
        //return baseSB.toString();
    }


    public void getInsertSQLForLoader2(PrintWriter pw) {
//        "objectClass: EPC-SubscriberQualification
//        permissions: 15
//        EPC-Name: EPC-SubscriberQualification
//        groupId: 4003
//        EPC-SubscriberQualificationData: SubscriberChargingSystemName:ChtOcs
//        ownerId: 0
//        shareTree: nodeName=PGNode"
        
        StringBuffer sb = new StringBuffer();
        sb.append(msisdn);
        sb.append(";");
        sb.append("EPC-SubscriberQualification");
        sb.append(";");
        sb.append("EPC-Name=EPC-SubscriberQualification,EPC-SubscriberId="+msisdn+",EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=PGNode");
        sb.append(";");
        sb.append("1900-01-01 14:39:46");
        sb.append(";");
        //sb.append("EPC-SubscriberQualification;0;4003;15;nodeName=PGNode;EPC-SubscriberQualification;SubscriberChargingSystemName:ChtOcs;");
        sb.append("EPC-SubscriberQualification;;;15;nodeName=PGNode;EPC-SubscriberQualification;SubscriberChargingSystemName:ChtOcs;");

        pw.print(sb.toString());
        
        /*
        sb.append("objectClass: EPC-SubscriberQualificationchr(10)");
        sb.append("permissions: 15chr(10)");
        sb.append("groupId: 4003chr(10)");
        sb.append("ownerId: 0chr(10)");
        sb.append("EPC-Name: EPC-SubscriberQualificationchr(10)");
        sb.append("EPC-SubscriberQualificationData: SubscriberChargingSystemName:ChtOcschr(10)");
        sb.append("shareTree: nodeName=PGNode");
        pw.print(sb.toString());
        */

        pw.print("\"");
        pw.print("objectClass: EPC-SubscriberQualification");
        pw.println("");
        pw.print("permissions: 15");
        pw.println("");
        //pw.print("groupId: 4003");
        //pw.println("");
        //pw.print("ownerId: 0");
        //pw.println("");
        pw.print("EPC-Name: EPC-SubscriberQualification");
        pw.println("");
        pw.print("EPC-SubscriberQualificationData: SubscriberChargingSystemName:ChtOcs");
        pw.println("");
        pw.print("shareTree: nodeName=PGNode");
        pw.print("\"");

        String gropupCols = ";;;;;;;;;;";
        //sb.append(gropupCols);
        
        pw.print(gropupCols);
        pw.println("");
        
        //return sb.toString();
    }
    
    //20200701 for 5G
    public void getInsertSQLForLoader3(PrintWriter pw) {
//    	dn: EPC-ContextName=QoS,EPC-SubjectResourceId=_Bearer_,EPC-SubjectId=886905443808,EPC-SubjectPolicyName=EPC-SubjectPolicyLocators,applicationName=EPC-PolicyRepository,nodeName=2L3PilotPCRF1
//    	changetype: add
//    	EPC-ContextName: QoS
//    	EPC-PolicyIds: 0:p_5G_QCI006_ARP06_1500M:Qualify
    	
    	StringBuffer sb = new StringBuffer();
        sb.append(msisdn);
        sb.append(";");
        sb.append("EPC-Subject");
        sb.append(";");
        sb.append("EPC-ContextName=QoS,EPC-SubjectResourceId=_Bearer_,EPC-SubjectId="+msisdn+",EPC-SubjectPolicyName=EPC-SubjectPolicyLocators,applicationName=EPC-PolicyRepository,nodeName=PGNode");
        sb.append(";");
        sb.append("1900-01-01 14:39:46");
        sb.append(";");
        sb.append("EPC-Subject;;QoS;"+policyId+";;;;");
        
        pw.print(sb.toString());
        
        pw.print("\"");
        pw.print("EPC-PolicyIds: " + policyId);
        pw.print("\"");
    	
        String gropupCols = ";;;;;;;;;;";
        
        pw.print(gropupCols);
        pw.println("");
    }
    
    public String getInsertSQL2() {
//      dn: EPC-SubscriberId=886900000001,EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=PC01PCRF
//      EPC-SubscriberId: 886989446653
//      EPC-GroupIds: Sy:20
//      EPC-GroupIds: Rx:10
              
      //INSERT INTO "SUBSCRIBER" (MSISDN,INFO,CREATE_TIME,MODIFY_TIME) VALUES ('886999001002','',null,now);
      String insSQL = "V_MSISDN;V_INFO;current_timestamp;current_timestamp\n";
      
      StringBuffer sb = new StringBuffer();
      sb.append("dn: EPC-SubscriberId="+msisdn+",EPC-SubscribersName=EPC-Subscribers,applicationName=EPC-EpcNode,nodeName=PC01PCRF");
      sb.append("\n");
      sb.append("EPC-SubscriberId: "+msisdn);
      sb.append("\n");
      
      for(String tag : tags){
          sb.append("EPC-OperatorSpecificInfo: "+tag);
          sb.append("\n");
      }
      for(String group : groups){
          sb.append("EPC-GroupIds: "+group.replaceAll(";", ""));
          sb.append("\n");
      }
      
      insSQL = insSQL.replaceFirst("V_MSISDN", msisdn);
      insSQL = insSQL.replaceFirst("V_INFO", sb.toString());
      
      
      return insSQL;
    }
    

    @Override
    public String toString() {
        //MSISDN;HasQualification,GroupId1;GroupId2;GroupId3;GroupId4;GroupId5;GroupId6;GroupId7;GroupId8;
        StringBuffer sb = new StringBuffer();
        sb.append(msisdn).append(CSV_SPLIT);
        sb.append((hasQualification ? "1" : "0")).append(CSV_SPLIT);

        //20200701 for 5G
        if(StringUtils.isNotBlank(policyId)){
        	sb.append(policyId);
        }
        sb.append(CSV_SPLIT);
        
        int needAddSize = groupsMaxSize - groups.size() - 1;
        if(needAddSize > 0){
            for(int i=0;i<needAddSize;i++){
                //補分號
                groups.add(CSV_SPLIT);
            }
        }
        if(groupsMaxSize == groups.size()){
            String lastGroup = groups.get(groupsMaxSize-1);
            groups.set(groupsMaxSize-1, lastGroup.substring(0, lastGroup.length()-1));
        }
        
        for(String group : groups){
            sb.append(group);
        }
        return sb.toString();
    }
    
    /*
    @Override
    public String toString() {
        //MSISDN;Tag1;Tag2;Tag3;Tag4;GroupId1;Priority1;startdate1;enddate1;GroupId2;Priority2;startdate2;enddate2;GroupId3;Priority3;startdate3;enddate3;GroupId4;Priority4;startdate4;enddate4;GroupId5;Priority5;startdate5;enddate5;GroupId6;Priority6;startdate6;enddate6;GroupId7;Priority7;startdate7;enddate7;GroupId8;Priority8;startdate8;enddate8;
        StringBuffer sb = new StringBuffer();
        sb.append(msisdn).append(CSV_SPLIT);

        int needAddSize = tagsMaxSize - tags.size();
        if(needAddSize > 0){
            for(int i=0;i<needAddSize;i++){
                tags.add("");
            }
        }
        
        int needAddSize = groupsMaxSize - groups.size();
        if(needAddSize > 0){
            for(int i=0;i<needAddSize;i++){
                groups.add(CSV_SPLIT+CSV_SPLIT+CSV_SPLIT+CSV_SPLIT);
            }
        }
        
        for(String tag : tags){
            sb.append(tag).append(CSV_SPLIT);
        }
        for(String group : groups){
            sb.append(group);
        }
        return sb.toString();
    }
    */
}
