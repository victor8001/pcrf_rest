/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: CompareDBsService.java
 * Package Name : com.cht.pcrf.consistent.service
 * Date 		: 2017年4月27日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.service;

import com.cht.pcrf.api.common.SyncDataType;
import com.cht.pcrf.consistent.common.CommonConstant;
import com.cht.pcrf.consistent.util.PropertyValue;
import com.cht.pcrf.core.dao.SubscriberRestDaoImpl;
import com.cht.pcrf.core.service.CoreDaoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CompareDBsService extends ServiceExcute{
    protected static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    private static final Date now = new Date();
    
    private SubscriberRestDaoImpl subscriberRestDao = CoreDaoFactory.getInstance().getSubscriberRestDao();
    
    @Override
    public Object execute(String execTS, Long execUserId, Object... params) throws Exception {
        final long sTime = System.currentTimeMillis();
        try {
            logger.info("====== start CompareDBsService ======");

            String dbnExecTs = getDBNExecTime(execTS, PropertyValue.DBN_PROC_SEC);
            Map<String, String> pOnlyMap = getInPrimaryOnly(SyncDataType.TYPE_4.getId(), dbnExecTs, PropertyValue.LOCATION_NAME_PRI, PropertyValue.LOCATION_NAME_SEC);
            Map<String, String> sOnlyMap = getInSecondaryOnly(SyncDataType.TYPE_5.getId(), dbnExecTs, PropertyValue.LOCATION_NAME_PRI, PropertyValue.LOCATION_NAME_SEC);
            Map<String, String> diffMap = getDBDiff(SyncDataType.TYPE_6.getId(), dbnExecTs, PropertyValue.LOCATION_NAME_PRI, PropertyValue.LOCATION_NAME_SEC);
            
//            String dbDiffFileName = params[0];
//            List<String[]> resultList = new ArrayList<String[]>();
            String dbDiffFileName = PropertyValue.WRITE_FILE_PATH + getCsvFileName("DB_DIFF_" + PropertyValue.LOCATION_NAME_PRI, now);
            
            for(String secLocation : PropertyValue.LOCATION_NAME_SEC){
                //String dbDiffFileName = PropertyValue.WRITE_FILE_PATH + getCsvFileName("DB_DIFF_" + PropertyValue.LOCATION_NAME_PRI + "_" + secLocation, now);
                
                if(pOnlyMap.get(secLocation) != null){
                    exportFile(pOnlyMap.get(secLocation), dbDiffFileName);
                }
                if(sOnlyMap.get(secLocation) != null){
                    exportFile(sOnlyMap.get(secLocation), dbDiffFileName);
                }
                if(diffMap.get(secLocation) != null){
                    exportFile(diffMap.get(secLocation), dbDiffFileName);
                }
                
//                resultList.add(new String[]{secLocation, dbDiffFileName});
            }
            
            return dbDiffFileName;
        } catch (Exception e) {
            logger.error("CompareDBsService exception, ", e);
            throw e;
        } finally {
            logger.info("====== CompareDBsService Execute finished, spend time: "
                    + (System.currentTimeMillis() - sTime) / 1000
                    + " seconds ======");

        }
    }

    /**
     * 取得只存在在第一台的資料
     * @param locationNamePri
     * @throws Exception 
     */
	private Map<String, String> getInPrimaryOnly(final String type, final String dbnExecTs, String locationNamePri, String[] locationNameSec) throws Exception {
        Map<String, String> resultMap = new HashMap<String, String>();
        for(String secLocation : locationNameSec){
            String diffFilePath = PropertyValue.WRITE_FILE_PATH
                    + getCsvFileName("DB_DIFF_" + locationNamePri + "_" + secLocation + "_" + type, now);
            
            subscriberRestDao.selectExistInPri(diffFilePath, type, dbnExecTs, locationNamePri, secLocation);
            resultMap.put(secLocation, diffFilePath);
        }
        
        return resultMap;
    }

    /**
     * 取得只存在在第二台的資料
     * @param locationNamePri
     * @return 
     * @throws Exception 
     */
    private Map<String, String> getInSecondaryOnly(final String type, final String dbnExecTs, String locationNamePri, String[] locationNameSec) throws Exception {
        Map<String, String> resultMap = new HashMap<String, String>();
        for(String secLocation : locationNameSec){
            String diffFilePath = PropertyValue.WRITE_FILE_PATH
                    + getCsvFileName("DB_DIFF_" + locationNamePri + "_" + secLocation + "_" + type, now);
            
            subscriberRestDao.selectExistInSec(diffFilePath, type, dbnExecTs, locationNamePri, secLocation);
            resultMap.put(secLocation, diffFilePath);
        }
        
        return resultMap;
    }

    /**
     * 取得差異
     * @param locationNamePri
     * @param locationNameSec
     * @return 
     * @throws Exception 
     */
    private Map<String, String> getDBDiff(final String type, final String dbnExecTs, String locationNamePri, String[] locationNameSec) throws Exception {
        Map<String, String> resultMap = new HashMap<String, String>();
        for(String secLocation : locationNameSec){
            String diffFilePath = PropertyValue.WRITE_FILE_PATH
                    + getCsvFileName("DB_DIFF_" + locationNamePri + "_" + secLocation + "_" + type, now);
            
            subscriberRestDao.selectDiff(diffFilePath, type, dbnExecTs, locationNamePri, secLocation);
            resultMap.put(secLocation, diffFilePath);
        }
        
        return resultMap;
    }

    public SubscriberRestDaoImpl getSubscriberDao() {
        return subscriberRestDao;
    }

    public void setSubscriberDao(SubscriberRestDaoImpl subscriberDao) {
        this.subscriberRestDao = subscriberDao;
    }    

}


