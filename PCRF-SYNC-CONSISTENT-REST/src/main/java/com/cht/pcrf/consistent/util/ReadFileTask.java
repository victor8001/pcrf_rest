package com.cht.pcrf.consistent.util;

import com.cht.pcrf.consistent.common.CommonConstant;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;

/**
 * 讀取每一行資料，並將資料放入Queue
 */
public class ReadFileTask implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    private String inFileDir, filenm;
    private boolean ignoreAuto;
    private BlockingQueue<String[]> _queue;
    private static Boolean finished = false;

    public ReadFileTask(String path, String filenm, boolean ignore, BlockingQueue<String[]> queue) {
        this.inFileDir = path;
        this.filenm = filenm;
        this.ignoreAuto = ignore;
        _queue = queue;
    }

    @Override
    public void run() {
        try {
            logger.info("parser file start. file:" + inFileDir);
            readSubListFile(inFileDir + File.separator + filenm);
            logger.info("parser file finish. file:" + filenm);
        } catch (Exception e) {
            logger.error("CovertSAPCDBNToCSV exception, ", e);
        }

    }

    private void readSubListFile(String inFileName) throws Exception {
        long startTime = System.currentTimeMillis();

        BufferedReader buf = null;
        String sCurrentLine = null;
        //List<String[]> datas = new ArrayList<String[]>();
        try {
            logger.info("ReadFileTask readData start ");
            int i = 0;
            buf = new BufferedReader(new FileReader(inFileName));
            while ((sCurrentLine = buf.readLine()) != null) {
                synchronized (sCurrentLine) {
                    i++;

                    if (sCurrentLine == null || sCurrentLine.trim().length() == 0 || i == 1)
                        continue;

                    // 4815890994626560-0000e80000000008
                    // [DATA:[length:16][~~~~0940000218~~][0000000a303934303030303231380000]]
                    // 4815890998427648-0000b80000000003
                    // [DATA:[length:16][~~~~emergency~~~][00000009656d657267656e6379000000]]

                    String key = StringUtils.trimToEmpty(sCurrentLine.split("\\[")[0]);// 4815890994626560-0000e80000000008
                    String msisdn = StringUtils.trimToEmpty(sCurrentLine.split("\\[")[3]);// [~~~~0940000218~~]
                    msisdn = msisdn.substring(4, msisdn.length() - 1);// 0940000218

                    if (msisdn.indexOf("emergency") == -1) {
                        //datas.add(new String[] { key, msisdn });
                        logger.debug("Put String array To Queue !");
                        _queue.put(new String[]{key, msisdn});
                    }
                }

                if (i % PropertyValue.READ_FILE_COUNT == 0) {
                    Thread.sleep(PropertyValue.READ_FILE_SLEEP);
                    logger.info("read Data sleep ");
                }
            }

            setFinished(true);
            logger.info("ReadFileTask readData end ");
        } catch (Exception e) {
            logger.error("readData exception, ", e);
            throw e;
        } finally {
            logger.info("readData execute time:" + (System.currentTimeMillis() - startTime));

            if (buf != null)
                try {
                    buf.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    public static synchronized Boolean getFinished() {
        return finished;
    }

    public static synchronized void setFinished(Boolean finished) {
        ReadFileTask.finished = finished;
    }
}
