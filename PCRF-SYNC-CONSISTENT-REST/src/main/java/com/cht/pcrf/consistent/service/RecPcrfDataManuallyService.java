/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: RecPcrfDataService.java
 * Package Name : com.cht.pcrf.consistent.service
 * Date 		: 2017年5月5日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.service;

import com.cht.pcrf.consistent.common.CommonConstant;
import com.cht.pcrf.core.dao.PcrfConsistenLogRestDaoImpl;
import com.cht.pcrf.core.service.CoreDaoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RecPcrfDataManuallyService extends ServiceExcute{
    private final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    private PcrfConsistenLogRestDaoImpl pcrfConsistenLogRestDao = CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao();
    
    /**
     * 
     * @param execTS : 執行時間
     * @param params[0] : pcrf_consistent_log.user_id
     * @param params[1] : pcrf_consistent_log.id
     * @param params[2] : pcrf_consistent_log.csv_path
     * @return
     * @throws Exception
     */
    @Override
    public Object execute(String execTS, Long execUserId, Object... params) throws Exception{
        final long sTime = System.currentTimeMillis();
        
        try{
            logger.info("====== start RecPcrfDataManuallyService ======");
            logger.info("params[0]:{}, params[1]:{}, ", params[0], params[1]);
            
            boolean result = false;
            try{
                logger.info(""+(params[0] == null || "-1".equals((String) params[0])));
                logger.info(""+(params[0] == null || "-1".equals((String) params[0])));
                //data recovery
                result = com.cht.pcrf.api.JobExecute.getInstance()
                    .getSyncRestService().syncByManul(
                            execUserId
                            , ((params[0] == null || "-1".equals((String) params[0])) ? null : Long.parseLong((String) params[0]))
                            , (String) params[1]);
                return result;
            } catch (Exception e) {
                logger.error("RecPcrfDataManuallyService exception, ", e);
            }
        } catch (Exception e) {
            logger.error("RecPcrfDataManuallyService exception, ", e);
            throw e;
        } finally{
            logger.info("====== RecPcrfDataManuallyService Execute finished, spend time: "
                + (System.currentTimeMillis() - sTime) / 1000
                + " seconds ======");
        }
        
        return true;
    }

    /*
    private void updateRecoveryResult(Long logId, boolean result){
        try {
            pcrfConsistenLogDao.updateSyncResult(logId, (result ? PcrfConsistenLog.SYNC_RESULT_2_SUCCESS : PcrfConsistenLog.SYNC_RESULT_1_FAIL));
        } catch (Exception e) {
            logger.error("updateRecoveryResult exception, ", e);
        }
    }
    */

    public PcrfConsistenLogRestDaoImpl getPcrfConsistenLogDao() {
        return pcrfConsistenLogRestDao;
    }

    public void setPcrfConsistenLogDao(PcrfConsistenLogRestDaoImpl pcrfConsistenLogDao) {
        this.pcrfConsistenLogRestDao = pcrfConsistenLogDao;
    }
    
}
