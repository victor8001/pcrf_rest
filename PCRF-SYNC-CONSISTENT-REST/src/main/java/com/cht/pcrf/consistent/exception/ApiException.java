/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: ApiException.java
 * Package Name : com.cht.pcrf.consistent.exception
 * Date 		: 2016年8月10日 
 * Author 		: LauraChu
 * Copyright (c) 2016 All Rights Reserved.
 */
package com.cht.pcrf.consistent.exception;

public class ApiException extends Exception{

    private static final long serialVersionUID = 4660836006757811186L;

    private String messageCode;
    private String messageText;
    private Throwable cause;
    
    public ApiException(String messageCode, String messageText) {
        super();
        this.messageCode = messageCode;
        this.messageText = messageText;
    }
    public ApiException(String messageCode, String messageText, Throwable cause) {
        super();
        this.messageCode = messageCode;
        this.messageText = messageText;
        this.cause = cause;
    }
    public String getMessageCode() {
        return messageCode;
    }
    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }
    public String getMessageText() {
        return messageText;
    }
    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
    public Throwable getCause() {
        return cause;
    }
    public void setCause(Throwable cause) {
        this.cause = cause;
    }
    
}
