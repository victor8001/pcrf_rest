/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: CommonUtil.java
 * Package Name : com.cht.pcrf.consistent.util
 * Date 		: 2017年5月4日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.util;

import com.cht.pcrf.consistent.common.CommonConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class CommonUtil {
    private final static Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);

    private static final String OS_NAME_WIN = "Windows";
    private static final String CAT_FILE_CMD_W = "type ${0} >> ${1}";
    private static final String CAT_FILE_CMD_L = "cat ${0} >> ${1}";
    
    public static void catFile(String sFilePath, String oFilePath) throws Exception {
        
        Process proc = null;
        long startTime = System.currentTimeMillis();
        try {
            logger.info("----- catFile Started -----");
            Runtime rt = Runtime.getRuntime();
            
            if (PropertyValue.OS_NAME.contains(OS_NAME_WIN)) {
                sFilePath = sFilePath.replace("/", "\\");
                oFilePath = oFilePath.replace("/", "\\");
                String command = CAT_FILE_CMD_W.replace("${0}", sFilePath).replace("${1}", oFilePath);
                String[] stringCommandWindows = { "cmd", "/C", command };
                logger.info("catFile cmd : "+ command);
                proc = rt.exec(stringCommandWindows);
            } else {
                String command = CAT_FILE_CMD_L.replace("${0}", sFilePath).replace("${1}", oFilePath);
                String[] stringCommandLinux = { "bash", "-c", command };
                logger.info("catFile cmd : "+ command);
                proc = rt.exec(stringCommandLinux);
            }

            String line = null;
 
            InputStream stderr = proc.getErrorStream ();
            InputStreamReader esr = new InputStreamReader (stderr);
            BufferedReader ebr = new BufferedReader (esr);
            logger.info("<info>");
            while ( (line = ebr.readLine ()) != null)
                logger.info(line);
            logger.info("</info>");
             
            InputStream stdout = proc.getInputStream ();
            InputStreamReader osr = new InputStreamReader (stdout);
            BufferedReader obr = new BufferedReader (osr);
            StringBuffer outSB = new StringBuffer();
            logger.info("<output>");
            while ( (line = obr.readLine ()) != null){
                outSB.append(line+CommonConstant.BR);
                logger.info(line);
            }
            logger.info ("</output>");
 
            int exitVal = proc.waitFor ();
            logger.info ("Process exitValue: " + exitVal);
            
            if(exitVal != 0) throw new Exception("catFile failed, e:" + outSB.toString());          
        } catch (Exception e) {
            logger.error("catFile failed, e:", e);
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            if(proc != null)
                proc.destroy();
            logger.info("catFile execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
    
    public static void invokeShell(String shellPath, String... params) throws Exception {
        
        Process proc = null;
        long startTime = System.currentTimeMillis();
        try {
            logger.info("----- invokeShell Started -----");
            Runtime rt = Runtime.getRuntime();
            String command = shellPath;
            if(params != null){
                for(String param : params){
                    command += " " + param;
                }
            }
            logger.info("shell cmd : "+ command);
            
            
            if (OS_NAME_WIN.equalsIgnoreCase(PropertyValue.OS_NAME)) {
                String[] stringCommandWindows = { "cmd", "/C", command };
                proc = rt.exec(stringCommandWindows);
            } else {
                String[] stringCommandLinux = { "bash", "-c", command };
                proc = rt.exec(stringCommandLinux);
            }

            String line = null;
            /*
            InputStream stderr = proc.getErrorStream ();
            InputStreamReader esr = new InputStreamReader (stderr);
            BufferedReader ebr = new BufferedReader (esr);
            logger.info("<info>");
            while ( (line = ebr.readLine ()) != null)
                logger.info(line);
            logger.info("</info>");
            */
             
            InputStream stdout = proc.getInputStream ();
            InputStreamReader osr = new InputStreamReader (stdout);
            BufferedReader obr = new BufferedReader (osr);
            StringBuffer outSB = new StringBuffer();
            logger.info("<output>");
            while ( (line = obr.readLine ()) != null){
                outSB.append(line+CommonConstant.BR);
                logger.info(line);
            }
            logger.info ("</output>");
 
            int exitVal = proc.waitFor ();
            logger.info ("Process exitValue: " + exitVal);
            
            if(exitVal != 0) throw new Exception("invokeShell failed, e:" + outSB.toString());          
        } catch (Exception e) {
            logger.error("invokeShell failed, e:", e);
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            if(proc != null)
                proc.destroy();
            logger.info("invokeShell execute time:"+(System.currentTimeMillis()-startTime));
        }
    }
}
