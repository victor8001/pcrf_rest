/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: CovertDBNToCSVService.java
 * Package Name : com.cht.pcrf.consistent.service
 * Date 		: 2017年4月27日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.service;

import com.cht.pcrf.consistent.bean.DBNFtpBean;
import com.cht.pcrf.consistent.common.CommonConstant;
import com.cht.pcrf.consistent.util.CommonUtil;
import com.cht.pcrf.consistent.util.FileUtil;
import com.cht.pcrf.consistent.util.JSchFTPUtil;
import com.cht.pcrf.consistent.util.PropertyValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map.Entry;

public class DownloadDBNService extends ServiceExcute{
    private final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);

    /**
     * params[0] : nodename 
     * params[1] : isPassDownload 
     */
    @Override
    public Object execute(String execTS, Long execUserId, Object... params) throws Exception{
        final long sTime = System.currentTimeMillis();

        try {
            logger.info("====== start DownloadDBNService ======");

            for (Object param : params) {
                logger.info("param:" + (String) param);
            }
            //call donwload file shell
            downloadDBNFile(Boolean.parseBoolean((String) params[1]), (String) params[0], (String) params[2]);
            
            return true;
        } catch (Exception e) {
            logger.error("DownloadDBNService exception, ", e);
            throw e;
        } finally {
            
            logger.info("====== DownloadDBNService Execute finished, spend time: "
                    + (System.currentTimeMillis() - sTime) / 1000
                    + " seconds ======");

        }
    }
    private void downloadDBNFile(boolean isPassDown, String nodename, String ftpYMD) throws Exception{
        if(nodename == null || "".equals(nodename) || "-1".equals(nodename)){
            //下載所有的node file
            for(Entry<String, DBNFtpBean> entry : PropertyValue.dbnFtpMap.entrySet()){
                donwloadDBNFile(isPassDown, entry.getKey(), ftpYMD);
            }
        }else{
            //指定nodename 
            donwloadDBNFile(isPassDown, nodename, ftpYMD);
        }
    }

    private void donwloadDBNFile(boolean isPassDown, String nodename, String ftpYMD) throws Exception {
        DBNFtpBean ftpBean = PropertyValue.dbnFtpMap.get(nodename);
        if(ftpBean == null){
            logger.error("Cannot find ftp setting , nodname:{}", nodename);
        }else{
//        	String ftpDir = ftpBean.getDir().replace("${ftpdate}", ftpYMD);
//        	ftpBean.setDir(ftpDir);
        	logger.error("ftp dir = ", ftpBean.getDir());
            String dbnSrcDir = PropertyValue.DBN_SRC_PATH + nodename;
            String dbnSrcDecDir = PropertyValue.DBN_SAPC_PATH;

            FileUtil.mkDir(dbnSrcDir);
            
            if(!isPassDown)
                JSchFTPUtil.downloadDirectory(ftpBean.getHost(), ftpBean.getPort(), ftpBean.getId(), ftpBean.getPw(), ftpBean.getBufsize()
                    , ftpBean.getDir(), dbnSrcDir, ftpYMD);
            /*
            FTPClient ftpClient = FTPUtil.createFtpClient(ftpBean.getHost(), ftpBean.getPort(), ftpBean.getId(), ftpBean.getPw(), ftpBean.getBufsize());
            FTPUtil.downloadDirectory(ftpClient, parentDir, "", dbnSrcDir);
                    */
            // run backupformater here
            CommonUtil.invokeShell(PropertyValue.SH_DECODE_DBN_PATH
                    , new String[]{dbnSrcDir, dbnSrcDecDir, nodename});
        }
    }

}

