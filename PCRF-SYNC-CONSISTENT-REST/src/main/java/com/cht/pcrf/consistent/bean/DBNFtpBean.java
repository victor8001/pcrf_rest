/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: DBNFtpBean.java
 * Package Name : com.cht.pcrf.consistent.bean
 * Date 		: 2017年5月16日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.bean;

public class DBNFtpBean {

    private String dir;
    private String host;
    private int port;
    private String id;
    private String pw;
    private int bufsize;
    
    public DBNFtpBean(String dir, String host, int port, String id, String pw, int bufsize) {
        super();
        this.dir = dir;
        this.host = host;
        this.port = port;
        this.id = id;
        this.pw = pw;
        this.bufsize = bufsize;
    }
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getPw() {
        return pw;
    }
    public void setPw(String pw) {
        this.pw = pw;
    }
    public int getBufsize() {
        return bufsize;
    }
    public void setBufsize(int bufsize) {
        this.bufsize = bufsize;
    }
    public String getDir() {
        return dir;
    }
    public void setDir(String dir) {
        this.dir = dir;
    }
    public int getPort() {
        return port;
    }
    public void setPort(int port) {
        this.port = port;
    }

}
