/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: ServiceExcute.java
 * Package Name : com.cht.pcrf.consistent
 * Date 		: 2017年4月27日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.service;

import com.cht.pcrf.consistent.common.CommonConstant;
import com.cht.pcrf.consistent.util.CommonUtil;
import com.cht.pcrf.consistent.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Date;

public abstract class ServiceExcute {
    protected static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    
    protected String FILE_PATTERN = "%s_%s.csv";
    
    public static String EXEC_TS_PATTERN = "yyyy-MM-dd HH:mm:ss";
    
    /**
     * 
     * @param execTS : yyyy-MM-dd HH:mm:ss
     * @param execUserId
     * @param params
     * @return
     * @throws Exception
     */
    public abstract Object execute(String execTS, Long execUserId, Object... params) throws Exception;
    
    protected String getCsvFileName(String type, Date now) {
        String csvFileName = "";

        try {
            csvFileName = String.format(FILE_PATTERN, type,
                    DateUtil.getNowDateTime(now));

        } catch (Exception e) {
            logger.error("getCsvFileName error ", e);
        }

        return csvFileName;
    }
    
    protected void exportFile(String sFilePath, String oFilePath) throws Exception {
//        oFilePath = PropertyValue.WRITE_FILE_PATH + oFilePath; 
        logger.info("{} export to {}:", sFilePath, oFilePath);
        CommonUtil.catFile(sFilePath, oFilePath);
        /*
        try (BufferedReader br = Files.newBufferedReader(Paths.get(sFilePath), StandardCharsets.UTF_8)) {
            for (String line = null; (line = br.readLine()) != null;) {
                FileUtil.writerFile(line, 
                        oFilePath, PropertyValue.WRITE_FILE_PATH);
            }
        }
        */
    }

    protected long getFileLength(String filePath) {
        try{
            if(filePath == null || "".equals(filePath)) return 0;
            
            File f = new File(filePath);
            if(f.exists()){
                return f.length();
            }
        }catch(Exception e){
            logger.error("getFileLength error. e:" + e);
        }
        return 0;
    }
    
    protected long getExecTime(String execTS) {
        return DateUtil.parseDate(execTS, ServiceExcute.EXEC_TS_PATTERN).getTime();
    }

    protected String getDBNExecTime(String execTS, Long subtractSec) {
        return DateUtil.formatDate(DateUtil.operateSeconds(DateUtil.parseDate(execTS, ServiceExcute.EXEC_TS_PATTERN), subtractSec), ServiceExcute.EXEC_TS_PATTERN);
    }
}
