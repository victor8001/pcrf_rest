/**
 * Project Name : PCRF-SYNC-API
 * File Name 	: LdapConstants.java
 * Package Name : com.cht.pcrf.api.common
 * Date 		: 2017年4月26日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.common;

public class LdapConstant {
    public static final String EPC_SUBSCRIBER = "EPC-Subscriber";
    public static final String EPC_SUBSCRIBER_LOWCASE = "epc-subscriber";
    public static final String EPC_SUBSCRIBERQUALIFICATION_LOWCASE = "epc-subscriberqualification";
    
    //20200701 for 5G
    public static final String EPC_SUBJECT = "EPC-Subject";
    public static final String EPC_SUBJECT_LOWCASE = "epc-subject";
}
