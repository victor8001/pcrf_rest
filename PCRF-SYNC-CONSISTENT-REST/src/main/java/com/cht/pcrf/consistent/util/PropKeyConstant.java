/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: PropKeyConstant.java
 * Package Name : com.cht.pcrf.consistent.util
 * Date 		: 2017年4月27日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.util;

public class PropKeyConstant {
    public static final String PROP_FILE_NAME = "consistent";
//    public static final String PROP_FILE_NAME = "consistent";

    public static final String OS_NAME = "os.name";

    public static final String SH_DECODE_DBN_PATH = "sh.decode.dbn.path";

    public static final String REST_DECODE_PATH = "sh.decode.path";
    
    public static final String SOURCE_FILE_PATH = "source.file.path";
    
    public static final String WRITE_FILE_PATH = "write.file.path";

    public static final String DBN_PROC_SEC = "dbn.proc.sec";
    
    public static final String FILE_LANGUAGE = "file.language";
    
    public static final String SH_ERROR_LOG = "sh.error.log";
    
    public static final String DBN_SRC_PATH = "dbn.src.path";

    public static final String DBN_SAPC_PATH = "dbn.sapc.path";
    
    public static final String DBN_CSV_PATH = "dbn.csv.path";

    public static final String DBN_SQL_PATH = "dbn.sql.path";
    
    public static final String DBN_AUTOPROVCSV_PATH = "dbn.autoprovcsv.path";
    
    public static final String DBN_AUTOPROV_IGNORE = "dbn.autoprov.ignore";
    
    public static final String DBN_CHECK_QUALIFICATION = "dbn.checkQualification";
    
    public static final String WRITE_FILE_COUNT = "write.file.count";
    
    public static final String WRITE_FILE_SLEEP = "write.file.sleep";
    
    public static final String LOCATION_NAME_PRI = "location.name.p";
    
    public static final String LOCATION_NAME_SEC = "location.name.s";
    
    public static final String DBN_FTP_NODENAMES="dbn.ftp.nodenames";

    public static final String DBN_FTP_DIR=".dbn.ftp.dir";
    
    public static final String DBN_FTP_HOST=".dbn.ftp.host";
    
    public static final String DBN_FTP_PORT=".dbn.ftp.port";
    
    public static final String DBN_FTP_ID=".dbn.ftp.id";
    
    public static final String DBN_FTP_PW=".dbn.ftp.pw";
    
    public static final String DBN_FTP_BUFSIZE=".dbn.ftp.bufsize";

    public static final String READ_FILE_COUNT = "read.file.count";

    public static final String READ_FILE_SLEEP = "read.file.sleep";

    public static final String PARSE_THREAD_NUM="parse.thread.num";

    public static final String WRITE_THREAD_NUM="write.thread.num";

    public static final String READ_QUEUE_SIZE="read.queue.size";

    public static final String DATA_QUEUE_SIZE="data.queue.size";

    public static final String PARSE_LINE_COUNT="parse.line.count";

    public static final String PARSE_LINE_SLEEP="parse.line.sleep";

}

