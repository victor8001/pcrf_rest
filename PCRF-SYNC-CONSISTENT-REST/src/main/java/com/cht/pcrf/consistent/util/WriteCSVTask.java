package com.cht.pcrf.consistent.util;

import com.cht.pcrf.consistent.common.CommonConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;

/**
 * 資料寫入 CSV
 * 這個Thread 不能寫入同一個檔案
 */
public class WriteCSVTask implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    private final BlockingQueue<String> queue;
    private String _folder;
    private String _filenm;

    private static volatile Boolean isFinished = false;

    public WriteCSVTask(String folder, String filenm, BlockingQueue<String> queue) {
        this.queue = queue;
        _filenm = filenm;
        _folder = folder;
    }

    @Override
    public void run() {
        String line;
        FileWriter writer = null;
        File file = new File(_folder + File.separator + _filenm);
        int counter = 0;
        try {
            logger.info("WriteCSVTask Write Data start ");
            while (getIsFinished()) {
                try {
                    // do things with line
                    writer = new FileWriter(file, true);
                    // block if the queue is empty
                    if (counter % PropertyValue.WRITE_FILE_COUNT == 0) {
                        Thread.sleep(PropertyValue.WRITE_FILE_SLEEP);
                        logger.info("write Data sleep ");
                    }
                    if (isNeedWait()) {
                        logger.info("Thread is waiting");
                        synchronized (this) {
                            line = queue.take();
                            counter++;

                        logger.info("Get Queue Size : " + queue.size());
                        logger.info("Writing");
                        writer.write(line + "\n");
                        continue;
                        }
                    }

                    if(!queue.isEmpty()){
                        synchronized (this) {
                            line = queue.take();
                            counter++;

                            logger.info("Get Queue Size : " + queue.size());
                            logger.info("Writing");
                            writer.write(line + "\n");
                            continue;
                        }
                    }

                    if (isTerminated()) {
                        logger.info("Thread is terminated");
                        break;
                    }
                } catch (InterruptedException ex) {
                    logger.info("Thread is interupted");
                    break; // FileTask has completed
                } finally {
                    if (writer != null) {
                        writer.flush();
                    }
                }
            }
            if(writer !=null){
                writer.close();
            }
            setIsFinished(true);
            logger.info("WriteCSVTask Write Data end ");
        } catch (IOException e) {
            logger.info("Thread is interupted");
            e.printStackTrace();
        }
    }

    private synchronized boolean isNeedWait() {
        return !ReadFileTask.getFinished() || !ParsingTask.getFinished() || (this.queue.size() > 0);
    }

    private synchronized boolean isTerminated() {
        return ReadFileTask.getFinished() && ParsingTask.getFinished() && (this.queue.size() == 0);
    }

    public static synchronized Boolean getIsFinished() {
        return isFinished;
    }

    public static synchronized void setIsFinished(Boolean isFinished) {
        WriteCSVTask.isFinished = isFinished;
    }
}
