/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: JSchFTPUtil.java
 * Package Name : com.cht.pcrf.consistent.util
 * Date 		: 2017年5月16日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.util;

import com.cht.pcrf.consistent.common.CommonConstant;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

public class JSchFTPUtil {
    private static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    private static final String LFTP_DOWNLOAD_CMD = "lftp -e 'set xfer:clobber on;set net:timeout 3;set net:max-retries 3;set net:reconnect-interval-base 2;set net:reconnect-interval-multiplier 1;set net:limit-rate $DOWNLOAD_SPPED;get $DOWNLOAD_REMOTE_PATH;exit' sftp://$FTP_ID_PW@$FTP_HOST";
    
    public static List<String> downloadDirectory(final String host, int port,
            final String username, final String password,
            final int speed, String serverDir, final String localDir, String ftpYMD)
            throws Exception {
    
        List<String> list = new ArrayList<String>();  
        ChannelSftp sftp = null;  
        Channel channel = null;  
        Session sshSession = null;  
        try {  

            logger.info("localDir:{}, serverDir:{} ", localDir, serverDir);
            
            JSch jsch = new JSch();  
            jsch.getSession(username, host, port);  
            sshSession = jsch.getSession(username, host, port);  
            sshSession.setPassword(password);  
            Properties sshConfig = new Properties();  
            sshConfig.put("StrictHostKeyChecking", "no");  
            sshSession.setConfig(sshConfig);  
            sshSession.connect();  
            logger.debug("Session connected!");  
            channel = sshSession.openChannel("sftp");  
            channel.connect();  
            logger.debug("Channel connected!");  
            sftp = (ChannelSftp) channel;  
            String shortServerDir = serverDir.substring(0, serverDir.indexOf("${ftpdate}"));
            logger.info("shortServerDir:{} ", shortServerDir);
            Vector<?> vector = sftp.ls(shortServerDir);  
            for (Object item:vector) {
                LsEntry entry = (LsEntry) item;  
                String currentFileName = entry.getFilename();
                logger.info("currentFileName:{} ", currentFileName);
                if (currentFileName.equals(".") || currentFileName.equals("..")) {
                    // skip parent directory and the directory itself
                    continue;
                }else if(currentFileName.indexOf(ftpYMD)!= -1){
                	serverDir = serverDir.replace("${ftpdate}", currentFileName);
                	logger.info("serverDir_replaced:{} ", serverDir);
                	 Vector<?> vectorIn = sftp.ls(serverDir);  
                     for (Object itemIn:vectorIn) {
                         LsEntry entryIn = (LsEntry) itemIn;  
                         String currentFileNameIn = entryIn.getFilename();
                         if (currentFileNameIn.equals(".") || currentFileNameIn.equals("..")) {
                             // skip parent directory and the directory itself
                             continue;
                         }
			                // download the file
			                downloadFileByCMD(host, port, username, password
			                        , serverDir, localDir, speed+","+speed, new String[]{currentFileNameIn});
                     }
//                boolean success = downloadSftpFile(sftp, serverDir, localDir,
//                        currentFileName);
//                if (success) {
//                    logger.info("DOWNLOADED the file: " + currentFileName);
//                } else {
//                    logger.error("COULD NOT download the file: " + currentFileName);
//                }
                }
            }
            
        } catch (Exception e) {  
            logger.error("Download File failed, e:" + e);
            throw e;
        } finally {  
            closeChannel(sftp);  
            closeChannel(channel);  
            closeSession(sshSession);  
        }  
        return list;  
    }  
    
    public static List<String> listFileNames(String host, int port, String username, final String password, String dir) {  
        List<String> list = new ArrayList<String>();  
        ChannelSftp sftp = null;  
        Channel channel = null;  
        Session sshSession = null;  
        try {  
            JSch jsch = new JSch();  
            jsch.getSession(username, host, port);  
            sshSession = jsch.getSession(username, host, port);  
            sshSession.setPassword(password);  
            Properties sshConfig = new Properties();  
            sshConfig.put("StrictHostKeyChecking", "no");  
            sshSession.setConfig(sshConfig);  
            sshSession.connect();  
            logger.debug("Session connected!");  
            channel = sshSession.openChannel("sftp");  
            channel.connect();  
            logger.debug("Channel connected!");  
            sftp = (ChannelSftp) channel;  
            Vector<?> vector = sftp.ls(dir);  
            for (Object item:vector) {  
                LsEntry entry = (LsEntry) item;  
                logger.info(entry.getFilename());  
            }  
        } catch (Exception e) {  
            e.printStackTrace();  
        } finally {  
            closeChannel(sftp);  
            closeChannel(channel);  
            closeSession(sshSession);  
        }  
        return list;  
    }  

    private static boolean downloadSftpFile(ChannelSftp chSftp, String ftpPath,
            String localPath, String fileName) throws Exception {
        try {
            String ftpFilePath = ftpPath + File.pathSeparator + fileName;
            String downloadFilePath = localPath + File.pathSeparator + fileName;
            chSftp.get(ftpFilePath, downloadFilePath);
            return true;
        } catch (Exception e) {
            logger.error("download failed, file:{}", fileName);
            throw e;
        }
    }
 
  
    private static void closeChannel(Channel channel) {  
        if (channel != null) {  
            if (channel.isConnected()) {  
                channel.disconnect();  
            }  
        }  
    }  
  
    private static void closeSession(Session session) {  
        if (session != null) {  
            if (session.isConnected()) {  
                session.disconnect();  
            }  
        }  
    }  

    /**
     * 預設以linux方式執行
     * @param downspeed
     * @param downloadFile
     * @throws Exception 
     */
    public static void downloadFileByCMD(String hostName, int port, String username,
            String password, String remoteFilePath, String localFilePath,
            String downloadSpped, 
            String[] fileArray) throws Exception {
        logger.info("== into download ==");

        long st = System.currentTimeMillis();

        try {
            for(String downFileName : fileArray){
                try {
                    //String command = "lftp -c 'get sftp://vsdumpftp:Vsip@2016@172.30.27.41:22/var/opt/vs/main/batchfiles/done/VOUCHER_BATCH_20170119_00005.RPT'";
                    //lftp -e 'set net:timeout 3;set net:max-retries 3;set net:reconnect-interval-base 2;set net:reconnect-interval-multiplier 1;set net:limit-rate 8192,4096;get $DOWNLOAD_LOCAL_PATH' sftp://$FTP_ID_PW@$FTP_HOST
                    if(remoteFilePath.lastIndexOf("/") != remoteFilePath.length()-1){
                        remoteFilePath = remoteFilePath + "/";
                    }
                    String downloadCMD = LFTP_DOWNLOAD_CMD;
                    ///"lftp -e 'set xfer:clobber on;set net:timeout 3;set net:max-retries 3;set net:reconnect-interval-base 2;set net:reconnect-interval-multiplier 1;set net:limit-rate $DOWNLOAD_SPPED;get $DOWNLOAD_REMOTE_PATH;exit' sftp://$FTP_ID_PW@$FTP_HOST";
                    downloadCMD = downloadCMD.replace("$DOWNLOAD_SPPED", downloadSpped);
                    downloadCMD = downloadCMD.replace("$DOWNLOAD_REMOTE_PATH", remoteFilePath + downFileName);
                    logger.info("DOWNLOAD_REMOTE_PATH = "+remoteFilePath + downFileName);
                    downloadCMD = downloadCMD.replace("$FTP_ID_PW", username + ":" + password);
                    downloadCMD = downloadCMD.replace("$FTP_HOST", hostName + ":" + port);
                    

                    String downloadCommand = "cd " + localFilePath + " && " + downloadCMD;
                    CommonUtil.invokeShell(downloadCommand, null);
                    logger.info("fileName->" + downFileName + " download.");
                } catch (Exception e) {
                    //logger.error("download failed, file:{}, e:{}", downFileName , e);
                    throw new Exception("download failed, file:{}" + downFileName , e);
                }
            }
            
            logger.info("DownloadFile response_time:" + (System.currentTimeMillis() - st)
                    + "response_code:0 response_msg: Success!");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.info("DownloadFile response_time:" + (System.currentTimeMillis() - st)
                    + "response_code:-2 response_msg: Fail!");
            throw e;
        }
    }
    
}
