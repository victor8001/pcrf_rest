/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: CompareDatas.java
 * Package Name : com.cht.pcrf.consistent.service
 * Date 		: 2017年5月4日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.service;

import com.cht.pcrf.consistent.bean.CompareResultBean;
import com.cht.pcrf.consistent.common.CommonConstant;
import com.cht.pcrf.consistent.util.FileUtil;
import com.cht.pcrf.consistent.util.PropertyValue;
import com.cht.pcrf.core.dao.PcrfConsistenLogRestDaoImpl;
import com.cht.pcrf.core.dao.PcrfInfoRestDaoImpl;
import com.cht.pcrf.core.model.PcrfConsistenLog;
import com.cht.pcrf.core.service.CoreDaoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.Date;

public class CompareDatasService extends ServiceExcute{
    protected static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    private static final Date now = new Date();

    private DownloadDBNService downloadDBNService;
    private CovertRestDBNToCSVService covertRestDBNToCSVService;
    private CompareDBsService compareDBsService;
    private CompareDbSapcService compareDbSapcService;
    private RecPcrfDataService recPcrfDataService;
    private PcrfConsistenLogRestDaoImpl pcrfConsistenLogRestDao = CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao();
    private PcrfInfoRestDaoImpl pcrfInfoDao = CoreDaoFactory.getInstance().getPcrfInfoRestDao();
    
    @Override
    public Object execute(String execTS, Long execUserId, Object... params) throws Exception {

        final long sTime = System.currentTimeMillis();
        Long logId = null;
        try {
            logger.info("====== start CompareDatasService ======");

            //save log
            long execTime = getExecTime(execTS);
            logId = saveExcutingLog(execTime, execUserId);
            
            //call donwload file shell (xx -> dbn_src -> dbn)
            downloadDBNService.execute(execTS, execUserId, params);
            
            //export pcrf csv(n file) (dbn -> convert)
            covertRestDBNToCSVService.execute(execTS, execUserId);
            
            //compare dbs(一次)
            String dbCompFilePath = (String) compareDBsService.execute(execTS, execUserId);
            
            //compare db&pcrf(n file) (convert -> db(temp_dbn))
            CompareResultBean compareResult = (CompareResultBean) compareDbSapcService.execute(execTS, execUserId, new String[]{dbCompFilePath});

            //recovery pcrf data
            //如果是使用者由ui啟動比對資料，就不自動回補資料 (回補資料 看pcrf_consistent的sync type)
            if(execUserId == null || execUserId == CommonConstant.DEFAULT_USER){
                recPcrfDataService.execute(execTS, execUserId, compareResult);
            }

            //update job status (compare -> java log(比對到差異會寫java log))
            updateLog(logId, null);
            
            return true;
        } catch (Exception e) {
            if(logId == null){
                saveFailLog(sTime, execUserId, e);
            }else{
                updateLog(logId, e);
            }
            logger.error("CompareDatasService exception, ", e);
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            
            logger.info("====== CompareDatasService Execute finished, spend time: "
                    + (System.currentTimeMillis() - sTime) / 1000
                    + " seconds ======");

        }
    }
    
    private long saveExcutingLog(long execTime, Long execUserId) throws Exception {
        //save log
        logger.info("save pcrf consisten log Excuting , nodename:{}", PropertyValue.LOCATION_NAME_PRI);

        long logId = pcrfConsistenLogRestDao.getNextPK();
        
        PcrfConsistenLog log = new PcrfConsistenLog();
        log.setId(logId);
        log.setPcrfId(CommonConstant.DEFAULT_PCRF);
        log.setExecStartTime(new Timestamp(execTime));
        log.setExecResult(PcrfConsistenLog.EXEC_RESULT_0);
        log.setExecUser(execUserId);
        pcrfConsistenLogRestDao.save(log);
        
        return logId;
    
    }
    
    
    private void saveFailLog(long execTime, Long execUserId, 
            Exception failEx) throws Exception {
        //find pcrf_info id
        //save log
        logger.info("save pcrf consisten log, nodename:{}, somthing fail", PropertyValue.LOCATION_NAME_PRI);

        String failLog = PropertyValue.WRITE_FILE_PATH + getCsvFileName("DB_DIFF_" + PropertyValue.LOCATION_NAME_PRI + "_FAIL", now);

        try{
            FileUtil.writerEx(failLog, failEx);
        } catch (Exception e) {
            logger.error("writerFile exception, ", e);
        }

        long logId = pcrfConsistenLogRestDao.getNextPK();
        
        PcrfConsistenLog log = new PcrfConsistenLog();
        log.setId(logId);
        log.setPcrfId(CommonConstant.DEFAULT_PCRF);
        log.setExecStartTime(new Timestamp(execTime));
        log.setExecEndTime(new Timestamp(System.currentTimeMillis()));
        log.setExecResult(PcrfConsistenLog.EXEC_RESULT_1);
        log.setExecUser(execUserId);
        log.setRptPath(failLog);
        pcrfConsistenLogRestDao.save(log);
        
    }
    
    private void updateLog(long logId, Exception failEx) throws Exception {
        //save log
        logger.info("update pcrf consisten log, nodename:{}", PropertyValue.LOCATION_NAME_PRI);

        PcrfConsistenLog log = new PcrfConsistenLog();
        log.setId(logId);
        log.setExecEndTime(new Timestamp(System.currentTimeMillis()));
        log.setExecResult(PcrfConsistenLog.EXEC_RESULT_4);
        
        
        if(failEx != null){
            String failLog = PropertyValue.WRITE_FILE_PATH + getCsvFileName("DB_DIFF_" + PropertyValue.LOCATION_NAME_PRI + "_FAIL", now);
    
            try{
                FileUtil.writerEx(failLog, failEx);
            } catch (Exception e) {
                logger.error("writerFile exception, ", e);
            }
            log.setRptPath(failLog);
            log.setExecResult(PcrfConsistenLog.EXEC_RESULT_1);
        }
        
        pcrfConsistenLogRestDao.updateExecResult(log);
        
    }

    public CovertRestDBNToCSVService getCovertRestDBNToCSVService() {
        return covertRestDBNToCSVService;
    }

    public void setCovertRestDBNToCSVService(CovertRestDBNToCSVService covertRestDBNToCSVService) {
        this.covertRestDBNToCSVService = covertRestDBNToCSVService;
    }

    public CompareDBsService getCompareDBsService() {
        return compareDBsService;
    }

    public void setCompareDBsService(CompareDBsService compareDBsService) {
        this.compareDBsService = compareDBsService;
    }

    public CompareDbSapcService getCompareDbSapcService() {
        return compareDbSapcService;
    }

    public void setCompareDbSapcService(CompareDbSapcService compareDbSapcService) {
        this.compareDbSapcService = compareDbSapcService;
    }

    public PcrfConsistenLogRestDaoImpl getPcrfConsistenLogDao() {
        return pcrfConsistenLogRestDao;
    }

    public void setPcrfConsistenLogDao(PcrfConsistenLogRestDaoImpl pcrfConsistenLogDao) {
        this.pcrfConsistenLogRestDao = pcrfConsistenLogDao;
    }

    public RecPcrfDataService getRecPcrfDataService() {
        return recPcrfDataService;
    }

    public void setRecPcrfDataService(RecPcrfDataService recPcrfDataService) {
        this.recPcrfDataService = recPcrfDataService;
    }

    public PcrfInfoRestDaoImpl getPcrfInfoDao() {
        return pcrfInfoDao;
    }

    public void setPcrfInfoDao(PcrfInfoRestDaoImpl pcrfInfoDao) {
        this.pcrfInfoDao = pcrfInfoDao;
    }

    public DownloadDBNService getDownloadDBNService() {
        return downloadDBNService;
    }

    public void setDownloadDBNService(DownloadDBNService downloadDBNService) {
        this.downloadDBNService = downloadDBNService;
    }

}
