/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: CovertDBNToCSVService.java
 * Package Name : com.cht.pcrf.consistent.service
 * Date 		: 2017年4月27日
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.service;

import com.cht.pcrf.consistent.common.CommonConstant;
import com.cht.pcrf.consistent.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.concurrent.*;

public class CovertRestDBNToCSVService extends ServiceExcute {
    private static final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    private static final String OS_NAME_WIN = "Windows";
    private static final String WRITE_SPLITE_STR = ";";
    private boolean isInit = false;

    @Override
    public Object execute(String execTS, Long execUserId, Object... params) throws Exception {
        final long sTime = System.currentTimeMillis();
        ExecutorService service = null;
        String filenm = null;
        try {
            if (params != null && params.length > 0
                    && "Y".equals(params[0])) {
                isInit = true;
            }

            logger.info("====== start CovertSAPCDBNToCSV ({}) ======", isInit);

            CovertRestDBNToCSVService test = new CovertRestDBNToCSVService();
            //args:來源檔,轉成的csv，轉成的sql，是否略過autoprovision
            String inFilePath = PropertyValue.DBN_SAPC_PATH;
            boolean ignoreAuto = ("Y".equalsIgnoreCase(PropertyValue.DBN_AUTOPROV_IGNORE)
                    ? true : false);

            FileUtil.mkDir(PropertyValue.DBN_CSV_PATH);
            FileUtil.mkDir(PropertyValue.DBN_SQL_PATH);
            FileUtil.mkDir(PropertyValue.DBN_AUTOPROVCSV_PATH);
            int threadNum = 1 + PropertyValue.PARSE_THREAD_NUM + PropertyValue.WRITE_THREAD_NUM;

            /*
            if(!ignoreAuto)
                FileUtil.mkDir(PropertyValue.DBN_AUTOPROVCSV_PATH);
            */

            List<String> fileList = FileUtil.getSourceCsv(inFilePath);
            BlockingQueue<String[]> readFileQueue = new ArrayBlockingQueue<String[]>(PropertyValue.DATA_QUEUE_SIZE);
            BlockingQueue<String> writeFileQueue = new ArrayBlockingQueue<String>(PropertyValue.READ_QUEUE_SIZE);
            service = Executors.newFixedThreadPool(threadNum);
            for (String inFileDir : fileList) {
                logger.info("parser file start. file:" + inFileDir);

                String inFileName = inFileDir;
                String fileName = inFileName.split(CommonConstant.DOT_PATTERN)[0];
                // 切檔 CSV
                String sepFilePattern = fileName + "_%d";
                filenm = fileName;

                // read file 僅能用一個 task
                service.execute(new ReadFileTask(inFilePath, inFileName, false, readFileQueue));
                // parsing thread 可以多個
                for (int i = 0; i < PropertyValue.PARSE_THREAD_NUM; i++) {
                    service.execute(new ParsingTask(inFilePath, fileName, readFileQueue, writeFileQueue));
                }
                // write file若有多個thread 寫入檔案必須分開，以免造成衝突
                if (PropertyValue.WRITE_THREAD_NUM > 1) {
                    for (int j = 0; j < PropertyValue.WRITE_THREAD_NUM; j++) {
                        service.execute(new WriteCSVTask(PropertyValue.DBN_CSV_PATH, String.format(sepFilePattern, j) + CommonConstant.DOT + CommonConstant.CSV, writeFileQueue));
                    }
                } else {
                    service.execute(new WriteCSVTask(PropertyValue.DBN_CSV_PATH, fileName + CommonConstant.DOT + CommonConstant.CSV, writeFileQueue));
                }
            }
        } catch (Exception e) {
            logger.error("CovertSAPCDBNToCSV exception, ", e);
            throw e;
        } finally {
            // shutdown service here
            if (service != null && !service.isShutdown()) {
                logger.debug("---- shutdown thread pool ----");
                service.shutdown();
                checkShutdownStatus(service);
            }
            if (PropertyValue.WRITE_THREAD_NUM > 1) {
                // merge & clean CSV file parts
                MergeAndCleanCSV(filenm);
            }
            logger.info("====== CovertSAPCDBNToCSV Execute finished, spend time: "
                    + (System.currentTimeMillis() - sTime) / 1000
                    + " seconds ======");
        }

        return true;
    }

    /**
     * Check Thread pool executor service is released or not
     *
     * @param service
     */
    private void checkShutdownStatus(ExecutorService service) {
        if (service != null) {
            while (true) {
                boolean done = service.isTerminated();
                if (done) {
                    logger.info("====== Execute finished ======");
                    break;
                }
                // shutdown dangling threads
                if(ReadFileTask.getFinished() && ParsingTask.getFinished() && WriteCSVTask.getIsFinished()){
                    logger.info("shutdown dangling threads");
                    try{
                        if (!service.awaitTermination(30, TimeUnit.SECONDS)) {
                            service.shutdownNow(); // Cancel currently executing tasks
                            // Wait a while for tasks to respond to being cancelled
                            if (!service.awaitTermination(30, TimeUnit.SECONDS))
                                logger.error("Pool did not terminate");
                        }
                    }catch (Exception e){
                        logger.error("Shutdown Exception e:",e);
                        service.shutdownNow();
                        // Preserve interrupt status
                        Thread.currentThread().interrupt();
                    }
                }
            }
        } else {
            logger.error("Executor Service can not be null !");
        }
    }

    /**
     * 合併多筆CSV檔案，並將file part檔案刪
     */
    private void MergeAndCleanCSV(String nodename) throws Exception {

        for (int i = 0; i < PropertyValue.WRITE_THREAD_NUM; i++) {
            String pattern = nodename + "_%d"+ CommonConstant.DOT + CommonConstant.CSV;
            logger.debug("pattern "+i+" : "+String.format(pattern,i));

            File src = new File(PropertyValue.DBN_CSV_PATH+String.format(pattern,i));
            logger.debug(String.format(pattern,i)+" is exist : "+src.exists());
            logger.debug(String.format(pattern,i)+" is readable : "+src.canRead());
            File dist = new File(PropertyValue.DBN_CSV_PATH+nodename+CommonConstant.DOT + CommonConstant.CSV);
            if(!src.exists()){
                continue;
            }else{
                FileUtil.appendToFile(src,dist);
                boolean isdelete = src.delete();
                logger.debug("isDeleted :"+isdelete);
            }
        }

    }

    /**
     * Delete file part CSV
     *
     * @param path
     * @param pattern
     */
    private void deleteCSV(String path, String pattern) {
        FileUtil.deleteFileWithPattern(path, pattern);
    }

}
