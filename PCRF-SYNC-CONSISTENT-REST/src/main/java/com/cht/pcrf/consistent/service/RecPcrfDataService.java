/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: RecPcrfDataService.java
 * Package Name : com.cht.pcrf.consistent.service
 * Date 		: 2017年5月5日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.service;

import com.cht.pcrf.consistent.bean.CompareResultBean;
import com.cht.pcrf.consistent.common.CommonConstant;
import com.cht.pcrf.core.dao.PcrfConsistenLogRestDaoImpl;
import com.cht.pcrf.core.model.PcrfConsistenLog;
import com.cht.pcrf.core.service.CoreDaoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RecPcrfDataService extends ServiceExcute{
    private final Logger logger = LoggerFactory.getLogger(CommonConstant.PCRF_SYNC_CONSISTENT);
    private PcrfConsistenLogRestDaoImpl pcrfConsistenLogRestDao = CoreDaoFactory.getInstance().getPcrfConsistenLogRestDao();
    
    @Override
    public Object execute(String execTS, Long execUserId, Object... params) throws Exception{
        final long sTime = System.currentTimeMillis();
        
        try{
            logger.info("====== start RecPcrfDataService ======");
            CompareResultBean compareResult = null;
            if(params != null && params.length > 0){
                compareResult = (CompareResultBean) params[0];
            }
            
            if(compareResult == null) return true;

            //TODO
//            compareResult.setDbCompResult(true);
            if(compareResult.isDbCompResult()){
                logger.info("dbCompResult is same, recovery pcrf data");

                for(PcrfConsistenLog log : compareResult.getLogList()){
                    boolean result = false;
                    try{
                        //data recovery
                        result = com.cht.pcrf.api.JobExecute.getInstance()
                            .getSyncRestService().syncBySystem(execTS,
                                    log.getId()
                                    ,execUserId
                                    , log.getRptPath().split(CommonConstant.COMMA)[0]);
                    } catch (Exception e) {
                        logger.error("RecPcrfDataService exception, ", e);
                    } finally{
                        //updateRecoveryResult(log.getId(), result);
                    }
                }
            }
            
        } catch (Exception e) {
            logger.error("RecPcrfDataService exception, ", e);
            throw e;
        } finally{
            //logger.error("====== Auto_Recovery_Finished ======");
            logger.info("====== RecPcrfDataService Execute finished, spend time: "
                + (System.currentTimeMillis() - sTime) / 1000
                + " seconds ======");
        }
        
        return true;
    }

    /*
    private void updateRecoveryResult(Long logId, boolean result){
        try {
            pcrfConsistenLogDao.updateSyncResult(logId, (result ? PcrfConsistenLog.SYNC_RESULT_2_SUCCESS : PcrfConsistenLog.SYNC_RESULT_1_FAIL));
        } catch (Exception e) {
            logger.error("updateRecoveryResult exception, ", e);
        }
    }
    */

    public PcrfConsistenLogRestDaoImpl getPcrfConsistenLogDao() {
        return pcrfConsistenLogRestDao;
    }

    public void setPcrfConsistenLogDao(PcrfConsistenLogRestDaoImpl pcrfConsistenLogDao) {
        this.pcrfConsistenLogRestDao = pcrfConsistenLogDao;
    }
    
}
