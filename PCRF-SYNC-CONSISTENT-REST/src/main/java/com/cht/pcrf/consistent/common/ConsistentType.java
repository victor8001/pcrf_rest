/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: ConsistentType.java
 * Package Name : com.cht.pcrf.consistent.common
 * Date 		: 2017年4月27日 
 * Author 		: LauraChu
 * Copyright (c) 2017 All Rights Reserved.
 */
package com.cht.pcrf.consistent.common;

public enum ConsistentType {
    CONVERT_SAPC_DBN("1","CONVERT_SAPC_DBN"),
    COMPARE_DBS("2","COMPARE_DBS"),
    COMPARE_DB_SAPC("3","COMPARE_DB_SAPC"),
    COMPARE_DATAS("4","COMPARE_DATAS"),
    SYNC_PCRF_DATAS_MANUALLY("5","SYNC_PCRF_DATAS_MANUALLY"),
    CONVERT_SAPC_DBN_FOR_INIT("6","CONVERT_SAPC_DBN_FOR_INIT"),;
    
    private String id;
    private String name;

    private ConsistentType(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
