/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: SoapMessageCode.java
 * Package Name : com.cht.pcrf.consistent.common
 * Date 		: 2016年8月10日 
 * Author 		: LauraChu
 * Copyright (c) 2016 All Rights Reserved.
 */
package com.cht.pcrf.consistent.common;

import java.util.ArrayList;
import java.util.List;

public class CommonConstant {

    public static final String PCRF_SYNC_CONSISTENT = "PCRF-SYNC-CONSISTENT";
    public static final String SUCCESS = "0000";
    public static final String PROCCESS_REQUIRED = "9999";
    public static final String SUCCESS_MSG = "Successful";
    public static final long DEFAULT_USER = -1L;
    public static final long DEFAULT_PCRF = -1L;
    
    public static List<String> locations = new ArrayList<String>();
    
    public static final String BR = "\n";
    
    public static final String DOT_PATTERN = "\\.";
    public static final String COMMA = ",";
    public static final String DOT = ".";
    public static final String SEMICOLON = ";";
    public final static String CSV = "csv";
    public final static String SQL = "sql";
    public final static String SY_AUTO_PROVISION = "Sy_Autoprovisioned";
    public final static String SY_AUTO_PROV_SHORT = "_AutoProv";
    public final static String SY_AUTO_PROV_ALARM_CSV = "AUTO_PROV_ALARM";
    
    
    static {
        locations.add("TP");
        locations.add("KH");
    }
}
