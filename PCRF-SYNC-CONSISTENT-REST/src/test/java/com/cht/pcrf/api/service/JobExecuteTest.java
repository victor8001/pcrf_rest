/**
 * Project Name : PCRF-SYNC-CONSISTENT
 * File Name 	: SyncServiceTest.java
 * Package Name : com.cht.pcrf.api.service
 * Date 		: 2016年8月8日 
 * Author 		: LauraChu
 * Copyright (c) 2016 All Rights Reserved.
 */
package com.cht.pcrf.api.service;

import com.cht.pcrf.consistent.JobExecute;
import com.cht.pcrf.consistent.common.ConsistentType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

//@RunWith(SpringJUnit4ClassRunner.class)

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/consistent-spring-bean.xml"
//        //,"file:src/main/resources/logback.xml" 
        })
public class JobExecuteTest {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

//    @Autowired(required = true)
//    private JobExecute syncService;
    
    @Before
    public void setUp() throws Exception {
    }

    /**
     */
    @Test
    public void testConvertSapcDBN() throws Exception {
        
        System.setProperty("api.config.path", "D:\\work\\tp-source\\CHT\\CHT_PCRF_DB_Sync\\source\\trunk\\PCRF-SYNC-API\\src\\main\\resources");
        System.setProperty("config.path", "D:\\work\\tp-source\\CHT\\CHT_PCRF_DB_Sync\\source\\trunk\\PCRF-SYNC-API\\src\\main\\resources");
//        String[] args = {"19970101010101", ConsistentType.CONVERT_SAPC_DBN_FOR_INIT.getId(), "-1"};
//        String[] args = {"20170624173001", ConsistentType.CONVERT_SAPC_DBN.getId(), "-1", "22"};
//        String[] args = {"20170624173001", ConsistentType.COMPARE_DBS.getId(), "-1", "22"};
        String[] args = {"20170624173001", ConsistentType.COMPARE_DB_SAPC.getId(), "-1"};
//        String[] args = {"20170507132727", ConsistentType.COMPARE_DATAS.getId(), "-1"};
        
//        String type= ConsistentType.COMPARE_DB_SAPC.getId() + "," + ConsistentType.COMPARE_DBS.getId();
//        String[] args = {type};
//        logger.info("start");
        JobExecute.getInstance().main(args);
//        System.setProperty("config.path","D:\\work\\tp-source\\CHT\\CHT_PCRF_DB_Sync\\source\\trunk\\PCRF-SYNC-API\\src\\main\\resources");
//    
//        syncService.syncBySub("d:/aa.csv");
    }

}

